## Sample React-Redux Admin starter template

demourl : https://customer-management-4b6dc.firebaseapp.com<br><br>
login credentials : userone@gmail.com, usertwo@gmail.com<br><br>
password: 123456789


## Installation

1. Run `npm install` in root directory, to install all required dependencies.
2. Rename `.env.example` to `.env` and add your API key as well as your root Restful api url.
3. Add your specific end points to the following files:
    * `login endpoint`<br>
        store > actions > auth.js > loginAttempt()
    * `Fetch all customers`<br>
        store > actions > Customers.js > loadCustomers()
    * `Create customer`<br>
        containers > Pages > UserFormActions > Customers > AddCustomer > AddCustomer.js > addCustomerHandler()
    * `Edit customer`<br>
        containers > Pages > UserFormActions > Customers > EditCustomer > EditCustomer.js > editCustomerHandler()
    * `Delete customer`<br>
        containers > Pages > Customers > Customers > Customers.js > deleteCustomerHandler()
    * `Show customer details`<br>
        *modify it as needed  :blush:*<br>
        containers > Pages > Customers > CustomerInformation > CustomerInformation.js  
4. Use `npm start` to run the app.
