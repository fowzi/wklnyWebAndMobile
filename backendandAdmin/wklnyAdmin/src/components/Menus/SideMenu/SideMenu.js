import React from "react";
import { NavLink } from "react-router-dom";
import "./sideMenu.css";
import Localization from "../../../localization";
import { GiQuicksand } from "react-icons/gi";

const sideMenu = props => {
  const rtl = Localization.getLanguage();
  let sideMenuWrapper =
    rtl === "ar"
      ? props.toggleClass
        ? "rtlhide"
        : "rtl"
      : props.toggleClass
      ? "hide"
      : null;
  return (
    <div id="side-menu-wrapper" className={sideMenuWrapper}>
      <div
        id={rtl === "ar" ? "side-menu" : "side-menultr"}
        style={{ overflow: "auto" }}
      >
        <div
          className="g-logo-background text-center pt-3 pb-3"
          style={{ backgroundColor: "#478a22" }}
        >
          <h2 className={"font-weight-bold"}>{Localization.logo}</h2>
        </div>

        <ul className="g-side-menu-ul pl-0 mb-0">
          <li>
            <NavLink exact className="pt-2 d-block" to="/">
              <span className="text-capitalize">{Localization.home}</span>
              <span className="mr-3">
                <i className="fa fa-dashboard" aria-hidden="true" />
              </span>
            </NavLink>
          </li>
          <li>
            <NavLink exact className="pt-2 d-block" to="/add-user">
              <span className="text-capitalize">{Localization.adduser}</span>
              <span className="mr-3">
                <i className="fa fa-user-plus" aria-hidden="true" />
              </span>
            </NavLink>
          </li>
          <li>
            <NavLink exact className="pt-2 d-block" to="/add-service-provider">
              <span className="text-capitalize">{Localization.adddriver}</span>
              <span className="mr-3">
                <i className="fa fa-user-plus" aria-hidden="true" />
              </span>
            </NavLink>
          </li>
          <li>
            <NavLink exact className="pt-2 d-block" to="/contracts">
              <span className="text-capitalize">التحويلات البنكية</span>
              <span className="mr-3">
                <i className="fa fa-money" aria-hidden="true" />
              </span>
            </NavLink>
          </li>
          <li>
            <NavLink exact className="pt-2 d-block" to="/more">
              <span className="text-capitalize">الخدمات / المزيد</span>
              <span className="mr-3">
                <i className="fa fa-plus" aria-hidden="true" />
              </span>
            </NavLink>
          </li>
          <li>
            <NavLink exact className="pt-2 d-block" to="/service-providers">
              <span className="text-capitalize">{Localization.drivers}</span>
              <span className="mr-3">
                {/* <i className="fa fa-car" aria-hidden="true" /> */}
                <GiQuicksand />
              </span>
            </NavLink>
          </li>
          <li>
            <NavLink exact className="pt-2 d-block" to="/clients">
              <span className="text-capitalize">{Localization.users}</span>
              <span className="mr-3">
                <i className="fa fa-user-o" aria-hidden="true" />
              </span>
            </NavLink>
          </li>
          <li>
            <NavLink exact className="pt-2 d-block" to="/problems">
              <span className="text-capitalize">المشكلات</span>
              <span className="mr-3">
                <i className="fa fa-bug" aria-hidden="true" />
              </span>
            </NavLink>
          </li>

          <li>
            <NavLink exact className="pt-2 d-block" to="/about">
              <span className="text-capitalize">من نحن</span>
              <span className="mr-3">
                <i className="fa fa-info" aria-hidden="true" />
              </span>
            </NavLink>
          </li>
          <li>
            <NavLink exact className="pt-2 d-block" to="/terms">
              <span className="text-capitalize">
                {Localization.PrivacyPolicy}
              </span>
              <span className="mr-3">
                <i className="fa fa-clipboard" aria-hidden="true" />
              </span>
            </NavLink>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default sideMenu;
