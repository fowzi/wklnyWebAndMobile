import React from "react";
import { NavLink, withRouter } from "react-router-dom";
import "./mainMenu.css";
import userPhoto from "../../../assets/images/female-placeholder-300x300.jpg";

import Localization from "../../../localization";
import { Button } from "reactstrap";
class MainMenu extends React.Component {
  state = {};
  render() {
    let rtl = Localization.getLanguage() === "ar" ? true : false;
    return (
      <div id="top-menu-wrapper">
        <header>
          <nav
            className="navbar navbar-expand-sm navbar-light bg-light"
            style={{ direction: rtl ? "rtl" : "ltr" }}
          >
            <div onClick={this.props.toggleSideMenu}>
              <span className="navbar-toggler-icon" />
            </div>
            <div className="container-fluid g-menu-links">
              {/* <ul className="navbar-nav "> */}
              {/* <li className="nav-item">
                <NavLink className="nav-link text-uppercase" to="/" exact>
                  الأسعار
                </NavLink>
              </li>
              <li className="nav-item float-sm-right">
                <NavLink className="nav-link text-uppercase" to="/" exact>
                  الرسائل
                </NavLink>
              </li> */}
              {/* </ul> */}
              <ul className="navbar-nav ">
                {/* <li className="nav-item">
                  <NavLink
                    className="nav-link text-uppercase"
                    to="/add-user"
                    exact
                  >
                    {Localization.adduser}{" "}
                  </NavLink>
                </li>
                <li className="nav-item float-sm-right">
                  <NavLink
                    className="nav-link text-uppercase"
                    to="/add-driver"
                    exact
                  >
                    {Localization.adddriver}{" "}
                  </NavLink>
                </li> */}
              </ul>

              <ul className="navbar-nav">
                <li>
                  <div className="d-flex flex-row">
                    <div className="p-0 m-0">
                      <img
                        className="rounded-circle g-userprofile"
                        // src={userPhoto}
                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABTVBMVEX///8/UbX/t01CQkL/mADTLy83SrOLlM7/pyZ4Rxk9QEL/uE3/nRgzMzP/lgD/u03FxcU2PEL/s0MxRrJWVlY8PDz/vk4rNkElPa//kgAuRLHQFBSsgkcxMTHh4/L/8uPd3d1tPRTsq0z/sjnji4v/oQDSJSVOXrpvb2/v7+/RHBz/qzYwU7vt7vf/5MP/v2T01NT/+vRdUUPR0dFseMNhbsCrq6uvtd2mrNnS1etEVrf/27jqqanaXV3ihob0rknfnUHGyub/9+3/y4V5g8j/rE3/16T55eX/qkPbLBz/rCNnSpvDhjd3R5FfX1+QkJC5NlD/0Z2HQ4SvOV7GMj7MMDaWQHjdKg5WTaaiPWuSmtHnnJzvwcH229vYSEior9rebW1xWT2MaDnftYLmmSuHVB/EkkqgaimcnJxra2ulfkf/x3vvlGMQMa3/7NS8HcLdAAAJ1UlEQVR4nO2aa3vTNhSAkzRL07hpmqShWUZ6C+ktKVAgvcHonVIu7RiXscHGBoPC2Pj/HyfJl0i2bMuWHCmg90Ofp46tR2+OfM6xnFRKo9FoNBqNRqPRaDQajUaj0Wg0Go3mG2NucuPj+aezsymCc9nTEsPK5PlUs1ks1gAjJM052ZPjZ2XjrFl0iznUlmXPj5fJ5QC9ryCIGyPNQL1hD+JGrRiiN9xBvDnF4jfEQVxuMvkNbRBvBqeX4Q/iR+YAQoobc7InHBX2FWopNovLkyuyZ83OyhT7CnWoFYGk7JkzsjISQxBJNkc2ZE+ehdiCkGJtCBzjLFHccWpOtkEIZ3yCcK1+lO0QyDJbHxNIU+X6uBGxTNCpjShbOeaECKqsKMYPKcpWoXMu4Ca0Fc9ky9AQtUYRRRW3qDgroYvmTdk+HiZFhhAiW8iD0AgCiqpV/g1xacaiqVjJEO0H8qlayYYjhIVymf6BWkGciq03fePy5WnqZ0WVHqVuxkqkSG+hVLrko6hSOj2PnklNvUslI5PJ+CiqVBOj3oV29KBexldRoVwzGckQj57NpX+oUZQt5rDMvkhper5RVGc7nDWE7sWZCYmiMtmULZP6RS8gisps+H8MX6RB0QuKomw1i08hhmHR84+iKm1N4CJliZ6voiIVMeDhnjV6fgtVkVTjVw2jRM8niorUfGqiiaPnjaIiydRb76MuTv8oTsmWQ0zR9KJHjx5F2XKImkvvn5jRo0WxJlsOstIUFT1KFIsqFESnWBS4o9dXtDc2lCj5TldaMIToAQyjYBvOydZLYeWwIEYPoQ0HSrKGKjSmiRoWVfiRjTbUhtpQPtpQG2pD+XwDhs0EDZsqGL4rFxIzLNTeydZLrVfaf48UEjIs3+i0K+uSDTt5wI0y3dAwWB6JaWchw/JfcPCKXMHDNpxE/q9piqFR2t1bKJVC/Eqlhb1dz+YANJx++wCO3T6UKThrCuYfvJ32GBqZi/n5+bG9YMXS3hg46yLjUgSG05cfmIO3DyQaWiEES+nydMFt+H5+DDAfqGg8Mk967zYsTH+oWGO3n0g07ORtw0sfymUyOObcAe74EHG2zpl/RJ5ULu+VbMN8RqJh2zHMlBZGSMP39uT3Agz37K/hPRnpkYVSxjFsyxNcxwwzxi45+TEnPP7LtB9o1zLdBf/2DSUWDNzQtRj5YggvUSGGqQpu6BeeoJpoBARaifvwSdvXMFO6mA8LoRPE+QvKSlYilx4EGBqlR6DUXSwE10NjAVTNMeqt6hjKrIdOQaQYwnZlNxP6KsMoZbwtDW4oNYSATNvfELacIX5BJ5mG7V25gqnUbjvAkAtk2L4mWzCVupcHji1B79UwjBbwy9+TrYe4+nk3fMIxuHZ4VbZany/il2nri2wpgtmWeMNZ2VIk4mMo+dnewzXhhgokUYInooNYkVzoPVwVbqhQHkWsi041Ldm7iB5E34iq3YagtRG7TCtqNDM4B2KXaUvqIxMdscv0s2wdCldFBrGlWiZFCH28kC1D5ba4XFO5LVuGjjBBRUMIgijqTmwpGkJx6VS9am/zRUwQFXv2JRDyhKHcUwWBiHWq7hqFCNjNUG33wg13PlWzm8F5wqfYUvomNDnkyTYVqT+8YOVzfMWKio8UFGIrDosgWKjx7sXWUCxRk3txFFvqbVwEEP1x2FC/TJAcRO1urim4MRNCh+kFsBVAoyN7ujEYz1ZYFY1Kdlz2dGMwPjExXmGIo2FU4KmypxuD8YkscgyNH/DLDqthFjp2An+b2IF+2SE2hI5AEkbScLmB6EE966whNjQls/lOBf2i2zD/ZCqdfNbWG35Dy3IiOw7Iwz/mv/jHw2/Y93S5fWWGvmhDJdGG2lB9tOHQG84cfw+g1T633AQ88XhG9oQjsbZ5VP1vPzV7+/A4G6SJ5LLHh7dnU/v/VY8212RPnI2ZraVGNZfO9cx/geZ9KOK2RMfuQzlEL53OVRu9LeUl194s1XNpRB1bdzCaE3Y0UeQmjh05yEzdvChX720Oftbs7N9qVNM2uVHXp2Y0s3jkHEZzzmXVxqiq9+TmTsOZJ5or7aSDf6l7alXiwnpvP9mpxmIrR8wSUKWttys/XKEc3XRdm2vsqLZYvX6AHe95V374jqa44724saNSHDdpfmCS2+4TgeB3FMXtBvXyJc/1ktheok4QrLUj15lIkKJ4lKMPUL+lRPEYrdOnB2dITtAS9Ciu+Y6Qq28NUoXKftrn+0cTJObnCLoVtwKGqC5JLh1H/gFEitipmKBLMXCItNQwbgcFENLo50NCkFDc97mNnUFuyXBDbAYHEMawZ5/rEsQVeyFfE/gaJSXVkBWKcJrTk0WX4eKJ9ckMyzBS6n+PWgPdX7/TnJ6sEoKrtiDWkgYp/jZwv7WwW9Ci6lxBRNGJoKsl9WXgN+PaDpsg3pxiipiguyX1HWiwims5RsF0eql/1XWn4l/vH1xiHaiftQYhyLhEIfV+HnxNMdxmyDO24gCjyLpE0bz6zekdO9ms3nGO+bWkNKruPjcxQgsYQcNpTu/aN+LiXfvQWki1dw01oIx6xJgbLPrN6Y+O4Y/2oaCWlMJg6uJWpK89jRWM504ufW4fivZlEXd1YkRIDRZ2c7qOPVtYg4W1pF6omz9iiTqlfpr/HTP83TwU7Y5GgyWeUG9FnpPTnL7u922rr9ERlpbUTSPhW3E/xpzs5vQVZvgKHWFqSd3Uk93ZiDMl8L2ja+9iXZtZLiLfhZBk12msL91uTv/AOu8/4AHWltRFPcFtRv8toxBQc4o/Pq3CA8wtqZvkDOOkGQQsYyfEPs1JnLpjUX2TlGDsKaHm9DpheD1aS+oaLinD2CFEzekdYpXeidiSEiQVxDjVywY0p1eIZ/xXUVtScrhkDOOvKkjqT8Lwz5iFx4T6WosbjlWVhs0pXixAuYjekuIshc83Om/iVS+bHrmduNjjGq2exF5/7Opl0v2JWKU/dblG87xCF8AM16oCPD7FDE8fc46WQK75jSvPALrEKuULIe3tKzeUF9ERDV9gnfcLXkPxyzR2S9rnJWb4kns04c1pzOcAnO5TZyfqKW8IE8imfOXe5KGda04f8g8mvOjzTwkE8Zll+Iw/hN6fQXDC19BYdH82m+/VnwUY0n6twwNfj+VgxVDIWA2x+zXc1RDR/RXmmsVfRYRQdEUUkWgAL2GuORVQKtLCUw13vTeBzSlvS2ojuObz13sT0Jxyt6QWYncVBXQ0Jt3ni8/FhFBwMqX/eDAG3Ren3C2pTV2koaBiAdj5RdRIYg0FdKUWXSHVHiH0BQbnDkYyNET23jw7f4khtOR/A4b1nHoIfQe1P6oiKv2YX6PRaDQajVz+BwAXZkEbOTj/AAAAAElFTkSuQmCC"
                        alt="profile-pic"
                      />
                    </div>
                    <div className="p-0 g-profile-text-container">
                      <div className="navbar-text text-right text-capitalize p-0 m-0 g-profile-pic-text">
                        {this.props.adminEmail}
                      </div>
                      <div className="p-0 m-0" style={{ TextAlign: "right" }}>
                        <NavLink
                          to="/logout"
                          className="g-profile-logout text-right"
                        >
                          {Localization.logout}
                        </NavLink>
                      </div>
                    </div>
                  </div>
                </li>
                {/* <li>
                  <Button
                    color="link"
                    size="sm"
                    style={{ alignSelf: "center" }}
                    type="button"
                    onClick={async () => {
                      let lan = Localization.getLanguage();
                      if (lan == "ar") {
                        Localization.setLanguage("en");
                        localStorage.setItem("@lan", "en");
                      }
                      if (lan == "en") {
                        localStorage.setItem("@lan", "ar");
                        Localization.setLanguage("ar");
                      }
                      window.location.reload();
                    }}
                  >
                    {Localization.getLanguage() === "ar" ? "EN" : "AR"}
                  </Button>
                </li>
              */}
              </ul>
            </div>
          </nav>
        </header>
      </div>
    );
  }
}

export default withRouter(MainMenu);
