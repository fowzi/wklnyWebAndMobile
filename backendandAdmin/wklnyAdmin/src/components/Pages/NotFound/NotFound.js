import React, { Component } from 'react';
import './notfound.css';

class NotFound extends Component{

    render(){
        return(
            <div className={'container-fluid mt-4 pr-4 pl-4'}>
            <h3>Page Not found.</h3>
            </div>
        )
    }
}

export default NotFound;