import React from "react";
import "./login.css";
import LoginForm from "../../../containers/AuthActions/Login/Login";

const login = () => (
  <div
    style={{
      display: "flex",
      flex: 1,
      minHeight: window.innerHeight,
      justifyContent: "center",
      alignItems: "center"
      //   height: window.screen.availHeight,
      //   backgroundImage: `url(${require("./Splash.png")})`,
      //   backgroundRepeat: "no-repeat",
      //   backgroundSize: "cover",
    }}
  >
    {window.innerWidth > 500 && (
      <div className={""}>
        <div
          className={"g-login-form z-depth-2"}
          style={{
            backgroundColor: "white",
            alignItems: "center",
            justifyContent: "center",
            padding: "unset"
          }}
        >
          <img
            className="g-login-image"
            src={require("../../../assets/images/Splash.png")}
          />
        </div>
      </div>
    )}
    <div className={" "}>
      <div className={"g-login-form z-depth-2"}>
        <LoginForm />
      </div>
    </div>
  </div>
);

export default login;
