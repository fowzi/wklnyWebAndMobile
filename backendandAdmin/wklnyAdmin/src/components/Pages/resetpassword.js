// import React, { Component } from "react";
// import { withRouter, Redirect } from "react-router";
// class resetpassword extends Component {
//   render() {
//     let code = this.props.match.params.code.split("=")[1];
//     if (!code || code.length < 5) {
//       return <Redirect to={"/"} />;
//     }
//     return <div>{code}</div>;
//   }
// }
// export default withRouter(resetpassword);

import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Table,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Alert
} from "reactstrap";
import { MdPerson } from "react-icons/md";
import { withRouter } from "react-router-dom";
import DataTable from "react-data-table-component";
import { FiMoreVertical } from "react-icons/fi";
import { connect } from "react-redux";
import { GetAllUsers, AddUser } from "../../redux/actions";

import moment from "moment";
import Localization from "../../localization";
import Spinner from "../../components/UI/Spinner/Spinner";
import { Add_User_Reset } from "../../redux/types";
import { Redirect } from "react-router";
import Axios from "axios";
class addUser extends Component {
  componentDidMount() {}

  state = {
    password: "",
    password2: "",
    visible: false,
    loading: false,
    code: this.props.match.params.code
  };
  submit = () => {
    this.setState({ loading: true });
    Axios.post("https://velooo.herokuapp.com/api/users/reset", this.state)
      .then(res => {
        this.setState({ loading: false, visible: true });

        console.log(res.data);
      })
      .catch(err => {
        console.log(err.response);
        this.setState({ loading: false });
      });
  };
  clearForm = () => {
    this.setState({
      password: "",
      password2: "",
      visible: false
    });
  };

  render() {
    console.log(this.props);
    let code = this.props.match.params.code;
    if (!code || code.length < 5) {
      return <Redirect to={"/"} />;
    }
    let errors = {};
    if (this.props.addUserErrors) {
      errors = this.props.addUserErrors;
    }
    return (
      <Container
        style={{
          marginTop: "20%",
          direction: Localization.getLanguage() === "ar" ? "rtl" : "ltr"
        }}
      >
        {this.state.visible && (
          <Alert
            color="success"
            style={{ textAlign: "center" }}
            toggle={this.clearForm}
          >
            تم تحديث كلمة المرور بنجاح{" "}
          </Alert>
        )}
        <Row
          style={{
            marginTop: 20,
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <Col
            sm={10}
            md={{ size: 8 }}
            lg={{ size: 8 }}
            style={{ alignSelf: "center" }}
          >
            {this.state.loading ? (
              <Spinner />
            ) : (
              <Form
                style={{
                  backgroundColor: "white",
                  textAlign: "center",
                  padding: " 2rem",
                  alignSelf: "center",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <FormGroup row>
                  <Label for="passwordid" sm={2}>
                    {Localization.password}
                  </Label>
                  <Col sm={10} style={{ alignSelf: "center" }}>
                    <Input
                      type="password"
                      name="password"
                      id="passwordid"
                      placeholder={Localization.password}
                      onChange={e => {
                        this.setState({ password: e.target.value });
                      }}
                      style={{
                        alignSelf: "center",
                        borderColor: errors.password ? "red" : null
                      }}
                      value={this.state.password}
                    />
                    {errors.password && (
                      <tex style={{ color: "red", fontSize: "0.5rem" }}>
                        {errors.password}
                      </tex>
                    )}
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="password2id" sm={2}>
                    {Localization.password}
                  </Label>
                  <Col sm={10} style={{ alignSelf: "center" }}>
                    <Input
                      type="password"
                      name="password2"
                      id="passwor2did"
                      placeholder={Localization.password}
                      onChange={e => {
                        this.setState({ password2: e.target.value });
                      }}
                      style={{
                        alignSelf: "center",
                        borderColor: errors.password2 ? "red" : null
                      }}
                      value={this.state.password2}
                    />
                    {errors.password2 && (
                      <tex style={{ color: "red", fontSize: "0.5rem" }}>
                        {errors.password2}
                      </tex>
                    )}
                  </Col>
                </FormGroup>

                <FormGroup check row>
                  <Col
                    style={{ alignItems: "center", justifyContent: "center" }}
                  >
                    <Button
                      color="green"
                      size="sm"
                      style={{ alignSelf: "center" }}
                      type="button"
                      onClick={this.submit}
                    >
                      تحديث كلمة المرور{" "}
                    </Button>
                  </Col>
                </FormGroup>
              </Form>
            )}
          </Col>{" "}
        </Row>
      </Container>
    );
  }
}

const mapState = state => {
  return {
    ...state.users
  };
};

const mapDispatch = dispatch => {
  return {
    AddUser: payload => AddUser(payload, dispatch),
    Reset: () => dispatch({ type: Add_User_Reset })
  };
};
export default connect(
  mapState,
  mapDispatch
)(addUser);
