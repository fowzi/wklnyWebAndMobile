import React, { Component } from "react";
import { Document, Page, pdfjs } from "react-pdf";

import {
  Input,
  Row,
  Col,
  Button,
  Alert,
  Form,
  FormGroup,
  Label
} from "reactstrap";
import Axios from "axios";
import { base_url } from "../../redux/config";
import store from "../../redux/index";
import ClipLoader from "react-spinners/ClipLoader";
import Localization from "../../localization";
import Spinner from "../../components/UI/Spinner/Spinner";
class MyApp extends Component {
  state = {
    numPages: null,
    pageNumber: 1,
    servicestype: null,
    servicestypename: null,
    selectedStrip: null,
    deleteservicestypeId: null,
    servicestypenameError: false,

    services: null,
    servicename: null,
    selectedServices: null,
    deleteSubdServicesId: null,
    servicesnameError: false,
    strip: null,
    deleteStrip: null,
    strips: null,
    stripError: null,
    city: null,
    cities: null,
    deleteCity: null,
    cityError: null,
    appinfo: {
      loading: false
    },
    pageType: 1,
    errors: {}
  };

  onDocumentLoadSuccess = ({ numPages }) => {
    this.setState({ numPages });
  };
  fetchCities = () => {
    Axios.get(base_url("admin/cities")).then(result => {
      this.setState({ cities: result.data.cities });
    });
  };
  fetchServicesTypes = () => {
    Axios.get(base_url("admin/servicestype")).then(result => {
      this.setState({ servicestype: result.data.services });
    });
  };
  fetchServices = () => {
    Axios.get(base_url("admin/services")).then(result => {
      console.log("====================================");
      console.log(result);
      console.log("====================================");
      this.setState({ services: result.data.services });
    });
  };
  fetchStrips = () => {
    Axios.get(base_url("admin/strips")).then(result => {
      console.log("====================================");
      console.log(result);
      console.log("====================================");
      this.setState({ strips: result.data.cities });
    });
  };
  deleteServicesTypes = id => {
    this.setState({ deleteservicestypeId: id });
    let token = store.getState().auth.admin.token;
    Axios.delete(base_url("/admin/service"), {
      headers: { Authorization: token },
      data: { id }
    }).then(result => {
      this.setState({ deleteservicestypeId: null });
      this.fetchServicesTypes();
    });
  };
  deleteServices = id => {
    this.setState({ deleteSubdServicesId: id });
    let token = store.getState().auth.admin.token;
    Axios.delete(base_url("/admin/subservice"), {
      headers: { Authorization: token },
      data: { id }
    }).then(result => {
      this.setState({ deleteSubdServicesId: null });
      this.fetchServicesTypes();
      this.fetchServices();
    });
  };
  deletCityById = id => {
    this.setState({ deleteCity: id });
    let token = store.getState().auth.admin.token;
    Axios.delete(base_url("/admin/city"), {
      headers: { Authorization: token },
      data: { id }
    })
      .then(result => {
        this.setState({ deleteCity: null });
        this.fetchServicesTypes();
        this.fetchServices();
        this.fetchCities();
      })
      .catch(err => {
        console.log("====================================");
        console.log(err.response);
        console.log("====================================");
      });
  };
  deletStripById = id => {
    this.setState({ deletStripById: id });
    let token = store.getState().auth.admin.token;
    Axios.delete(base_url("/admin/strip"), {
      headers: { Authorization: token },
      data: { id }
    })
      .then(result => {
        this.setState({ deleteStrip: null });
        this.fetchServicesTypes();
        this.fetchServices();
        this.fetchCities();
        this.fetchStrips();
      })
      .catch(err => {
        console.log("====================================");
        console.log(err.response);
        console.log("====================================");
      });
  };
  fetchAppInfo = () => {
    Axios.get(base_url("admin/appinfo")).then(result => {
      console.log("=================appinfo===================");
      console.log(result);
      console.log("====================================");
      this.setState({ appinfo: { ...result.data, loading: false } });
    });
  };
  componentDidMount() {
    this.fetchServicesTypes();
    this.fetchServices();
    this.fetchCities();
    this.fetchStrips();
    this.fetchAppInfo();
  }

  render() {
    let errors = this.state.errors;

    return (
      <div>
        <Row style={{ marginTop: "2rem", justifyContent: "flex-end" }}>
          <Button
            size="sm"
            color="green"
            onClick={() => this.setState({ pageType: 4 })}
          >
            الحساب
          </Button>
          <Button
            size="sm"
            color="green"
            onClick={() => this.setState({ pageType: 3 })}
          >
            القطاعات
          </Button>{" "}
          <Button
            size="sm"
            color="green"
            onClick={() => this.setState({ pageType: 2 })}
          >
            المدن
          </Button>
          <Button
            size="sm"
            color="green"
            onClick={() => this.setState({ pageType: 1 })}
          >
            الخدمات
          </Button>
        </Row>
        {this.state.pageType === 1 && (
          <div>
            <Row
              style={{
                marginTop: window.innerHeight * 0.1,
                justifyContent: "center",
                //   alignItems: "center",
                textAlign: "center"
              }}
            >
              <Col
                sm={12}
                lg={8}
                style={{ alignSelf: "center", justifyContent: "center" }}
              >
                <div
                  style={{
                    backgroundColor: "#478a22",
                    color: "white",
                    padding: "0.8rem 0.15rem",
                    textAlign: "center",
                    fontSize: "1.25rem",
                    width: "100%"
                  }}
                >
                  الخدمات
                </div>
              </Col>
            </Row>
            <Row
              style={{
                marginTop: window.innerHeight * 0.1,
                justifyContent: "center",
                //   alignItems: "center",
                textAlign: "center"
              }}
            >
              <Col lg={4} sm={8} style={{ marginBottom: "1rem" }}>
                <Row style={{}}>
                  <div
                    style={{
                      width: "1000%",
                      textAlign: "center",
                      alignSelf: "center"
                    }}
                  >
                    الخدمات{" "}
                  </div>
                </Row>
                <Alert
                  color="danger"
                  style={{ textAlign: "center" }}
                  isOpen={this.state.servicesnameError}
                  toggle={() => this.setState({ servicesnameError: false })}
                >
                  {this.state.servicesnameError}
                </Alert>
                <Row style={{ justifyContent: "center" }}>
                  <Button
                    size="sm"
                    color="green"
                    onClick={() => {
                      let token = store.getState().auth.admin.token;
                      Axios.put(
                        base_url("admin/addsubservice"),
                        {
                          name: this.state.servicename,
                          id: this.state.selectedServices
                        },
                        { headers: { Authorization: token } }
                      )
                        .then(result => {
                          this.fetchServicesTypes();
                          this.fetchServices();
                        })
                        .catch(error => {
                          this.setState({
                            servicesnameError: error.response.data.err
                          });
                        });
                    }}
                  >
                    إضافة
                  </Button>
                  <Input
                    style={{
                      width: "70%",
                      textAlign: "right",
                      alignSelf: "center"
                    }}
                    placeholder="الخدمة"
                    value={this.state.servicename}
                    onChange={e =>
                      this.setState({ servicename: e.target.value + "" })
                    }
                  />
                </Row>
                <Input
                  type="select"
                  name="select"
                  placeholder={"اختر نوع الخدمة"}
                  onChange={e => {
                    this.setState({ selectedServices: e.target.value });
                  }}
                  style={{
                    alignSelf: "center",
                    direction: "rtl"
                  }}
                >
                  <option disabled selected>
                    اختر نوع الخدمة
                  </option>
                  {this.state.servicestype &&
                    this.state.servicestype.map(element => (
                      <option value={element._id}>{element.name}</option>
                    ))}
                </Input>

                <Col
                  style={{
                    width: "100%",
                    background: "white"
                  }}
                >
                  <Row style={{}} />

                  {this.state.services &&
                    this.state.services.map(element => {
                      let type =
                        element.type._id === this.state.selectedServices ? (
                          <Row style={{}}>
                            <Button
                              size="sm"
                              color="red"
                              onClick={() => this.deleteServices(element._id)}
                            >
                              {this.state.deleteSubdServicesId ===
                              element._id ? (
                                <ClipLoader
                                  //    css={override}
                                  sizeUnit={"rem"}
                                  size={1}
                                  color={"white"}
                                  loading={true}
                                />
                              ) : (
                                "حذف"
                              )}{" "}
                            </Button>

                            <div
                              style={{
                                width: "70%",
                                textAlign: "right",
                                alignSelf: "center"
                              }}
                            >
                              {element.name}
                            </div>
                          </Row>
                        ) : null;
                      return type;
                    })}
                </Col>
              </Col>

              <Col
                lg={4}
                sm={8}
                style={{
                  borderLeft: "5px solid #477149",
                  marginBottom: "1rem"
                }}
              >
                <Row style={{}}>
                  <div
                    style={{
                      width: "1000%",
                      textAlign: "center",
                      alignSelf: "center"
                    }}
                  >
                    جهات الخدمة
                  </div>
                </Row>
                <Alert
                  color="danger"
                  style={{ textAlign: "center" }}
                  isOpen={this.state.servicestypenameError}
                  toggle={() => this.setState({ servicestypenameError: false })}
                >
                  {this.state.servicestypenameError}
                </Alert>
                <Input
                  type="select"
                  name="select"
                  placeholder={"اختر قطاع الخدمة"}
                  onChange={e => {
                    this.setState({ selectedStrip: e.target.value });
                  }}
                  style={{
                    alignSelf: "center",
                    direction: "rtl",
                    padding: 1
                  }}
                >
                  <option disabled selected>
                    اختر قطاع الخدمة
                  </option>
                  {this.state.strips &&
                    this.state.strips.map(element => (
                      <option value={element._id}>{element.name}</option>
                    ))}
                </Input>
                <Row style={{ justifyContent: "center" }}>
                  <Button
                    size="sm"
                    color="green"
                    onClick={() => {
                      let token = store.getState().auth.admin.token;
                      Axios.put(
                        base_url("admin/addservice"),
                        {
                          name: this.state.servicestypename,
                          strip: this.state.selectedStrip
                        },
                        { headers: { Authorization: token } }
                      )
                        .then(result => {
                          this.fetchServicesTypes();
                        })
                        .catch(error => {
                          this.setState({
                            servicestypenameError: error.response.data.err
                          });
                        });
                    }}
                  >
                    إضافة
                  </Button>
                  <Input
                    style={{
                      width: "70%",
                      textAlign: "right",
                      alignSelf: "center"
                    }}
                    placeholder="جهة الخدمة"
                    value={this.state.servicestypename}
                    onChange={e =>
                      this.setState({ servicestypename: e.target.value + "" })
                    }
                  />
                </Row>

                <Col
                  style={{
                    width: "100%",
                    background: "white"
                  }}
                >
                  {this.state.servicestype &&
                    this.state.servicestype.map(item => (
                      <Row
                        style={{
                          marginBottom: "2px",
                          borderBottom: "1px solid #eee"
                        }}
                      >
                        <Button
                          size="sm"
                          color="red"
                          onClick={() => this.deleteServicesTypes(item._id)}
                        >
                          {this.state.deleteservicestypeId === item._id ? (
                            <ClipLoader
                              //    css={override}
                              sizeUnit={"rem"}
                              size={1}
                              color={"white"}
                              loading={true}
                            />
                          ) : (
                            "حذف"
                          )}{" "}
                        </Button>
                        <div
                          style={{
                            width: "70%",
                            textAlign: "right",
                            alignSelf: "center"
                            // display: "flex",
                            // justifyContent: "space-between"
                          }}
                        >
                          {item.name}
                        </div>
                      </Row>
                    ))}
                </Col>
              </Col>
            </Row>
          </div>
        )}
        {this.state.pageType === 2 && (
          <div>
            <Row
              style={{
                marginTop: window.innerHeight * 0.1,
                justifyContent: "center",
                //   alignItems: "center",
                textAlign: "center"
              }}
            >
              <Col
                sm={12}
                lg={8}
                style={{ alignSelf: "center", justifyContent: "center" }}
              >
                <div
                  style={{
                    backgroundColor: "#478a22",
                    color: "white",
                    padding: "0.8rem 0.15rem",
                    textAlign: "center",
                    fontSize: "1.25rem",
                    width: "100%"
                  }}
                >
                  المدن
                </div>
              </Col>
            </Row>
            <Row style={{ justifyContent: "center", alignItems: "center" }}>
              <Col lg={8} sm={10} style={{ marginBottom: "1rem" }}>
                <Alert
                  color="danger"
                  style={{ textAlign: "center" }}
                  isOpen={this.state.cityError}
                  toggle={() => this.setState({ cityError: false })}
                >
                  {this.state.cityError}
                </Alert>
                <Row style={{ justifyContent: "center" }}>
                  <Button
                    size="sm"
                    color="green"
                    onClick={() => {
                      let token = store.getState().auth.admin.token;
                      Axios.put(
                        base_url("admin/addcity"),
                        { name: this.state.city },
                        { headers: { Authorization: token } }
                      )
                        .then(result => {
                          this.fetchCities();
                        })
                        .catch(error => {
                          this.setState({
                            cityError: error.response.data.error
                          });
                        });
                    }}
                  >
                    إضافة
                  </Button>
                  <Input
                    style={{
                      width: "70%",
                      textAlign: "right",
                      alignSelf: "center"
                    }}
                    placeholder="اسم المدينة"
                    value={this.state.city}
                    onChange={e => this.setState({ city: e.target.value + "" })}
                  />
                </Row>
                <Row style={{ justifyContent: "center", alignItems: "center" }}>
                  <Col
                    style={{
                      background: "white",
                      alignSelf: "center",
                      maxWidth: 500
                    }}
                  >
                    {this.state.cities &&
                      this.state.cities.map(item => (
                        <Row style={{}}>
                          <Button
                            size="sm"
                            color="red"
                            onClick={() => this.deletCityById(item._id)}
                          >
                            {this.state.deleteCity === item._id ? (
                              <ClipLoader
                                //    css={override}
                                sizeUnit={"rem"}
                                size={1}
                                color={"white"}
                                loading={true}
                              />
                            ) : (
                              "حذف"
                            )}{" "}
                          </Button>

                          <div
                            style={{
                              width: "70%",
                              textAlign: "right",
                              alignSelf: "center"
                            }}
                          >
                            {item.name}
                          </div>
                        </Row>
                      ))}
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        )}
        {this.state.pageType === 3 && (
          <div>
            <Row
              style={{
                marginTop: window.innerHeight * 0.1,
                justifyContent: "center",
                //   alignItems: "center",
                textAlign: "center"
              }}
            >
              <Col
                sm={12}
                lg={8}
                style={{ alignSelf: "center", justifyContent: "center" }}
              >
                <div
                  style={{
                    backgroundColor: "#478a22",
                    color: "white",
                    padding: "0.8rem 0.15rem",
                    textAlign: "center",
                    fontSize: "1.25rem",
                    width: "100%"
                  }}
                >
                  القطاعات
                </div>
              </Col>
            </Row>
            <Row style={{ justifyContent: "center", alignItems: "center" }}>
              <Col lg={8} sm={10} style={{ marginBottom: "1rem" }}>
                <Alert
                  color="danger"
                  style={{ textAlign: "center" }}
                  isOpen={this.state.stripError}
                  toggle={() => this.setState({ stripError: false })}
                >
                  {this.state.stripError}
                </Alert>
                <Row style={{ justifyContent: "center" }}>
                  <Button
                    size="sm"
                    color="green"
                    onClick={() => {
                      let token = store.getState().auth.admin.token;
                      Axios.put(
                        base_url("admin/addstrip"),
                        { name: this.state.strip },
                        { headers: { Authorization: token } }
                      )
                        .then(result => {
                          this.fetchStrips();
                        })
                        .catch(error => {
                          this.setState({
                            stripError: error.response.data.error
                          });
                        });
                    }}
                  >
                    إضافة
                  </Button>
                  <Input
                    style={{
                      width: "70%",
                      textAlign: "right",
                      alignSelf: "center"
                    }}
                    placeholder="اسم القطاع"
                    value={this.state.strip}
                    onChange={e =>
                      this.setState({ strip: e.target.value + "" })
                    }
                  />
                </Row>
                <Row style={{ justifyContent: "center", alignItems: "center" }}>
                  <Col
                    style={{
                      background: "white",
                      alignSelf: "center",
                      maxWidth: 500
                    }}
                  >
                    {this.state.strips &&
                      this.state.strips.map(item => (
                        <Row style={{}}>
                          <Button
                            size="sm"
                            color="red"
                            onClick={() => this.deletStripById(item._id)}
                          >
                            {this.state.deleteStrip === item._id ? (
                              <ClipLoader
                                //    css={override}
                                sizeUnit={"rem"}
                                size={1}
                                color={"white"}
                                loading={true}
                              />
                            ) : (
                              "حذف"
                            )}{" "}
                          </Button>

                          <div
                            style={{
                              width: "70%",
                              textAlign: "right",
                              alignSelf: "center"
                            }}
                          >
                            {item.name}
                          </div>
                        </Row>
                      ))}
                  </Col>
                </Row>
              </Col>
            </Row>
          </div>
        )}
        {this.state.pageType === 4 && (
          <div>
            <Row
              style={{
                marginTop: window.innerHeight * 0.1,
                justifyContent: "center",
                //   alignItems: "center",
                textAlign: "center"
              }}
            >
              <Col
                sm={12}
                lg={8}
                style={{ alignSelf: "center", justifyContent: "center" }}
              >
                <div
                  style={{
                    backgroundColor: "#478a22",
                    color: "white",
                    padding: "0.8rem 0.15rem",
                    textAlign: "center",
                    fontSize: "1.25rem",
                    width: "100%"
                  }}
                >
                  معلومات الحساب
                </div>
              </Col>
            </Row>
            <Row
              style={{
                marginTop: 20,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <Col lg={{ size: 8 }} style={{ alignSelf: "center" }}>
                {this.props.addUsersLoading ? (
                  <Spinner />
                ) : (
                  <Form
                    style={{
                      backgroundColor: "white",
                      textAlign: "center",
                      padding: " 2rem",
                      alignSelf: "center",
                      justifyContent: "center",
                      alignItems: "center",
                      direction: "rtl"
                    }}
                  >
                    <FormGroup row>
                      <Label
                        for="emailid"
                        sm={2}
                        style={{ fontSize: "0.8rem" }}
                      >
                        {Localization.email}
                      </Label>
                      <Col style={{ alignSelf: "center" }}>
                        <Input
                          type="email"
                          name="email"
                          id="emailid"
                          placeholder={Localization.email}
                          value={this.state.appinfo.contactMail}
                          onChange={e => {
                            this.setState({
                              appinfo: {
                                ...this.state.appinfo,
                                contactMail: e.target.value
                              }
                            });
                          }}
                          style={{
                            alignSelf: "center",
                            borderColor: errors.contactMail ? "red" : null
                          }}
                        />
                        {errors.contactmail && (
                          <tex style={{ color: "red", fontSize: "0.5rem" }}>
                            {errors.contactmail}
                          </tex>
                        )}
                      </Col>
                    </FormGroup>

                    <FormGroup row>
                      <Label for="phone" sm={2}>
                        {Localization.phone}
                      </Label>
                      <Col>
                        <Input
                          type="tel"
                          name="phone"
                          id="phone"
                          placeholder={Localization.phone}
                          onChange={e => {
                            this.setState({
                              appinfo: {
                                ...this.state.appinfo,
                                phone: e.target.value
                              }
                            });
                          }}
                          style={{
                            alignSelf: "center",
                            borderColor: errors.phone ? "red" : null
                          }}
                          value={this.state.appinfo.phone}
                        />
                        {errors.phone && (
                          <tex style={{ color: "red", fontSize: "0.5rem" }}>
                            {errors.phone}
                          </tex>
                        )}
                      </Col>
                    </FormGroup>

                    <FormGroup row>
                      <Label for="name" sm={2}>
                        {"العنوان"}
                      </Label>
                      <Col>
                        <Input
                          type="text"
                          name="address"
                          id="address"
                          placeholder={"العنوان"}
                          style={{
                            alignSelf: "center",
                            borderColor: errors.address ? "red" : null
                          }}
                          onChange={e => {
                            this.setState({
                              appinfo: {
                                ...this.state.appinfo,
                                address: e.target.value
                              }
                            });
                          }}
                          value={this.state.appinfo.address}
                        />
                        {errors.address && (
                          <tex style={{ color: "red", fontSize: "0.5rem" }}>
                            {errors.address}
                          </tex>
                        )}
                      </Col>
                    </FormGroup>

                    <FormGroup row>
                      <Label for="merchant_email" sm={2}>
                        {"merchant_email"}
                      </Label>
                      <Col>
                        <Input
                          type="text"
                          name="merchant_email"
                          id="merchant_email"
                          placeholder={"paytabs merchant_email"}
                          style={{
                            alignSelf: "center",
                            borderColor: errors.merchant_email ? "red" : null
                          }}
                          onChange={e => {
                            this.setState({
                              appinfo: {
                                ...this.state.appinfo,
                                merchant_email: e.target.value
                              }
                            });
                          }}
                          value={this.state.appinfo.merchant_email}
                        />
                        {errors.merchant_email && (
                          <tex style={{ color: "red", fontSize: "0.5rem" }}>
                            {errors.merchant_email}
                          </tex>
                        )}
                      </Col>
                    </FormGroup>

                    <FormGroup row>
                      <Label for="secret_key" sm={2}>
                        {"secret_key"}
                      </Label>
                      <Col>
                        <Input
                          type="text"
                          name="secret_key"
                          id="secret_key"
                          placeholder={"paytabs secret_key"}
                          style={{
                            alignSelf: "center",
                            borderColor: errors.secret_key ? "red" : null
                          }}
                          onChange={e => {
                            this.setState({
                              appinfo: {
                                ...this.state.appinfo,
                                secret_key: e.target.value
                              }
                            });
                          }}
                          value={this.state.appinfo.secret_key}
                        />
                        {errors.secret_key && (
                          <tex style={{ color: "red", fontSize: "0.5rem" }}>
                            {errors.secret_key}
                          </tex>
                        )}
                      </Col>
                    </FormGroup>

                    <FormGroup row>
                      <Label for="site_url" sm={2}>
                        {"site_url"}
                      </Label>
                      <Col>
                        <Input
                          type="text"
                          name="site_url"
                          id="site_url"
                          placeholder={"paytabs site_url"}
                          style={{
                            alignSelf: "center",
                            borderColor: errors.site_url ? "red" : null
                          }}
                          onChange={e => {
                            this.setState({
                              appinfo: {
                                ...this.state.appinfo,
                                site_url: e.target.value
                              }
                            });
                          }}
                          value={this.state.appinfo.site_url}
                        />
                        {errors.site_url && (
                          <tex style={{ color: "red", fontSize: "0.5rem" }}>
                            {errors.site_url}
                          </tex>
                        )}
                      </Col>
                    </FormGroup>

                    <FormGroup check row>
                      <Col
                        style={{
                          alignItems: "center",
                          justifyContent: "center"
                        }}
                      >
                        <Button
                          color="green"
                          size="lg"
                          type="button"
                          onClick={() => {
                            let token = store.getState().auth.admin.token;
                            Axios.post(
                              base_url("admin/appinfo"),
                              { ...this.state.appinfo },
                              { headers: { Authorization: token } }
                            )
                              .then(result => {
                                this.setState({
                                  appinfo: {
                                    ...this.state.appinfo,
                                    ...result.data
                                  },
                                  errors: {}
                                });
                              })
                              .catch(error => {
                                if (error.response.data.errors) {
                                  this.setState({
                                    errors: error.response.data.errors
                                  });
                                }
                              });
                          }}
                        >
                          {"تحديث"}
                        </Button>
                      </Col>
                    </FormGroup>
                  </Form>
                )}
              </Col>{" "}
            </Row>
          </div>
        )}
      </div>
    );
  }
}

export default MyApp;
