import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Table,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Alert
} from "reactstrap";
import { MdPerson } from "react-icons/md";
import { withRouter } from "react-router-dom";
import DataTable from "react-data-table-component";
import { FiMoreVertical } from "react-icons/fi";
import { connect } from "react-redux";
import { GetAllUsers, AddUser } from "../../redux/actions";

import moment from "moment";
import Localization from "../../localization";
import Spinner from "../../components/UI/Spinner/Spinner";
import { Add_User_Reset, Update_Trips_Price_Reset } from "../../redux/types";
import { fetchPrices, updatePrices } from "../../redux/actions/prices";

class Prices extends Component {
  componentDidMount() {
    this.props.Reset();
    this.props.fetchPrices();
  }
  componentWillReceiveProps(nxt) {
    this.props = nxt;
    if (nxt.prices) {
      let Prices = Object.values(nxt.prices.trip);
      this.setState({
        visible: false,
        Prices: Prices
      });
    }
    if (this.props.updateSucess) {
      this.setState({
        visible: true
      });
    }
  }
  state = {
    visible: false,
    aditionalKilos: "",
    Prices: []
  };
  submit = () => {
    this.props.updatePrices({
      prices: this.state.Prices
    });
  };
  clearForm = () => {
    this.setState({
      email: "",
      name: "",
      password: "",
      password2: "",
      phone: "",
      visible: false
    });
  };

  render() {
    let errors = {};
    if (this.props.addUserErrors) {
      errors = this.props.addUserErrors;
    }
    return (
      <Container
        style={{
          marginTop: 50,
          direction: Localization.getLanguage() === "ar" ? "rtl" : "ltr"
        }}
      >
        {this.state.visible && (
          <Alert
            color="success"
            style={{ textAlign: "center" }}
            toggle={this.clearForm}
          >
            {Localization.updateSuccess}
          </Alert>
        )}
        <Row
          style={{
            marginTop: 20,
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <Col
            sm={10}
            md={{ size: 8 }}
            lg={{ size: 8 }}
            style={{ alignSelf: "center" }}
          >
            <div
              style={{
                backgroundColor: "#a96500",
                color: "white",
                padding: "0.15rem",
                textAlign: "center",
                fontSize: "1.25rem"
              }}
            >
              {Localization.prices}
            </div>
            {this.props.updatePricesLoading ? (
              <Spinner />
            ) : (
              <Form
                style={{
                  backgroundColor: "white",
                  textAlign: "center",
                  padding: " 2rem",
                  alignSelf: "center",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                {this.state.Prices.map((item, index) => {
                  return (
                    <div
                      style={{
                        border: "1px #ccc solid",
                        padding: "1rem",
                        marginBottom: "0.8rem"
                      }}
                    >
                      <FormGroup row>
                        <div style={{ width: "100%" }}>
                          <text>{item.label}</text>
                        </div>
                        <Label for="normalPrice" sm={2}>
                          {Localization.normalTripPrice}
                        </Label>
                        <Col>
                          <Input
                            type="number"
                            name="normalPrice"
                            id="normalPrice"
                            min={1}
                            placeholder={Localization.normalTripPrice}
                            style={{
                              alignSelf: "center",
                              borderColor: errors.normalPrice ? "red" : null
                            }}
                            onChange={e => {
                              let Prices = this.state.Prices;
                              Prices[index] = {
                                ...Prices[index],
                                price: e.target.value
                              };
                              this.setState({ Prices: Prices });
                            }}
                            value={this.state.Prices[index].price}
                          />
                          {errors.normalPrice && (
                            <tex style={{ color: "red", fontSize: "0.5rem" }}>
                              {errors.normalPrice}
                            </tex>
                          )}
                        </Col>
                      </FormGroup>

                      <FormGroup row>
                        <Label
                          for="additional"
                          sm={2}
                          style={{ fontSize: "0.8rem" }}
                        >
                          {Localization.aditionalKilos}
                        </Label>
                        <Col style={{ alignSelf: "center" }}>
                          <Input
                            type="number"
                            min={1}
                            name="additional"
                            id="additional"
                            placeholder={Localization.aditionalKilos}
                            style={{
                              alignSelf: "center",
                              borderColor: errors.aditionalKilos ? "red" : null
                            }}
                            onChange={e => {
                              let Prices = this.state.Prices;
                              Prices[index] = {
                                ...Prices[index],
                                killo: e.target.value
                              };
                              this.setState({ Prices: Prices });
                              this.setState({ aditionalKilos: e.target.value });
                            }}
                            value={this.state.Prices[index].killo}
                          />
                          {errors.aditionalKilos && (
                            <tex style={{ color: "red", fontSize: "0.5rem" }}>
                              {errors.aditionalKilos}
                            </tex>
                          )}
                        </Col>
                      </FormGroup>
                    </div>
                  );
                })}
                <FormGroup check row style={{ padding: 0 }}>
                  <Col
                    style={{
                      alignItems: "center",
                      justifyContent: "center",
                      flex: 1
                    }}
                  >
                    <Button
                      color="green"
                      size="sm"
                      style={{
                        alignSelf: "center",
                        backgroundColor: "#678c8c !important",
                        textAlign: "center"
                      }}
                      type="button"
                      onClick={this.submit}
                      block
                    >
                      {Localization.update}
                    </Button>
                  </Col>
                </FormGroup>
              </Form>
            )}
          </Col>{" "}
        </Row>
      </Container>
    );
  }
}

const mapState = state => {
  return {
    ...state.users,
    ...state.prices
  };
};

const mapDispatch = dispatch => {
  return {
    fetchPrices: () => fetchPrices(dispatch),
    updatePrices: payload => updatePrices(payload, dispatch),
    Reset: () => dispatch({ type: Update_Trips_Price_Reset })
  };
};
export default connect(
  mapState,
  mapDispatch
)(Prices);
