import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Table,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Media,
  Alert,
  Input
} from "reactstrap";
import { MdPerson, MdClose, MdSend } from "react-icons/md";
import { withRouter } from "react-router-dom";
import DataTable from "react-data-table-component";
import { FiMoreVertical } from "react-icons/fi";
import { connect } from "react-redux";
import { GetAllUsers, DeleteUser } from "../../redux/actions";
import io from "socket.io-client";
import moment from "moment";
import Localization from "../../localization";
import {
  GetAllTrips,
  GetFinishedServices,
  GetProblemServices
} from "../../redux/actions/trips";
import { base_url, socketUrl } from "../../redux/config";
import Axios from "axios";

const rowTheme = {
  title: {
    fontSize: "22px",
    fontColor: "#FFFFFF",
    backgroundColor: "#a96500"
  },
  rows: {
    // spaced allows the following properties
    // spacing: "spaced",
    // spacingMargin: "3px",
    // borderColor: "rgba(0,0,0,.12)",
    // border: "1px solid",
    // backgroundColor: "white",
    // height: "52px"
    spacing: "default",
    fontSize: "13px",
    fontColor: "rgba(0,0,0,.87)",
    backgroundColor: "transparent",
    borderWidth: "1px",
    // "borderColor": "black",
    stripedColor: "rgba(0,0,0,.03)",
    hoverFontColor: "rgba(0,0,0,.87)",
    hoverBackgroundColor: "#eee"
  },
  cells: {
    cellPadding: "48px",
    // border: "1px",
    borderWidth: "1px"
  }
};
class Clients extends Component {
  socket = null;
  componentWillMount() {
    this.props.getAllTrips(1);
    moment.locale("ar");
  }

  state = {
    chatIsOpen: false,
    detailsIsOpen: false,
    rowId: null,
    buttonActionUpdateFail: false,
    buttonActionUpdateSuccess: false,

    Chat: {
      row: null,
      data: null,
      loading: false,
      Text: null
    }
  };

  columns = [
    {
      name: Localization.user,
      selector: "user",
      sortable: true,
      center: true,
      cell: row => {
        return <div> {row.order.user.name}</div>;
      }
    },
    {
      name: Localization.driver,
      selector: "drive",
      sortable: true,
      center: true,
      cell: row => <div>{row.provider.name}</div>
    },
    {
      name: "سعر الخدمة",
      selector: "price",
      sortable: true,
      center: true,
      cell: row => <div>{row.price}</div>
    },
    {
      name: "الخدمة",
      selector: "details",
      sortable: true,
      center: true,
      cell: row => <div>{row.order.details}</div>
    },
    {
      name: "مدة الخدمة",
      cell: row => <div>{row.time}</div>,
      sortable: true,
      center: true
    },
    {
      allowOverflow: true,
      ignoreRowClick: true,
      name: "الخيارات",
      cell: row => (
        <div style={{ flexDirection: "column", display: "flex" }}>
          <Button
            size="sm"
            color="green"
            onClick={() => {
              this.setState({ detailsIsOpen: row });
            }}
          >
            التفاصيل
          </Button>
          <Button
            size="sm"
            color="green"
            onClick={async e => {
              await this.setState({
                Chat: { row: row, data: [], loading: true },
                chatIsOpen: row
              });

              console.log("====================================");
              console.log(this.state.chatIsOpen, row);
              console.log("====================================");
              this.socket = io(socketUrl);
              this.socket.emit("joinroom", {
                room: row.order._id,
                token: this.props.token
              });
              this.socket.on("send", payload => {
                this.setState({
                  Chat: {
                    ...this.state.Chat,
                    data: [...this.state.Chat.data, payload]
                  }
                });
              });
              Axios.get(base_url("admin/chat/" + row.order._id), {
                headers: { Authorization: this.props.token }
              })
                .then(result => {
                  this.setState({
                    Chat: { data: result.data.result, loading: false }
                  });
                })
                .catch(e => {
                  this.setState({ Chat: { data: [], loading: false } });

                  console.log("===========eeror=========================");
                  console.log(e, e.response);
                  console.log("====================================");
                });
              // this.toggle(row._id);
            }}
          >
            المحادثة
          </Button>
        </div>
      ),
      sortable: false,
      center: true
    }
    // {
    //   button: true,
    //   allowOverflow: true,
    //   ignoreRowClick: true,
    //   cell: row => (
    //     <Button
    //       size="sm"
    //       color="green"
    //       onClick={e => {
    //         this.toggle(row._id);
    //       }}
    //     >
    //       المحادثة
    //     </Button>
    //   ),
    //   sortable: false,
    //   center: true
    // }
  ];

  toggle = id => {
    this.setState({ chatIsOpen: true });
  };
  renderCustomdetails = ({ title, details }) => (
    <Row
      style={{
        justifyContent: "space-around",
        textAlign: "center",
        marginBottom: "1rem"
      }}
    >
      <text style={{ width: "50%" }}>{title}</text>
      <text style={{ color: "#15141491", width: "50%" }}>{details}</text>
    </Row>
  );
  render() {
    return (
      <Container style={{ marginTop: 50, direction: "rtl" }}>
        <Row
          style={{
            marginTop: 20,
            alignItems: "center"
          }}
        >
          {!this.state.chatIsOpen && !this.state.detailsIsOpen && (
            <Col
              className=".col-sm-12 .col-md-12 .offset-md-12"
              style={{ flex: 1, height: "20%" }}
            >
              <div
                style={{
                  backgroundColor: "#478a22",
                  color: "white",
                  padding: "0.8rem 0.15rem",
                  textAlign: "center",
                  fontSize: "1.25rem"
                }}
              >
                معاملات معطلة{" "}
              </div>{" "}
              <DataTable
                style={{
                  bordered: "1px",
                  border: "1px solid black",
                  textAlign: "center"
                }}
                title={Localization.trips}
                style={{ backgroundColor: "white" }}
                columns={this.columns}
                data={this.props.ProblemOrders.orders}
                striped
                highlightOnHover
                fixedHeader
                noHeader
                pagination={false}
                customTheme={rowTheme}
              />
            </Col>
          )}
          {this.state.chatIsOpen && (
            <Col
              className=".col-sm-12 .col-md-12 .offset-md-12"
              style={{ flex: 1 }}
            >
              <Col
                style={{
                  display: "flex",
                  flexDirection: "column",
                  flex: 1
                }}
              >
                <Alert
                  color="success"
                  style={{ textAlign: "center" }}
                  isOpen={this.state.buttonActionUpdateSuccess}
                  toggle={() =>
                    this.setState({ buttonActionUpdateSuccess: null })
                  }
                >
                  تم التحديث بنجاح
                </Alert>
                <Alert
                  color="warning"
                  style={{ textAlign: "center" }}
                  isOpen={this.state.buttonActionUpdateFail}
                  toggle={() => this.setState({ buttonActionUpdateFail: null })}
                >
                  حدث خطأ ما
                </Alert>
              </Col>
              <div style={{ textAlign: "right", width: "100%" }}>
                <Button
                  size="sm"
                  color="red"
                  s
                  onClick={() => {
                    Axios.post(
                      base_url("admin/problemcashback"),
                      { offer_id: this.state.chatIsOpen._id },
                      {
                        headers: { Authorization: this.props.token }
                      }
                    )
                      .then(result => {
                        this.setState({
                          buttonActionUpdateFail: false,
                          buttonActionUpdateSuccess: true
                        });
                        this.props.getAllTrips(1);
                      })
                      .catch(e => {
                        this.setState({
                          buttonActionUpdateSuccess: false,
                          buttonActionUpdateFail: true
                        });
                        console.log(
                          "===========eeror========================="
                        );
                        console.log(e, e.response);
                        console.log("====================================");
                      });
                  }}
                >
                  الغاء المعاملة{" "}
                </Button>
                <Button
                  size="sm"
                  color="black"
                  style={{}}
                  onClick={() => {
                    Axios.post(
                      base_url("admin/problemdone"),
                      { offer_id: this.state.chatIsOpen._id },
                      {
                        headers: { Authorization: this.props.token }
                      }
                    )
                      .then(result => {
                        this.setState({
                          buttonActionUpdateFail: false,
                          buttonActionUpdateSuccess: true
                        });
                        this.props.getAllTrips(1);
                      })
                      .catch(e => {
                        this.setState({
                          buttonActionUpdateSuccess: false,
                          buttonActionUpdateFail: true
                        });
                        console.log(
                          "===========eeror========================="
                        );
                        console.log(e, e.response);
                        console.log("====================================");
                      });
                  }}
                >
                  اتمام المعاملة
                </Button>
              </div>
              <div
                style={{
                  backgroundColor: "#478a22",
                  color: "white",
                  padding: "0.8rem 0.15rem",
                  textAlign: "center",
                  fontSize: "1.25rem",
                  width: "100%",
                  display: "flex"
                }}
              >
                <div style={{ width: "100%" }}>
                  {this.state.chatIsOpen.order.service.name}
                </div>
                <Button
                  close
                  style={{ float: "left", color: "white" }}
                  onClick={() => {
                    this.socket.disconnect();
                    this.socket = null;

                    this.setState({ chatIsOpen: false });
                  }}
                />
              </div>{" "}
              <div
                style={{
                  background: "white",
                  minHeight: window.innerHeight * 0.4,
                  maxHeight: window.innerHeight * 0.6,
                  overflowY: "auto",
                  overflowX: "hidden"
                }}
              >
                <Row
                  style={{
                    justifyContent: "center",
                    padding: "1rem",
                    alignItems: "center",
                    marginBottom: "100px"
                  }}
                >
                  <Col lg={12} m={12} sm={12}>
                    {this.state.Chat.data.map(e => {
                      let userName = "d";
                      let avatarurl =
                        "http://aux.iconspalace.com/uploads/1867938351348566395.png";
                      if (e.sendertype === "providers") {
                        avatarurl = base_url(`user/image/1/${e.from}`);
                        userName = this.state.chatIsOpen.provider.name;
                      }
                      if (e.sendertype === "users") {
                        avatarurl = base_url(`user/image/0/${e.from}`);
                        userName = this.state.chatIsOpen.order.user.name;
                      }
                      if (e.sendertype === "admin") {
                        userName = "أدمن";
                        avatarurl =
                          "http://aux.iconspalace.com/uploads/1867938351348566395.png";
                      }
                      console.log("====================================");
                      console.log(userName, avatarurl, e);
                      console.log("====================================");
                      return (
                        <div
                          style={{
                            display: "flex",
                            flexDirection:
                              e.sendertype === "admin" ? "row" : "row-reverse"
                          }}
                          key={e._id}
                        >
                          <img
                            src={avatarurl}
                            style={{
                              height: "3rem",
                              width: "2rem",
                              margin: "0 5px"
                            }}
                          />
                          <div>
                            <div>{userName}</div>
                            <div
                              style={{ direction: "ltr", fontSize: "0.5rem" }}
                            >
                              {moment(e.date).format("DD-MM-YYYY hh:mm A")}
                            </div>
                            <div
                              style={{
                                border: "1px solid #eee",
                                background: "#eee",
                                borderRadius: "5px",
                                // margin: "0px 10px",
                                padding: "5px",
                                left: "3rem",
                                marginTop: "15px"
                              }}
                            >
                              {e.text}{" "}
                            </div>
                          </div>
                        </div>
                      );
                    })}
                    {/* <div
                      style={{ display: "flex", flexDirection: "row-reverse" }}
                    >
                      <img
                        src="http://aux.iconspalace.com/uploads/1867938351348566395.png"
                        style={{ height: "3rem" }}
                      />
                      <div>
                        <div>أحمد الحارثي</div>
                        <div style={{ direction: "lrt", fontSize: "0.5rem" }}>
                          26/6/1994 10:25AM
                        </div>
                        <div
                          style={{
                            border: "1px solid #eee",
                            background: "#eee",
                            borderRadius: "5px",
                            // margin: "0px 10px",
                            padding: "5px",
                            left: "3rem",
                            marginTop: "15px"
                          }}
                        >
                          يمكنني القيام بالمهمة المذكورة في اقرب وقت ممكن
                        </div>
                      </div>
                    </div>
                    <div
                      style={{ display: "flex", flexDirection: "row-reverse" }}
                    >
                      <img
                        src="http://aux.iconspalace.com/uploads/1867938351348566395.png"
                        style={{ height: "3rem" }}
                      />
                      <div>
                        <div>أحمد الحارثي</div>
                        <div style={{ direction: "lrt", fontSize: "0.5rem" }}>
                          26/6/1994 10:25AM
                        </div>
                        <div
                          style={{
                            border: "1px solid #eee",
                            background: "#eee",
                            borderRadius: "5px",
                            float: "left",
                            textAlign: "right",
                            padding: "5px",
                            marginTop: "15px"
                            // maxWidth: "70%"
                          }}
                          className="col-sm-12 col-lg-8 col-md-10"
                        >
                          يمكنني القيام بالمهمة المذكورة في اقرب وقت ممكن يمكنني
                          القيام بالمهمة المذكورة في اقرب وقت ممكن يمكنني القيام
                          بالمهمة المذكورة في اقرب وقت ممكن يمكنني القيام
                          بالمهمة المذكورة في اقرب وقت ممكن
                        </div>
                      </div>
                    </div>

                    <div style={{ display: "flex", flexDirection: "row" }}>
                      <img
                        src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABTVBMVEX///8/UbX/t01CQkL/mADTLy83SrOLlM7/pyZ4Rxk9QEL/uE3/nRgzMzP/lgD/u03FxcU2PEL/s0MxRrJWVlY8PDz/vk4rNkElPa//kgAuRLHQFBSsgkcxMTHh4/L/8uPd3d1tPRTsq0z/sjnji4v/oQDSJSVOXrpvb2/v7+/RHBz/qzYwU7vt7vf/5MP/v2T01NT/+vRdUUPR0dFseMNhbsCrq6uvtd2mrNnS1etEVrf/27jqqanaXV3ihob0rknfnUHGyub/9+3/y4V5g8j/rE3/16T55eX/qkPbLBz/rCNnSpvDhjd3R5FfX1+QkJC5NlD/0Z2HQ4SvOV7GMj7MMDaWQHjdKg5WTaaiPWuSmtHnnJzvwcH229vYSEior9rebW1xWT2MaDnftYLmmSuHVB/EkkqgaimcnJxra2ulfkf/x3vvlGMQMa3/7NS8HcLdAAAJ1UlEQVR4nO2aa3vTNhSAkzRL07hpmqShWUZ6C+ktKVAgvcHonVIu7RiXscHGBoPC2Pj/HyfJl0i2bMuWHCmg90Ofp46tR2+OfM6xnFRKo9FoNBqNRqPRaDQajUaj0Wg0Go3mG2NucuPj+aezsymCc9nTEsPK5PlUs1ks1gAjJM052ZPjZ2XjrFl0iznUlmXPj5fJ5QC9ryCIGyPNQL1hD+JGrRiiN9xBvDnF4jfEQVxuMvkNbRBvBqeX4Q/iR+YAQoobc7InHBX2FWopNovLkyuyZ83OyhT7CnWoFYGk7JkzsjISQxBJNkc2ZE+ehdiCkGJtCBzjLFHccWpOtkEIZ3yCcK1+lO0QyDJbHxNIU+X6uBGxTNCpjShbOeaECKqsKMYPKcpWoXMu4Ca0Fc9ky9AQtUYRRRW3qDgroYvmTdk+HiZFhhAiW8iD0AgCiqpV/g1xacaiqVjJEO0H8qlayYYjhIVymf6BWkGciq03fePy5WnqZ0WVHqVuxkqkSG+hVLrko6hSOj2PnklNvUslI5PJ+CiqVBOj3oV29KBexldRoVwzGckQj57NpX+oUZQt5rDMvkhper5RVGc7nDWE7sWZCYmiMtmULZP6RS8gisps+H8MX6RB0QuKomw1i08hhmHR84+iKm1N4CJliZ6voiIVMeDhnjV6fgtVkVTjVw2jRM8niorUfGqiiaPnjaIiydRb76MuTv8oTsmWQ0zR9KJHjx5F2XKImkvvn5jRo0WxJlsOstIUFT1KFIsqFESnWBS4o9dXtDc2lCj5TldaMIToAQyjYBvOydZLYeWwIEYPoQ0HSrKGKjSmiRoWVfiRjTbUhtpQPtpQG2pD+XwDhs0EDZsqGL4rFxIzLNTeydZLrVfaf48UEjIs3+i0K+uSDTt5wI0y3dAwWB6JaWchw/JfcPCKXMHDNpxE/q9piqFR2t1bKJVC/Eqlhb1dz+YANJx++wCO3T6UKThrCuYfvJ32GBqZi/n5+bG9YMXS3hg46yLjUgSG05cfmIO3DyQaWiEES+nydMFt+H5+DDAfqGg8Mk967zYsTH+oWGO3n0g07ORtw0sfymUyOObcAe74EHG2zpl/RJ5ULu+VbMN8RqJh2zHMlBZGSMP39uT3Agz37K/hPRnpkYVSxjFsyxNcxwwzxi45+TEnPP7LtB9o1zLdBf/2DSUWDNzQtRj5YggvUSGGqQpu6BeeoJpoBARaifvwSdvXMFO6mA8LoRPE+QvKSlYilx4EGBqlR6DUXSwE10NjAVTNMeqt6hjKrIdOQaQYwnZlNxP6KsMoZbwtDW4oNYSATNvfELacIX5BJ5mG7V25gqnUbjvAkAtk2L4mWzCVupcHji1B79UwjBbwy9+TrYe4+nk3fMIxuHZ4VbZany/il2nri2wpgtmWeMNZ2VIk4mMo+dnewzXhhgokUYInooNYkVzoPVwVbqhQHkWsi041Ldm7iB5E34iq3YagtRG7TCtqNDM4B2KXaUvqIxMdscv0s2wdCldFBrGlWiZFCH28kC1D5ba4XFO5LVuGjjBBRUMIgijqTmwpGkJx6VS9am/zRUwQFXv2JRDyhKHcUwWBiHWq7hqFCNjNUG33wg13PlWzm8F5wqfYUvomNDnkyTYVqT+8YOVzfMWKio8UFGIrDosgWKjx7sXWUCxRk3txFFvqbVwEEP1x2FC/TJAcRO1urim4MRNCh+kFsBVAoyN7ujEYz1ZYFY1Kdlz2dGMwPjExXmGIo2FU4KmypxuD8YkscgyNH/DLDqthFjp2An+b2IF+2SE2hI5AEkbScLmB6EE966whNjQls/lOBf2i2zD/ZCqdfNbWG35Dy3IiOw7Iwz/mv/jHw2/Y93S5fWWGvmhDJdGG2lB9tOHQG84cfw+g1T633AQ88XhG9oQjsbZ5VP1vPzV7+/A4G6SJ5LLHh7dnU/v/VY8212RPnI2ZraVGNZfO9cx/geZ9KOK2RMfuQzlEL53OVRu9LeUl194s1XNpRB1bdzCaE3Y0UeQmjh05yEzdvChX720Oftbs7N9qVNM2uVHXp2Y0s3jkHEZzzmXVxqiq9+TmTsOZJ5or7aSDf6l7alXiwnpvP9mpxmIrR8wSUKWttys/XKEc3XRdm2vsqLZYvX6AHe95V374jqa44724saNSHDdpfmCS2+4TgeB3FMXtBvXyJc/1ktheok4QrLUj15lIkKJ4lKMPUL+lRPEYrdOnB2dITtAS9Ciu+Y6Qq28NUoXKftrn+0cTJObnCLoVtwKGqC5JLh1H/gFEitipmKBLMXCItNQwbgcFENLo50NCkFDc97mNnUFuyXBDbAYHEMawZ5/rEsQVeyFfE/gaJSXVkBWKcJrTk0WX4eKJ9ckMyzBS6n+PWgPdX7/TnJ6sEoKrtiDWkgYp/jZwv7WwW9Ci6lxBRNGJoKsl9WXgN+PaDpsg3pxiipiguyX1HWiwims5RsF0eql/1XWn4l/vH1xiHaiftQYhyLhEIfV+HnxNMdxmyDO24gCjyLpE0bz6zekdO9ms3nGO+bWkNKruPjcxQgsYQcNpTu/aN+LiXfvQWki1dw01oIx6xJgbLPrN6Y+O4Y/2oaCWlMJg6uJWpK89jRWM504ufW4fivZlEXd1YkRIDRZ2c7qOPVtYg4W1pF6omz9iiTqlfpr/HTP83TwU7Y5GgyWeUG9FnpPTnL7u922rr9ERlpbUTSPhW3E/xpzs5vQVZvgKHWFqSd3Uk93ZiDMl8L2ja+9iXZtZLiLfhZBk12msL91uTv/AOu8/4AHWltRFPcFtRv8toxBQc4o/Pq3CA8wtqZvkDOOkGQQsYyfEPs1JnLpjUX2TlGDsKaHm9DpheD1aS+oaLinD2CFEzekdYpXeidiSEiQVxDjVywY0p1eIZ/xXUVtScrhkDOOvKkjqT8Lwz5iFx4T6WosbjlWVhs0pXixAuYjekuIshc83Om/iVS+bHrmduNjjGq2exF5/7Opl0v2JWKU/dblG87xCF8AM16oCPD7FDE8fc46WQK75jSvPALrEKuULIe3tKzeUF9ERDV9gnfcLXkPxyzR2S9rnJWb4kns04c1pzOcAnO5TZyfqKW8IE8imfOXe5KGda04f8g8mvOjzTwkE8Zll+Iw/hN6fQXDC19BYdH82m+/VnwUY0n6twwNfj+VgxVDIWA2x+zXc1RDR/RXmmsVfRYRQdEUUkWgAL2GuORVQKtLCUw13vTeBzSlvS2ojuObz13sT0Jxyt6QWYncVBXQ0Jt3ni8/FhFBwMqX/eDAG3Ren3C2pTV2koaBiAdj5RdRIYg0FdKUWXSHVHiH0BQbnDkYyNET23jw7f4khtOR/A4b1nHoIfQe1P6oiKv2YX6PRaDQajVz+BwAXZkEbOTj/AAAAAElFTkSuQmCC"
                        style={{ height: "3rem" }}
                      />
                      <div style={{ textAlign: "right" }}>
                        <div>ادمن</div>
                        <div style={{ direction: "lrt", fontSize: "0.5rem" }}>
                          26/6/1994 10:25AM
                        </div>
                        <div
                          style={{
                            border: "1px solid #eee",
                            background: "#eee",
                            borderRadius: "5px",
                            // margin: "0px 10px",
                            padding: "5px",
                            left: "3rem",
                            marginTop: "15px",
                            maxWidth: "70%"
                          }}
                        >
                          بعد مراجعة المعاملة وجدنا ان مقدم الخدمة قام بدوره علي
                          اتم وجه لذلك سيتم تحويل مستحقاته بعد مراجعة المعاملة
                          وجدنا ان مقدم الخدمة قام بدوره علي اتم وجه لذلك سيتم
                          تحويل مستحقاته بعد مراجعة المعاملة وجدنا ان مقدم
                          الخدمة قام بدوره علي اتم وجه لذلك سيتم تحويل مستحقاته
                        </div>
                      </div>
                    </div>
                   */}
                  </Col>
                </Row>
                <div
                  style={{
                    color: "white",
                    margin: "0.8rem 0.15rem",
                    textAlign: "center",
                    fontSize: "1.25rem",
                    width: "98%",
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    position: "absolute",
                    bottom: 0,
                    maxWidth: "100%"
                  }}
                >
                  <Input
                    value={this.state.Chat.Text}
                    type="textarea"
                    style={{ width: "90%" }}
                    onChange={e => {
                      this.setState({
                        Chat: { ...this.state.Chat, Text: e.target.value }
                      });
                    }}
                  />
                  <Button
                    size="sm"
                    color="green"
                    style={{}}
                    onClick={e => {
                      console.log("====================================");
                      console.log(this.state.Chat.Text);
                      console.log("====================================");
                      this.socket.emit("send", {
                        token: this.props.token,
                        text: this.state.Chat.Text,
                        texttype: "text",
                        isProblem: false,
                        room: this.state.chatIsOpen.order._id
                      });
                    }}
                  >
                    إرسال
                  </Button>
                </div>{" "}
              </div>
            </Col>
          )}
          {this.state.detailsIsOpen && (
            <Col
              className=".col-sm-12 .col-md-12 .offset-md-12"
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <div
                style={{
                  backgroundColor: "#478a22",
                  color: "white",
                  padding: "0.8rem 0.15rem",
                  textAlign: "center",
                  fontSize: "1.25rem",
                  width: "100%"
                }}
              >
                <text>تفاصيل المعاملة</text>
                <Button
                  close
                  style={{ float: "left", color: "white" }}
                  onClick={() => this.setState({ detailsIsOpen: false })}
                />
              </div>
              <h4
                style={{
                  color: "green",
                  textDecorationLine: "underline",
                  textAlign: "center"
                }}
              >
                المعاملة
              </h4>
              {this.renderCustomdetails({
                title: "عنوان الخدمة",
                details: this.state.detailsIsOpen.order.service.name
              })}
              {this.renderCustomdetails({
                title: "التاريخ",
                details: moment(this.state.detailsIsOpen.order.date).format(
                  "DD-MM-YYYY hh:mm A"
                )
              })}
              {this.renderCustomdetails({
                title: "السعر",
                details: `من ${this.state.detailsIsOpen.order.pidfrom} إلي ${
                  this.state.detailsIsOpen.order.pidto
                } ريال`
              })}
              {this.state.detailsIsOpen.order.files.map((e, index) => {
                return (
                  <Row
                    style={{
                      justifyContent: "space-around",
                      marginBottom: "1rem"
                    }}
                  >
                    <text>ملف</text>
                    <a
                      style={{ color: "#15141491" }}
                      target="_blank"
                      href={base_url(
                        `api/order/file/${index}/${
                          this.state.chatIsOpen.order._id
                        }`
                      )}
                    >
                      {e}
                    </a>
                  </Row>
                );
              })}
              <h4
                style={{
                  color: "green",
                  textDecorationLine: "underline",
                  textAlign: "center"
                }}
              >
                العرض
              </h4>
              {this.renderCustomdetails({
                title: "مقدمة العرض",
                details: this.state.detailsIsOpen.introduction
              })}
              {this.renderCustomdetails({
                title: "المدة",
                details: this.state.detailsIsOpen.time
              })}
              {this.renderCustomdetails({
                title: "السعر",
                details: this.state.detailsIsOpen.price
              })}
            </Col>
          )}
        </Row>
      </Container>
    );
  }
}

const mapState = state => {
  return {
    ...state.trips,
    token: state.auth.admin.token
  };
};

const mapDispatch = dispatch => {
  return {
    getAllTrips: page => GetProblemServices(page, dispatch)
  };
};
export default connect(
  mapState,
  mapDispatch
)(Clients);
