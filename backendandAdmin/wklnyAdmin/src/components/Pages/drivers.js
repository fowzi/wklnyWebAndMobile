import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Alert,
  Button,
  Table,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  CustomInput,
  FormGroup,
  Label,
  Input,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from "reactstrap";
import { MdPerson } from "react-icons/md";
import { withRouter } from "react-router-dom";
import DataTable from "react-data-table-component";
import { FiMoreVertical } from "react-icons/fi";
import { height } from "window-size";

import { connect } from "react-redux";
import {
  FetchAllDrivers,
  DeleteDriver,
  DisableDriver,
  FetchDriverDetails
} from "../../redux/actions";
import moment from "moment";
import Localization from "../../localization";
import DriverModal from "../modal";
import Spinner from "../../components/UI/Spinner/Spinner";
import Axios from "axios";
import { base_url } from "../../redux/config";
import store from "../../redux";
const rowTheme = {
  title: {
    fontSize: "22px",
    fontColor: "#FFFFFF",
    backgroundColor: "green"
  },
  rows: {
    // spaced allows the following properties
    // spacing: "spaced",
    // spacingMargin: "3px",
    // borderColor: "rgba(0,0,0,.12)",
    // border: "1px solid",
    // backgroundColor: "white",
    // height: "52px"
    spacing: "default",
    fontSize: "13px",
    fontColor: "rgba(0,0,0,.87)",
    backgroundColor: "transparent",
    borderWidth: "1px",
    // "borderColor": "black",
    stripedColor: "rgba(0,0,0,.03)",
    hoverFontColor: "rgba(0,0,0,.87)",
    hoverBackgroundColor: "#eee"
  },
  cells: {
    cellPadding: "48px",
    // border: "1px",
    borderWidth: "1px"
  }
};
class Drivers extends Component {
  componentDidMount() {
    this.props.getDriversNow(0);
  }
  toggle = (id = null) => {
    if (typeof id === "string") {
      this.props.getDriverDetails(id);
    }
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  };

  state = {
    dropdownOpen: false,
    rowId: null,
    modal: false,
    trans_balance: false,
    trans_loading: false,
    alertMessageSuccess: false,
    alertMessageFail: false
  };

  columns = [
    {
      name: Localization.name,
      selector: "name",
      sortable: true,
      center: true,
      grow: true
    },
    {
      name: Localization.email,
      selector: "email",
      sortable: true,
      center: true,
      grow: true
    },
    {
      name: Localization.phone,
      selector: "phone",
      sortable: true,
      center: true,
      grow: true
    },
    {
      name: Localization.city,
      selector: "cityName",
      cell: row => (
        <div>{row.city && row.city.name ? row.city.name : "---"}</div>
      ),
      center: true,
      sortable: true,
      grow: true
    },
    {
      name: Localization.welcomeDate,
      cell: row => <div>{moment(row.date).format("YYYY-MM-DD")}</div>,
      sortable: true,
      center: true,
      grow: true
    },
    {
      name: "الرصيد",
      selector: "balance",
      cell: row => <div>{row.balance}</div>,
      sortable: true,
      center: true,
      grow: true
    },
    {
      name: Localization.rating,
      cell: row => (
        <div>
          {[1, 2, 3, 4, 5].map((t, i) => (
            <i
              className="fa fa-star"
              aria-hidden="true"
              style={{
                padding: "1px",
                color: row.driverRating > i ? "green" : "gray"
              }}
            />
          ))}
        </div>
      ),
      sortable: true,
      center: true
    },

    // {
    //   name: Localization.active,
    //   selector: "verfied",
    //   sortable: true,
    //   center: true,
    //   width: "30px",
    //   cell: row => (
    //     <FormGroup check>
    //       <Label check>
    //         <Input
    //           type="checkbox"
    //           style={{ marginRight: "1px" }}
    //           defaultChecked={row.verfied}
    //           onChange={t => {
    //             let v = t.target.checked ? 1 + "" : 0 + "";
    //             this.props.VerifiyDriverNow(row._id, v);
    //           }}
    //         />
    //         .
    //       </Label>
    //     </FormGroup>
    //   )
    // },
    {
      name: Localization.delete,
      cell: row => (
        <span onClick={() => this.props.deleteDriverNow(row._id)}>
          <i className="fa fa-trash-o" aria-hidden="true" />
        </span>
      ),
      ignoreRowClick: true,
      allowOverflow: true,
      button: true
    }
  ];

  detailscolumns = [
    {
      name: Localization.details,
      selector: "details",
      sortable: true,
      center: true,
      cell: d =>
        d.type === "img" ? (
          <img height="200px" width="300px" src={d.details} />
        ) : (
          <text>{d.details}</text>
        )
    },
    {
      name: "#",
      selector: "name",
      sortable: true,
      center: true,
      width: "30%",
      wrap: true
    }
  ];

  render() {
    return (
      <Container style={{ marginTop: 50, direction: "rtl" }}>
        <div>
          <Modal
            size="lg"
            style={{ marginLeft: "4%", marginBottom: "100px" }}
            isOpen={this.state.modal}
            toggle={this.toggle}
            className={this.props.className}
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
                justifyContent: "space-between",
                borderBottom: "1px solid gray"
              }}
            >
              <ModalHeader
                toggle={this.toggle}
                style={{ textAlign: "center", width: "10%" }}
              />
              <div
                style={{
                  width: "90%",
                  textAlign: "center",
                  padding: "0px 1rem 0px 1rem"
                }}
              >
                {" "}
                {Localization.details}
              </div>
            </div>
            <ModalBody>
              {this.props.driverDetailsLoading ? (
                <Spinner />
              ) : (
                <div>
                  <DataTable
                    style={{
                      bordered: "1px",
                      border: "1px solid black",
                      textAlign: "center",
                      height: "1000px"
                    }}
                    title={null}
                    style={{ backgroundColor: "white" }}
                    columns={this.detailscolumns}
                    data={this.props.driverDetails}
                    highlightOnHover
                    noHeader
                    customTheme={rowTheme}
                    responsive={true}
                  />
                  <Row
                    style={{
                      justifyContent: "center",
                      alignItems: "center",
                      borderTop: "1px solid #ccc",
                      direction: "rtl"
                    }}
                  >
                    <Col lg={8} sm={8} md={8}>
                      <Input
                        type="number"
                        placeholder=" تحديث الرصيد"
                        value={this.state.trans_balance}
                        onChange={e =>
                          this.setState({ trans_balance: e.target.value })
                        }
                      />
                    </Col>
                    <Col lg={2} sm={4} md={4}>
                      <Button
                        title="تحويل"
                        color="green"
                        size="sm"
                        onClick={() => {
                          if (
                            this.state.trans_balance >
                            this.props.driverDetails.balance
                          ) {
                          }
                          let token = store.getState().auth.admin.token;

                          let config = {
                            headers: {
                              Authorization: token
                            }
                          };
                          console.log("====================================");
                          console.log(this.props.driverDetails);
                          console.log("====================================");
                          Axios.post(
                            base_url("admin/driver/balancetransfer"),
                            {
                              provider_id: this.props.driverDetails[0]._id,
                              balance: this.state.trans_balance
                            },
                            config
                          )
                            .then(result => {
                              this.setState({
                                alertMessageSuccess: result.data.success
                              });
                              this.props.getDriverDetails(
                                this.props.driverDetails[0]._id
                              );
                            })
                            .catch(err => {
                              this.setState({
                                alertMessageFail: err.response.data.error
                              });
                            });
                        }}
                      >
                        تحديث الرصيد
                      </Button>
                    </Col>
                    {this.state.alertMessageSuccess && (
                      <Alert
                        color="success"
                        style={{ textAlign: "center" }}
                        toggle={() => {
                          this.setState({ alertMessageSuccess: false });
                        }}
                      >
                        {this.state.alertMessageSuccess}
                      </Alert>
                    )}
                    {this.state.alertMessageFail && (
                      <Alert
                        color="warning"
                        style={{ textAlign: "center" }}
                        toggle={() => {
                          this.setState({ alertMessageFail: false });
                        }}
                      >
                        {this.state.alertMessageFail}
                      </Alert>
                    )}
                  </Row>
                </div>
              )}
            </ModalBody>
          </Modal>
        </div>
        <Row
          style={{
            marginTop: 20,
            alignItems: "center"
          }}
        >
          <Col className=".col-sm-12 .col-md-12 .offset-md-12" style={{}}>
            <div
              style={{
                backgroundColor: "#478a22",
                color: "white",
                padding: "0.8rem 0.15rem",
                textAlign: "center",
                fontSize: "1.25rem"
              }}
            >
              مزودي الخدمة{" "}
            </div>{" "}
            <DataTable
              style={{
                bordered: "1px",
                border: "1px solid black",
                textAlign: "center"
              }}
              title={Localization.drivers}
              style={{ backgroundColor: "white" }}
              columns={this.columns}
              data={this.props.drivers}
              striped
              highlightOnHover
              fixedHeader
              noHeader
              customTheme={rowTheme}
              onRowClicked={(row, indx) => {
                this.toggle(row._id);
              }}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapState = state => {
  return {
    ...state.drivers
  };
};

const mapDispatch = dispatch => {
  return {
    VerifiyDriverNow: (driverId, verified) =>
      DisableDriver(driverId, verified, dispatch),
    deleteDriverNow: id => DeleteDriver(id, dispatch),
    getDriversNow: page => FetchAllDrivers(dispatch, page),
    getDriverDetails: id => FetchDriverDetails({ driver_id: id }, dispatch)
  };
};
export default connect(
  mapState,
  mapDispatch
)(Drivers);
