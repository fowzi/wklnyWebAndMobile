import React, { Component } from "react";
import Chart from "react-apexcharts";
import { Col, Row } from "reactstrap";
import "./dashboard.css";
import Axios from "axios";
import { base_url } from "../../../redux/config";
import Store from "../../../redux";
class Donut extends Component {
  constructor(props) {
    super(props);

    this.state = {
      options: {},
      series: [props.count.users, props.count.providers],
      labels: ["مستخدم", "مقدم خدمة"],
      labels2: [
        "خدمات مكتملة",
        //  "خدمات ملغية",
        "خدمات جارية"
      ],
      balance: 0
    };
    this.legend = {
      show: true,
      showForSingleSeries: false,
      showForNullSeries: true,
      showForZeroSeries: true,
      position: "bottom",
      horizontalAlign: "center",
      floating: false,
      fontSize: "14px",
      fontFamily: "Helvetica, Arial",
      width: undefined,
      height: undefined,
      formatter: undefined,
      offsetX: 0,
      offsetY: 0,
      labels: {
        colors: undefined,
        useSeriesColors: false
      },
      markers: {
        width: 12,
        height: 12,
        strokeWidth: 0,
        strokeColor: "#fff",
        radius: 12,
        customHTML: undefined,
        onClick: undefined,
        offsetX: 0,
        offsetY: 0
      },
      itemMargin: {
        horizontal: 20,
        vertical: 5
      },
      onItemClick: {
        toggleDataSeries: true
      },
      onItemHover: {
        highlightDataSeries: true
      }
    };
  }
  componentDidMount() {
    Axios.get(base_url("admin/balance"), {
      headers: { Authorization: Store.getState().auth.admin.token }
    }).then(res => {
      console.log("==bal==================================");
      console.log(res);
      console.log("====================================");
      this.setState({ balance: res.data.balance });
    });
  }
  render() {
    console.log("====================================");
    console.log(this.props);
    console.log("====================================");
    return (
      <Row style={{ justifyContent: "center", alignItems: "center" }}>
        <Col lg={4} className="balance-container">
          <div className="balance">
            <h1>الرصيد</h1>
            <h1
              style={{
                color: "green",
                textDecoration: "underline",
                marginRight: "0.5rem"
              }}
            >
              {this.state.balance + ""} ريال
            </h1>
          </div>
        </Col>
        <Col lg={4}>
          <div className="donut">
            <Chart
              options={{
                labels: this.state.labels,
                colors: ["#008000", "#E4572E"], // "#66DA26"
                legend: this.legend
              }}
              series={this.state.series}
              type="donut"
              width="100%"
            />
          </div>
        </Col>
        <Col lg={4}>
          <div className="line">
            <Chart
              options={{
                labels: this.state.labels2,
                colors: [
                  "#757575",
                  // "#E4572E",
                  "#008000"
                ],
                legend: this.legend
              }}
              series={[
                this.props.count.doneOrders,
                // this.props.count.cancledOrders,
                this.props.count.runningorders
              ]}
              type="donut"
              width="100%"
            />
          </div>
        </Col>
      </Row>
    );
  }
}

export default Donut;
