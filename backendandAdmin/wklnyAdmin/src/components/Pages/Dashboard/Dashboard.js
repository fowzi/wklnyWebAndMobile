import React, { Component } from "react";
import "./dashboard.css";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Table
} from "reactstrap";
import {
  MdPerson,
  MdDirectionsCar,
  MdFlightTakeoff,
  MdFlightLand
} from "react-icons/md";
import { FaUsers, FaRunning, FaGripfire, FaCheckDouble } from "react-icons/fa";
import { GiQuicksand } from "react-icons/gi";
import { withRouter } from "react-router-dom";
import Localization from "../../../localization";
import Spinner from "../../../components/UI/Spinner/Spinner";
import { Link } from "react-router-dom";

import moment from "moment";

import { connect } from "react-redux";
import { getCount } from "../../../redux/actions";
import { GetAllTrips } from "../../../redux/actions/trips";
import Donut from "./DonutChart";

class Dashboard extends Component {
  componentWillMount() {
    this.props.getAllTrips();
    this.props.fetchCountNow();
  }

  card_render = (Icon, subtitle, title, link, action) => (
    <Col
      sm={6}
      lg={3}
      style={{
        marginBottom: "2rem",
        textAlign: "right",
        color: "white",
        minWidth: "130px"
      }}
    >
      <Link to={link} className="card-outline">
        <div className="card-head ">{title}</div>
        <div
          style={{
            display: "flex",
            justifyContent: "space-between",
            alignItems: "center",
            backgroundColor: "ghostwhite",
            padding: "1rem"
          }}
        >
          {<Icon className="card-outline-icon" size={"5rem"} />}
          <h1 style={{ color: "green" }}>{subtitle}</h1>
        </div>
        <a className="card-head ">المزيد</a>
      </Link>
    </Col>
  );

  render() {
    console.log("===============count=====================");
    console.log(this.props.count);
    console.log("====================================");
    return this.props.count.users ? (
      <Container style={{ marginTop: 50, direction: "rtl" }}>
        <Row>
          {this.card_render(
            FaUsers,
            this.props.count && this.props.count.users
              ? this.props.count.users
              : 0,
            `عدد ${Localization.users}`,
            "/clients",

            () => {}
          )}
          {this.card_render(
            GiQuicksand,
            this.props.count && this.props.count.providers
              ? this.props.count.providers
              : 0,
            `عدد ${Localization.provider}`,
            "service-providers",

            () => {}
          )}
          {this.card_render(
            FaGripfire,
            this.props.count && this.props.count.runningorders
              ? this.props.count.runningorders
              : 0,
            `عدد ${Localization.openTreats}`,
            "servicerunning",

            () => {}
          )}
          {this.card_render(
            FaCheckDouble,
            this.props.count && this.props.count.doneOrders
              ? this.props.count.doneOrders
              : 0,
            `عدد ${Localization.FinishedTreats}`,
            "serviceended",

            () => {}
          )}
        </Row>
        <Row
          style={{
            marginTop: 20,
            alignItems: "center"
          }}
        >
          <Col className=".col-sm-12 .col-md-8 .offset-md-8">
            <Donut {...this.props} />
          </Col>{" "}
        </Row>
      </Container>
    ) : (
      <Spinner />
    );
  }
}

const mapState = state => {
  return {
    ...state.count,
    ...state.trips
  };
};
const mapDispatch = dispatch => {
  return {
    fetchCountNow: () => getCount(dispatch),
    getAllTrips: () => GetAllTrips(dispatch)
  };
};

export default connect(
  mapState,
  mapDispatch
)(withRouter(Dashboard));
