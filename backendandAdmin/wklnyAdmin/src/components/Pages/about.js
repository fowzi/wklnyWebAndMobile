import React, { Component } from "react";
import { Document, Page, pdfjs } from "react-pdf";
import store from "../../redux/index";

import { Input, Row, Col, Button, Alert } from "reactstrap";
import Axios from "axios";
import { base_url } from "../../redux/config";
class MyApp extends Component {
  state = {
    numPages: null,
    updated: null,
    updatedError: null,
    about: null
  };

  onDocumentLoadSuccess = ({ numPages }) => {
    this.setState({ numPages });
  };
  componentDidMount() {
    this.getAbout();
  }
  getAbout = () => {
    Axios.get(base_url("admin/about")).then(result => {
      this.setState({ about: result.data.about });
    });
  };
  updateAbout = () => {
    let token = store.getState().auth.admin.token;

    Axios.post(
      base_url("admin/about"),
      { about: this.state.about },
      { headers: { Authorization: token } }
    )
      .then(result => {
        this.setState({ updated: true });
      })
      .catch(error => {
        this.setState({ updatedError: error.response.data.error });
      });
  };
  render() {
    return (
      <Row
        style={{
          marginTop: window.innerHeight * 0.1,
          justifyContent: "center",
          alignItems: "center",
          textAlign: "center"
        }}
      >
        <Col lg={8} style={{}}>
          <Alert
            color="success"
            style={{ textAlign: "center" }}
            isOpen={this.state.updated}
            toggle={() => this.setState({ updated: null })}
          >
            تم التحديث بنجاح
          </Alert>
          <Alert
            color="warning"
            style={{ textAlign: "center" }}
            isOpen={this.state.updatedError}
            toggle={() => this.setState({ updatedError: null })}
          >
            {this.state.updatedError}{" "}
          </Alert>
          <div
            style={{ backgroundColor: "green", padding: 12, color: "white" }}
          >
            من نحن
          </div>
        </Col>
        <Col lg={8}>
          <Input
            type="textarea"
            style={{ minHeight: window.innerHeight * 0.4, textAlign: "right" }}
            placeholder="من نحن"
            value={this.state.about}
            onChange={e => this.setState({ about: e.target.value })}
          />
        </Col>
        <Col lg={8} style={{ marginTop: "2rem" }}>
          <Button
            size="large"
            color="green"
            block
            onClick={() => this.updateAbout()}
          >
            تحديث
          </Button>
        </Col>
      </Row>
    );
  }
}

export default MyApp;
