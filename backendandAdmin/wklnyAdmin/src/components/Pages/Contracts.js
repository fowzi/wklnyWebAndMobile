import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Table,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Alert,
  Input
} from "reactstrap";
import { MdPerson } from "react-icons/md";
import { withRouter } from "react-router-dom";
import DataTable from "react-data-table-component";
import { FiMoreVertical } from "react-icons/fi";
import { connect } from "react-redux";
import { GetAllUsers, DeleteUser } from "../../redux/actions";

import moment from "moment";
import Localization from "../../localization";
import Axios from "axios";
import { base_url } from "../../redux/config";

const rowTheme = {
  title: {
    fontSize: "22px",
    fontColor: "#FFFFFF",
    backgroundColor: "green"
  },
  rows: {
    // spaced allows the following properties
    // spacing: "spaced",
    // spacingMargin: "3px",
    // borderColor: "rgba(0,0,0,.12)",
    // border: "1px solid",
    // backgroundColor: "white",
    // height: "52px"
    spacing: "default",
    fontSize: "0.7rem",
    fontColor: "rgba(0,0,0,.87)",
    backgroundColor: "transparent",
    borderWidth: "1px",
    // "borderColor": "black",
    stripedColor: "rgba(0,0,0,.03)",
    hoverFontColor: "rgba(0,0,0,.87)",
    hoverBackgroundColor: "#eee"
  },
  cells: {
    cellPadding: "48px",
    // border: "1px",
    borderWidth: "1px"
  }
};
class Clients extends Component {
  FetchContractsDetails = row => {
    Axios.get(base_url("users/contracts/" + row.user[0]._id + "/" + 1), {
      headers: {
        Authorization: this.props.token
      }
    }).then(result => {
      console.log("=====det===============================");
      console.log(result, row);
      console.log("====================================");
      this.setState({
        contractOpen: {
          isOpen: true,
          user: row.user[0],
          data: result.data.result
        }
      });
    });
  };
  componentWillMount() {
    Axios.post(
      base_url("admin/contracts"),
      { page: 1 },
      {
        headers: {
          Authorization: this.props.token
        }
      }
    )
      .then(result => {
        console.log("=====cont===============================");
        console.log(result.data);
        console.log("====================================");
        this.setState({ contracts: result.data });
      })
      .catch(e => {
        console.log("error====================================");
        console.log(e);
        console.log("====================================");
      });
    moment.locale("ar");
  }

  state = {
    dropdownOpen: false,
    rowId: null,
    page: 0,
    contracts: [],
    contractOpen: { isOpen: false, user: null, data: [], InputText: "" },
    alertMessageSuccess: false,
    alertMessageFail: false,
    trans_balance: null
  };

  columns = [
    {
      name: Localization.name,
      cell: row => <div>{row.user[0].name}</div>,
      // selector: "user.name",
      sortable: true,
      center: true,
      grow: window.innerWidth < 500 ? true : false
    },
    {
      name: "التاريخ",
      cell: row => (
        <div style={{ direction: "ltr" }}>
          {" "}
          {moment(row.date).format("YYYY-MM-DD  hh-mm A ")}
        </div>
      ),
      sortable: true,
      center: true,
      selector: "date"
    }
  ];

  render() {
    console.log("====================================");
    console.log(this.props.count.users, this.props.users.length);
    console.log("====================================");
    return (
      <Container style={{ marginTop: 50, direction: "rtl" }}>
        <Row
          style={{
            marginTop: 20,
            alignItems: "center"
          }}
        >
          {!this.state.contractOpen.isOpen && (
            <Col
              className=".col-sm-12 .col-md-12 .offset-md-12"
              style={{ flex: 1, height: "20%" }}
            >
              <div
                style={{
                  backgroundColor: "#478a22",
                  color: "white",
                  padding: "0.8rem 0.15rem",
                  textAlign: "center",
                  fontSize: "1.25rem"
                }}
              >
                التحويلات البنكية{" "}
              </div>{" "}
              <DataTable
                // pageSizeOptions={[20, 30, 50, 100, 200, 500]}
                style={{
                  bordered: "1px",
                  border: "1px solid black",
                  textAlign: "center"
                }}
                title={Localization.users}
                style={{ backgroundColor: "white" }}
                columns={this.columns}
                data={this.state.contracts}
                striped
                highlightOnHover
                fixedHeader
                noHeader
                pagination={false}
                customTheme={rowTheme}
                onRowClicked={(row, indx) => {
                  console.log("====================================");
                  console.log(row);
                  console.log("====================================");
                  this.FetchContractsDetails(row);
                  this.setState({
                    rowId: row
                  });
                }}
              />
            </Col>
          )}
          {this.state.contractOpen.isOpen && (
            <Col
              className=".col-sm-12 .col-md-12 .offset-md-12"
              style={{ flex: 1 }}
            >
              <Row
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  borderTop: "1px solid #ccc",
                  direction: "rtl"
                }}
              >
                <Col lg={8} sm={8} md={8}>
                  <Input
                    type="number"
                    placeholder=" تحديث الرصيد"
                    value={this.state.trans_balance}
                    onChange={e =>
                      this.setState({ trans_balance: e.target.value })
                    }
                  />
                </Col>
                <Col lg={2} sm={4} md={4}>
                  <Button
                    title="تحويل"
                    color="green"
                    size="sm"
                    onClick={() => {
                      if (!this.state.trans_balance) {
                        return;
                      }
                      let token = this.props.token;

                      let config = {
                        headers: {
                          Authorization: token
                        }
                      };

                      Axios.put(
                        base_url("users/charge"),
                        {
                          user: this.state.contractOpen.user._id,
                          balance: this.state.trans_balance
                        },
                        config
                      )
                        .then(result => {
                          console.log("====================================");
                          console.log(result);
                          console.log("====================================");
                          this.setState({
                            alertMessageSuccess: result.data.success
                          });
                        })
                        .catch(err => {
                          console.log("====================================");
                          console.log(err);
                          console.log("====================================");
                          this.setState({
                            alertMessageFail: err.response.data.error
                          });
                        });
                    }}
                  >
                    تحديث الرصيد
                  </Button>
                </Col>
                {this.state.alertMessageSuccess && (
                  <Alert
                    color="success"
                    style={{ textAlign: "center" }}
                    toggle={() => {
                      this.setState({ alertMessageSuccess: false });
                    }}
                  >
                    {this.state.alertMessageSuccess}
                  </Alert>
                )}
                {this.state.alertMessageFail && (
                  <Alert
                    color="warning"
                    style={{ textAlign: "center" }}
                    toggle={() => {
                      this.setState({ alertMessageFail: false });
                    }}
                  >
                    {this.state.alertMessageFail}
                  </Alert>
                )}
              </Row>
              <div
                style={{
                  backgroundColor: "#478a22",
                  color: "white",
                  padding: "0.8rem 0.15rem",
                  textAlign: "center",
                  fontSize: "1.25rem",
                  width: "100%",
                  display: "flex"
                }}
              >
                <div style={{ width: "100%" }}>
                  {this.state.contractOpen.user.name}
                </div>
                <Button
                  close
                  style={{ float: "left", color: "white" }}
                  onClick={() => {
                    this.setState({
                      contractOpen: { isOpen: false, user: null }
                    });
                  }}
                />
              </div>{" "}
              <div
                style={{
                  background: "white",
                  minHeight: window.innerHeight * 0.4,
                  maxHeight: window.innerHeight * 0.6,
                  overflowY: "auto",
                  overflowX: "hidden"
                }}
              >
                <Row
                  style={{
                    justifyContent: "center",
                    padding: "1rem",
                    alignItems: "center",
                    marginBottom: "100px"
                  }}
                >
                  <Col lg={12} m={12} sm={12}>
                    {this.state.contractOpen.data.map(e => {
                      let userName = "d";
                      let avatarurl =
                        "http://aux.iconspalace.com/uploads/1867938351348566395.png";
                      if (e.sendertype === "providers") {
                        avatarurl = base_url(`user/image/1/${e.from}`);
                        userName = this.state.chatIsOpen.provider.name;
                      }
                      if (e.sendertype === "user") {
                        avatarurl = base_url(`user/image/0/${e.from}`);
                        userName = this.state.contractOpen.user.name;
                      }
                      if (e.sendertype === "admin") {
                        userName = "أدمن";
                        avatarurl =
                          "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABTVBMVEX///8/UbX/t01CQkL/mADTLy83SrOLlM7/pyZ4Rxk9QEL/uE3/nRgzMzP/lgD/u03FxcU2PEL/s0MxRrJWVlY8PDz/vk4rNkElPa//kgAuRLHQFBSsgkcxMTHh4/L/8uPd3d1tPRTsq0z/sjnji4v/oQDSJSVOXrpvb2/v7+/RHBz/qzYwU7vt7vf/5MP/v2T01NT/+vRdUUPR0dFseMNhbsCrq6uvtd2mrNnS1etEVrf/27jqqanaXV3ihob0rknfnUHGyub/9+3/y4V5g8j/rE3/16T55eX/qkPbLBz/rCNnSpvDhjd3R5FfX1+QkJC5NlD/0Z2HQ4SvOV7GMj7MMDaWQHjdKg5WTaaiPWuSmtHnnJzvwcH229vYSEior9rebW1xWT2MaDnftYLmmSuHVB/EkkqgaimcnJxra2ulfkf/x3vvlGMQMa3/7NS8HcLdAAAJ1UlEQVR4nO2aa3vTNhSAkzRL07hpmqShWUZ6C+ktKVAgvcHonVIu7RiXscHGBoPC2Pj/HyfJl0i2bMuWHCmg90Ofp46tR2+OfM6xnFRKo9FoNBqNRqPRaDQajUaj0Wg0Go3mG2NucuPj+aezsymCc9nTEsPK5PlUs1ks1gAjJM052ZPjZ2XjrFl0iznUlmXPj5fJ5QC9ryCIGyPNQL1hD+JGrRiiN9xBvDnF4jfEQVxuMvkNbRBvBqeX4Q/iR+YAQoobc7InHBX2FWopNovLkyuyZ83OyhT7CnWoFYGk7JkzsjISQxBJNkc2ZE+ehdiCkGJtCBzjLFHccWpOtkEIZ3yCcK1+lO0QyDJbHxNIU+X6uBGxTNCpjShbOeaECKqsKMYPKcpWoXMu4Ca0Fc9ky9AQtUYRRRW3qDgroYvmTdk+HiZFhhAiW8iD0AgCiqpV/g1xacaiqVjJEO0H8qlayYYjhIVymf6BWkGciq03fePy5WnqZ0WVHqVuxkqkSG+hVLrko6hSOj2PnklNvUslI5PJ+CiqVBOj3oV29KBexldRoVwzGckQj57NpX+oUZQt5rDMvkhper5RVGc7nDWE7sWZCYmiMtmULZP6RS8gisps+H8MX6RB0QuKomw1i08hhmHR84+iKm1N4CJliZ6voiIVMeDhnjV6fgtVkVTjVw2jRM8niorUfGqiiaPnjaIiydRb76MuTv8oTsmWQ0zR9KJHjx5F2XKImkvvn5jRo0WxJlsOstIUFT1KFIsqFESnWBS4o9dXtDc2lCj5TldaMIToAQyjYBvOydZLYeWwIEYPoQ0HSrKGKjSmiRoWVfiRjTbUhtpQPtpQG2pD+XwDhs0EDZsqGL4rFxIzLNTeydZLrVfaf48UEjIs3+i0K+uSDTt5wI0y3dAwWB6JaWchw/JfcPCKXMHDNpxE/q9piqFR2t1bKJVC/Eqlhb1dz+YANJx++wCO3T6UKThrCuYfvJ32GBqZi/n5+bG9YMXS3hg46yLjUgSG05cfmIO3DyQaWiEES+nydMFt+H5+DDAfqGg8Mk967zYsTH+oWGO3n0g07ORtw0sfymUyOObcAe74EHG2zpl/RJ5ULu+VbMN8RqJh2zHMlBZGSMP39uT3Agz37K/hPRnpkYVSxjFsyxNcxwwzxi45+TEnPP7LtB9o1zLdBf/2DSUWDNzQtRj5YggvUSGGqQpu6BeeoJpoBARaifvwSdvXMFO6mA8LoRPE+QvKSlYilx4EGBqlR6DUXSwE10NjAVTNMeqt6hjKrIdOQaQYwnZlNxP6KsMoZbwtDW4oNYSATNvfELacIX5BJ5mG7V25gqnUbjvAkAtk2L4mWzCVupcHji1B79UwjBbwy9+TrYe4+nk3fMIxuHZ4VbZany/il2nri2wpgtmWeMNZ2VIk4mMo+dnewzXhhgokUYInooNYkVzoPVwVbqhQHkWsi041Ldm7iB5E34iq3YagtRG7TCtqNDM4B2KXaUvqIxMdscv0s2wdCldFBrGlWiZFCH28kC1D5ba4XFO5LVuGjjBBRUMIgijqTmwpGkJx6VS9am/zRUwQFXv2JRDyhKHcUwWBiHWq7hqFCNjNUG33wg13PlWzm8F5wqfYUvomNDnkyTYVqT+8YOVzfMWKio8UFGIrDosgWKjx7sXWUCxRk3txFFvqbVwEEP1x2FC/TJAcRO1urim4MRNCh+kFsBVAoyN7ujEYz1ZYFY1Kdlz2dGMwPjExXmGIo2FU4KmypxuD8YkscgyNH/DLDqthFjp2An+b2IF+2SE2hI5AEkbScLmB6EE966whNjQls/lOBf2i2zD/ZCqdfNbWG35Dy3IiOw7Iwz/mv/jHw2/Y93S5fWWGvmhDJdGG2lB9tOHQG84cfw+g1T633AQ88XhG9oQjsbZ5VP1vPzV7+/A4G6SJ5LLHh7dnU/v/VY8212RPnI2ZraVGNZfO9cx/geZ9KOK2RMfuQzlEL53OVRu9LeUl194s1XNpRB1bdzCaE3Y0UeQmjh05yEzdvChX720Oftbs7N9qVNM2uVHXp2Y0s3jkHEZzzmXVxqiq9+TmTsOZJ5or7aSDf6l7alXiwnpvP9mpxmIrR8wSUKWttys/XKEc3XRdm2vsqLZYvX6AHe95V374jqa44724saNSHDdpfmCS2+4TgeB3FMXtBvXyJc/1ktheok4QrLUj15lIkKJ4lKMPUL+lRPEYrdOnB2dITtAS9Ciu+Y6Qq28NUoXKftrn+0cTJObnCLoVtwKGqC5JLh1H/gFEitipmKBLMXCItNQwbgcFENLo50NCkFDc97mNnUFuyXBDbAYHEMawZ5/rEsQVeyFfE/gaJSXVkBWKcJrTk0WX4eKJ9ckMyzBS6n+PWgPdX7/TnJ6sEoKrtiDWkgYp/jZwv7WwW9Ci6lxBRNGJoKsl9WXgN+PaDpsg3pxiipiguyX1HWiwims5RsF0eql/1XWn4l/vH1xiHaiftQYhyLhEIfV+HnxNMdxmyDO24gCjyLpE0bz6zekdO9ms3nGO+bWkNKruPjcxQgsYQcNpTu/aN+LiXfvQWki1dw01oIx6xJgbLPrN6Y+O4Y/2oaCWlMJg6uJWpK89jRWM504ufW4fivZlEXd1YkRIDRZ2c7qOPVtYg4W1pF6omz9iiTqlfpr/HTP83TwU7Y5GgyWeUG9FnpPTnL7u922rr9ERlpbUTSPhW3E/xpzs5vQVZvgKHWFqSd3Uk93ZiDMl8L2ja+9iXZtZLiLfhZBk12msL91uTv/AOu8/4AHWltRFPcFtRv8toxBQc4o/Pq3CA8wtqZvkDOOkGQQsYyfEPs1JnLpjUX2TlGDsKaHm9DpheD1aS+oaLinD2CFEzekdYpXeidiSEiQVxDjVywY0p1eIZ/xXUVtScrhkDOOvKkjqT8Lwz5iFx4T6WosbjlWVhs0pXixAuYjekuIshc83Om/iVS+bHrmduNjjGq2exF5/7Opl0v2JWKU/dblG87xCF8AM16oCPD7FDE8fc46WQK75jSvPALrEKuULIe3tKzeUF9ERDV9gnfcLXkPxyzR2S9rnJWb4kns04c1pzOcAnO5TZyfqKW8IE8imfOXe5KGda04f8g8mvOjzTwkE8Zll+Iw/hN6fQXDC19BYdH82m+/VnwUY0n6twwNfj+VgxVDIWA2x+zXc1RDR/RXmmsVfRYRQdEUUkWgAL2GuORVQKtLCUw13vTeBzSlvS2ojuObz13sT0Jxyt6QWYncVBXQ0Jt3ni8/FhFBwMqX/eDAG3Ren3C2pTV2koaBiAdj5RdRIYg0FdKUWXSHVHiH0BQbnDkYyNET23jw7f4khtOR/A4b1nHoIfQe1P6oiKv2YX6PRaDQajVz+BwAXZkEbOTj/AAAAAElFTkSuQmCC";
                      }
                      console.log("====================================");
                      console.log(userName, avatarurl, e);
                      console.log("====================================");
                      return (
                        <div
                          style={{
                            display: "flex",
                            flexDirection:
                              e.sendertype === "admin" ? "row" : "row-reverse"
                          }}
                          key={e._id}
                        >
                          <img
                            src={avatarurl}
                            style={{
                              height: "3rem",
                              width: "2rem",
                              margin: "0 5px"
                            }}
                          />
                          <div
                            style={{
                              textAlign:
                                e.sendertype === "admin" ? "right" : "left"
                            }}
                          >
                            <div>{userName}</div>
                            <div
                              style={{ direction: "ltr", fontSize: "0.5rem" }}
                            >
                              {moment(e.date).format("DD-MM-YYYY hh:mm A")}
                            </div>
                            <div
                              style={{
                                border: "1px solid #eee",
                                background: "#eee",
                                borderRadius: "5px",
                                // margin: "0px 10px",
                                padding: "5px",
                                left: "3rem",
                                marginTop: "15px"
                              }}
                            >
                              {e.texttype === "text" ? (
                                e.text
                              ) : (
                                <img
                                  src={e.text}
                                  style={{
                                    height: "13rem",
                                    width: "15rem",
                                    margin: "0 5px"
                                  }}
                                />
                              )}
                            </div>
                          </div>
                        </div>
                      );
                    })}
                  </Col>
                </Row>
                <div
                  style={{
                    color: "white",
                    margin: "0.8rem 0.15rem",
                    textAlign: "center",
                    fontSize: "1.25rem",
                    width: "98%",
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    position: "absolute",
                    bottom: 0,
                    maxWidth: "100%"
                  }}
                >
                  <Input
                    value={this.state.contractOpen.InputText}
                    type="textarea"
                    style={{ width: "90%" }}
                    onChange={e => {
                      this.setState({
                        contractOpen: {
                          ...this.state.contractOpen,
                          InputText: e.target.value
                        }
                      });
                    }}
                  />
                  <Button
                    size="sm"
                    color="green"
                    style={{}}
                    onClick={e => {
                      Axios.post(
                        base_url("users/contract"),
                        {
                          room: this.state.rowId._id,
                          text: this.state.contractOpen.InputText,
                          texttype: "text"
                        },
                        {
                          headers: {
                            Authorization: this.props.token
                          }
                        }
                      )
                        .then(result => {
                          console.log(
                            "=======contract============================="
                          );
                          console.log(result);
                          console.log("====================================");
                          this.FetchContractsDetails(this.state.rowId);
                          this.setState({
                            contractOpen: {
                              ...this.state.contractOpen.InputText
                            }
                          });
                          // AlertMessage("success", "", "تم الاضافة بنجاح بانتظار مراجعة الادمن");
                        })
                        .catch(err => {
                          console.log(
                            "==send Contract Error=================================="
                          );
                          console.log(err.response.data.error);
                          console.log("====================================");
                          // AlertMessage("error", "خطأ", err.response.data.error);
                        });
                      // console.log("====================================");
                      // console.log(this.state.Chat.Text);
                      // console.log("====================================");
                      // this.socket.emit("send", {
                      //   token: this.props.token,
                      //   text: this.state.Chat.Text,
                      //   texttype: "text",
                      //   isProblem: false,
                      //   room: this.state.chatIsOpen.order._id
                      // });
                    }}
                  >
                    إرسال
                  </Button>
                </div>{" "}
              </div>
            </Col>
          )}
        </Row>
      </Container>
    );
  }
}

const mapState = state => {
  return {
    ...state.users,
    ...state.count,
    token: state.auth.admin.token
  };
};

const mapDispatch = dispatch => {
  return {
    deleteUserNow: id => DeleteUser(id, dispatch),
    getAllUersNow: page => GetAllUsers(dispatch, page)
  };
};
export default connect(
  mapState,
  mapDispatch
)(Clients);
