import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Table,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  CustomInput,
  FormGroup,
  Label,
  Input
} from "reactstrap";
import { MdPerson } from "react-icons/md";
import { withRouter } from "react-router-dom";
import DataTable from "react-data-table-component";
import { FiMoreVertical } from "react-icons/fi";
import { height } from "window-size";

import { connect } from "react-redux";
import {
  FetchAllDrivers,
  DeleteDriver,
  DisableDriver
} from "../../redux/actions";
import moment from "moment";
import Localization from "../../localization";

const rowTheme = {
  title: {
    fontSize: "22px",
    fontColor: "#FFFFFF",
    backgroundColor: "#a96500"
  },
  rows: {
    // spaced allows the following properties
    // spacing: "spaced",
    // spacingMargin: "3px",
    // borderColor: "rgba(0,0,0,.12)",
    // border: "1px solid",
    // backgroundColor: "white",
    // height: "52px"
    spacing: "default",
    fontSize: "13px",
    fontColor: "rgba(0,0,0,.87)",
    backgroundColor: "transparent",
    borderWidth: "1px",
    // "borderColor": "black",
    stripedColor: "rgba(0,0,0,.03)",
    hoverFontColor: "rgba(0,0,0,.87)",
    hoverBackgroundColor: "#eee"
  },
  cells: {
    cellPadding: "48px",
    // border: "1px",
    borderWidth: "1px"
  }
};
class Drivers extends Component {
  componentDidMount() {
    this.props.getDriversNow();
  }
  table_render = () => (
    <Table bordered style={{ backgroundColor: "white", direction: "rtl" }}>
      <thead>
        <tr>
          <th style={{ textAlign: "center" }}>#</th>
          <th style={{ textAlign: "center" }}>السائق</th>
          <th style={{ textAlign: "center" }}>العميل</th>
          <th style={{ textAlign: "center" }}>السعر</th>
          <th style={{ textAlign: "center" }}>التاريخ </th>
          <th style={{ textAlign: "center" }}>تفاصيل </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th style={{ textAlign: "center" }} scope="row">
            1
          </th>
          <td style={{ textAlign: "center" }}>Mark</td>
          <td style={{ textAlign: "center" }}>Otto</td>
          <td style={{ textAlign: "center" }}>@mdo</td>
          <td style={{ textAlign: "center" }}>{Date.now()}</td>
          <td style={{ textAlign: "center" }}>
            <Button color="info" size="sm">
              التفاصيل
            </Button>{" "}
          </td>
        </tr>
        <tr>
          <th style={{ textAlign: "center" }} scope="row">
            1
          </th>
          <td style={{ textAlign: "center" }}>Mark</td>
          <td style={{ textAlign: "center" }}>Otto</td>
          <td style={{ textAlign: "center" }}>@mdo</td>
          <td style={{ textAlign: "center" }}>{Date.now()}</td>
          <td style={{ textAlign: "center" }}>
            <Button color="info" size="sm">
              التفاصيل
            </Button>{" "}
          </td>
        </tr>

        <tr>
          <th style={{ textAlign: "center" }} scope="row">
            1
          </th>
          <td style={{ textAlign: "center" }}>Mark</td>
          <td style={{ textAlign: "center" }}>Otto</td>
          <td style={{ textAlign: "center" }}>@mdo</td>
          <td style={{ textAlign: "center" }}>{Date.now()}</td>
          <td style={{ textAlign: "center" }}>
            <Button color="info" size="sm">
              التفاصيل
            </Button>{" "}
            <Button color="info" size="sm">
              حذف الرحلة
            </Button>{" "}
          </td>
        </tr>
      </tbody>
    </Table>
  );
  customDropDown = props => (
    <Dropdown
      direction="down"
      isOpen={this.state.dropdownOpen && this.state.rowId === props.row.email}
      toggle={() => this.toggle(props.row.email)}
    >
      <DropdownToggle tag="a" className="nav-link">
        <FiMoreVertical />
      </DropdownToggle>
      <DropdownMenu>
        <DropdownItem style={{ textAlign: "center" }}>التفاصيل</DropdownItem>
        <DropdownItem style={{ textAlign: "center" }}>
          تعطيل الحساب
        </DropdownItem>
      </DropdownMenu>
    </Dropdown>
  );
  toggle = email => {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen,
      rowId: email
    }));
  };
  state = {
    dropdownOpen: false,
    rowId: null
  };

  columns = [
    {
      name: Localization.name,
      selector: "name",
      sortable: true,
      center: true
    },
    {
      name: Localization.email,
      selector: "email",
      sortable: true,
      center: true
    },
    {
      name: Localization.phone,
      selector: "phone",
      sortable: true,
      center: true
    },
    {
      name: Localization.identityNumber,
      selector: "identity",
      sortable: true
    },
    {
      name: Localization.vechileNumber,
      selector: "vehicalnum",
      center: true
    },
    {
      name: Localization.city,
      selector: "cityName",
      center: true
    },
    {
      name: Localization.welcomeDate,
      cell: row => <div>{moment(row.date).format("YYYY-MM-DD")}</div>,
      sortable: true,
      center: true
    },
    {
      name: Localization.rating,
      cell: row => (
        <div>
          {[1, 2, 3, 4, 5].map((t, i) => (
            <i
              className="fa fa-star"
              aria-hidden="true"
              style={{
                padding: "1px",
                color: row.driverRating > i ? "green" : "gray"
              }}
            />
          ))}
        </div>
      ),
      sortable: true,
      center: true
    },
    {
      name: Localization.active,
      selector: "verfied",
      sortable: true,
      center: true,
      width: "30px",
      cell: row => (
        <FormGroup check>
          <Label check>
            <Input
              type="checkbox"
              style={{ marginRight: "1px" }}
              defaultChecked={row.verfied}
              onChange={t => {
                let v = t.target.checked ? 1 + "" : 0 + "";
                this.props.VerifiyDriverNow(row._id, v);
              }}
            />
            .
          </Label>
        </FormGroup>
      )
    },

    {
      name: Localization.delete,
      cell: row => (
        <span onClick={() => this.props.deleteDriverNow(row._id)}>
          <i className="fa fa-trash-o" aria-hidden="true" />
        </span>
      ),
      ignoreRowClick: true,
      allowOverflow: true,
      button: true
    }
  ];

  render() {
    return (
      <Container style={{ marginTop: 50, direction: "rtl" }}>
        <Row
          style={{
            marginTop: 20,
            alignItems: "center"
          }}
        >
          <Col className=".col-sm-12 .col-md-12 .offset-md-12" style={{}}>
            {/* {this.table_render()} */}
            <DataTable
              style={{
                bordered: "1px",
                border: "1px solid black",
                textAlign: "center"
              }}
              title={Localization.drivers}
              style={{ backgroundColor: "white" }}
              columns={this.columns}
              data={this.props.drivers.filter(d => d.verfied === true)}
              striped
              highlightOnHover
              fixedHeader
              pagination
              customTheme={rowTheme}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapState = state => {
  return {
    ...state.drivers
  };
};

const mapDispatch = dispatch => {
  return {
    VerifiyDriverNow: (driverId, verified) =>
      DisableDriver(driverId, verified, dispatch),
    deleteDriverNow: id => DeleteDriver(id, dispatch),
    getDriversNow: () => FetchAllDrivers(dispatch)
  };
};
export default connect(
  mapState,
  mapDispatch
)(Drivers);
