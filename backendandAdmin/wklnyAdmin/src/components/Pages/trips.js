import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Table,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import { MdPerson } from "react-icons/md";
import { withRouter } from "react-router-dom";
import DataTable from "react-data-table-component";
import { FiMoreVertical } from "react-icons/fi";
import { connect } from "react-redux";
import { GetAllUsers, DeleteUser } from "../../redux/actions";

import moment from "moment";
import Localization from "../../localization";
import { GetAllTrips, deleteTrip } from "../../redux/actions/trips";

const rowTheme = {
  title: {
    fontSize: "22px",
    fontColor: "#FFFFFF",
    backgroundColor: "#a96500"
  },
  rows: {
    // spaced allows the following properties
    // spacing: "spaced",
    // spacingMargin: "3px",
    // borderColor: "rgba(0,0,0,.12)",
    // border: "1px solid",
    // backgroundColor: "white",
    // height: "52px"
    spacing: "default",
    fontSize: "13px",
    fontColor: "rgba(0,0,0,.87)",
    backgroundColor: "transparent",
    borderWidth: "1px",
    // "borderColor": "black",
    stripedColor: "rgba(0,0,0,.03)",
    hoverFontColor: "rgba(0,0,0,.87)",
    hoverBackgroundColor: "#eee"
  },
  cells: {
    cellPadding: "48px",
    // border: "1px",
    borderWidth: "1px"
  }
};
class Clients extends Component {
  componentWillMount() {
    this.props.getAllTrips();
    moment.locale("ar");
  }

  state = {
    dropdownOpen: false,
    rowId: null
  };

  columns = [
    {
      name: Localization.user,
      selector: "user",
      sortable: true,
      center: true,
      cell: row => (
        <div> {row.user ? row.user.name : Localization.userDeleted}</div>
      )
    },
    {
      name: Localization.driver,
      selector: "drive",
      sortable: true,
      center: true,
      cell: row => (
        <div>{row.accepted_driver ? row.accepted_driver.name : "--"}</div>
      )
    },
    {
      name: Localization.prices,
      selector: "price",
      sortable: true,
      center: true,
      cell: row => <div>{row.price}</div>
    },
    {
      name: Localization.details,
      selector: "details",
      sortable: true,
      center: true,
      cell: row => <div>{row.details}</div>
    },
    {
      name: Localization.createdDate,
      cell: row => <div>{moment(row.date).format("YYYY-MM-DD")}</div>,
      sortable: true,
      center: true
    },
    {
      name: Localization.status,
      selector: "status",
      sortable: true,
      center: true,
      cell: row => (
        <div>
          {row.status === "0"
            ? Localization.tripStill
            : row.status === "1"
            ? Localization.tripFinish
            : Localization.tripCanceled}
        </div>
      )
    },
    {
      name: Localization.delete,
      cell: row => (
        <div onClick={() => this.props.deleteTrip(row._id)}>
          <i className="fa fa-trash-o" aria-hidden="true" />
        </div>
      ),
      ignoreRowClick: true,
      allowOverflow: true,
      button: true
    }
  ];

  render() {
    return (
      <Container style={{ marginTop: 50, direction: "rtl" }}>
        <Row
          style={{
            marginTop: 20,
            alignItems: "center"
          }}
        >
          <Col
            className=".col-sm-12 .col-md-12 .offset-md-12"
            style={{ flex: 1, height: "20%" }}
          >
            {/* {this.table_render()} */}
            <DataTable
              style={{
                bordered: "1px",
                border: "1px solid black",
                textAlign: "center"
              }}
              title={Localization.trips}
              style={{ backgroundColor: "white" }}
              columns={this.columns}
              data={this.props.trips}
              striped
              highlightOnHover
              fixedHeader
              pagination
              customTheme={rowTheme}
            />
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapState = state => {
  return {
    ...state.trips
  };
};

const mapDispatch = dispatch => {
  return {
    getAllTrips: () => GetAllTrips(dispatch),
    deleteTrip: id => deleteTrip(id, dispatch)
  };
};
export default connect(
  mapState,
  mapDispatch
)(Clients);
