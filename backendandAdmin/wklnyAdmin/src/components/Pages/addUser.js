import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Table,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Alert
} from "reactstrap";
import { MdPerson } from "react-icons/md";
import { withRouter } from "react-router-dom";
import DataTable from "react-data-table-component";
import { FiMoreVertical } from "react-icons/fi";
import { connect } from "react-redux";
import { GetAllUsers, AddUser } from "../../redux/actions";

import moment from "moment";
import Localization from "../../localization";
import Spinner from "../../components/UI/Spinner/Spinner";
import { Add_User_Reset } from "../../redux/types";
import Axios from "axios";
import { base_url } from "../../redux/config";

class addUser extends Component {
  componentDidMount() {
    this.props.Reset();
    this.getCities();
  }
  componentWillReceiveProps(nxt) {
    this.props = nxt;
    if (nxt.addUserDone) {
      this.setState({
        visible: true,
        email: "",
        password: "",
        password2: "",
        name: "",
        phone: ""
      });
    }
  }
  state = {
    email: "",
    password: "",
    password2: "",
    name: "",
    phone: "",
    visible: false,
    cities: null,
    city: ""
  };
  submit = () => {
    this.props.AddUser(this.state);
  };
  clearForm = () => {
    this.setState({
      city: "",
      email: "",
      name: "",
      password: "",
      password2: "",
      phone: "",
      visible: false
    });
  };
  getCities = () => {
    Axios.get(base_url("admin/cities")).then(result => {
      this.setState({ cities: result.data.cities });
    });
  };

  render() {
    let errors = {};
    if (this.props.addUserErrors) {
      errors = this.props.addUserErrors;
    }
    return (
      <Container
        style={{
          marginTop: 50,
          direction: Localization.getLanguage() === "ar" ? "rtl" : "ltr"
        }}
      >
        {this.state.visible && (
          <Alert
            color="success"
            style={{ textAlign: "center" }}
            toggle={this.clearForm}
          >
            {Localization.successAdd}
          </Alert>
        )}
        <Row
          style={{
            marginTop: 20,
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <Col lg={{ size: 12 }} style={{ alignSelf: "center" }}>
            <div
              style={{
                backgroundColor: "#478a22",
                color: "white",
                padding: "0.8rem 0.15rem",
                textAlign: "center",
                fontSize: "1.25rem"
              }}
            >
              {Localization.adduser}
            </div>
            {this.props.addUsersLoading ? (
              <Spinner />
            ) : (
              <Form
                style={{
                  backgroundColor: "white",
                  textAlign: "center",
                  padding: " 2rem",
                  alignSelf: "center",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <FormGroup row>
                  <Label for="name" sm={2}>
                    {Localization.name}
                  </Label>
                  <Col>
                    <Input
                      type="text"
                      name="name"
                      id="name"
                      placeholder={Localization.name}
                      style={{
                        alignSelf: "center",
                        borderColor: errors.name ? "red" : null
                      }}
                      onChange={e => {
                        this.setState({ name: e.target.value });
                      }}
                      value={this.state.name}
                    />
                    {errors.name && (
                      <tex style={{ color: "red", fontSize: "0.5rem" }}>
                        {errors.name}
                      </tex>
                    )}
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="emailid" sm={2} style={{ fontSize: "0.8rem" }}>
                    {Localization.email}
                  </Label>
                  <Col style={{ alignSelf: "center" }}>
                    <Input
                      type="email"
                      name="email"
                      id="emailid"
                      placeholder={Localization.email}
                      value={this.state.email}
                      onChange={e => {
                        this.setState({ email: e.target.value });
                      }}
                      style={{
                        alignSelf: "center",
                        borderColor: errors.email ? "red" : null
                      }}
                    />
                    {errors.email && (
                      <tex style={{ color: "red", fontSize: "0.5rem" }}>
                        {errors.email}
                      </tex>
                    )}
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="phone" sm={2}>
                    {Localization.phone}
                  </Label>
                  <Col>
                    <Input
                      type="tel"
                      name="phone"
                      id="phone"
                      placeholder={Localization.phone}
                      onChange={e => {
                        this.setState({ phone: e.target.value });
                      }}
                      style={{
                        alignSelf: "center",
                        borderColor: errors.phone ? "red" : null
                      }}
                      value={this.state.phone}
                    />
                    {errors.phone && (
                      <tex style={{ color: "red", fontSize: "0.5rem" }}>
                        {errors.phone}
                      </tex>
                    )}
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="city" sm={2}>
                    {Localization.city}
                  </Label>
                  <Col sm={10} style={{ alignSelf: "center" }}>
                    <Input
                      type="select"
                      name="select"
                      id="city"
                      placeholder={Localization.city}
                      onChange={e => {
                        this.setState({ city: e.target.value });
                      }}
                      style={{
                        alignSelf: "center",
                        borderColor: errors.city ? "red" : null
                      }}
                    >
                      <option disabled selected>
                        اختر المدينة
                      </option>
                      {this.state.cities &&
                        this.state.cities.map(element => (
                          <option value={element._id}>{element.name}</option>
                        ))}
                    </Input>
                    {errors.password && (
                      <tex style={{ color: "red", fontSize: "0.5rem" }}>
                        {errors.password}
                      </tex>
                    )}
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="passwordid" sm={2}>
                    {Localization.password}
                  </Label>
                  <Col sm={10} style={{ alignSelf: "center" }}>
                    <Input
                      type="password"
                      name="password"
                      id="passwordid"
                      placeholder={Localization.password}
                      onChange={e => {
                        this.setState({ password: e.target.value });
                      }}
                      style={{
                        alignSelf: "center",
                        borderColor: errors.password ? "red" : null
                      }}
                      value={this.state.password}
                    />
                    {errors.password && (
                      <tex style={{ color: "red", fontSize: "0.5rem" }}>
                        {errors.password}
                      </tex>
                    )}
                  </Col>
                </FormGroup>

                <FormGroup row>
                  <Label for="password2id" sm={2}>
                    {Localization.password}
                  </Label>
                  <Col sm={10} style={{ alignSelf: "center" }}>
                    <Input
                      type="password"
                      name="password2"
                      id="passwor2did"
                      placeholder={Localization.password}
                      onChange={e => {
                        this.setState({ password2: e.target.value });
                      }}
                      style={{
                        alignSelf: "center",
                        borderColor: errors.password2 ? "red" : null
                      }}
                      value={this.state.password2}
                    />
                    {errors.password2 && (
                      <tex style={{ color: "red", fontSize: "0.5rem" }}>
                        {errors.password2}
                      </tex>
                    )}
                  </Col>
                </FormGroup>

                <FormGroup check row>
                  <Col
                    style={{ alignItems: "center", justifyContent: "center" }}
                  >
                    <Button
                      color="green"
                      size="lg"
                      type="button"
                      onClick={this.submit}
                    >
                      {Localization.adduser}
                    </Button>
                  </Col>
                </FormGroup>
              </Form>
            )}
          </Col>{" "}
        </Row>
      </Container>
    );
  }
}

const mapState = state => {
  return {
    ...state.users
  };
};

const mapDispatch = dispatch => {
  return {
    AddUser: payload => AddUser(payload, dispatch),
    Reset: () => dispatch({ type: Add_User_Reset })
  };
};
export default connect(
  mapState,
  mapDispatch
)(addUser);
