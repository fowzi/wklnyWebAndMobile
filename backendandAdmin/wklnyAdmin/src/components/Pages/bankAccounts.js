import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Table,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Form,
  FormGroup,
  Label,
  Input,
  FormText,
  Alert
} from "reactstrap";
import { MdPerson } from "react-icons/md";
import { withRouter } from "react-router-dom";
import DataTable from "react-data-table-component";
import { FiMoreVertical } from "react-icons/fi";
import { connect } from "react-redux";
import { GetAllUsers, AddUser } from "../../redux/actions";

import moment from "moment";
import Localization from "../../localization";
import Spinner from "../../components/UI/Spinner/Spinner";
import { Add_User_Reset, Update_Bank_Reset } from "../../redux/types";
import { fetchBanks, updateBanks } from "../../redux/actions/banks";
class bankAccounts extends Component {
  componentDidMount() {
    // this.props.Reset();
    this.props.fetchBanks();
  }
  componentWillReceiveProps(nxt) {
    if (nxt.banks) {
      this.setState({
        name0: this.props.banks[0].name,
        name1: this.props.banks[1].name,
        name2: this.props.banks[2].name,
        iban0: this.props.banks[0].iban,
        iban1: this.props.banks[1].iban,
        iban2: this.props.banks[2].iban,
        acountNumber0: this.props.banks[0].acountNumber,
        acountNumber1: this.props.banks[1].acountNumber,
        acountNumber2: this.props.banks[2].acountNumber
      });
    }
    this.props = nxt;
    if (nxt.addUserDone) {
      this.setState({
        visible: false,
        email: "",
        password: "",
        password2: "",
        name: "",
        phone: ""
      });
    }
  }
  state = {
    name1: "",
    name2: "",
    name3: "",
    iban1: "",
    iban1: "",
    iban1: "",
    acountNumber: "",
    acountNumber: "",
    acountNumber: "",
    visible: false
  };
  submit = payload => {
    this.props.updateBank(payload);
  };

  renderItem = (item, index) => {
    let bank = {
      name: item.name,
      iban: item.iban,
      acountNum: item.acountNumber
    };

    let errors = {};
    {
      if (this.props.bankUpateLoading === index) return <Spinner />;
    }

    return (
      <Form
        style={{
          backgroundColor: "white",
          textAlign: "center",
          padding: " 2rem",
          alignSelf: "center",
          justifyContent: "center",
          alignItems: "center",
          marginBottom: "1rem"
        }}
      >
        {this.props.bankUpateSuccess === index && (
          <Alert
            color="success"
            style={{ textAlign: "center" }}
            toggle={this.clearForm}
          >
            {Localization.updateSuccess}
          </Alert>
        )}
        {this.props.bankUpdateFail === index && (
          <Alert
            color="danger"
            style={{ textAlign: "center" }}
            toggle={this.clearForm}
          >
            {Localization.updateFail}
          </Alert>
        )}
        <FormGroup row>
          <Label for={"bn" + index} sm={2}>
            {Localization.BankName}
          </Label>
          <Col>
            <Input
              type="text"
              name="bankName"
              id={"bn" + index}
              placeholder={Localization.BankName}
              style={{
                alignSelf: "center",
                borderColor: errors.normalPrice ? "red" : null
              }}
              onChange={e => {
                this.setState({
                  [`name${index}`]: e.target.value
                });
              }}
              value={this.state[`name${index}`]}
            />
            {errors.normalPrice && (
              <tex style={{ color: "red", fontSize: "0.5rem" }}>
                {errors.normalPrice}
              </tex>
            )}
          </Col>
        </FormGroup>

        <FormGroup row>
          <Label for={"IBAN" + index} sm={2}>
            {Localization.normalTripPrice}
          </Label>
          <Col>
            <Input
              type="number"
              name="IBAN1"
              id={"IBAN" + index}
              min={1}
              placeholder={Localization.IBAN_Account}
              style={{
                alignSelf: "center",
                borderColor: errors.normalPrice ? "red" : null
              }}
              onChange={e => {
                this.setState({
                  [`iban${index}`]: e.target.value
                });
              }}
              value={this.state[`iban${index}`]}
            />
            {errors.normalPrice && (
              <tex style={{ color: "red", fontSize: "0.5rem" }}>
                {errors.normalPrice}
              </tex>
            )}
          </Col>
        </FormGroup>

        <FormGroup row>
          <Label for={"accountNum" + index} sm={2}>
            {Localization.AccountNumber}
          </Label>
          <Col>
            <Input
              type="number"
              name="accountNum"
              id={"accountNum" + index}
              min={1}
              placeholder={Localization.AccountNumber}
              style={{
                alignSelf: "center",
                borderColor: errors.normalPrice ? "red" : null
              }}
              onChange={e => {
                this.setState({
                  [`acountNumber${index}`]: e.target.value
                });
              }}
              value={this.state[`acountNumber${index}`]}
            />
            {errors.normalPrice && (
              <tex style={{ color: "red", fontSize: "0.5rem" }}>
                {errors.normalPrice}
              </tex>
            )}
          </Col>
        </FormGroup>
        <div
          style={{
            height: "0.1rem",
            backgroundColor: "#8a8a8a61",
            margin: "0.5rem"
          }}
        />

        <FormGroup row>
          <Col
            style={{
              alignItems: "center",
              justifyContent: "center"
            }}
          >
            <Button
              block
              color="green"
              size="sm"
              style={{
                alignSelf: "center",
                backgroundColor: "#678c8c !important",
                textAlign: "center"
              }}
              type="button"
              onClick={() => {
                let payload = {
                  index: index,
                  id: item._id,
                  name: this.state[`name${index}`],
                  iban: this.state[`iban${index}`],
                  acountNumber: this.state[`acountNumber${index}`]
                };
                this.props.updateBank(payload);
              }}
            >
              {Localization.update}
            </Button>
          </Col>
        </FormGroup>
      </Form>
    );
  };
  clearForm = () => {
    this.props.Reset();
  };

  render() {
    return (
      <Container
        style={{
          marginTop: 50,
          direction: Localization.getLanguage() === "ar" ? "rtl" : "ltr"
        }}
      >
        {this.state.visible && (
          <Alert
            color="success"
            style={{ textAlign: "center" }}
            toggle={this.clearForm}
          >
            {Localization.successAdd}
          </Alert>
        )}
        <Row
          style={{
            marginTop: 20,
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <Col
            sm={10}
            md={{ size: 8 }}
            lg={{ size: 8 }}
            style={{ alignSelf: "center" }}
          >
            <div
              style={{
                backgroundColor: "#a96500",
                color: "white",
                padding: "0.15rem",
                textAlign: "center",
                fontSize: "1.25rem"
              }}
            >
              {Localization.bankAccounts}
            </div>
            {this.props.addUsersLoading ? (
              <Spinner />
            ) : (
              <div>
                {this.props.banks &&
                  this.props.banks.map((item, index) =>
                    this.renderItem(item, index)
                  )}
              </div>
            )}
          </Col>{" "}
        </Row>
      </Container>
    );
  }
}

const mapState = state => {
  return {
    ...state.users,
    ...state.banks
  };
};

const mapDispatch = dispatch => {
  return {
    updateBank: payload => updateBanks(payload, dispatch),
    fetchBanks: () => fetchBanks(dispatch),
    Reset: () => dispatch({ type: Update_Bank_Reset })
  };
};
export default connect(
  mapState,
  mapDispatch
)(bankAccounts);
