import React, { Component } from "react";
import { Document, Page, pdfjs } from "react-pdf";
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${
  pdfjs.version
}/pdf.worker.js`;

class MyApp extends Component {
  state = {
    numPages: null,
    pageNumber: 1
  };

  onDocumentLoadSuccess = ({ numPages }) => {
    this.setState({ numPages });
  };

  render() {
    const { pageNumber, numPages } = this.state;

    return ( 
      <div
        style={{
        
           justifyContent: "center",
           alignItems: "center",
           display: "flex",
           padding: "20px",
         alignSelf:'center',
        }}
      >
        <Document file="1.pdf"
         onLoadSuccess={this.onDocumentLoadSuccess}
         
         >
          <Page pageNumber={1} />
          <Page pageNumber={2} />
        </Document>
       
      </div>
    );
  }
}

export default MyApp;
