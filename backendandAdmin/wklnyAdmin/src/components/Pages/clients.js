import React, { Component } from "react";
import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardText,
  CardBody,
  CardTitle,
  CardSubtitle,
  Button,
  Table,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import { MdPerson } from "react-icons/md";
import { withRouter } from "react-router-dom";
import DataTable from "react-data-table-component";
import { FiMoreVertical } from "react-icons/fi";
import { connect } from "react-redux";
import { GetAllUsers, DeleteUser } from "../../redux/actions";

import moment from "moment";
import Localization from "../../localization";

const rowTheme = {
  title: {
    fontSize: "22px",
    fontColor: "#FFFFFF",
    backgroundColor: "green"
  },
  rows: {
    // spaced allows the following properties
    // spacing: "spaced",
    // spacingMargin: "3px",
    // borderColor: "rgba(0,0,0,.12)",
    // border: "1px solid",
    // backgroundColor: "white",
    // height: "52px"
    spacing: "default",
    fontSize: "0.7rem",
    fontColor: "rgba(0,0,0,.87)",
    backgroundColor: "transparent",
    borderWidth: "1px",
    // "borderColor": "black",
    stripedColor: "rgba(0,0,0,.03)",
    hoverFontColor: "rgba(0,0,0,.87)",
    hoverBackgroundColor: "#eee"
  },
  cells: {
    cellPadding: "48px",
    // border: "1px",
    borderWidth: "1px"
  }
};
class Clients extends Component {
  componentWillMount() {
    this.props.getAllUersNow(0);
    moment.locale("ar");
  }

  state = {
    dropdownOpen: false,
    rowId: null,
    page: 0
  };

  columns = [
    {
      name: Localization.name,
      selector: "name",
      sortable: true,
      center: true,
      grow: window.innerWidth < 500 ? true : false
    },
    {
      name: Localization.email,
      selector: "email",
      sortable: true,
      center: true,
      grow: window.innerWidth < 500 ? true : false
    },
    {
      name: Localization.phone,
      selector: "phone",
      sortable: true,
      center: true,
      grow: window.innerWidth < 500 ? true : false
    },
    {
      name: Localization.welcomeDate,
      cell: row => <div>{moment(row.date).format("YYYY-MM-DD")}</div>,
      sortable: true,
      center: true,
      selector: "date"
    },
    {
      name: "الرصيد",
      selector: "balance",
      cell: row => <div>{row.balance}</div>,
      sortable: true,
      center: true
    },
    {
      name: Localization.delete,
      cell: row => (
        <span onClick={() => this.props.deleteUserNow(row._id)}>
          <i className="fa fa-trash-o" aria-hidden="true" />
        </span>
      ),
      ignoreRowClick: true,
      allowOverflow: true,
      button: true
    }
  ];

  render() {
    console.log("====================================");
    console.log(this.props.count.users, this.props.users.length);
    console.log("====================================");
    return (
      <Container style={{ marginTop: 50, direction: "rtl" }}>
        <Row
          style={{
            marginTop: 20,
            alignItems: "center"
          }}
        >
          <Col
            className=".col-sm-12 .col-md-12 .offset-md-12"
            style={{ flex: 1, height: "20%" }}
          >
            <div
              style={{
                backgroundColor: "#478a22",
                color: "white",
                padding: "0.8rem 0.15rem",
                textAlign: "center",
                fontSize: "1.25rem"
              }}
            >
              المستخدمين{" "}
            </div>{" "}
            <DataTable
              // pageSizeOptions={[20, 30, 50, 100, 200, 500]}
              style={{
                bordered: "1px",
                border: "1px solid black",
                textAlign: "center"
              }}
              title={Localization.users}
              style={{ backgroundColor: "white" }}
              columns={this.columns}
              data={this.props.users}
              striped
              highlightOnHover
              fixedHeader
              noHeader
              pagination={false}
              // paginationTotalRows={this.props.users.lenght}
              // paginationPerPage={10}
              // paginationRowsPerPageOptions={[10]}
              customTheme={rowTheme}
            />
            {Number(this.props.count.users) >
              Number(this.props.users.length) && (
              <Button
                color="#eee"
                block
                size="sm"
                type="button"
                style={{ marginTop: "1rem" }}
                onClick={() => {
                  this.setState({ page: this.state.page + 1 });
                  this.props.getAllUersNow(this.state.page);
                }}
              >
                المزيد
              </Button>
            )}
          </Col>
        </Row>
      </Container>
    );
  }
}

const mapState = state => {
  return {
    ...state.users,
    ...state.count
  };
};

const mapDispatch = dispatch => {
  return {
    deleteUserNow: id => DeleteUser(id, dispatch),
    getAllUersNow: page => GetAllUsers(dispatch, page)
  };
};
export default connect(
  mapState,
  mapDispatch
)(Clients);
