import React from 'react';
import './pageContentWrapper.css';

const pageContentWrapper = (props) => {

    return (
        <React.Fragment>
            <div id={"main-page-content-wrapper"}>
                {props.children}
            </div>
        </React.Fragment>

    )
}

export default pageContentWrapper;