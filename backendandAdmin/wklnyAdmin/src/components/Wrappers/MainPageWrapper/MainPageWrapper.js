import React from "react";
import "./mainPageWrapper.css";
import Localization from "../../../localization";

const mainPageWrapper = props => {
  const rtl = Localization.getLanguage();
  let mainPageWrapperClass =
    rtl === "ar"
      ? props.toggleClass
        ? "rtlhide"
        : "rtl"
      : props.toggleClass
      ? "hide"
      : null;
  return (
    <div id={"main-page-wrapper"} className={mainPageWrapperClass}>
      {props.children}
    </div>
  );
};

export default mainPageWrapper;
