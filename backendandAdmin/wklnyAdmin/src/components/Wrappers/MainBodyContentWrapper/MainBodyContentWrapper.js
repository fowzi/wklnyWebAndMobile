import React from 'react';

const mainBodyContentWrapper = (props) => {

    return (
        <div id={'main-body-content-wrapper'}>
            {props.children}
        </div>
    )

}

export default mainBodyContentWrapper;