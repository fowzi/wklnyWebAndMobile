import React from 'react';
import {Link} from 'react-router-dom';
import UserPhoto from '../../../assets/images/female-placeholder-300x300.jpg';
import './customer.css';

const customer = (props) => {

    return (
        <div className="card mb-5 rounded">
            <div className="card-body p-2">
                <div className="g-card-content">
                    <div className="p-0 g-card-img-container">
                        <img className="g-customer-card-image" src={UserPhoto}
                             alt="Collins Nixon"/>
                    </div>

                    <div className="mr-auto g-customer-information">
                        <h3 className="g-green-color text-capitalize">{props.first_name} {props.second_name}</h3>
                        <h6>Email: <span className="g-user-titles">{props.email_address}</span></h6>
                        <h6>Phone Number: <span className="g-user-titles">{props.phone_number}</span></h6>
                        <h6>Loan Limit: <span className="g-user-titles">Ksh {props.loan_limit}</span></h6>
                        <h6>Branch: <span className="g-user-titles text-capitalize">{props.branch}</span></h6>
                    </div>

                    <div className="g-card-buttons">
                        <div className="pb-2">
                            <Link className="btn btn-sm btn-brown g-customer-buttons"
                                     title="Edit Customer Info"
                                     to={"/customer/" + props.user_key + "/edit"}><span><i className="fa fa-edit"></i></span></Link>
                        </div>


                        <div className="pb-2">
                            <Link className="btn btn-sm btn-brown g-customer-buttons"
                                     title="Show Customer Details"
                                     to={"/customer/" + props.user_key + "/show"}><span><i
                                className="fa fa-address-card"></i></span></Link>
                        </div>


                        <div>
                            <button className="btn btn-sm btn-brown g-customer-buttons"
                                     title="Delete Customer"
                                     onClick={props.deleteCustomer}
                                     ><span><i
                                className="fa fa-trash"></i></span></button>
                        </div>

                    </div>

                </div>

            </div>

        </div>

    )

}

export default customer;