import React from 'react';
import './addFilterCustomer.css';

const addFilterCustomer = (props) => {

    return(
        <div className="container-fluid mt-4 pr-4 pl-4">
            <div className="d-flex flex-row d-flex justify-content-between">
                <div>
                    <select className="custom-select custom-select-sm"
                            onChange={props.changed}
                            disabled={props.disableSelect}
                            name="filter_branch">
                        <option value="">Type</option>
                        <option value="branch_one">Branch 1</option>
                        <option value="branch_two">Branch 2</option>
                        <option value="branch_three">Branch 3</option>
                    </select>
                </div>
                <div>
                    <button type="button" data-toggle="modal" onClick={props.clickedShowModal}
                            className="btn btn-warning btn-sm m-0 g-modal-toggle">Add Customer
                    </button>
                </div>
            </div>
        </div>
    )
}

export default addFilterCustomer;