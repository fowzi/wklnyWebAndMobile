import React from 'react';
import './modal.css';
import Backdrop from '../Backdrop/Backdrop';

const modal = (props) => {
    return(
        <React.Fragment>
            <Backdrop show={props.show} clicked={props.hideModal}/>
                        <div className={['modal',props.show ? 'show':'hide'].join(' ')}>
                <div className="modal-dialog">
                    <div className="modal-content">
                       {props.title ? <div className="modal-header">
                             <h5 className="modal-title">{props.title}</h5>
                            { props.showCloseButton ? <button type="button" className="close" onClick={props.hideModal}>
                                <span aria-hidden="true">&times;</span>
                            </button> : ''}
                        </div> : ''}
                        {props.body ? <div className="modal-body" style={props.style}>
                            {props.body}
                        </div>: ''}
                        {props.footer ? <div className="modal-footer">{props.children}</div>: ''}
                    </div>
                </div>


            </div>
        </React.Fragment>
    )
}

export default modal;