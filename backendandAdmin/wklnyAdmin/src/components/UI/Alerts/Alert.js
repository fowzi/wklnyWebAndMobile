import React from 'react';
import './alert.css'

const alert = (props) => {

    return (
        <React.Fragment>
            <div
                className={['alert', props.alertType,'alert-dismissible', 'fade', 'show'].join(' ')}
                role="alert">
                {props.children}
                <button type="button" className="close" onClick={props.hideAlert}>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </React.Fragment>
    )
}

export default alert;