import React from 'react';
import './inputs.css';

const input = (props) => {

    let inputElement = null;
    let inputClasses = ['form-control']

    if (props.invalid && props.shouldValidate && props.touched) {
        inputClasses.push('error')
    }
    if (!props.invalid && props.shouldValidate && props.touched) {
        inputClasses.push('success')
    }

    switch (props.inputtype) {
        case('input'):
            inputElement = (
                <input className={inputClasses.join(' ')}
                       onChange={props.changed}
                       value={props.value}
                       {...props.inputConfig} />
            );
            break;
        case('select'):
            inputElement = (
                <select className={inputClasses.join(' ')}
                        onChange={props.changed}
                        {...props.inputConfig}
                        value={props.value}>
                    {props.inputConfig.options.map(option => (
                        <option key={option.value} value={option.value}>
                            {option.displayValue}
                        </option>
                    ))}
                </select>

            );
            break;
        case('textarea'):
            inputElement = (
                <textarea className={inputClasses.join(' ')}
                          onChange={props.changed}
                          value={props.value}
                          {...props.inputConfig} />
            );
            break;
        default:
            inputElement = (
                <input className={inputClasses.join(' ')}
                       value={props.value}
                       {...props.inputConfig} />
            );
    }

    return (
        <React.Fragment>
            <div className="form-group">
                <label htmlFor={props.inputConfig.id}>{props.label}</label>
                {inputElement}
                <div className="errorMessage">{props.errorMessage}</div>
            </div>
        </React.Fragment>
    )
}

export default input