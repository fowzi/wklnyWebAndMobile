import React from 'react';
import Modal from '../Modal/Modal';

const confirmation = (props) => {
    return (
        <React.Fragment>
            <Modal style={{minHeight:30}}
                show={props.show}
                title={props.title}
                showCloseButton={props.showCloseButton}
                hideModal={props.closeConfirmation}
                body={props.body}
                footer={true} >
                <button type="button" className="btn btn-brown" onClick={props.secondaryClick}>{props.secondaryButton}</button>
                <button type="button" className="btn  btn-warning" onClick={props.primaryClick}>{props.primaryButton}</button>
            </Modal>
        </React.Fragment>
    )
}

export default confirmation;