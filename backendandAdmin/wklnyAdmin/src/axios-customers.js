import axios from 'axios';

// use an axios instance to make db transactions

const instance = axios.create({
    baseURL: `${process.env.REACT_APP_BASE_URL}`
});

export default instance;