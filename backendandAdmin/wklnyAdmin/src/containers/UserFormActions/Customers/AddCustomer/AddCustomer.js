import React, {Component} from 'react';
import { connect } from 'react-redux';
import { addNewCustomerSuccess, addNewCustomerFailure} from '../../../../store/actions/index'
import '../user-actions.css';
import Input from '../../../../components/UI/Inputs/Inputs';
import ValidateFunc from '../../../Utility/FormValidations/customerValidate';
import axios from '../../../../axios-customers';

class AddCustomer extends Component {

    state = {
        customerForm: {
            first_name: {
                inputtype: 'input',
                label: 'First Name',
                inputConfig: {
                    type: 'text',
                    id: 'first_name',
                    placeholder: 'First Name'
                },
                value: '',
                valid: false,
                touched: false,
                toBeValidated: true
            },

            second_name: {
                inputtype: 'input',
                label: 'Second Name',
                inputConfig: {
                    type: 'text',
                    id: 'second_name',
                    placeholder: 'Second Name'
                },
                value: '',
                valid: false,
                touched: false,
                toBeValidated: true
            },

            email_address: {
                inputtype: 'input',
                label: 'Email',
                inputConfig: {
                    type: 'email',
                    placeholder: 'Email',
                    id: 'email_address',
                },
                value: '',
                valid: false,
                touched: false,
                toBeValidated: true
            },

            phone_number: {
                inputtype: 'input',
                label: 'Phone Number',
                inputConfig: {
                    type: 'text',
                    placeholder: 'Phone Number',
                    id: 'phone_number',
                },
                value: '',
                errorsMsg: '',
                valid: false,
                touched: false,
                toBeValidated: true
            },

            branch: {
                inputtype: 'select',
                label: 'Branch',
                inputConfig: {
                    id: 'branch',
                    options:
                        [
                            {value: "", displayValue: "Select Branch"},
                            {value: "branch_one", displayValue: "Branch 1"},
                            {value: "branch_two", displayValue: "Branch 2"},
                            {value: "branch_three", displayValue: "Branch 3"},
                        ],
                },
                value: '',
                errorsMsg: '',
                valid: false,
                touched: false,
                toBeValidated: true
            },

            loan_limit: {
                inputtype: 'input',
                label: 'Loan Limit',
                inputConfig: {
                    type: 'text',
                    placeholder: 'Loan Limit',
                    inputid: 'loan_limit',
                },
                value: '',
                errorsMsg: '',
                valid: false,
                touched: false,
                toBeValidated: true
            },

            photo: {
                inputtype: 'input',
                label: 'Customers Photo',
                inputConfig: {
                    type: 'file',
                    id: 'customer_photo',
                    accept: '.png, .jpg, .jpeg'
                },
                errorsMsg: '',
                value: '',
                valid: true,
                toBeValidated: true
            }
        },
        formIsValid: false,
    }

// react controlled form components
    inputChangeHandler = (event, inputIdentifier) => {
        // use deep cloning to be able to get the values of nesteds objects
        const updateCustomerFrom = {...this.state.customerForm}
        const updateFormElement = {...updateCustomerFrom[inputIdentifier]}

        updateFormElement.value = event.target.value;
        const validationResults = ValidateFunc(inputIdentifier, updateFormElement.value);
        updateFormElement.valid = validationResults.isValid;
        updateFormElement.errorsMsg = validationResults.errorsMsg;
        updateFormElement.touched = true;
        updateCustomerFrom[inputIdentifier] = updateFormElement

        // handle disabling and enabling submit buttton
        let formIsValid = true;
        for (let identifier in updateCustomerFrom) {
            formIsValid = updateCustomerFrom[identifier].valid && formIsValid;
        }
        this.setState({customerForm: updateCustomerFrom, formIsValid: formIsValid})
    }

    addCustomerHandler = (event) => {
        this.props.loadingTrue();
        event.preventDefault();
        if (this.state.formIsValid) {
            const customerData = {}
            for (let elementIdentifier in this.state.customerForm) {
                customerData[elementIdentifier] = this.state.customerForm[elementIdentifier].value
            }
            axios.post('/customers.json?auth=' + this.props.authToken, customerData)
                .then(response => {
                    const newCustomer = {
                        key:response.data['name'],
                        first_name: customerData['first_name'],
                        second_name: customerData['second_name'],
                        email_address: customerData['email_address'],
                        phone_number: customerData['phone_number'],
                        loan_limit: parseInt(customerData['loan_limit'], 10).toLocaleString(),
                        branch: customerData['branch'].replace('_', ' ')
                    };
                    this.props.loadingFalse();
                    this.props.closeModal();
                    // add new customer success redux action
                    this.props.addCustomerSuccessProp(newCustomer)
                })
                .catch(error => {

                    // Error handling can be done here
                    this.props.loadingFalse();
                    this.props.closeModal();
                    // add new customer failure redux action
                    this.props.addCustomerFailureProp(error)

                });
            // console.log(customerData);
        }
    }

    addCustomerKeyHandler = (event) => {
        if (event.keyCode === 13) return this.addCustomerHandler(event)
    }

    render() {

        const formElementArray = [];
        for (let key in this.state.customerForm) {
            formElementArray.push({
                id: key,
                config: this.state.customerForm[key]
            });
        }

        let form = (
            <form onSubmit={this.addCustomerHandler}
                  onKeyUp={this.addCustomerKeyHandler}
                  id="g-add-customer-form"
                  noValidate>

                {formElementArray.map(formElement => (
                    <Input inputtype={formElement.config.inputtype}
                           label={formElement.config.label}
                           key={formElement.id}
                           inputConfig={formElement.config.inputConfig}
                           value={formElement.config.value}
                           invalid={!formElement.config.valid}
                           shouldValidate={formElement.config.toBeValidated}
                           touched={formElement.config.touched}
                           errorMessage={formElement.config.errorsMsg}
                        // use anonymous function to enable us pass parameters to the changeHandlerMethod
                           changed={(event) => this.inputChangeHandler(event, formElement.id)}/>
                ))};

                <div className="text-right">
                    <button
                        type="submit"
                        disabled={!this.state.formIsValid}
                        className="btn btn-warning g-customer-button">
                        Add Customer
                    </button>
                </div>

            </form>
        )

        return (
            <React.Fragment>
                <p className="g-form-notice">All fields are required</p>
                {form}
            </React.Fragment>
        )
    }

}
const mapStateToProps = state => {
    return{
        authToken:state.authRed.token
    }
}

const mapDispatchToProps = dispatch => {
    return{
        addCustomerSuccessProp: (customer) => dispatch(addNewCustomerSuccess(customer)),
        addCustomerFailureProp: (error) => dispatch(addNewCustomerFailure(error)),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(AddCustomer);