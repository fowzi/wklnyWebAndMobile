import React, {Component} from 'react';
import { connect } from 'react-redux';
import '../user-actions.css';
import Input from '../../../../components/UI/Inputs/Inputs';
import ValidateFunc from '../../../Utility/FormValidations/customerValidate';
import axios from '../../../../axios-customers';
import Modal from '../../../../components/UI/Modal/Modal';
import Spinner from '../../../../components/UI/Spinner/Spinner';

class EditCustomer extends Component {

    state = {
        customer: null,
        customerId: this.props.customerToEditId,
        customerForm: {
            first_name: {
                inputtype: 'input',
                label: 'First Name',
                inputConfig: {
                    type: 'text',
                    id: 'first_name',
                    placeholder: 'First Name'
                },
                value: this.props.first_name,
                valid: this.props.formFieldValid,
                touched: false,
                toBeValidated: true
            },

            second_name: {
                inputtype: 'input',
                label: 'Second Name',
                inputConfig: {
                    type: 'text',
                    id: 'second_name',
                    placeholder: 'Second Name'
                },
                value: this.props.second_name,
                valid: this.props.formFieldValid,
                touched: false,
                toBeValidated: true
            },

            email_address: {
                inputtype: 'input',
                label: 'Email',
                inputConfig: {
                    type: 'email',
                    placeholder: 'Email',
                    id: 'email_address',
                },
                value: this.props.email_address,
                valid: this.props.formFieldValid,
                touched: false,
                toBeValidated: true
            },

            phone_number: {
                inputtype: 'input',
                label: 'Phone Number',
                inputConfig: {
                    type: 'text',
                    placeholder: 'Phone Number',
                    id: 'phone_number',
                },
                value: this.props.phone_number,
                errorsMsg: '',
                valid: this.props.formFieldValid,
                touched: false,
                toBeValidated: true
            },

            branch: {
                inputtype: 'select',
                label: 'Branch',
                inputConfig: {
                    id: 'branch',
                    options:
                        [
                            {value: "", displayValue: "Select Branch"},
                            {value: "branch_one", displayValue: "Branch 1"},
                            {value: "branch_two", displayValue: "Branch 2"},
                            {value: "branch_three", displayValue: "Branch 3"},
                        ],
                },
                value: this.props.branch,
                errorsMsg: '',
                valid: this.props.formFieldValid,
                touched: false,
                toBeValidated: true
            },

            loan_limit: {
                inputtype: 'input',
                label: 'Loan Limit',
                inputConfig: {
                    type: 'text',
                    placeholder: 'Loan Limit',
                    inputid: 'loan_limit',
                },
                value: this.props.loan_limit,
                errorsMsg: '',
                valid: this.props.formFieldValid,
                touched: false,
                toBeValidated: true
            },

            photo: {
                inputtype: 'input',
                label: 'Customers Photo',
                inputConfig: {
                    type: 'file',
                    id: 'customer_photo',
                    accept:'image'
                },
                errorsMsg: '',
                value: '',
                valid: true,
                toBeValidated: false
            }
        },
        formIsValid: this.props.formIsValid,
        updating: false
    }

    editCustomerHandler = (event) => {
        event.preventDefault();
        this.setState({updating: true})
        if (this.state.formIsValid) {
            const newCustomerInfo = {};
            for (let elementIdentifier in this.state.customerForm) {
                newCustomerInfo[elementIdentifier] = this.state.customerForm[elementIdentifier].value
            }

            axios.put('/customers/' + this.state.customerId + '.json?auth=' + this.props.authToken, newCustomerInfo)
                .then(response => {
                    this.setState({updating: false})
                    this.props.showAlert('alert-success', 'Customers Information updated Successfully')
                })
                .catch(error => {
                    console.log(error)
                    this.setState({updating: false})
                    this.props.showAlert('alert-danger', 'Unable to save customer information please try again!')
                })
        }
    }

    inputChangeHandler(event, inputIdentifier) {
        // use deep cloning to be able to get the values of nesteds objects
        const updateCustomerFrom = {...this.state.customerForm}
        const updateFormElement = {...updateCustomerFrom[inputIdentifier]}

        updateFormElement.value = event.target.value;
        const validationResults = ValidateFunc(inputIdentifier, updateFormElement.value);
        updateFormElement.valid = validationResults.isValid;
        updateFormElement.errorsMsg = validationResults.errorsMsg;
        updateFormElement.touched = true;
        updateCustomerFrom[inputIdentifier] = updateFormElement

        // handle disabling and enabling submit buttton
        let formIsValid = true;
        for (let identifier in updateCustomerFrom) {
            formIsValid = updateCustomerFrom[identifier].valid && formIsValid;
        }
        this.setState({customerForm: updateCustomerFrom, formIsValid: formIsValid})
        this.setState({customerForm: updateCustomerFrom})
    }

    render() {

        // console.log(this.state.customerForm.first_name.value)
        const formElementArray = [];
        for (let key in this.state.customerForm) {
            formElementArray.push({
                id: key,
                config: this.state.customerForm[key]
            });
        }

        let loading = this.state.updating ? <Modal show={this.state.updating} body={<Spinner/>}/>: '';

        let editForm = (
            <form onSubmit={this.editCustomerHandler}
                  noValidate>

                {formElementArray.map(inputElement => (
                    <Input inputtype={inputElement.config.inputtype}
                           label={inputElement.config.label}
                           key={inputElement.id}
                           value={inputElement.config.value}
                           invalid={!inputElement.config.valid}
                           inputConfig={inputElement.config.inputConfig}
                           shouldValidate={inputElement.config.toBeValidated}
                           touched={inputElement.config.touched}
                           errorMessage={inputElement.config.errorsMsg}
                           changed={(event) =>
                               this.inputChangeHandler(event, inputElement.id)
                           }
                    />
                ))}

                <div className="text-right">
                    <button
                        type="submit"
                        disabled={!this.state.formIsValid}
                        className="btn btn-warning g-customer-button">
                        Edit Customer
                    </button>
                </div>

            </form>

        )

        return (
            <React.Fragment>
                <p className="g-form-notice">All fields are required</p>
                {editForm}
                {loading}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return{
        authToken: state.authRed.token
    }
}

export default connect(mapStateToProps)(EditCustomer);
