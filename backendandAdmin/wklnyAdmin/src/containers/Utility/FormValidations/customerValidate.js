export default function (identifier, value) {

    let isValid = true;
    let errMsg = '';

    switch (identifier) {

        case('first_name'):
        case('second_name'):
            if (value.trim() === '' && isValid) {
                isValid = false;
                errMsg = 'Name must have a value';
            } else if (!value.match(/^[a-zA-Z]*$/) && isValid) {
                isValid = false;
                errMsg = 'Name must be letter only';
            } else if (value.length <= 2 && isValid) {
                isValid = false;
                errMsg = 'Name must be more than two characters long';
            }
            break;

        case('email_address') :
        case('admin_email') :
            if (value.trim() === '' && isValid) {
                isValid = false;
                errMsg = 'Email Address Must have a value';
            } else {
                const pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
                if (!pattern.test(value)) {
                    isValid = false;
                    errMsg = "Please enter a valid email address"
                }
            }
            break;

        case('phone_number'):
            if (value.trim() === '' && isValid) {
                isValid = false;
                errMsg = 'Phone Number must have a value';
            } else if (!value.match(/^07[0-9]{8}$/)) {
                isValid = false;
                errMsg = 'Phone number must begin with \'07\' and be 10 numbers long'
            }
            break;

        case('branch'):
            if (value.trim() === '' && isValid) {
                isValid = false;
                errMsg = 'Please select a Branch';
            }
            break;

        case('loan_limit'):
            if (value.trim() === '' && isValid) {
                isValid = false;
                errMsg = 'Loan Limit must have a value';
            } else if (!value.match(/^[0-9]*$/)) {
                isValid = false;
                errMsg = 'Please use only numbers';
            } else if (value > 50000) {
                isValid = false;
                errMsg = 'Loan limit should not exceed Ksh. 50,000';
            }
            break;

        case('photo'):
            if (value.trim() !== '') {
                // var fileSize = file.files[0].size / 1024 / 1024 ; // in MB
                if (!value.match(/(.*?)\.(jpg|jpeg|png)$/)) {
                    isValid = false;
                    errMsg = 'Only jpg/jpeg/png images are accepted';
                }
                // else if(fileSize > 2){
                //    isValid = false;
                //    errMsg = 'Image should not be larger than 2MBs';
                // }
            }
            break;

        case('user_name'):
            if (value.trim() === '' && isValid) {
                isValid = false;
                errMsg = 'User name must have a value';
            }
            break;

        case('password'):
            if (value.trim() === '' && isValid) {
                isValid = false;
                errMsg = 'Password must have a value';
            }
            break;

        default:

    }

    return {isValid: isValid, errorsMsg: errMsg};
}