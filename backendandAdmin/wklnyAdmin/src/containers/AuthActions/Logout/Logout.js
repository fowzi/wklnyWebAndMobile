import React, { Component } from "react";
import { logout } from "../../../store/actions/index";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { adminLogOut } from "../../../redux/types";

class Logout extends Component {
  componentDidMount() {
    this.props.logoutProp();
  }

  render() {
    return <Redirect to={"/"} />;
  }
}

const mapDispatchToProps = dispatch => {
  return {
    logoutProp: () => dispatch({ type: adminLogOut })
  };
};

export default connect(
  null,
  mapDispatchToProps
)(Logout);
