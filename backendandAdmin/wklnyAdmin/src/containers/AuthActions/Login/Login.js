import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import "./login.css";
import { loginAttempt } from "../../../store/actions/index";
import Input from "../../../components/UI/Inputs/Inputs";
import ValidateFunc from "../../Utility/FormValidations/customerValidate";
import Spinner from "../../../components/UI/Spinner/Spinner";
import { AdminLogin } from "../../../redux/actions/auth";

class Login extends Component {
  state = {
    userAdmin: {
      admin_email: {
        inputtype: "input",
        label: "Admin Email",
        inputConfig: {
          type: "email",
          id: "admin_email",
          placeholder: ""
        },
        value: "",
        valid: false,
        touched: false,
        toBeValidated: true
      },

      password: {
        inputtype: "input",
        label: "Password",
        inputConfig: {
          type: "password",
          id: "password",
          placeholder: ""
        },
        value: "",
        valid: false,
        touched: false,
        toBeValidated: true
      }
    },
    formIsValid: false
  };
  //https://hln.herokuapp.com/
  // 206.189.183.18:3000/
  //134.209.231.14
  adminLoginHandler = event => {
    event.preventDefault();
    this.props.Login(
      this.state.userAdmin.admin_email.value,
      this.state.userAdmin.password.value
    );
    return <Redirect to="/" />;
  };

  inputChangeHandler = (event, inputIdentifier) => {
    // use deep cloning to be able to get the values of nesteds objects
    const updateCustomerFrom = { ...this.state.userAdmin };
    const updateFormElement = { ...updateCustomerFrom[inputIdentifier] };

    updateFormElement.value = event.target.value;
    const validationResults = ValidateFunc(
      inputIdentifier,
      updateFormElement.value
    );
    updateFormElement.valid = validationResults.isValid;
    updateFormElement.errorsMsg = validationResults.errorsMsg;
    updateFormElement.touched = true;
    updateCustomerFrom[inputIdentifier] = updateFormElement;

    // handle disabling and enabling submit buttton
    let formIsValid = true;
    for (let identifier in updateCustomerFrom) {
      formIsValid = updateCustomerFrom[identifier].valid && formIsValid;
    }
    this.setState({ userAdmin: updateCustomerFrom, formIsValid: formIsValid });
  };
  componentDidMount() {
    this.props.Reset();
  }
  render() {
    let loginFormFieldsArray = [];
    for (let key in this.state.userAdmin) {
      loginFormFieldsArray.push({
        id: key,
        config: this.state.userAdmin[key]
      });
    }

    let loginForm = (
      <form onSubmit={this.adminLoginHandler} noValidate>
        {loginFormFieldsArray.map(formField => (
          <Input
            inputtype={formField.config.inputtype}
            label={formField.config.label}
            key={formField.id}
            inputConfig={formField.config.inputConfig}
            value={formField.config.value}
            invalid={!formField.config.valid}
            shouldValidate={formField.config.toBeValidated}
            touched={formField.config.touched}
            errorMessage={formField.config.errorsMsg}
            // use anonymous function to enable us pass parameters to the changeHandlerMethod
            changed={event => this.inputChangeHandler(event, formField.id)}
          />
        ))}

        <div className={"g-login-btn pt-md-4"}>
          <button
            type="submit"
            disabled={!this.state.formIsValid}
            className="btn btn-green btn-lg btn-block "
          >
            Login
          </button>
        </div>
      </form>
    );

    if (this.props.loading) {
      loginForm = <Spinner />;
    }

    let error = this.props.loginErrorProp ? (
      <div className={"alert alert-danger g-login-error"}>
        {this.props.loginErrorProp}
      </div>
    ) : (
      ""
    );

    let authRedirect = null;
    if (this.props.admin) {
      authRedirect = <Redirect to={"/"} />;
    }

    return (
      <React.Fragment>
        {authRedirect}
        <span className={"g-login-title text-center pb-4 pt-3"}>
          Login To Continue
        </span>
        {error}
        {loginForm}
      </React.Fragment>
    );
  }
}

const matchStateToProps = state => {
  return {
    // loginErrorProp: state.authRed.error,
    // loadingProps: state.authRed.loading,
    // isAuthenticated: state.authRed.token !== null
    ...state.auth
  };
};

const mapDispatchToProps = dispatch => {
  return {
    // we execute loginSuccess action creator so that it returns the action type
    // onLoginAttempt: (email, password) => dispatch(loginAttempt(email, password))
    Login: (email, password) =>
      AdminLogin({ email: email, password }, dispatch),
    Reset: () => dispatch({ type: "adminReset" })
  };
};

export default connect(
  matchStateToProps,
  mapDispatchToProps
)(Login);
