import React, { Component } from 'react';
// import customer info css here

class CustomerInfo extends Component{

    state = {
        userId:''
    }

    componentDidMount(){
        if(this.props.match.params.userid){
            this.setState({userId:this.props.match.params.userid})
            // axios request to get user information can be done here
        }
    }

    render(){
        return(
            <div className={'container-fluid mt-4 pr-4 pl-4'}>
                <h3>This page will show information about a customer of id: {this.state.userId}</h3>
            </div>

        )
    }
}

export default CustomerInfo;