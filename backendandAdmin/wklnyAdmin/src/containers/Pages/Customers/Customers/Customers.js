import React, {Component} from 'react';
import {connect} from 'react-redux';
import {loadCustomers, closeAlert, deleteCustomerSuccess, deleteCustomerFailure} from '../../../../store/actions/index'
import './customers.css';
import AddFilterCustomer from '../../../../components/Customers/AddFilterCustomer/AddFilterCustomer';
import Customer from '../../../../components/Customers/Customer/Customer';
import Modal from '../../../../components/UI/Modal/Modal';
import AddCustomerForm from '../../../UserFormActions/Customers/AddCustomer/AddCustomer';
import Spinner from '../../../../components/UI/Spinner/Spinner';
import Alert from '../../../../components/UI/Alerts/Alert';
import Confirmation from '../../../../components/UI/Confirmation/Confirmation';
import axios from '../../../../axios-customers';

class Customers extends Component {

    state = {
        showModal: false,
        loading: false,
        filterValue: '',
        deleting: false,
        showDeleteConfirmation: false,
        customerKey:null,
        customerId:null
    }

    componentDidMount() {
        this.props.loadCustomersProp(this.props.authToken);
    }

    deleteCustomerHandler = ( key,customerId) => {
        this.setState({showDeleteConfirmation:false, loading: true, deleting: true, showModal: true})
        axios.delete('/customers/' + customerId + '.json?auth=' + this.props.authToken)
            .then(response => {
                this.setState({
                    showModal: false,
                    showAlert: true,
                    loading: false,
                });
                this.props.deleteCustomerSuccessProp(key, customerId)
            })
            .catch(error => {
                this.setState({
                    showModal: false,
                    loading: false,
                });
                this.props.deleteCustomerFailureProp()
            });
    }

    showModalHandler = () => {
        this.setState({showModal: true})
    }

    hideModalHandler = () => {
        this.setState({showModal: false})
    }

    setLoadingTrueHandler = () => {
        this.setState({loading: true})
    }

    setLoadingFalseHandler = () => {
        this.setState({loading: false})
    }

    closeConfirmation = () =>{
        this.setState({showDeleteConfirmation:false})
    }

    showConfirmationHandler = (key, id) => {
        this.setState({showDeleteConfirmation:true, customerKey:key,customerId:id})
    }

    // filter function
    filterCustomersHandler = (event) => {
        let valueToFilter = null
        event.target.value === null ? valueToFilter = null : valueToFilter = event.target.value.replace('_', ' ');
        this.setState({filterValue: valueToFilter});
    }

    render() {

        let customers = this.props.customerFetchError ? '' : <Spinner/>;

        if (this.props.customers) {
            customers = Object.keys(this.props.customers)
                .filter(key => this.props.customers[key].branch === this.state.filterValue || !this.state.filterValue)
                .map(key => {
                    return <Customer
                        key={this.props.customers[key].key}
                        user_key={this.props.customers[key].key}
                        first_name={this.props.customers[key].first_name}
                        second_name={this.props.customers[key].second_name}
                        email_address={this.props.customers[key].email_address}
                        phone_number={this.props.customers[key].phone_number}
                        branch={this.props.customers[key].branch}
                        loan_limit={this.props.customers[key].loan_limit}
                        deleteCustomer={() => this.showConfirmationHandler(key, this.props.customers[key].key)}
                    />
                })
        }

        let alert = this.props.showAlert ? <Alert
                alertType={this.props.alertType}
                hideAlert={this.props.closeAlert}>
                {this.props.alertMessage}
            </Alert>
            : '';

        let customerForm = <AddCustomerForm
            loadingTrue={this.setLoadingTrueHandler}
            loadingFalse={this.setLoadingFalseHandler}
            closeModal={this.hideModalHandler}/>

        if (this.state.loading) {
            customerForm = <Spinner/>
        }

        return (
            <React.Fragment>

                <AddFilterCustomer
                    clickedShowModal={this.showModalHandler}
                    changed={this.filterCustomersHandler}
                    // disable select filter when there are no customers
                    disableSelect={this.props.customers ? false : true}
                />

                <Confirmation show={this.state.showDeleteConfirmation}
                              title={'Confirm Delete'}
                              showCloseButton={true}
                              body={'Are tou sure you want to delete the customer?'}
                              closeConfirmation={this.closeConfirmation}
                              secondaryButton={'Cancel'}
                              primaryButton={'Confirm Delete'}
                              secondaryClick={this.closeConfirmation}
                              primaryClick={() => this.deleteCustomerHandler(this.state.customerKey, this.state.customerId)}/>

                <Modal title={this.state.loading ? null : 'Add New Customers'}
                       show={this.state.showModal}
                       hideModal={ this.state.loading ? null : this.hideModalHandler}
                       showCloseButton={!this.state.loading}
                       body={customerForm}>
                </Modal>
                <div className="container-fluid mt-4 pr-4 pl-4" id="customer-listView">
                    {alert}
                    {customers}
                </div>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        authToken:state.authRed.token,
        customers: state.custRed.customers,
        customerFetchError: state.custRed.customerFetchError,
        showAlert: state.custRed.showAlert,
        alertType: state.custRed.alertType,
        alertMessage: state.custRed.alertMessage
    }
}

const mapDispatchToProps = dispatch => {
    return {
        loadCustomersProp: (token) => dispatch(loadCustomers(token)),
        deleteCustomerSuccessProp: (key,customerId) => dispatch(deleteCustomerSuccess(key,customerId)),
        deleteCustomerFailureProp: () => dispatch(deleteCustomerFailure()),
        closeAlert: () => dispatch(closeAlert()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Customers);