import React, {Component} from 'react';
import { connect } from 'react-redux';
import EditForm from '../../../UserFormActions/Customers/EditCustomer/EditCustomer';
import Alert from '../../../../components/UI/Alerts/Alert';
import axios from '../../../../axios-customers';
import Spinner from '../../../../components/UI/Spinner/Spinner';
import './customer-edit.css';

class CustomerEdit extends Component {

    state = {
        customerId: '',
        customerInfo: null,
        customerInfoFetchError: false,
        showAlert: false,
        alertType: '',
        alertMessage: '',
        formElementsValid: false,
        formIsValid: false,
    }

    componentDidMount() {
        if (this.props.match.params.userid) {
            // get user id and fetch information
            axios.get('/customers/' + this.props.match.params.userid + '.json?auth=' + this.props.authToken)
                .then(response => {
                    if (response.data) {
                        const customer = response.data;
                        this.setState({
                            customerId: this.props.match.params.userid,
                            formElementsValid: true,
                            formIsValid: true,
                            customerInfo: customer
                        })
                    } else {
                        this.setState({
                            customerInfoFetchError: true,
                            showAlert: true,
                            alertType: 'alert-danger',
                            alertMessage: 'Unknown customer information please try again!'
                        })
                    }
                })
                .catch(error => {
                    this.setState({
                        customerInfoFetchError: true,
                        showAlert: true,
                        alertType: 'alert-danger',
                        alertMessage: 'Unable to get customer information please try again!'
                    });

                })
        }
    };

    hideAlertHandler = () => {
        this.setState({showAlert: false})
    }

    showAlertHandler = (type, message) => {
        this.setState({showAlert: true})
        this.setState({alertType: type})
        this.setState({alertMessage: message})
    }

    render() {

        let alert = this.state.showAlert ? <Alert
            alertType={this.state.alertType}
            hideAlert={this.hideAlertHandler}>
            {this.state.alertMessage}
        </Alert> : '';

        let editform = this.state.customerInfoFetchError ? '' : <Spinner/>

        if (this.state.customerInfo) {
            editform = (
                <div className={"g-edit-col z-depth-2"}>
                    <EditForm
                        {...this.state.customerInfo}
                        customerToEditId={this.state.customerId}
                        formFieldValid={this.state.formElementsValid}
                        showAlert={this.showAlertHandler}
                        formIsValid={this.formIsValid}/>
                </div>
            )
        }

        return (
            <div className={'container mt-4 pr-4 pl-4'}>
                {/*<h3>Edit customer id: {this.state.userId} information</h3>*/}
                <div className="row">
                    <div className="col-sm-12 col-md-8 offset-md-2 p-0">
                        {alert}
                    </div>
                    <div className="col-sm-12 col-md-8 offset-md-2">
                        {editform}
                    </div>

                </div>

            </div>

        )
    }
}

const mapStateToProps = state => {
    return{
        authToken: state.authRed.token
    }
}

export default connect(mapStateToProps)(CustomerEdit);