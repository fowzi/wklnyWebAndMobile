import * as actionTypes from '../actions/actionTypes';

const initialState = {
    token: null,
    adminId: null,
    email: null,
    loading: false,
    error: null,
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.LOGIN_START:
            return {
                ...state,
                error: null,
                loading: true,
                loginError: null
            }
        case actionTypes.LOGIN_SUCCESS:
            return {
                ...state,
                isAuthenticated: true,
                loading: false,
                Error: null,
                token: action.token,
                adminId: action.adminId,
                email: action.adminEmail,

            }
        case actionTypes.LOGIN_FAILURE:
            let error = null;
            switch (action.error) {
                case 'EMAIL_NOT_FOUND':
                    error = 'Email not found';
                    break;
                case 'INVALID_PASSWORD':
                    error = 'Invalid credentials';
                    break;
                default:
                    error = 'Unable to login! Please try again'
            }

            return {
                ...state,
                error: error,
                loading: false,
            }
        case actionTypes.LOGOUT: {
            return {
                token: null,
                adminId: null,
                email: null,
                customers:null
            }
        }

        default:
            return state
    }
}

export default reducer;

// auth function must run synchrounously, we can therefore not use it to run asynch code
// we can use action creators to run asynch code