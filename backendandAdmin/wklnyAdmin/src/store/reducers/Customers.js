import * as actionTypes from '../actions/actionTypes';

const initialState = {
    customers: null,
    customerFetchError: false,
    alertType: '',
    alertMessage: '',
    showAlert: false,
}

const reducer = (state = initialState, action) => {
    switch (action.type) {

        case actionTypes.LOADING_CUSTOMERS_SUCCESS:
            const customers = action.customers;
            const updatedCustomers = Object.keys(customers).map(key => {
                return {
                    key: key,
                    first_name: customers[key].first_name,
                    second_name: customers[key].second_name,
                    email_address: customers[key].email_address,
                    phone_number: customers[key].phone_number,
                    loan_limit: parseInt(customers[key].loan_limit, 10).toLocaleString(),
                    branch: customers[key].branch.replace('_', ' ')
                }
            })
            return {
                ...state,
                customers: updatedCustomers
            }

        case actionTypes.LOADING_CUSTOMERS_FAILURE:
            let errValues =  ['alert-danger','Could not load Customers. Please try again']
            if(state.customers){
                errValues = ['alert-warning','Unable to reload Customers.']
            }
            const [alertType,alertMessage] = errValues
            return {
                ...state,
                customerFetchError: true,
                showAlert: true,
                alertType: alertType,
                alertMessage: alertMessage,
            }

        case actionTypes.ADD_NEW_CUSTOMER_SUCCESS:
            return {
                ...state,
                customerFetchError: false,
                customers: state.customers.concat(action.customer),
                showAlert: true,
                alertType: 'alert-success',
                alertMessage: 'Customers successfully added'
            }

        case actionTypes.ADD_NEW_CUSTOMER_FAILURE:
            return {
                ...state,
                showAlert: true,
                alertType: 'alert-danger',
                alertMessage: 'Unable to add customer please try again'
            }

        case actionTypes.DELETE_CUSTOMER_SUCCESS: {
            // use filter method to filter instead of the one used below
            const newCustomers = state.customers
            delete newCustomers[action.key]
            return {
                ...state,
                customers: newCustomers,
                showAlert: true,
                alertType: 'alert-success',
                alertMessage: 'Customers successfully deleted'
            }
        }

        case actionTypes.DELETE_CUSTOMER_FAILURE: {
            return {
                ...state,
                showAlert: true,
                alertType: 'alert-danger',
                alertMessage: 'Unable to delete customer please try again'
            }
        }

        case actionTypes.CLOSE_ALERT:
            return {
                ...state,
                showAlert: !state.showAlert,
                alertType: '',
                alertMessage: ''
            }

        default:
            return state;

    }
};

export default reducer;

