import axios from "../../axios-customers";
import * as actionTypes from "./actionTypes";

export const loadingCustomersSuccess = (customers) => {
    return {
        type: actionTypes.LOADING_CUSTOMERS_SUCCESS,
        customers: {...customers}
    }
}

export const loadingCustomersFailure = (errors) => {
    return {
        type: actionTypes.LOADING_CUSTOMERS_FAILURE,
        errors: errors
    }
}

export const loadCustomers = (token) => {
    return dispatch => {
        axios.get('/customers.json?auth='+token)
            .then(response => {
                dispatch(loadingCustomersSuccess(response.data))
            })
            .catch(errors => {
                dispatch(loadingCustomersFailure(errors))
            })
    }
}

export const addNewCustomerSuccess = (customer) => {
    return {
        type: actionTypes.ADD_NEW_CUSTOMER_SUCCESS,
        customer: customer
    }
}

export const addNewCustomerFailure = (error) => {
    return {
        type: actionTypes.ADD_NEW_CUSTOMER_FAILURE,
        error: error
    }
}

export const deleteCustomerSuccess = (key,customerId) => {
    return {
        type: actionTypes.DELETE_CUSTOMER_SUCCESS,
        key:key,
        id: customerId
    }
}

export const deleteCustomerFailure = () => {
    return {
        type: actionTypes.DELETE_CUSTOMER_FAILURE,
    }
}

export const closeAlert = () => {
    return {
        type: actionTypes.CLOSE_ALERT
    }
}