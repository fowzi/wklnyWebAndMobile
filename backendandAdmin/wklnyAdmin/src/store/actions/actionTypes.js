export const LOADING_CUSTOMERS_SUCCESS = 'LOADING_CUSTOMERS_SUCCESS';
export const LOADING_CUSTOMERS_FAILURE = 'LOADING_CUSTOMERS_FAILURE';

export const ADD_NEW_CUSTOMER_SUCCESS = 'ADD_NEW_CUSTOMER_SUCCESS';
export const ADD_NEW_CUSTOMER_FAILURE = 'ADD_NEW_CUSTOMER_FAILURE';

export const LOGIN_START = 'LOGIN_START';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const LOGOUT = 'LOGOUT';

export const CLOSE_ALERT = 'CLOSE_ALERT';

export const DELETE_CUSTOMER_SUCCESS = 'DELETE_CUSTOMER_SUCCESS';
export const DELETE_CUSTOMER_FAILURE = 'DELETE_CUSTOMER_FAILURE';

// action creators are functions that return an action or create an action
// they are used to run async code inside the auth which normally does not

// NB: only synchronous actions may edit the store

// NB: for action creators to run async code we need to have a middleware called redux-thunk
// it allows the action creators to not return the action itself but to return a function that will
// eventually dispatch the action, therefore allowing us to run async code
// async action creators will in turn dispatch actions created by sync ones

// the async action creators never make it to the auth and are only used to run async code

// they can receive any payload we want to pass with a particular action
// the convention for creating an action creator is to use the lowercase of the identifier
// or camel case incase they are two words













