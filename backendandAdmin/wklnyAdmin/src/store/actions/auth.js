import axios from "axios";
// login success sync action creator to be dispatched by loginAttempt async action creator
import * as actionTypes from "./actionTypes";

export const loginStart = () => {
  return {
    type: actionTypes.LOGIN_START
  };
};

export const loginSuccess = (id, token, email) => {
  return {
    type: actionTypes.LOGIN_SUCCESS,
    adminId: id,
    token: token,
    adminEmail: email
  };
};

export const loginFailure = error => {
  return {
    type: actionTypes.LOGIN_FAILURE,
    error: error
  };
};

export const logout = () => {
  localStorage.removeItem("g-token");
  // localStorage.removeItem('g-user-id');
  localStorage.removeItem("g-expiration-time");
  localStorage.removeItem("g-user-email");
  return {
    type: actionTypes.LOGOUT
  };
};

// logout user after the expiration time sent from the server is up
export const checkTimeOut = timeout => {
  return dispatch => {
    setTimeout(() => {
      dispatch(logout());
    }, timeout * 1000);
  };
};

// login attempt action creator that dispatches loginSuccess or loginFailure sync action creators
export const loginAttempt = (email, password) => {
  return dispatch => {
    dispatch(loginStart());
    // axios turns object to json
    const authData = {
      email: email,
      password: password
    };
    // axios.post(`https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=${process.env.REACT_APP_CUSTOMER_MANAGEMENT_API_KEY}`
    //     , authData)
    axios
      .post(`localhost:5000/api/admin/login`, authData, {
        origin: "*",
        methods: "GET,HEAD,PUT,PATCH,POST,DELETE"
      })
      .then(response => {
        console.log("============response========================");
        console.log(response);
        console.log("====================================");
        const expirationDate =
          new Date().getTime() + response.data.expiresIn * 1000;
        localStorage.setItem("g-token", response.data.idToken);
        // localStorage.setItem('g-user-id', response.data.localId)
        localStorage.setItem("g-expiration-time", null);
        localStorage.setItem("email", response.data.email);
        dispatch(
          loginSuccess(
            response.data.localId,
            response.data.idToken,
            response.data.email
          )
        );
        dispatch(checkTimeOut(response.data.expiresIn));
      })
      .catch(error => {
        console.log("============response========================");
        console.log(error);
        console.log("====================================");

        dispatch(loginFailure(error.response.data.error.message));
      });
  };
};

// determine if the user was logged in before by checking tokenId from local storage
export const authCheck = () => {
  return dispatch => {
    const token = localStorage.getItem("g-token");
    if (!token) {
      dispatch(logout());
    } else {
      const expirationTime = localStorage.getItem("g-expiration-time");
      if (expirationTime < new Date().getTime()) {
        dispatch(logout());
      } else {
        const userId = localStorage.getItem("g-user-id");
        const userEmail = localStorage.getItem("g-user-email");
        // axios.post(`https://www.googleapis.com/identitytoolkit/v3/relyingparty/getAccountInfo?key=${process.env.REACT_APP_CUSTOMER_MANAGEMENT_API_KEY}`, userPayload)
        //     .then(response => {
        //         console.log(response.data)
        //     })
        //     .catch(error => {
        //         console.log(error)
        //     })
        dispatch(loginSuccess(userId, token, userEmail));
        dispatch(checkTimeOut((expirationTime - new Date().getTime()) / 1000));
      }
    }
  };
};
