import React, { Component } from "react";
import "./App.css";
import { Route, Switch, withRouter, Redirect } from "react-router-dom";

import Layout from "./hoc/Layout/Layout";
import Customers from "./containers/Pages/Customers/Customers/Customers";
import Loans from "./components/Pages/Customers/Loans/Loans";
import CustomerInfo from "./containers/Pages/Customers/CustomerInformation/CustomerInfo";
import CustomerEdit from "./containers/Pages/Customers/CustomerEdit/CustomerEdit";
import Login from "./components/Pages/Login/Login";
import Logout from "./containers/AuthActions/Logout/Logout";
import Dashboard from "./components/Pages/Dashboard/Dashboard";

import Drivers from "./components/Pages/drivers";
import Clients from "./components/Pages/clients";
import Trips from "./components/Pages/trips";
import runningServices from "./components/Pages/runningServices";
import prices from "./components/Pages/prices";
import Contracts from "./components/Pages/Contracts";
import { connect } from "react-redux";
import { adminLoginFailed } from "./redux/types";
import addDriver from "./components/Pages/addDriver";
import addUser from "./components/Pages/addUser";
import Policy from "./components/Pages/privacy";
import verifiedDrivers from "./components/Pages/verifiedDrivers";
import TripsEnded from "./components/Pages/TripsEnded";
import About from "./components/Pages/about";
import More from "./components/Pages/more";
import problems from "./components/Pages/problems";

import localization from "./localization";
import Resetpassword from "./components/Pages/resetpassword";
import bankAccounts from "./components/Pages/bankAccounts";
import Terms from "./components/Pages/terms";

class App extends Component {
  constructor(props) {
    super(props);
    this.handleLoc();
  }
  handleLoc = () => {
    // let lan = localStorage.getItem("@lan");
    // if (!lan) {
    //   localization.setLanguage("en");
    //   return;
    // }
    localization.setLanguage("ar");
  };
  render() {
    let routes = (
      <Switch>
        <Route path="/resetpassword/:code" component={Resetpassword} />
        <Route path="/" component={Login} />

        <Redirect to={"/"} />
      </Switch>
    );

    if (this.props.admin) {
      routes = (
        <Layout>
          <Switch>
            <Route path="/" exact component={Dashboard} />
            <Route path="/customers" component={Customers} />
            <Route path="/service-providers" component={Drivers} />
            <Route path="/verifiedDrivers" component={verifiedDrivers} />
            <Route path="/clients" component={Clients} />
            <Route path="/prices" component={prices} />
            <Route path="/trips" component={Trips} />
            <Route path="/serviceended" component={TripsEnded} />
            <Route path="/servicerunning" component={runningServices} />
            <Route path="/loans" component={Loans} />
            <Route path="/logout" component={Logout} />
            <Route path="/add-service-provider" component={addDriver} />
            <Route path="/add-user" component={addUser} />
            <Route path="/Policy" component={Policy} />
            <Route path="/bank" component={bankAccounts} />
            <Route path="/contracts" component={Contracts} />
            <Route path="/terms" component={Terms} />
            <Route path="/about" component={About} />
            <Route path="/problems" component={problems} />
            <Route path="/more" component={More} />
            <Route path="/customer/:userid/show" component={CustomerInfo} />
            <Redirect to={"/"} />
            {/*<Route component={NotFound}/>*/}
          </Switch>
        </Layout>
      );
    }
    return <div className="App">{routes}</div>;
  }
}

const matchStateToProps = state => {
  return {
    ...state.auth
  };
};
const mapDispatchToProps = dispatch => {
  return {
    reset: () => dispatch({ type: adminLoginFailed })
  };
};

export default withRouter(
  connect(
    matchStateToProps,
    mapDispatchToProps
  )(App)
);
