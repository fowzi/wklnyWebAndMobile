import React, { Component } from "react";
import { connect } from "react-redux";
import SideMenu from "../../components/Menus/SideMenu/SideMenu";
import MainMenu from "../../components/Menus/MainMenu/MainMenu";
import MainPageWrapper from "../../components/Wrappers/MainPageWrapper/MainPageWrapper";
import PageContentWrapper from "../../components/Wrappers/PageContentWrapper/PageContentWrapper";
import MainBodyContentWrapper from "../../components/Wrappers/MainBodyContentWrapper/MainBodyContentWrapper";

class Layout extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    showSideMenu: false
  };
  async componentDidMount() {
    let sideState = await localStorage.getItem("@sidemenu");
    console.log("==========sidemenu==========================");
    console.log(sideState);
    console.log("====================================");
    if (sideState === "open") {
      this.setState({ showSideMenu: true });
    } else {
      this.setState({ showSideMenu: false });
    }
  }
  sideMenuToggleHandler = async () => {
    await this.setState(prevState => {
      return { showSideMenu: !prevState.showSideMenu };
    });
    this.state.showSideMenu === true
      ? await localStorage.setItem("@sidemenu", "open")
      : await localStorage.setItem("@sidemenu", "close");
  };

  render() {
    return (
      <React.Fragment>
        <MainPageWrapper toggleClass={this.state.showSideMenu}>
          <SideMenu toggleClass={this.state.showSideMenu} />
          <PageContentWrapper>
            <MainMenu
              toggleSideMenu={this.sideMenuToggleHandler}
              adminEmail={this.props.admin.email}
              admin={this.props.admin}
            />
            <MainBodyContentWrapper>
              {this.props.children}
            </MainBodyContentWrapper>
          </PageContentWrapper>
        </MainPageWrapper>
      </React.Fragment>
    );
  }
}

const mapState = state => {
  return {
    ...state.auth
  };
};

export default connect(mapState)(Layout);
