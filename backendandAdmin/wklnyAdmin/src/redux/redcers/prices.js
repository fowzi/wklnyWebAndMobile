import {
  Fetch_Trips_Price,
  Update_Trips_Price_Attemp,
  Update_Trips_Price_Success,
  Update_Trips_Price_Fail,
  Update_Trips_Price_Reset
} from "../types";

const initialState = {
  prices: null,
  updatePricesLoading: false,
  updateSucess: false,
  updateField: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case Fetch_Trips_Price:
      return { ...state, prices: action.payload };
    case Update_Trips_Price_Attemp:
      return { ...state, updatePricesLoading: true };
    case Update_Trips_Price_Success:
      return {
        ...state,
        updatePricesLoading: false,
        updateSucess: true,
        prices: action.payload
      };
    case Update_Trips_Price_Fail:
      return {
        ...state,
        updatePricesLoading: false,
        updateField: action.payload
      };
    case Update_Trips_Price_Reset:
      return {
        ...state,
        updatePricesLoading: false,
        updateSucess: false,
        updateField: null
      };
    default:
      return state;
  }
};
