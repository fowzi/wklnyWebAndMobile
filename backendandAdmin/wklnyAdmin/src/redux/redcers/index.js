import { persistCombineReducers, persistStore } from "redux-persist";
import storage from "redux-persist/lib/storage";

import drivers from "./drivers";
import auth from "./auth";
import users from "./users";
import count from "./count";
import trips from "./trips";
import prices from "./prices";
import banks from "./banks";

const persistConfig = {
  key: "root",
  storage
};

export default persistCombineReducers(persistConfig, {
  drivers,
  users,
  count,
  auth,
  users,
  trips,
  prices,
  banks
});
