import {
  Fetch_Drivers_Attemp,
  Fetch_Drivers_Success,
  Fetch_Drivers_Fail,
  Add_Driver_Success,
  Add_Driver_Attemp,
  Add_Driver_Fail,
  Add_Driver_Reset,
  Fetch_Driver_Details_Attemp,
  Fetch_Driver_Details_Success,
  Fetch_Driver_Details_Fail,
  Fetch_Driver_Details_Reset
} from "../types";

const initialState = {
  drivers: [],
  loading: false,
  addDriverLoading: false,
  addDriverDone: false,
  addDriverErrors: false,
  driverDetails: null,
  driverDetailsLoading: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case Fetch_Drivers_Attemp:
      return { ...state, loading: true };
    case Fetch_Drivers_Success:
      return { ...state, loading: false, drivers: action.payload };
    case Fetch_Drivers_Fail:
      return { ...state, loading: false };

    case Add_Driver_Attemp:
      return { ...state, addDriverLoading: true };
    case Add_Driver_Success:
      return {
        ...state,
        addDriverLoading: false,
        addDriverDone: true,
        addDriverErrors: null
      };
    case Add_Driver_Fail:
      return {
        ...state,
        addDriverDone: false,
        addDriverLoading: false,
        addDriverErrors: action.payload
      };
    case Add_Driver_Reset:
      return {
        ...state,
        addDriverDone: false,
        addDriverLoading: false,
        addDriverErrors: false
      };
    case Fetch_Driver_Details_Attemp:
      return {
        ...state,
        driverDetailsLoading: true
      };
    case Fetch_Driver_Details_Success:
      return {
        ...state,
        driverDetailsLoading: false,
        driverDetails: action.payload
      };
    case Fetch_Driver_Details_Fail:
      return {
        ...state,
        driverDetailsLoading: false
      };
    case Fetch_Driver_Details_Reset:
      return {
        ...state,
        driverDetailsLoading: false,
        driverDetails: null
      };
    default:
      return state;
  }
};
