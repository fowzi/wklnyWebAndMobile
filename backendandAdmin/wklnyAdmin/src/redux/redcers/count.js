import { Get_Count_Attemp, Get_Count_Success, Get_Count_Fail } from "../types";

const initialState = {
  count: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case Get_Count_Attemp:
      return { ...state };
    case Get_Count_Success:
      return { ...state, count: action.payload };
    case Get_Count_Fail:
      return { ...state };

    default:
      return state;
  }
};
