import {
  adminLoginAttemp,
  adminLoginSuccess,
  adminLoginFailed,
  adminLogOut
} from "../types";

const initialState = {
  admin: null,
  loading: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case adminLoginAttemp:
      return { ...state, loading: true };
    case adminLoginSuccess:
      return { ...state, loading: false, admin: action.payload };
    case adminLoginFailed:
      return { ...state, loading: false };
    case adminLogOut:
      return { ...state, admin: null, loading: false };
    case "adminReset":
      return initialState;

    default:
      return state;
  }
};
