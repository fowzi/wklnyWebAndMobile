import {
  Fetch_Banks,
  Update_Bank_Attemp,
  Update_Bank_Success,
  Update_Bank_Fail,
  Update_Bank_Reset
} from "../types";

const initialState = {
  banks: null,
  bankUpateLoading: false,
  bankUpdateFail: false,
  bankUpateSuccess: false
};

export default (state = initialState, action) => {
  switch (action.type) {
    case Fetch_Banks:
      return { ...state, banks: action.payload };
    case Update_Bank_Attemp:
      return { ...state, bankUpateLoading: action.payload };
    case Update_Bank_Success:
      return {
        ...state,
        bankUpateLoading: false,
        bankUpateSuccess: action.payload
      };
    case Update_Bank_Fail:
      return {
        ...state,
        bankUpateLoading: false,
        bankUpdateFail: action.payload
      };
    case Update_Bank_Reset:
      return {
        ...state,
        bankUpateLoading: false,
        bankUpdateFail: false,
        bankUpateSuccess: false
      };

    default:
      return state;
  }
};
