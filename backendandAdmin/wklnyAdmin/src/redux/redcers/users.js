import {
  Fetch_Users_Attemp,
  Fetch_Users_Success,
  Fetch_Users_Fail,
  Add_User_Attemp,
  Add_User_Success,
  Add_User_Fail,
  Add_User_Reset
} from "../types";

const initialState = {
  getAllUersLoading: false,
  users: [],
  addUserErrors: null,
  addUserDone: false,
  addUsersLoading: false,
  page: -1
};

export default (state = initialState, action) => {
  switch (action.type) {
    case Fetch_Users_Attemp:
      return { ...state, getAllUersLoading: true };
    case Fetch_Users_Success:
      return {
        ...state,
        getAllUersLoading: false,
        users: [...action.payload]
      };

    case Fetch_Users_Fail:
      return { ...state, getAllUersLoading: false };

    case Add_User_Attemp:
      return { ...state, addUsersLoading: true };
    case Add_User_Success:
      return {
        ...state,
        addUsersLoading: false,
        addUserDone: true,
        addUserErrors: null
      };
    case Add_User_Fail:
      return {
        ...state,
        addUsersLoading: false,
        addUserErrors: action.payload
      };
    case Add_User_Reset:
      return {
        ...state,
        addUsersLoading: false,
        addUserErrors: false,
        addUserDone: false
      };
    default:
      return state;
  }
};
