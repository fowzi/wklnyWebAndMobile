import {
  Fetch_Trips_Attemp,
  Fetch_Trips_Success,
  Fetch_Trips_Fail,
  Fetch_Trips_Reset,
  Fetch_RunningOrders_Attemp,
  Fetch_RunningOrders_Success,
  Fetch_RunningOrders_Fail,
  Fetch_DoneOrders_Attemp,
  Fetch_DoneOrders_Success,
  Fetch_DoneOrders_Fail,
  Fetch_Problems_Orders_Attemp,
  Fetch_Problems_Orders_Success,
  Fetch_Problems_Orders_Fail
} from "../types";

const initialState = {
  trips: [],
  loading: false,
  RunningOrders: {
    loading: false,
    orders: []
  },
  DoneOrders: {
    loading: false,
    orders: []
  },
  ProblemOrders: {
    loading: false,
    orders: []
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case Fetch_Trips_Attemp:
      return { ...state, loading: true };
    case Fetch_Trips_Success:
      return { ...state, loading: false, trips: action.payload };
    case Fetch_Trips_Fail:
      return { ...state, loading: false };
    case Fetch_Trips_Reset:
      return {
        ...state,
        loading: false
      };
    case Fetch_RunningOrders_Attemp:
      return {
        ...state,
        RunningOrders: { ...state.RunningOrders, loading: true }
      };
    case Fetch_RunningOrders_Success:
      return {
        ...state,
        RunningOrders: { orders: action.payload, loading: false }
      };
    case Fetch_RunningOrders_Fail:
      return {
        ...state,
        RunningOrders: { ...state.RunningOrders, loading: false }
      };

    case Fetch_DoneOrders_Attemp:
      return {
        ...state,
        DoneOrders: { ...state.DoneOrders, loading: true }
      };
    case Fetch_DoneOrders_Success:
      return {
        ...state,
        DoneOrders: { orders: action.payload, loading: false }
      };
    case Fetch_DoneOrders_Fail:
      return {
        ...state,
        DoneOrders: { ...state.DoneOrders, loading: false }
      };

    case Fetch_Problems_Orders_Attemp:
      return {
        ...state,
        ProblemOrders: { ...state.ProblemOrders, loading: true }
      };
    case Fetch_Problems_Orders_Success:
      return {
        ...state,
        ProblemOrders: { orders: action.payload, loading: false }
      };
    case Fetch_Problems_Orders_Fail:
      return {
        ...state,
        ProblemOrders: { ...state.ProblemOrders, loading: false }
      };
    default:
      return state;
  }
};
