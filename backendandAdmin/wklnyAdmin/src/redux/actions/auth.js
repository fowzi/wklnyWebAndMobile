import axios from "axios";
import {
  adminLoginAttemp,
  adminLoginSuccess,
  adminLoginFailed
} from "../types";
import { base_url } from "../config";

export const AdminLogin = (payload, dispatch) => {
  dispatch({ type: adminLoginAttemp });
  axios
    .post(base_url("admin/login"), payload)
    .then(res => {
      dispatch({ type: adminLoginSuccess, payload: res.data.admin });
    })
    .catch(error => {
      dispatch({ type: adminLoginFailed });
    });
};
// export const AdminLogOut = dispatch => {
//   dispatch({ type: adminLoginAttemp });
//   axios
//     .post("https://velooo.herokuapp.com/api/admin/login")
//     // .post("https://velooo.herokuapp.com/api/admin/login")
//     .then(res => {
//       console.log("=============res=======================");
//       console.log(res);
//       console.log("====================================");
//       dispatch({ type: adminLoginSuccess, payload: res.data.admin });
//     })
//     .catch(error => {
//       console.log("====================================");
//       console.log(error);
//       console.log("====================================");
//       dispatch({ type: adminLoginFailed });
//     });
// };
