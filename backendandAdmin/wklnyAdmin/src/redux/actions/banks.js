import axios from "axios";
import {
  Fetch_Banks,
  Update_Bank_Attemp,
  Update_Bank_Success,
  Update_Bank_Fail
} from "../types";
import store from "../index";
export const fetchBanks = dispatch => {
  let token = store.getState().auth.admin.token;

  let config = {
    headers: {
      Authorization: token
    }
  };
  return (
    axios
      .post("https://velooo.herokuapp.com/api/admin/banks", null, config)
      //   .get("https://velooo.herokuapp.com/api/admin/banks", null, config)
      .then(e => {
        console.log(e.data);
        dispatch({ type: Fetch_Banks, payload: e.data });
      })
      .catch(err => {
        console.log(err.response);
      })
  );
};
export const updateBanks = (payload, dispatch) => {
  let token = store.getState().auth.admin.token;
  dispatch({ type: Update_Bank_Attemp, payload: payload.index });
  let config = {
    headers: {
      Authorization: token
    }
  };
  return (
    axios
      .post("https://velooo.herokuapp.com/api/admin/banks/update", payload, config)
      //   .post("https://velooo.herokuapp.com/api/admin/banks/update", payload, config)

      //   .get("https://velooo.herokuapp.com/api/trip/prices", null, config)
      .then(e => {
        dispatch({ type: Update_Bank_Success, payload: payload.index });
      })
      .catch(err => {
        console.log(err.response);
        dispatch({ type: Update_Bank_Fail, payload: payload.index });
      })
  );
};
