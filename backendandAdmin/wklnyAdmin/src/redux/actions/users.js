import axios from "axios";

import {
  Fetch_Users_Attemp,
  Fetch_Users_Success,
  Fetch_Users_Fail,
  Delete_User_Attemp,
  Delete_User_Success,
  Delete_User_Fail,
  Add_User_Attemp,
  Add_User_Success,
  Add_User_Fail
} from "../types";
import store from "../index";
import { base_url } from "../config";
import { getCount } from "./count";

export const GetAllUsers = (dispatch, page) => {
  let token = store.getState().auth.admin.token;
  dispatch({ type: Fetch_Users_Attemp });
  let config = {
    headers: {
      Authorization: token
    }
  };
  return axios
    .post(base_url("admin/users"), { page }, config)
    .then(e => {
      let data =
        page === 0 ? e.data : [...store.getState().users.users, ...e.data];
      dispatch({ type: Fetch_Users_Success, payload: data });
    })
    .catch(e => {
      dispatch({ type: Fetch_Users_Fail, error: e.response });
    });
};

export const DeleteUser = (user_id, dispatch) => {
  let token = store.getState().auth.admin.token;
  dispatch({ type: Delete_User_Attemp });

  let headers = {
    Authorization: token
  };
  const data = {
    user_id
  };
  return axios
    .delete(base_url("admin/users/delete"), {
      headers,
      data
    })
    .then(e => {
      dispatch({ type: Delete_User_Success, payload: e.data });
      getCount(dispatch);
      GetAllUsers(dispatch, 0);
    })
    .catch(e => {
      dispatch({ type: Delete_User_Fail, error: e.response });
    });
};

export const AddUser = (payload, dispatch) => {
  let token = store.getState().auth.admin.token;
  dispatch({ type: Add_User_Attemp });
  let config = {
    headers: {
      Authorization: token
    }
  };
  return axios
    .post(base_url("admin/users/add"), payload, config)
    .then(e => {
      dispatch({ type: Add_User_Success, payload: e.data });
    })
    .catch(e => {
      console.log(e.response.data);
      dispatch({ type: Add_User_Fail, payload: e.response.data });
    });
};
