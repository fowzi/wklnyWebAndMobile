import axios from "axios";

import {
  Fetch_Users_Fail,
  Fetch_Trips_Attemp,
  Fetch_Trips_Success,
  Fetch_RunningOrders_Success,
  Fetch_RunningOrders_Fail,
  Fetch_DoneOrders_Success,
  Fetch_DoneOrders_Attemp,
  Fetch_RunningOrders_Attemp,
  Fetch_Problems_Orders_Attemp,
  Fetch_Problems_Orders_Success,
  Fetch_Problems_Orders_Fail
} from "../types";
import store from "../index";
import { base_url } from "../config";

export const GetProblemServices = (page, dispatch) => {
  let token = store.getState().auth.admin.token;
  dispatch({ type: Fetch_Problems_Orders_Attemp });
  let config = {
    headers: {
      Authorization: token
    }
  };
  return axios
    .get(base_url("admin/problemorders/" + page), config)
    .then(e => {
      console.log("====================================");
      console.log(e.data);
      console.log("====================================");
      dispatch({ type: Fetch_Problems_Orders_Success, payload: e.data.orders });
    })
    .catch(e => {
      dispatch({ type: Fetch_Problems_Orders_Fail, error: e.response });
    });
};
export const GetFinishedServices = dispatch => {
  let token = store.getState().auth.admin.token;
  dispatch({ type: Fetch_DoneOrders_Attemp });
  let config = {
    headers: {
      Authorization: token
    }
  };
  return axios
    .get(base_url("admin/doneorders"), config)
    .then(e => {
      dispatch({ type: Fetch_DoneOrders_Success, payload: e.data.result });
    })
    .catch(e => {
      dispatch({ type: Fetch_RunningOrders_Fail, error: e.response });
    });
};
export const GetRunningServices = dispatch => {
  let token = store.getState().auth.admin.token;
  dispatch({ type: Fetch_RunningOrders_Attemp });
  let config = {
    headers: {
      Authorization: token
    }
  };
  return axios
    .get(base_url("admin/runningorders"), null, config)
    .then(e => {
      dispatch({ type: Fetch_RunningOrders_Success, payload: e.data });
    })
    .catch(e => {
      dispatch({ type: Fetch_RunningOrders_Fail, error: e.response });
    });
};

export const GetAllTrips = dispatch => {
  let token = store.getState().auth.admin.token;
  dispatch({ type: Fetch_Trips_Attemp });
  let config = {
    headers: {
      Authorization: token
    }
  };
  return axios
    .post("https://velooo.herokuapp.com/api/admin/trips", null, config)
    .then(e => {
      dispatch({ type: Fetch_Trips_Success, payload: e.data });
    })
    .catch(e => {
      dispatch({ type: Fetch_Users_Fail, error: e.response });
    });
};
export const deleteTrip = (trip_id, dispatch) => {
  let token = store.getState().auth.admin.token;
  dispatch({ type: Fetch_Trips_Attemp });
  let headers = {
    Authorization: token
  };
  let data = { trip_id: trip_id };
  return (
    axios
      // .post("https://velooo.herokuapp.com/api/admin/trips", null, config)
      .delete("https://velooo.herokuapp.com/api/admin/trips/delete", {
        headers,
        data
      })
      .then(e => {
        GetAllTrips(dispatch);
      })
  );
};
