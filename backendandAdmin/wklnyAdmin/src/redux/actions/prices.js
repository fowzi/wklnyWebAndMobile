import axios from "axios";
import {
  Fetch_Trips_Price,
  Update_Trips_Price_Success,
  Update_Trips_Price_Attemp,
  Update_Trips_Price_Fail
} from "../types";
import store from "../index";
export const fetchPrices = dispatch => {
  let token = store.getState().auth.admin.token;

  dispatch({ type: Fetch_Trips_Price });
  let config = {
    headers: {
      Authorization: token
    }
  };
  return (
    axios
      .get("https://velooo.herokuapp.com/api/trip/prices", null, config)
      //   .get("https://velooo.herokuapp.com/api/trip/prices", null, config)
      .then(e => {
        console.log(e);
        dispatch({ type: Fetch_Trips_Price, payload: e.data[0] });
      })
      .catch(err => {
        console.log(err.response);
      })
  );
};
export const updatePrices = (payload, dispatch) => {
  let token = store.getState().auth.admin.token;
  dispatch({ type: Update_Trips_Price_Attemp });
  let config = {
    headers: {
      Authorization: token
    }
  };
  return (
    axios
      .post(
        "https://velooo.herokuapp.com/api/admin/prices/update",
        // "http://localhost:5000/api/admin/prices/update",
        payload,
        config
      )
      //   .get("https://velooo.herokuapp.com/api/trip/prices", null, config)
      .then(e => {
        dispatch({ type: Update_Trips_Price_Success, payload: e.data });
      })
      .catch(err => {
        dispatch({ type: Update_Trips_Price_Fail, payload: err.response });
      })
  );
};
