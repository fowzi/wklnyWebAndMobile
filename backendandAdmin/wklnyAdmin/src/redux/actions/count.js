import axios from "axios";

import { Get_Count_Attemp, Get_Count_Success, Get_Count_Fail } from "../types";
import store from "../index";
import { base_url } from "../config";

export const getCount = dispatch => {
  dispatch({ type: Get_Count_Attemp });
  let token = store.getState().auth.admin.token;

  let config = {
    headers: {
      Authorization: token
    }
  };
  return axios
    .post(base_url("admin/counts"), null, config)
    .then(e => {
      dispatch({ type: Get_Count_Success, payload: e.data });
    })
    .catch(e => {
      dispatch({ type: Get_Count_Fail, error: e.response });
    });
};
