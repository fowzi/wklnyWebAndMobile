import axios from "axios";

import {
  Fetch_Drivers_Attemp,
  Fetch_Drivers_Success,
  Fetch_Drivers_Fail,
  Delete_Driver_Attemp,
  Delete_Driver_Success,
  Delete_Driver_Fail,
  Disable_Driver_Attemp,
  Disable_Driver_Success,
  Disable_Driver_Fail,
  Add_Driver_Attemp,
  Add_Driver_Success,
  Add_Driver_Fail,
  Fetch_Driver_Details_Attemp,
  Fetch_Driver_Details_Fail,
  Fetch_Driver_Details_Success
} from "../types";
import store from "../index";
import { base_url } from "../config";

export const FetchAllDrivers = (dispatch, page) => {
  dispatch({ type: Fetch_Drivers_Attemp });
  let token = store.getState().auth.admin.token;

  let config = {
    headers: {
      Authorization: token
    }
  };
  return axios
    .post(base_url("admin/providers"), { page }, config)
    .then(e => {
      let data =
        page === 0 ? e.data : [...store.getState().drivers.drivers, ...e.data];
      dispatch({ type: Fetch_Drivers_Success, payload: data });
    })
    .catch(e => {
      dispatch({ type: Fetch_Drivers_Fail, error: e.response });
    });
};
export const DisableDriver = (driver_id, verfied, dispatch) => {
  dispatch({ type: Disable_Driver_Attemp });
  let token = store.getState().auth.admin.token;

  let config = {
    headers: {
      Authorization: token
    }
  };
  return axios
    .post(base_url("admin/drivers/disable"), { driver_id, verfied }, config)
    .then(e => {
      dispatch({ type: Disable_Driver_Success, payload: e.data });
      // FetchAllDrivers(dispatch);
    })
    .catch(e => {
      dispatch({ type: Disable_Driver_Fail, error: e.response });
    });
};

export const DeleteDriver = (driver_id, dispatch) => {
  dispatch({ type: Delete_Driver_Attemp });
  let token = store.getState().auth.admin.token;
  let headers = {
    Authorization: token
  };
  const data = {
    driver_id
  };
  return axios
    .delete(base_url("admin/drivers/delete"), {
      headers,
      data
    })
    .then(e => {
      dispatch({ type: Delete_Driver_Success, payload: e.data });
      FetchAllDrivers(dispatch, 0);
    })
    .catch(e => {
      dispatch({ type: Delete_Driver_Fail, error: e.response });
    });
};

export const AddDriver = (payload, dispatch) => {
  dispatch({ type: Add_Driver_Attemp });
  let token = store.getState().auth.admin.token;

  let config = {
    headers: {
      Authorization: token
    }
  };
  return axios
    .post(base_url("admin/drivers/add"), payload, config)
    .then(e => {
      console.log(e);
      dispatch({ type: Add_Driver_Success, payload: e.data });
    })
    .catch(e => {
      console.log(e.response.data);
      dispatch({ type: Add_Driver_Fail, payload: e.response.data });
    });
};
export const FetchDriverDetails = (payload, dispatch) => {
  dispatch({ type: Fetch_Driver_Details_Attemp });
  let token = store.getState().auth.admin.token;

  let config = {
    headers: {
      Authorization: token
    }
  };
  // driver_id
  return axios
    .post(base_url("admin/drivers/details"), payload, config)
    .then(e => {
      console.log("====================================");
      console.log(e.data);
      console.log("====================================");
      let data = [
        {
          name: "الاسم",
          details: e.data.name,
          type: "text",
          _id: e.data._id
        },
        {
          name: "المدينة",
          details: (e.data.city && e.data.city.name) || "---",
          type: "text"
        },
        {
          name: "البريد الإلكتروني",
          details: e.data.email,
          type: "text"
        },
        // {
        //   name: "التقيم",
        //   details: e.data.driverRating,
        //   type: "text"
        // },
        {
          name: "الهاتف",
          details: e.data.phone,
          type: "text"
        },
        {
          name: "الرصيد",
          details: `${e.data.balance ? e.data.balance : 0} RS`
        },
        {
          name: " البنك",
          details: `${e.data.bankName ? e.data.bankName : "----"}`
        },

        {
          name: "الحساب البنكي",
          details: `${e.data.bankAccount ? e.data.bankAccount : "----"}`
        }
      ];
      dispatch({ type: Fetch_Driver_Details_Success, payload: data });
    })
    .catch(e => {
      console.log(e);
      console.log(e.response.data);
      dispatch({ type: Fetch_Driver_Details_Fail, payload: e.response.data });
    });
};
