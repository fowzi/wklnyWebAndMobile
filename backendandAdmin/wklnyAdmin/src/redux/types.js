/**
 *                                          driver
 */

export const Fetch_Drivers_Attemp = "fetch-driver-attemp";
export const Fetch_Drivers_Success = "fetch-driver-success";
export const Fetch_Drivers_Fail = "fetch-driver-fail";

export const adminLoginAttemp = "admin-login-attemp";
export const adminLoginSuccess = "admin-login-success";
export const adminLoginFailed = "admin-login-failed";
export const adminLogOut = "admin-log-out";

export const Fetch_Users_Attemp = "fetch-users-attemp";
export const Fetch_Users_Success = "fetch-users-success";
export const Fetch_Users_Fail = "fetch-users-fail";

export const Delete_Driver_Attemp = "delete-driver-attemp";
export const Delete_Driver_Success = "delete-driver-success";
export const Delete_Driver_Fail = "delete-driver-fail";

export const Disable_Driver_Attemp = "disabele-driver-attemp";
export const Disable_Driver_Success = "disable-driver-success";
export const Disable_Driver_Fail = "diable-driver-fail";

export const Delete_User_Attemp = "delete-user-attemp";
export const Delete_User_Success = "delete-user-success";
export const Delete_User_Fail = "delete-user-fail";

/**
 *                                          statics
 */

export const Get_Count_Attemp = "get-count-attemp";
export const Get_Count_Success = "get-count-success";
export const Get_Count_Fail = "get-count-fail";
export const Add_User_Attemp = "Add-user-attemp";
export const Add_User_Success = "Add-user-success";
export const Add_User_Fail = "Add-user-fail";
export const Add_User_Reset = "Add-user-reset";

export const Add_Driver_Attemp = "Add-driver-attemp";
export const Add_Driver_Success = "Add-driver-success";
export const Add_Driver_Fail = "Add-driver-fail";
export const Add_Driver_Reset = "Add-driver-reset";

export const Fetch_Trips_Attemp = "fetch-Trips-attemp";
export const Fetch_Trips_Success = "fetch-Trips-success";
export const Fetch_Trips_Fail = "fetch-Trips-fail";
export const Fetch_Trips_Reset = "fetch-Trips-reset";

export const Fetch_RunningOrders_Attemp = "Fetch_RunningOrders-attemp";
export const Fetch_RunningOrders_Success = "Fetch_RunningOrders-success";
export const Fetch_RunningOrders_Fail = "Fetch_RunningOrders-fail";
export const Fetch_RunningOrders_Reset = "Fetch_RunningOrders-reset";

export const Fetch_DoneOrders_Attemp = "Fetch_DoneOrders_Attemp";
export const Fetch_DoneOrders_Success = "Fetch_DoneOrders-success";
export const Fetch_DoneOrders_Fail = "Fetch_DoneOrders-fail";
export const Fetch_DoneOrders_Reset = "Fetch_DoneOrders-reset";

export const Fetch_Problems_Orders_Attemp = "Fetch_Problems_Orders_Attemp";
export const Fetch_Problems_Orders_Success = "Fetch_Problems_Orders-success";
export const Fetch_Problems_Orders_Fail = "Fetch_Problems_Orders-fail";
export const Fetch_Problems_Orders_Reset = "Fetch_Problems_Orders-reset";

export const Fetch_Trips_Price = "fetch-Trips-prices";
export const Update_Trips_Price_Attemp = "Update-Trips-Prices-Attemp";
export const Update_Trips_Price_Success = "Update-Trips-Prices-success";
export const Update_Trips_Price_Fail = "Update-Trips-Prices-fail";
export const Update_Trips_Price_Reset = "Update-Trips-Prices-reset";

export const Fetch_Banks = "fetch-Banks";
export const Update_Bank_Attemp = "Update-Bank-Attemp";
export const Update_Bank_Success = "Update-Bank-success";
export const Update_Bank_Fail = "Update-Bank-fail";
export const Update_Bank_Reset = "Update-Bank-reset";

export const Fetch_Driver_Details_Attemp = "Fetch-Driver-Details-Attemp";
export const Fetch_Driver_Details_Success = "Fetch-Driver-Details-Success";
export const Fetch_Driver_Details_Fail = "Fetch-Driver-Details-Fail";
export const Fetch_Driver_Details_Reset = "Fetch-Driver-Details-Reset";
