import { Types } from "mongoose";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const ContractsSchema = new Schema({
  from: {
    type: Schema.Types.ObjectId,
    required: true
  },
  room: {
    type: Types.ObjectId,
    ref: "users",
    required: true
  },
  sendertype: {
    type: String,
    required: true
  },
  text: {
    type: String,
    required: true
  },
  texttype: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  }
});
export default mongoose.model("contracts", ContractsSchema);
