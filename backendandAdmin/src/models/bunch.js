import { Types } from "mongoose";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const BunchSchema = new Schema({
  unit: {
    type: Number,
    required: true
  },
  date: {
    type: Number,
    default: Date.now
  }
});

export default mongoose.model("bunches", BunchSchema);
