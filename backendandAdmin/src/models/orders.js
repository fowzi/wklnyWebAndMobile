import { Schema, Types, model } from "mongoose";

const orderSchema = new Schema({
  user: {
    type: Types.ObjectId,
    ref: "users",
    autopopulate: false,
    required: true
  },
  strip: {
    type: Types.ObjectId,
    ref: "strips",
    autopopulate: true
  },
  service: {
    type: Types.ObjectId,
    ref: "subservice",
    required: true,
    autopopulate: true
  },
  city: {
    type: Types.ObjectId,
    ref: "cites",
    autopopulate: true
  },
  pidfrom: {
    type: Number,
    required: true
  },
  pidto: {
    type: Number,
    required: true
  },
  details: {
    type: String,
    required: true
  },
  files: {
    type: [Object],
    default: []
  },
  status: {
    //0 not  accepted yet,1 user accepted ,2 driver say i done, 3 user says done is ok
    type: Number,
    default: 0
  },
  date: {
    type: Date,
    default: Date.now
  }
});
orderSchema.plugin(require("mongoose-autopopulate"));
export default model("orders", orderSchema);
