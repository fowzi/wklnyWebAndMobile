import { Types } from "mongoose";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const ProviderSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  phone: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  city: {
    type: Types.ObjectId,
    ref: "cites",
    autopopulate: true
  },
  balance: {
    type: Number,
    default: 0
  },
  bankName: {
    type: String
  },
  bankAccount: {
    type: String
  },
  avatar: {
    type: String,
    default:
      "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQy08cK8wogTcvUJYvty4hAPwvKxTIJEqneUkNc3r4CBLkroZyn"
  },
  date: {
    type: Date,
    default: Date.now
  },
  token: {
    type: String
  },
  online: {
    type: Boolean,
    default: true
  },
  verfied: {
    type: Boolean,
    default: false
  },
  rate: {
    type: Number,
    default: 0
  },
  deleted: {
    type: Boolean,
    default: false
  }
});
ProviderSchema.plugin(require("mongoose-autopopulate"));

export default mongoose.model("providers", ProviderSchema);
