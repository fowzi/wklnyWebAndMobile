const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const serviceSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  strip: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "strips",
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  deleted: {
    type: Boolean,
    default: false
  }
});
export default mongoose.model("service", serviceSchema);
