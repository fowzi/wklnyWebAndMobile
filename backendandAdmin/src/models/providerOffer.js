import { Schema, Types, model } from "mongoose";

const providerorders = new Schema({
  order: {
    type: Types.ObjectId,
    ref: "orders",
    autopopulate: false,
    required: true
  },
  provider: {
    type: Types.ObjectId,
    ref: "providers",
    autopopulate: true
  },
  introduction: {
    type: String,
    required: true
  },
  time: {
    type: String,
    required: true
  },
  price: {
    type: String,
    required: true
  },
  providerRate: {
    type: Number
  },
  userRate: {
    type: Number,
    default: 0
  },
  status: {
    type: Number,
    default: 0
  },
  isProblem: {
    type: Boolean,
    default: false
  },
  problemIsSolved: {
    type: Boolean,
    default: false
  }
});
providerorders.plugin(require("mongoose-autopopulate"));
export default model("providerorders", providerorders);
