import mongoose, { Schema } from "mongoose";

const CitiesSchema = new Schema({
  name: { type: String, required: true },
  createdAt: {
    type: Date,
    default: Date.now
  },
  deleted: {
    type: Boolean,
    default: false
  }
});
export default mongoose.model("cites", CitiesSchema);
