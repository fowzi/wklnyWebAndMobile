const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const paytabsSchema = new Schema({
  merchant_email: {
    type: String,
    default: "ebrahimhassan121@gmail.com"
  },
  secret_key: {
    type: String,
    default:
      "e4nfwo4dVPHBYexJsBl1KxTpLpRP0hWfljuRIz4EoMgn1N6tpsv90pyMPpBPHx52RATA9YfyLwVNk2gJvMSvbrh0RTQyrVAjwuTF"
  },
  site_url: {
    type: String,
    default: "could-b.com"
  }
});
export default mongoose.model("paytabs", paytabsSchema);
