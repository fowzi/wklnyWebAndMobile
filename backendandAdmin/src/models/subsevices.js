const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const subserviceSchema = new Schema({
  type: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "service",
    required: true,
    autopopulate: true
  },
  name: {
    type: String,
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
  deleted: {
    type: Boolean,
    default: false
  }
});
subserviceSchema.plugin(require("mongoose-autopopulate"));
export default mongoose.model("subservice", subserviceSchema);
