import mongoose, { Schema } from "mongoose";
import generateUniqueKey from "mongoose-generate-unique-key";
const ForgetPasswordsSchema = new Schema({
  user: { type: String, required: true },
  email: { type: String },
  userType: {
    type: String
  },
  code: { type: String },
  createdAt: {
    type: Date,
    default: Date.now
  },
  deleted: {
    type: Boolean,
    default: false
  }
});
ForgetPasswordsSchema.plugin(
  generateUniqueKey("code", () => String(Math.floor(Math.random() * 1000000)))
);
export default mongoose.model("forgetpasswordscode", ForgetPasswordsSchema);
