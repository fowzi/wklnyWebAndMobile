const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const PhoneCodes = new Schema({
  phone: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  code: {
    type: String,
    required: true
  }
});
export default mongoose.model("phonecodes", PhoneCodes);
