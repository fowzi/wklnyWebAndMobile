import { Schema, Types, model } from "mongoose";

const StripSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now
  },
  deleted: {
    type: Boolean,
    default: false
  }
});
export default model("strips", StripSchema);
