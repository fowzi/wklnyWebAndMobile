import { Types } from "mongoose";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const chatSchema = new Schema({
  from: {
    type: Schema.Types.ObjectId,
    required: true
  },
  room: {
    type: Types.ObjectId,
    required: true
  },
  sendertype: {
    type: String,
    required: true
  },
  text: {
    type: String,
    required: true
  },
  texttype: {
    type: String,
    required: true
  },
  isProblem: {
    type: Boolean,
    default: false
  },
  seen: {
    type: Boolean,
    default: false
  },
  date: {
    type: Date,
    default: Date.now
  },
  problemSolved: {
    type: Boolean,
    default: false
  }
});
export default mongoose.model("chat", chatSchema);
