import { Types } from "mongoose";

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const DriverServiceSchema = new Schema({
  provider: {
    type: String,
    required: true,
    ref: "providers",
    autopopulate: false
  },
  service: {
    type: String,
    required: true,
    ref: "subservice",
    autopopulate: false
  },
  date: {
    type: Date,
    default: Date.now
  }
});
DriverServiceSchema.plugin(require("mongoose-autopopulate"));
export default mongoose.model("providerservice", DriverServiceSchema);
