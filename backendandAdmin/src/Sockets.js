import Chat from "./models/Chat";
import { socketAuth } from "./config/passport";
import orders from "./models/orders";
import providerOffer from "./models/providerOffer";
import { sendNotification } from "./config/SendNotifiction";
export const SocketsFunctions = io => {
  io.on("connection", socket => {
    socket.on("joinroom", function(payload) {
      let roomName = payload.room;
      socket.join(roomName);
      socketAuth(payload.token).then(auth => {
        if (!auth) {
          socket.emit("chat", "error");
          return;
        }
        socket.idF = auth._id;
        socket.userType = auth.type;
      });
    });
    socket.on("send", async payload => {
      const token = payload.token;
      let auth = await socketAuth(token);
      if (!auth) {
        socket.emit("chat", "error");
        return;
      }
      const { text, texttype, room, isProblem, userOrderDone } = payload;
      let roomName = room;
      let new_payload = {
        text,
        texttype,
        from: auth._id,
        room,
        sendertype: auth.type,
        isProblem: isProblem === true ? true : false
      };
      await new Chat(new_payload).save().then(chat => {
        io.sockets.in(roomName).emit("send", chat);
        if (new_payload.isProblem == true) {
          providerOffer
            .findOneAndUpdate(
              { order: room + "", status: { $ne: 0 } },
              { isProblem: true }
            )
            .lean(true)
            .then(updated => {});
        }
        io.in(roomName).clients(async (err, clients) => {
          const ArrconnectedUsersTypes = await clients.map(e => {
            return io.sockets.connected[e].userType;
          });
          if (ArrconnectedUsersTypes.indexOf("users") < 0) {
            await providerOffer
              .findOne({ order: roomName })
              .populate({
                path: "order",
                select: {
                  _id: 1,
                  service: 1,
                  user: 1,
                  details: 1,
                  status: 1,
                  city: 1,
                  files: "$files.size",
                  status: 1
                },
                populate: {
                  path: "service city user",
                  select: {
                    avatar: 0,
                    password: 0,
                    city: 0,
                    phone: 0,
                    balance: 0,
                    email: 0
                  }
                }
              })
              .populate("order.user", "_id name token")
              .populate("provider", "name _id rate ")
              .lean(true)
              .then(result => {
                const message = {
                  user: result.order.user,
                  provider: result.provider,
                  service: result.order.service,
                  offer: {
                    _id: result._id,
                    status: result.status,
                    price: result.price,
                    time: result.time,
                    introduction: result.introduction
                  },
                  order: {
                    ...result.order,
                    files: result.order.files.map(f => f.name)
                  },
                  files: result.order.files.map(f => f.name),
                  room: result.order._id,
                  _id: result.order._id
                };
                const userToken = result.order.user.token;
                sendNotification(
                  "رسالة جديدة من " + auth.name,
                  text,
                  { message, text: text, notificationType: "chat" },
                  userToken
                );
                console.log("====================================");
                console.log(result, message);
                console.log("====================================");
              });
          }
          if (ArrconnectedUsersTypes.indexOf("providers") < 0) {
            await providerOffer
              .findOne({ order: roomName })
              .populate({
                path: "order",
                select: {
                  _id: 1,
                  service: 1,
                  user: 1,
                  details: 1,
                  status: 1,
                  city: 1,
                  files: "$files.size",
                  status: 1
                },
                populate: {
                  path: "service city user",
                  select: {
                    avatar: 0,
                    password: 0,
                    city: 0,
                    phone: 0,
                    balance: 0,
                    email: 0
                  }
                }
              })
              .populate("order.user", "_id name")
              .populate("provider", "name _id rate token ")
              .lean(true)
              .then(result => {
                const message = {
                  user: result.order.user,
                  provider: result.provider,
                  service: result.order.service,
                  offer: {
                    _id: result._id,
                    status: result.status,
                    price: result.price,
                    time: result.time,
                    introduction: result.introduction
                  },
                  order: {
                    ...result.order,
                    files: result.order.files.map(f => f.name)
                  },
                  files: result.order.files.map(f => f.name),
                  room: result.order._id,
                  _id: result.order._id
                };
                const userToken = result.provider.token;
                sendNotification(
                  "رسالة جديدة من " + auth.name,
                  text,
                  { message, text: text, notificationType: "chat" },
                  userToken
                );
              });

            // await providerOffer
            //   .findOne({ order: roomName }, "provider")
            //   .then(r => {
            //     const providerToken = r.provider.token;
            //     sendNotification(
            //       "رسالة جديدة من " + auth.name,
            //       text,
            //       { roomId: roomName, text: text, notificationType: "chat" },
            //       providerToken
            //     );
            //   });
          }
        });
      });
    });
  });
};
