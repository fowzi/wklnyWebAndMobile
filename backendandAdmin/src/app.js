import express from "express";
import path from "path";
import cookieParser from "cookie-parser";
import logger from "morgan";
import indexRouter from "./routes/index";
import mongoose from "mongoose";
import passport from "passport";
import bcrypt from "bcryptjs";
import keys from "./config/keys";
import Admin from "./models/admin";
import cors from "cors";
import Socket from "socket.io";
import Http from "http";
import jwt_decode from "jwt-decode";
import zip from "express-easy-zip";

// import paytabs from "paytabs";
const app = express();
app.use(logger("dev"));
app.use(cors());
app.use(express.json({ limit: "50mb" }));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
import passportConfig from "./config/passport";
import bunch from "./models/bunch";
import PAYTABSModel from "./models/paytabs";
import User from "./models/User";
import Provider from "./models/Provider";
import forgetPassword from "./models/forgetPassword";
import request from "request";
// import Chat from "./models/Chat";
passportConfig(passport);

const mailer = require("express-mailer"); // call express
// Configure express-mail and setup default mail data.
mailer.extend(app, {
  from: "admin@wklny.com",
  host: "smtp.gmail.com", // hostname
  secureConnection: true, // use SSL
  port: 465, // port for secure SMTP
  transportMethod: "SMTP", // default is SMTP. Accepts anything that nodemailer accepts
  auth: {
    user: "wklnyapp@gmail.com", // gmail id
    pass: "wklny123456" // gmail password
  }
});

mongoose.connect(keys.mongoURI).then(() => {
  console.log("db connected");
});
mongoose.connection.on("error", err => {
  console.log(`DB connection error: ${err.message}`);
});
app.set("views", __dirname + "/views");
app.set("view engine", "pug");
const sendEmail = (email, user, code, res) => {
  const mailOptions = {
    to: email,
    subject: "reset Password - اعادة تعيين كلمة المرور -   تطبيق وكلني",
    user: {
      // data to view template, you can access as - user.name
      name: user.name,
      message: "الكود الخاص بإعادة تعيين كلمة المرور",
      code
    }
  };
  // Send email.
  app.mailer.send("email", mailOptions, function(err, message) {
    if (err) {
      console.log(err);
      res.send("البريد الإلكتروني غير صحيح");
      return;
    }
    return res.json({ success: "برجاء مراجعة البريد الالكتروني" });
  });
};
app.post("/forgetpassword", (req, res) => {
  const email = req.body.email;
  if (!email)
    return res.status(400).json({ error: "يجب كتابة البريد الالكتروني" });
  User.findOne({ email }, "_id name")
    .lean(true)
    .then(user => {
      if (user) {
        forgetPassword.deleteMany({ user: user._id }).then(() => {
          new forgetPassword({
            email,
            user: user._id,
            userType: "users"
          })
            .save()
            .then(result => {
              sendEmail(email, user, result.code, res);
            });
        });
      } else {
        Provider.findOne({ email }, "_id name")
          .lean(true)
          .then(provider => {
            if (provider) {
              forgetPassword.deleteMany({ user: provider._id }).then(() => {
                new forgetPassword({
                  user: provider._id,
                  userType: "providers"
                })
                  .save()
                  .then(result => {
                    sendEmail(email, user, result.code, res);
                  });
              });
            } else {
              return res
                .status(400)
                .json({ error: "البريد الإلكتروني غير موجود" });
            }
          });
      }
    });
});

Admin.findOne({}).then(result => {
  if (result) return;
  bcrypt.genSalt(10, (err, salt) => {
    bcrypt.hash("123456", salt, (err, hash) => {
      if (err) throw err;
      new Admin({
        name: "admin",
        email: "admin@gmail.com",
        password: hash
      }).save();
    });
  });
});
bunch.find({}).then(result => {
  if (!result || result.length < 1) {
    new bunch({
      unit: 100
    }).save();
    new bunch({
      unit: 200
    }).save();
    new bunch({
      unit: 300
    }).save();
  }
});

PAYTABSModel.findOne({}).then(result => {
  if (result) return;
  new PAYTABSModel({}).save();
});
app.use("/api", indexRouter);

app.use(express.static(path.join(__dirname, "../wklnyAdmin/build/")));
app.get("*", (req, res) => {
  res.sendFile(
    path.resolve(__dirname, "../", "wklnyAdmin", "build", "index.html")
  );
});

// let options = {
//   method: "POST",
//   url: "http://www.shamelsms.net/api/httpSMS.aspx",
//   qs: {
//     username: "skanfake",
//     password: "114477",
//     message: "Verfiy Code 123456",
//     mobile: "9665064610d32",
//     sender: "Bashrh"
//   }
// };

// request(options, function(error, response, body) {
//   // if (error) throw new Error(error);

//   console.log(response.statusMessage, response.statusCode, body);
// });
export default app;
