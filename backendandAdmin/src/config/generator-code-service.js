generator = require("password-generator");
const generateVerifyCode = () => generator(4, false, /\d/);

module.exports = generateVerifyCode;
