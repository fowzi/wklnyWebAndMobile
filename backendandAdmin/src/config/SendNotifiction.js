const admin = require("firebase-admin");

const serviceAccount = require("./service_account.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://wklny-48e46.firebaseio.com"
});
export const sendNotification = async (title, body, data, token) => {
  if (!token) {
    console.log("token not valid");
    return;
  }
  const payload = {
    notification: {
      title,
      body,
      sound: "default"
    },
    data: { data: JSON.stringify(data) }
  };
  const options = {
    priority: "high"
  };

  await admin.messaging().sendToDevice(token, payload, options);
  return;
};
