const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
import User from "../models/User";
import Drivers from "../models/Provider";
import admin from "../models/admin";
import keys from "./keys";
import jwt_decode from "jwt-decode";
const opts = {};
opts.secretOrKey = keys.secretOrKey;
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();

export default passport => {
  passport.use(
    new JwtStrategy(opts, (jwt_payload, done) => {
      admin.findById(jwt_payload.id, { avatar: 0 }).then(admin => {
        if (!admin) {
          return User.findOne(
            { _id: jwt_payload.id + "", deleted: { $ne: true } },
            { avatar: 0 }
          )
            .then(user => {
              if (user) {
                if (true || jwt_payload.password == user.password)
                  return done(null, { ...user._doc, type: "user" });
                return done(null, false);
              } else {
                Drivers.findById(
                  { _id: jwt_payload.id + "", deleted: { $ne: true } },
                  { avatar: 0 }
                ).then(driver => {
                  if (driver) {
                    if (true || jwt_payload.password == user.password)
                      return done(null, { ...driver._doc, type: "driver" });
                    return done(null, false);
                  } else {
                    return done(null, false);
                  }
                });
              }
            })
            .catch(err => console.log(err));
        } else {
          return done(null, { ...admin._doc, type: "admin" });
        }
      });
    })
  );
};
export const socketAuth = async token => {
  try {
    let decoded = await jwt_decode(token + "");
    return await admin.findById(decoded.id + "", { avatar: 0 }).then(admin => {
      if (!admin) {
        return User.findById(decoded.id + "", { avatar: 0 }).then(user => {
          if (user) {
            return { ...user._doc, type: "users" };
          } else {
            return Drivers.findById(decoded.id + "", { avatar: 0 }).then(
              driver => {
                if (driver) {
                  return { ...driver._doc, type: "providers" };
                } else {
                  return null;
                }
              }
            );
          }
        });
      } else {
        return { ...admin._doc, type: "admin" };
      }
    });
  } catch (error) {
    return null;
  }
};
