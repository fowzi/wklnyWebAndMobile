const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateRegisterInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : "";
  data.email = !isEmpty(data.email) ? data.email : "";
  data.password = !isEmpty(data.password) ? data.password : "";
  data.password2 = !isEmpty(data.password2) ? data.password2 : "";
  data.phone = !isEmpty(data.phone) ? data.phone : "";
  // data.token = !isEmpty(data.token) ? data.token : "";
  data.city = !isEmpty(data.city) ? data.city : "";

  if (!Validator.isLength(data.name, { min: 2, max: 30 })) {
    errors.name = "Name must be between 2 and 30 characters";
  }
  if (Validator.isEmpty(data.name)) {
    errors.name = "خانة الاسم مطلوبة";
  }
  if (Validator.isEmpty(data.phone)) {
    errors.phone = "حانة الهاتف مطلوبة";
  }
  if (Validator.isEmpty(data.email)) {
    errors.email = "خانة البريد مطلوبة";
  }

  if (!Validator.isEmail(data.email)) {
    errors.email = "هذا البريد غير صحيح";
  }

  if (Validator.isEmpty(data.password)) {
    errors.password = "حقل كلمة المرور";
  }

  if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = "يجب ان تحتوي خانة كلمة المرور علي ٦ أحرف علي الأقل";
  }

  if (Validator.isEmpty(data.password2)) {
    errors.password2 = "حقل تاكيد كلمة المرور مطلوب";
  }

  if (!Validator.equals(data.password, data.password2)) {
    errors.password2 = "يجب تساوي كلمتي المرور";
  }
  // if (Validator.isEmpty(data.token)) {
  //   errors.token = "يجب تفعيل الاشعارات";
  // }
  if (Validator.isEmpty(data.city)) {
    errors.token = "يجب اختيار المدينة ";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
