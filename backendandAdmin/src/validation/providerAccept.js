const Validator = require("validator");
const isEmpty = require("./is-empty");

export default function validateRegisterInput(data) {
  let errors = {};
  data.introduction = !isEmpty(data.introduction) ? data.introduction : "";
  data.time = !isEmpty(data.time) ? data.time : "";
  data.price = !isEmpty(data.price) ? data.price : "";
  data.order_id = !isEmpty(data.order_id) ? data.order_id : "";

  if (Validator.isEmpty(data.introduction)) {
    errors.introduction = "introduction field is required";
  }
  if (Validator.isEmpty(data.time)) {
    errors.time = "time field is required";
  }
  if (Validator.isEmpty(data.price)) {
    errors.price = "price field is required";
  }
  if (Validator.isEmpty(data.order_id)) {
    errors.order_id = "order_id field is required";
  }
  return {
    errors,
    isValid: isEmpty(errors)
  };
}
