const Validator = require("validator");
const isEmpty = require("./is-empty");

export default function validateRegisterInput(data) {
  let errors = {};
  data.strip = !isEmpty(data.strip) ? data.strip : "";
  data.service = !isEmpty(data.service) ? data.service : "";
  data.city = !isEmpty(data.city) ? data.city : "";
  data.pidfrom = !isEmpty(String(data.pidfrom)) ? data.pidfrom : "";
  data.pidto = !isEmpty(String(data.pidto)) ? data.pidto : "";
  data.details = !isEmpty(data.details) ? data.details : "";

  if (Validator.isEmpty(data.strip)) {
    errors.strip = "strip field is required";
  }
  if (Validator.isEmpty(data.service)) {
    errors.service = "service field is required";
  }
  if (Validator.isEmpty(data.city)) {
    errors.city = "city field is required";
  }
  // if (Validator.isEmpty(data.pidfrom)) {
  //   errors.pidfrom = "pidfrom field is required";
  // }
  // if (Validator.isEmpty(data.pidto)) {
  //   errors.pidto = "pidto field is required";
  // }
  if (Validator.isEmpty(data.details)) {
    errors.details = "details field is required";
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
}
