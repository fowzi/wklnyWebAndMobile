const Validator = require("validator");
const isEmpty = require("../is-empty");

module.exports = function validateRegisterInput(data) {
  let errors = {};
  data.name = !isEmpty(data.name) ? data.name : "";
  data.phone = !isEmpty(data.phone) ? data.phone : "";
  data.city = !isEmpty(data.city) ? data.city : "";
  data.email = !isEmpty(data.email) ? data.email : "";
  data.password = !isEmpty(data.password) ? data.password : "";
  data.password2 = !isEmpty(data.password2) ? data.password2 : "";
  // data.token = !isEmpty(data.token) ? data.token : "";
  data.services =
    (data.services && typeof data.services === "object") ||
    !isEmpty(data.services)
      ? data.services
      : null;

  if (Validator.isEmpty(data.name)) {
    errors.name = "Name field is required";
  }
  if (Validator.isEmpty(data.email)) {
    errors.email = "Email field is required";
  }
  if (!Validator.isEmail(data.email)) {
    errors.email = "Email is invalid";
  }
  if (Validator.isEmpty(data.phone)) {
    errors.phone = "phone field is required";
  }
  if (!Validator.isLength(data.phone, { min: 6 })) {
    errors.phone = "phone must be at least 6 Numbers";
  }
  if (Validator.isEmpty(data.password)) {
    errors.password = "Password field is required";
  }
  if (!Validator.isLength(data.password, { min: 6, max: 30 })) {
    errors.password = "Password must be at least 6 characters";
  }
  if (Validator.isEmpty(data.password2)) {
    errors.password2 = "Confirm Password field is required";
  }
  if (!Validator.equals(data.password, data.password2)) {
    errors.password2 = "Passwords must match";
  }
  // if (Validator.isEmpty(data.token)) {
  //   errors.token = "token field is required";
  // }
  if (Validator.isEmpty(data.city)) {
    errors.city = "city field is required";
  }
  if (!data.services) {
    errors.services = "services field is required";
  }
  if (data.services && typeof data.services === "string") {
    data.services = [data.services];
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
