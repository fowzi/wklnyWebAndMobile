const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateLoginInput(data) {
  let errors = {};

  data.email = data.email ? data.email : "";
  data.password = data.password ? data.password : "";
  // data.token = data.token ? data.token : "";

  if (!Validator.isEmail(data.email)) {
    errors.email = " البريد الالكتروني غير صحيح";
  }
  if (Validator.isEmpty(data.password)) {
    errors.password = "كلمة المرور غير صحيحة";
  }
  // if (Validator.isEmpty(data.token)) {
  //   errors.token = "لابد من تفعيل الاشعارات";
  // }
  return {
    errors,
    isValid: isEmpty(errors)
  };
};
