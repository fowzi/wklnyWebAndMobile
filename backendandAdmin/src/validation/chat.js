const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports =  function validateChat(data) {
  let errors = {};

  data.text = !isEmpty(data.text) ? data.text : "";
  data.type = !isEmpty(data.text) ? data.type : "";
  data.to = !isEmpty(data.to) ? data.to : "";

  if (Validator.isEmpty(data.text)) {
    errors.text = "Message is required";
  }
  if (Validator.isEmpty(data.type)) {
    errors.type = "type is required";
  }
  if (Validator.isEmpty(data.to)) {
    errors.to = "to is required";
  }
  return {
    errors,
    isValid: isEmpty(errors)
  };
};
