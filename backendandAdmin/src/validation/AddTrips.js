const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateAddTripsInput(data) {
  let errors = {};

  data.from_lang = !isEmpty(data.from_lang) ? data.from_lang : "";
  data.from_lat = !isEmpty(data.from_lat) ? data.from_lat : "";
  data.to_lang = !isEmpty(data.to_lang) ? data.to_lang : "";
  data.to_lat = !isEmpty(data.to_lat) ? data.to_lat : "";
  data.price = !isEmpty(data.price) ? data.price : "";
  data.details = !isEmpty(data.details) ? data.details : "";
  data.source = !isEmpty(data.source) ? data.source : "";
  data.destination = !isEmpty(data.destination) ? data.destination : "";
  data.place_id = !isEmpty(data.place_id) ? data.place_id : "";
  data.vehicalType = !isEmpty(data.vehicalType) ? data.vehicalType : "";
  // data.name = !isEmpty(data.name) ? data.name : "";
  // data.phone = !isEmpty(data.phone) ? data.phone : "";

  if (Validator.isEmpty(data.from_lang)) {
    errors.from_lang = "from_lang field is required";
  }
  if (Validator.isEmpty(data.from_lat)) {
    errors.from_lat = "from_lat field is required";
  }
  if (Validator.isEmpty(data.to_lang)) {
    errors.to_lang = "to_lang field is required";
  }
  if (Validator.isEmpty(data.to_lat)) {
    errors.to_lat = "to_lat field is required";
  }
  if (Validator.isEmpty(data.price)) {
    errors.price = "price field is required";
  }
  if (Validator.isEmpty(data.details)) {
    errors.details = "details field is required";
  }
  if (Validator.isEmpty(data.source)) {
    errors.source = "source field is required";
  }
  if (Validator.isEmpty(data.destination)) {
    errors.destination = "destination field is required";
  }
  if (Validator.isEmpty(data.place_id)) {
    errors.place_id = "place_id field is required";
  }
  if (Validator.isEmpty(data.vehicalType)) {
    errors.vehicalType = "vehicalType field is required";
  }
  // if (Validator.isEmpty(data.name)) {
  //   errors.name = "name field is required";
  // }
  // if (Validator.isEmpty(data.phone)) {
  //   errors.phone = "phone field is required";
  // }
  if (!(Number(data.vehicalType) in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9])) {
    errors.vehicalType = "vehicalType field is not correct";
  }
  return {
    errors,
    isValid: isEmpty(errors)
  };
};
