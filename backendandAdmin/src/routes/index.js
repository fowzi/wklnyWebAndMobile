var express = require("express");
var router = express.Router();
import UserRouter from "./users";
import ProviderRouter from "./Provider";
import AdminRouter from "./admin";
import Chat from "./chat";
import UserModel from "../models/User";
import PaytabsModel from "../models/paytabs";
import ProviderModel from "../models/Provider";
import ContacUsModel from "../models/ContactUs";
import { orderIsCompleted, getOrderById } from "./order";
import paytabs from "paytabs_api";
const passport = require("passport");
import bcrypt from "bcryptjs";
import orders from "../models/orders";
import keys from "../config/keys";
import bunch from "../models/bunch";
import forgetPassword from "../models/forgetPassword";
import admin from "../models/admin";
const jwt = require("jsonwebtoken");

router.get(
  "/paytabs",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    PaytabsModel.findOne({})
      .lean(true)
      .then(result => {
        return res.json(result);
      });
  }
);
router.get("/contactdetails", (req, res) => {
  admin.findOne({}, "contactmail phone address").then(result => {
    res.json(result);
  });
});
router.get(
  "/logout",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { _id, type } = req.user;
    if (type === "user") {
      return UserModel.findByIdAndUpdate(_id, { token: null })
        .select({ _id: 1 })
        .lean(true)
        .then(result => {
          return res.json("logout done");
        })
        .catch(e => {
          console.log("===========logout user error=========================");
          console.log(e);
          console.log("====================================");
        });
    } else
      return ProviderModel.findByIdAndUpdate(_id, { token: null })
        .select({ _id: 1 })
        .lean(true)
        .then(result => {
          return res.json("logout done");
        })
        .catch(e => {
          console.log(
            "===========logout provider error========================="
          );
          console.log(e);
          console.log("====================================");
        });
  }
);
router.get("/bunches", (req, res) => {
  bunch.find({}).then(bunches => {
    return res.json({ bunches });
  });
});
/* GET users listing. */
router.get("/", function(req, res, next) {
  res.send("respond with a resource");
});
router.get(
  "/order/:id",
  passport.authenticate("jwt", { session: false }),
  getOrderById
);
router.use("/users", UserRouter);
router.use("/providers", ProviderRouter);
router.use("/admin", AdminRouter);
router.use("/chat", Chat);
router.get("/payment", (req, res) => {
  res.send("سيتم تحويلك الأن برجاء الإنتظار");
});
router.post("/payment", (req, res) => {
  console.log("=======req.body /[ay]=============================");
  console.log(req.body);
  console.log("====================================");
  const { payment_reference } = req.body;

  paytabs.verifyPayment(
    {
      merchant_email: "ebrahimhassan121@gmail.com",
      secret_key:
        "e4nfwo4dVPHBYexJsBl1KxTpLpRP0hWfljuRIz4EoMgn1N6tpsv90pyMPpBPHx52RATA9YfyLwVNk2gJvMSvbrh0RTQyrVAjwuTF",
      payment_reference
    },
    verifyPayment
  );

  function verifyPayment(result) {
    console.log(result);
    switch (result.response_code + "") {
      case "100": {
        return res.redirect(
          `/api/payment?status=success`
          // &response_code=${
          //   result.response_code
          // }&transaction_id=${result.transaction_id}&currency=${
          //   result.currency
          // }&pt_invoice_id=${result.pt_invoice_id}&amount=${result.amount}`
        );
      }
      default: {
        return res.redirect(`http://localhost:5000/api/payment?status=fail`);
      }
    }
  }

  // res.json({ ...req.body });
});
router.get("/user/image/:type/:id", (req, res) => {
  const model = req.param("type") == 0 ? UserModel : ProviderModel;
  model
    .findById(req.param("id"), ["avatar"])
    .then(result => {
      if (!result) return res.status(500).json({ error: "not found" });

      let avatar = result.avatar;
      if (result.avatar.includes(".com")) {
        avatar = `data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQDxUSEhIQExISEhAPDxAXFRUQEhARFREXFhcSExUaHSggGBolGxYVITEhJSkrLi4uFx8zODMsNygtLi4BCgoKDg0OGhAQGi0lHyUtLS0tLS0tLS0tLy0tLS0tLS0tLSstLS0tLS0tLS0tLS0tLS0tLS0rLS0tLS0rLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAwQCBQYBB//EADsQAAIBAQQFCgQFBQADAAAAAAABAgMEBREhEjFBUWEGEzJScYGRocHRIkJisRQjM3LhgpKisvBDwvH/xAAaAQEBAAMBAQAAAAAAAAAAAAAAAQIDBAUG/8QAMBEBAAICAQMBBwIGAwEAAAAAAAECAxESBCExQQUTIjJRYXGBkSOhscHR8EJS8eH/2gAMAwEAAhEDEQA/APuIAAAAAAAAAAAjrVowWlJqK3sxvetI3adQk2iI3LT2m/8AZTjj9Usl3LWebl9pR4xx+sua3U/9Ya2teVon88uyKw+2ZxW6vNf1/ZonLe3qrys1olsm+1+5qmmW3nacbyjlYLQtUX3Ne5PdZI9E4WYO3WqjrdRduOHsWM2fH6yvK9fWV6x8rJrKok1v1Px1eR1Yvad4+eNtleptHl0VgvSlW6Ms+q8n3bz1MPVY8vyz3+jqplrfwunQ2AAAAAAAAAAAAAAAAAAAAAAFG87yjQjvm+jDfxfA5up6muGPv9GvJlikOeqOpVlpVG5S+WOyPBI8PJkvltu3dw2ta891ujd61y/t9zbTB/2bK4vquwgo5JJdh0RER4bYiI8PWyhpLeibBoo1tuuSlU1LQlvWrvRov09beOzXbHEuatVnrWWWerH4ZLovsexnFelsUtFqzWXUXByiU8IVHwU+O6Xuer0fX7+DJ+/+XThz77WdKes6wAAAAAAAAAAAAAAAAAAAK9vtcaNNzls1Le9iNWbLGKk2lhe8VjcuYoxlUlzk85y1LcuB89a1sluVvMvPmZtO5bShRUVx2s66UikN9axVBaLellFYvfsNd88R4Y2yxHhra9sqS+ZrgsjntltPq1Te0qVRt62zXMsUEjFHkLXUh0ZyXfl4Fi9o8SsWmF6y8opxyqRUl1llLw1M306qY+ZsjNPq3dOpRtNNpYTi8pLau1bGdUTTJX6w3RMWhyF62Gdkq4rFwl0XvXVfE8/LjnHb7Oa9OMuw5LXtzsNCTxaWMHtlHd2o9n2f1POOFvMePw6+ny8o4y356TpAAAAAAAAAAAAAAAAAABzV+1udtEaS6MFpT7f/AJh4ni+0MvPJFI8Q4uotu3FYs8MM/DgjXirqOUpjrqNyq2u0OWS1fc1ZMnLtHhhe+1KZpa0EzEQTIiCZBBMghmRCzWudGenB4Pbua3NbUWt5pO4WLTE7h1alTt1ma1PattOotT/7YehuufH/AL5dPbJVy9z2qdCtg8pQljhxTwlHvRw4r2xZIn1hz1ma22+oUqilFSWqSTXY0fV1tFoiYepE7jbMqgAAAAAAAAAAAAAAAAwOQu985OpUeuU2l2Y4+x8zNud5t9ZeZM8rTLYWqeC0V3m/LbUcYbck6jShM5mlDMiIJmIgmREEyCCZBDMiIJmItXHeDoVk2/glhGouG/uNuHJwt9meO3GVjlXR5u1RqLVUSk+1ZPywM+qrxyb+q5Y1bbteTFfTs0fpcoeDy8mj3OgvywR9uztwTukNsdrcAAAAAAAAAAAAAAAAIrS8KcnujJ+RhknVZ/CW8S5a4P01+5s+cweYebj8rNeWLbLedzstO52rTMEQzIiCZiIJkRBMggmQQzIiCZiIJkG3vSpz9lsz1yU5UX25fwdGW3LHWf0bLzusS6zkY/yqkd1T/wBV7Hr+y5/h2j7/ANnV0vyy6E9N0gAAAAAAAAAAAAAAADCtHGLW9NeKMbRuJhJ8ONuOphHDc2u8+Xxzxl5dZ1K9MzVBMghmREEzEQTIiCZBBMghmREEzEQTINncSc8Ivo05ut/U46KXr3GdZ7a++zfbTseRkPyZy61R4diS/k932XH8KZ+su7po+F0B6TpAAAAAAAAAAAAAAAAADip0uatNWnsb5yHY8/XyPmepx+7zWr+v7vMy143lcUsUYxO2MSimFQzIiCZiIJkRBMggmQQzIiCZiIdFt4JYtvBLewOhhQVns7XzPpPfJ5eRnrUI7K4bLzVmpxevR0pdss/U+l6TH7vDWr1MVeNIhsDpbAAAAAAAAAAAAAAAAAA57lXZHhG0RWdPKa3wft6nl+0sG6xkj08/hy9TTcclClUTSa1NYo8eJcXhlJme2W0MkQYOi2TabY/hOJE2xdhXWYNoal2vZJd+RNG1C0WWcdcXhvWaMdCnoOTwSbb2Ig3d13bzfxSzm9X0/wAmURpFyx2f8TaYw/8AHS+Oo9jeyPp4nV0mH3uWI9I7y3YacrO1PpHpAAAAAAAAAAAAAAAAAAA8nFNNNYprBreiTETGpHGW6yOyVMHi6E3+XLqPqs+e6vppwW7fLPj/AA83Ni4T9kpytIAAAAAADxRW5AVrTWk5KlTWlUnkkvl4sta2vaK18yyrWbTqHVXLdsbNSUVnJ/FUl1pex9H03Txhpxjz6vSx44pXS+dDYAAAAAAAAAAAAAAAAAAABFabPCpBwmlKMsmmY3pW9eNo7JMRMalyduu2rZM441aHjOmuK2rj9jwep6K+HvXvX+cODLgmvePDChaITWMWn912o49udKAAAAPJSSWLaS3vICrCtUry5uzx0n81R5Qgt+JnixXy240hnSk3nUOnuW5oWaOPSqS6dR63wW5HvdL0lcEfWfWXoYsUUj7tmdbaAAAAAAAAAAAAAAAAAAAAAqWy3QprFvPYlm32HPm6iuOGu+SKtDbLwnU1vRj1V6vaeXlzXyfNPb6Oa95t5aK2Qp44xxUutH4Tivwnw57TX0QwvCtDapLiszV3YbSq+t8PB/wNm4JX6up/l/A5G4Vq1/y2KK/yZOScoVKd5wlL87nGtmDy717FrMb+Ii8erf3fXpv4qE9F7dF/7RZ147RHfHOm+sx5rLobBfOqNXBPUpfLL2fA9LB1n/G7qpm9LN1GSaxR6ETExuHRE7elAAAAAAAAAAAAAAAAAAAai8r1wxjDPZwb257ltfccHUdXEfDRz5MvpDS1JNtyk8ZPb6JbEebe+u8+XPM68qdaTZz2tMtVpmVOoYMFWoiMVaoRFeaIiCaIxQyRBhCcovGLaa1NZMROvCROnRXVfqn+XWwxeSnsf7t3adOPNE9rOmmXfazp7FeFSg88ZUsk3rcd2PoztxdTfDPfvV0VyTSfs6ilVU4qUXinmmezS0WjlHh2xMTG4ZmSgAAAAAAAAAAAAAAADUX1b2mqNPpy6TXyR9zz+t6ma/w6eZ/k582TXw18qkqMYQ0duWZwW1jrr1aZ1WNKFaJzS0yp1EYsVWoiMVWojFFaaIxQTREV5oiIpIiI3EIQouTwSxb1IaNO3uC1KEFRq4NNaOk9+58Dtw5I1ws7MV9fDZuLDaHZqmhJ/lTfwvqv/tZ1dPmnp78LfLLfjt7u2p8OjPadgAAAAAAAAAAAAAABVvK2KjTc3r1RW+T1I09RmjFSbSwyX4V20d303FOrPOc8/E8Wm4icl/MuSvaOU+Xsni8TTM7nctczthOCZEULRRa7N5jMMZhSqIxYq1SJGKtNERXnEiIZxIiGUSIks9inUfwrLbJ6kWI2RG24s1ijSWWb2y2v2RlEabIjRUKN3dlpVopOlN/HFYwlvS1P0Omk+9rwny6KTzrxny3nJ+2ucXTn06eXFx1eWrwPT6DPN68LeYdOC+44z5htz0HQAAAAAAAAAAAAAA5m8an4i06C/Tpa9zlt9EeJ1V/fZuEeI/3/AOOPJPO+vSEtaeL4LJHPkvylrvO5RmtiAeMCtWskZcOwmmMwo1btlsafkY8WPFUqXdU6vmicZY8ZQu7ar+XzROMpxkjc03rcV5jicJWKV1U45vGT46vAvGFisLDWGorJBMxYq1QIipWiVOanHXF49vAVtNZ3BEzE7h007Sozp2mHRnhpr7p+a7js957u9ctfE+XVy1MXh1cJJpNZppNPgz34mJjcO+J2yKAAAAAAAAAAAAqXra+aoyntwwj+55I0dTl91jmzDJbjWZaC7KehS0n0pvXw/wCxPDx/Dj5esuKvau/qlNbEAAeAYsI8ZBiwMGERsiIpERDMCvMxYq1QIq1TFG25O1uchUs73OpT7dqXk/E6MU8qzT9m7FO4mrquTNp06Og+lTej3bPXwPX9nZeeLjPmHb09t119G4O9vAAAAAAAAAAABznKirpTp0l++X2j6nke077muOPy5OpncxVlVywitUUkjjyedR6NV/OkZrYgADwDFhHjIMWBgwiNkRFIiIZgV5mLFWqBFWqYowsNq5qtCfVksf2vJ+WJlS3G0SVtxtEu0uufNWxx+Wqm1/sj0ejt7vqJr6T/AOu3FPHJr6unPbdoAAAAAAAAAAAOTnPnLbOWyD0V/RH3PAzW59TM/T+zhtO8sysNnPM7lreAAAHgGLCPGQYsDBhEbIiKREQzArzMWKtUCKtUxRTqkYy6iz2rGnZq22OEZdsJYM6YvqaZP97OmLfLZ3aZ9M9N6AAAAAAAAAAeSeCx3ZiRxtzvHTn1sX/dI+YpO5tb/e8vNrPmV4AAAAeAYsI8ZBiwMGERsiIpERDMCvMxYq1QIq1TFFOqRjLcXTPSsko9Sq8OyUU/ujZveP8AEttJ+D9X0S76mlRhLfCL8j6bDbljrP2h6tJ3WJWDayAAAAAAAAAENseFKb3Qm/8AFmGSdUn8Sxt4lyVyfpP+n1PmcXyz+jzq+JXjJQAAA8AxYR4yDFgYMIjZERSIiGYFeZixVqgRVqmKKdUjGWxuGX5dZcab8pmUT8E/mGzH4l9EuGWNlp/t9T6To53gr+HqYfkhfOlsAAAAAAAAAENtWNKa+if+rMMsbpP4ljb5Zchcj/LfZF/c+Yx/LLzq+GwM1AAADwDFhHjIMWBgwiNkRFIiIZgV5mLFWqBFWqYop1SMZXLleEKv9P2l7iJ7MqeJfSbhjhZaX7E/HM+m6SNYK/h62L5IXzpbAAAAAAAAAB5JYrDfkJ7jh7s+CTg9jnTfapZHysRxtNXl+J02hmyAAADwDFhHjIMWBgwiNkRFIiIZgV5mLFWqBFWqYop1SMZbC76bVHjUmkvt6CI32Z0js+n2WloU4x6sYx8FgfWY68axX6PYrGoiEpmoAAAAAAAAAAcdfdB0rVJrVUSqxf1LKS9T5/2hi4Zpn69/8uDPXV9rNKppJPec8TuGuJZlUAAeAYsI8ZBiwMGERsiIpERDMCvMxYq1QIq1TFFSMdOahHW9b2JbWYsfM6dTcdkVW1Qil8FFact2K1L7HZ0OL3maPpHd1YKcr/h3R9I9IAAAAAAAAAAAGs5QXc69L4f1IPTpve1rj3nJ1mD32PUeY8NWbHzr93MWG1Yb8HlJbYyWtHz0Txl5/hs0zYyNID3SRQCsWEeMgxYGDCI2REUiIhmBVq1EtbRixUa1pWzPyJMsZlQrVZSy37EYzLGZbOx2fmYaTWNSWSX2iRnWNO75N3a6FH4v1Kj06nDdE+k6Lp/c4+/mfL08GPhXv5bY7G4AAAAAAAAAAAADnOUFzyxdaisW/wBWmvn+qP1fc8rruj5fxMcd/WP7/ly5sO/iq1FktmWWcdq2xe7gePFtOSJX41FJYpmyJ2oyjBsgxc3vCbYutIbNo3aJcBs2jlaZcCbTaKdplw8CbTavO0S3k2m1apNvayIrzIxRKm5PBLFkF+z2WNJaUs5bOHBcQyiNOm5O3LJyVessHrpU38v1Piex0PRa/iZI/Ef3d2DD/wArOoPXdYAAAAAAAAAAAAAABpb3uCNVupTfN1dr+Wf7l6nB1PQ1y/FXtb+v5aMmCLd48ubrxqUJYVYuD2T1wl2SPEy4cmGdWjX9HFalqT3T07Xvz4oxi/1TaVVovaZcoBlEciIikBFIiIpgQSIjDmpPUmRNM42LrPuXuTa8VizQcnoUYOctuGpcZSMseO+SdVjbOtZt2rDpbn5Oqm1UrNTqa0vkh2b2e10vs+uP4r95/lDsxdPFe9vLfnpOkAAAAAAAAAAAAAAAAAMalOMlhJJp601in3EmsTGpSY35aS18l6MsXTlKk90c4f2s8/L7NxW717f0/Zot09Z8dmrr3Fa4alTqrg9CXg8jgyezc1fl1LRbp7x47qNSNWHTpVo8dFteKOW2DLTzWf2appaPMIvxsOth24r7mrlpi9/FQ60fFF5A68OtHxQ5SiOVop9ZfcnIZUp6XQhUm/pg2ZVpa3yxMsorM+IXqF02upqpqmt836LM6sfQZ7+mvy2VwXn0bSy8lY661SVT6V8EPdnfi9l0jved/wAm+vTRHzN9Z7PCnHRhGMYrYlgejSlaRqsah0xWI7QlM1AAAAAAAAAAAAAAAAAAAAAAAGpvn0OTqGrI4e8ekzws3lw3VKWs0Va4dRcmw9bpfDrxOvp6l2HsR4dcMiqAAAAAAAAAAAAB/9k=`;
      }
      const im = avatar.split(",")[1];
      const img = Buffer.from(im, "base64");
      res.writeHead(200, {
        "Content-Type": "image/png",
        "Content-Length": img.length
      });
      res.end(img);
    })
    .catch(err => {
      res.status(500).json({ error: "not found" });
    });
});
router.get("/order/file/:index/:id", (req, res) => {
  const orderId = req.param("id");
  const indexFile = Number(req.param("index"));
  orders
    .findById(orderId, "files")
    .then(result => {
      if (!result) return res.status(500).json({ error: "not found" });
      const file = result.files[indexFile];
      res.writeHead(200, {
        "Content-Type": "application/pdf",
        "Content-Disposition": `attachment; filename="${file.name}"`
      });

      const download = Buffer.from(file.uri.toString("utf-8"), "base64");

      res.end(download);
    })
    .catch(err => {
      res.status(500).json({ error: "not found" });
    });
});
router.post(
  "/ordercomplete",
  passport.authenticate("jwt", { session: false }),
  orderIsCompleted
);
router.post(
  "/update",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    let userType = req.user.type === "user" ? UserModel : ProviderModel;
    const { email, phone, name, avatar, city } = req.body;
    console.log("====================================");
    console.log(req.user, req.body);
    console.log("====================================");
    let errors = {};
    if (!email) errors.email = "يجب إدخال بريد إلكتروني صحيح";
    if (!phone) errors.phone = "يجب إدخال رقم الهاتف";
    if (!name) errors.name = "يجب إدخال الإسم";
    if (!city) errors.city = "يجب إدخال المدينة";
    if (Object.keys(errors).length !== 0)
      return res.status(400).json({ errors });
    // return res.status(400).json({ errors });

    userType.findOne({ email: email + "" }).then(result => {
      console.log(email, result);
      if (!result || (result && result._id + "" == req.user._id + "")) {
        userType.findOne({ phone: phone }).then(resultPhone => {
          if (resultPhone._id + "" === req.user._id + "") {
            resultPhone.phone = phone;
            resultPhone.email = email;
            resultPhone.name = name;
            resultPhone.city = city;
            resultPhone.avatar = avatar ? avatar : resultPhone.avatar;
            resultPhone.save().then(UpdatedresultPopulated => {
              UpdatedresultPopulated.populate("city")
                .execPopulate()
                .then(Updatedresult => {
                  jwt.sign(
                    { id: Updatedresult._id },
                    keys.secretOrKey,
                    // { expiresIn: 3600 },
                    (err, token) => {
                      let returnedData = {
                        ...Updatedresult._doc,
                        token: "Bearer " + token,
                        avatar: null
                      };
                      delete returnedData.password;
                      delete returnedData.avatar;
                      res.json({
                        user: returnedData
                      });
                    }
                  );
                });
              // Sign Token

              // res.json({ Updatedresult });
            });
          } else {
            return res.status(400).json({
              errors: { text: "رقم الهاتف مستخدم من قبل مستخدم أخر" }
            });
          }
        });
      } else {
        return res.status(400).json({
          errors: { text: "البريد الإلكتروني مستخدم من قبل مستخدم أخر" }
        });
      }
    });
  }
);
router.put(
  "/changepassword",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    let { password, newPassword } = req.body;
    let errors = {};
    if (!password) errors.password = "يجب إدخال كلمة السر الحالية";
    if (!newPassword) errors.newPassword = "يجب إدخال كلمة السر الجديدة";
    console.log("====================================");
    console.log(password, newPassword);
    console.log("====================================");
    if (Object.keys(errors).length === 0) {
      bcrypt.compare(password, req.user.password).then(async isMatch => {
        if (isMatch) {
          let userType = req.user.type === "user" ? UserModel : ProviderModel;
          userType
            .findById(req.user._id)
            .then(user => {
              bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newPassword, salt, (err, hash) => {
                  if (err) throw err;

                  user.password = hash;
                  user.save();
                  res.json({ success: "تم تحديث كلمة المرور بنجاح" });
                });
              });
            })
            .catch(err => {
              res.json({ error: "حدث خطأ ما " });
            });
        } else {
          errors.password = "يجب إدخال كلمة سر صحيحة";
          return res.status(400).json({ errors });
        }
      });
    } else {
      return res.status(400).json({ errors });
    }
  }
);

router.post("/forgetpasswordchange", (req, res) => {
  let { code, newPassword, email } = req.body;
  let errors = {};
  if (!code) errors.code = "يجب إدخال الكود المرسل علي الإيميل";
  if (!email) errors.email = "يجب إدخال البريد الإلكتروني";
  if (!newPassword) errors.newPassword = "يجب إدخال كلمة السر الجديدة";

  if (Object.keys(errors).length === 0) {
    forgetPassword
      .findOne({ code: code + "", email: req.body.email })
      .then(codeData => {
        if (codeData) {
          let userType =
            codeData.userType === "users" ? UserModel : ProviderModel;
          userType
            .findById(codeData.user)
            .then(user => {
              bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newPassword, salt, (err, hash) => {
                  if (err) throw err;
                  user.password = hash;
                  user.save();
                  res.json({ success: "تم تغيير كلمة المرور بنجاح" });
                });
              });
            })
            .catch(err => {
              res.json({ error: "حدث خطأ ما " });
            });
        } else {
          return res.status(400).json({ error: "الكود غير صحيح" });
        }
      });
  } else {
    return res.status(400).json({ errors });
  }
});

router.get(
  "/balnce",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    let ModelType = req.user.type === "user" ? UserModel : ProviderModel;
    ModelType.findById(req.user._id, "balance")
      .lean(true)
      .then(result => {
        res.json({ balance: result.balance });
      });
  }
);
router.post("/contactus", (req, res) => {
  const email = req.body.email;
  const name = req.body.name;
  const phone = req.body.phone;
  const message = req.body.message;
  let errors = {};
  if (typeof email === "undefined" || email === null) {
    errors.email = "email is required";
  }
  if (typeof name === "undefined" || name === null) {
    errors.name = "name is required";
  }
  if (typeof phone === "undefined" || phone === null) {
    errors.phone = "topic is required";
  }
  if (typeof message === "undefined" || message === null) {
    errors.message = "message is required";
  }
  if (Object.keys(errors).length > 0) {
    return res.status(400).json(errors);
  }
  new ContacUsModel({
    email,
    name,
    phone,
    message
  })
    .save()
    .then(mes => {
      res.json({
        message: "thanks for ContactUS ,we will contact you on mail soon"
      });
    });
});
module.exports = router;
