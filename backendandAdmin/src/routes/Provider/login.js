import ProviderModel from "../../models/Provider";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
const validateLoginInput = require("../../validation/Driver/login");
import keys from "../../config/keys";

export default (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body);

  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;
  const notitoken = req.body.token;
  console.log("====================================");
  console.log(req.body);
  console.log("====================================");
  // Find user by email
  ProviderModel.findOne({ email: email, deleted: { $ne: true } }, { avatar: 0 })
    .populate("city")
    .then(driver => {
      // Check for user
      if (!driver) {
        errors.email = "email not found";
        return res.status(400).json(errors);
      }

      // Check Password
      bcrypt.compare(password, driver.password).then(isMatch => {
        if (isMatch) {
          driver.token = notitoken;
          driver.save();
          const payload = {
            id: driver._id,
            password: driver.password
          };
          jwt.sign(payload, keys.secretOrKey, (err, token) => {
            let reternedData = {
              ...driver._doc,
              token: "Bearer " + token
            };
            delete reternedData.password;
            res.json({
              provider: reternedData
            });
          });
        } else {
          errors.password = "Password incorrect";
          return res.status(400).json(errors);
        }
      });
    })
    .catch(e => {
      errors.password = "Password incorrect";
      return res.status(400).json(errors);

      console.log("====================================");
      console.log(e);
      console.log("====================================");
    });
};
