import Driver from "../../models/Provider";
import DriverServices from "../../models/DriverServices";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import gravatar from "gravatar";
const validateRegisterInput = require("../../validation/Driver/register");
import keys from "../../config/keys";
import PhoneVerfiyCodes from "../../models/PhoneVerfiyCodes";
import request from "request";

export default (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body);

  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }
  Driver.findOne({ email: req.body.email }).then(driver => {
    if (driver) {
      errors.email = "Email already exists";
      return res.status(400).json(errors);
    } else {
      Driver.findOne({ phone: req.body.phone, type: "providers" }).then(
        phone => {
          if (phone) {
            errors.phone = "Phone already exists";
            return res.status(400).json(errors);
          } else {
            const randomNumberBetween0and19 =
              Math.floor(1000 + Math.random() * 9000) + "";
            let options = {
              method: "GET",
              url: "http://www.shamelsms.net/api/httpSMS.aspx",
              qs: {
                username: "skanfaker",
                password: "114477",
                message: "code : " + randomNumberBetween0and19,
                mobile: req.body.phone + "",
                sender: "Bashrh"
              }
            };
            request(options, function(error, response, body) {
              if (error) throw new Error(error);
            });
            PhoneVerfiyCodes.findOne({
              phone: req.body.phone,
              type: "providers"
            }).then(resultPhone => {
              if (resultPhone) {
                resultPhone.code = randomNumberBetween0and19;
                resultPhone.save();
              } else {
                new PhoneVerfiyCodes({
                  phone: req.body.phone,
                  code: randomNumberBetween0and19,
                  type: "providers"
                }).save();
              }
              return res.json({ success: true });
            });
          }
        }
      );
    }
  });
};
