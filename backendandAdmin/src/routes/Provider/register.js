import Driver from "../../models/Provider";
import DriverServices from "../../models/DriverServices";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import gravatar from "gravatar";
const validateRegisterInput = require("../../validation/Driver/register");
import keys from "../../config/keys";
import PhoneVerfiyCodes from "../../models/PhoneVerfiyCodes";

export default (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body);
  const { code } = req.body;
  if (!code || typeof code === "undefined") {
    return res.status(400).json({ errors: { code: "الكود غير صحيح" } });
  }
  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }
  return PhoneVerfiyCodes.findOne({
    phone: req.body.phone,
    code: code + ""
  }).then(IsPhoneExist => {
    if (!IsPhoneExist) {
      return res.status(400).json({ errors: { code: "الكود غير صحيح" } });
    } else {
      Driver.findOne({ email: req.body.email }).then(driver => {
        if (driver) {
          errors.email = "Email already exists";
          return res.status(400).json(errors);
        } else {
          Driver.findOne({ phone: req.body.phone }).then(phone => {
            if (phone) {
              errors.phone = "Phone already exists";
              return res.status(400).json(errors);
            } else {
              const avatar = gravatar.url(req.body.email, {
                s: "200", // Size
                r: "pg", // Rating
                d: "mm" // Default
              });
              const newDriver = new Driver({
                name: req.body.name,
                phone: req.body.phone,
                email: req.body.email,
                password: req.body.password,
                city: req.body.city,
                token: req.body.token,
                avatar
              });
              bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newDriver.password, salt, (err, hash) => {
                  if (err) throw err;
                  newDriver.password = hash;
                  newDriver
                    .save()
                    .then(driver => {
                      req.body.services.forEach((element, index) => {
                        DriverServices.findOne({
                          driver: driver._id,
                          service: element
                        }).then(isServiceExist => {
                          if (isServiceExist) {
                            return;
                          }
                          new DriverServices({
                            provider: driver._id,
                            service: element
                          }).save();
                        });
                        if (req.body.services.length - 1 === index) {
                          const payload = {
                            id: driver.id,
                            password: driver.password
                          }; // Create JWT Payload
                          jwt.sign(payload, keys.secretOrKey, (err, token) => {
                            return res.json({
                              success: true,
                              driver: {
                                ...driver._doc,
                                nottoken: driver.token,
                                token: "Bearer " + token
                              }
                            });
                          });
                        }
                      });
                    })
                    .catch(err => console.log(err));
                });
              });
            }
          });
        }
      });
    }
  });
};
