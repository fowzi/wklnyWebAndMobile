const express = require("express");
const router = express.Router();
const gravatar = require("gravatar");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
import keys from "../../config/keys";
const passport = require("passport");
// Load Input Validation
const validateLoginInput = require("../../validation/Driver/login");

// Load User model
const Driver = require("../../models/Provider");
// const Trips = require("../../models/Trips");
// const Contracts = require("../../models/contracts");
// const Vehicals = require("../../models/vehicals");

// const phoneVerfiyModel = require("../../models/messageVefiy");
// const sendMess = require("../../config/message-service");

// @route   GET api/users/register
// @desc    Register user
// @access  Public
import register from "./register";
import VerfiyPhone from "./VerfiyPhone";
import login from "./login";
import {
  providerAcceptOrder,
  providerOrderDone,
  getOpenOrdersBasedOnCity,
  getuserOrders,
  getProviderOrders
} from "../order";
import Provider from "../../models/Provider";
import providerOffer from "../../models/providerOffer";
import Chat from "../../models/Chat";
import orders from "../../models/orders";
import DriverServices from "../../models/DriverServices";
router.post("/register", register);
router.post("/login", login);
router.post("/verifyphone", VerfiyPhone);

router.get(
  "/open-orders/:page",
  passport.authenticate("jwt", { session: false }),
  getOpenOrdersBasedOnCity
);
router.get(
  "/myorders",
  passport.authenticate("jwt", { session: false }),
  getProviderOrders
);
router.post(
  "/makeOffer",
  passport.authenticate("jwt", { session: false }),
  providerAcceptOrder
);
router.post(
  "/addService",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const service = req.body.service;
    new DriverServices({
      provider: req.user._id,
      service
    }).then(result => {
      res.json({ success: "تم إضافة الخدمة بنجاح" });
    });
  }
);
router.get(
  "/myServices",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    DriverServices.find({ provider: req.user._id }, "service")
      .populate({ path: "service", populate: "type" })
      .lean(true)
      .then(result => {
        res.json({ result: result.map(e => e.service) });
      });
  }
);
router.delete(
  "/service/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const ServiceId = req.params.id;
    console.log("=========params===========================");
    console.log(req.params);
    console.log("====================================");
    DriverServices.findOneAndDelete({
      provider: req.user._id,
      service: ServiceId
    })
      .then(result => {
        if (result) {
          return DriverServices.find({ provider: req.user._id }, "service")
            .populate({ path: "service", populate: "type" })
            .lean(true)
            .then(result => {
              return res.json({ result: result.map(e => e.service) });
            });
        } else {
          return res.status(400).json({ error: " الخدمة غير متاحة " });
        }
      })
      .catch(err => {
        res.status(400).json({ error: " الخدمة غير متاحة " });
        console.log("====================================");
        console.log(err);
        console.log("====================================");
        return;
      });
  }
);
router.put(
  "/service/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const ServiceId = req.params.id;

    new DriverServices({
      provider: req.user._id,
      service: ServiceId
    })
      .save()
      .then(result => {
        if (result) {
          return DriverServices.find({ provider: req.user._id }, "service")
            .populate({ path: "service", populate: "type" })
            .lean(true)
            .then(result => {
              return res.json({ result: result.map(e => e.service) });
            });
        } else {
          return res.status(400).json({ error: " الخدمة غير متاحة " });
        }
      })
      .catch(err => {
        res.status(400).json({ error: " الخدمة غير متاحة " });
        console.log("============err========================");
        console.log(err);
        console.log("====================================");
        return;
      });
  }
);
router.put(
  "/bank",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { bankName, bankAccount } = req.body;
    Provider.findByIdAndUpdate(
      req.user._id,
      { bankName, bankAccount },
      { new: true }
    )
      .lean(true)
      .then(result => {
        if (result) {
          return res.json({
            result: {
              bankName: result.bankName,
              bankAccount: result.bankAccount,
              balance: result.balance
            }
          });
        } else {
          return res.status(400).json({ error: " الخدمة غير متاحة " });
        }
      })
      .catch(err => {
        res.status(400).json({ error: " الخدمة غير متاحة " });
        console.log("============err update bank========================");
        console.log(err);
        console.log("====================================");
        return;
      });
  }
);
router.get(
  "/bank",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { bankName, bankAccount } = req.body;
    Provider.findById(req.user._id, "balance bankName bankAccount")
      .lean(true)
      .then(result => {
        return res.json({
          result: {
            bankName: result.bankName,
            bankAccount: result.bankAccount,
            balance: result.balance
          }
        });
      })
      .catch(err => {
        res.status(400).json({ error: " الخدمة غير متاحة " });
        console.log("============err update bank========================");
        console.log(err);
        console.log("====================================");
        return;
      });
  }
);

router.post(
  "/ordercompletedsuccess",
  passport.authenticate("jwt", { session: false }),
  providerOrderDone
);
router.post(
  "/checkcode",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    let code = req.body.code;

    if (!code) {
      res.status(400).json({ error: "code field is required" });
    }
    let _id = req.user._id;
    let user_type = req.user.type;
    let dnyamicUserModel = user_type === "user" ? User : Driver;
    phoneVerfiyModel
      .findOneAndUpdate(
        { user: _id },
        { vefied: true },
        { new: true, upsert: true, sort: { date: -1 } }
      )
      .then(result => {
        if (result && code === result.code) {
          return dnyamicUserModel
            .findOneAndUpdate({ _id }, { phoneverfied: true }, { new: true })
            .then(updated => {
              if (updated) {
                return res.json({ success: true });
              }
            })
            .catch(err => {
              res.status(500).json({ error: "Something went wrong" });
            });
        } else {
          return res.status(400).json({ error: "code not correct" });
        }
      });
  }
);
router.get(
  "/counts",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    let counts = {};
    await providerOffer
      .count({ provider: req.user._id, status: { $in: [1, 2] } })
      .then(count => (counts.Running = count));
    await providerOffer
      .count({ provider: req.user._id, status: 3 })
      .lean(true)
      .then(count => (counts.Finished = count));
    await DriverServices.find({ provider: req.user._id }, ["service"])
      .lean(true)
      .then(services => {
        let servicesID = services.map(e => e.service);
        providerOffer
          .find({ provider: req.user._id }, "order")
          .lean(true)
          .then(ordersArr => {
            const IdsOfOrders = ordersArr.map(e => e.order);
            console.log("====================================");
            console.log(IdsOfOrders);
            console.log("====================================");
            orders
              .count({
                status: 0,
                city: req.user.city,
                service: { $in: servicesID },
                _id: { $nin: IdsOfOrders }
              })
              .lean(true)
              .then(count => {
                counts.openOrders = count;
              });
          });
      });
    await providerOffer
      .find({ provider: req.user._id }, "order")
      .lean(true)
      .then(ordersObject => {
        const rooms = ordersObject.map(e => e.order);
        return Chat.aggregate()
          .match({ room: { $in: rooms } })
          .group({
            _id: "$room"
          })
          .then(count => {
            counts.Messages = count.length;
          });
      });
    return res.json({ counts });
  }
);
// @route   GET api/users/login
// @desc    Login User / Returning JWT Token
// @access  Public

/**driver upload contract */
router.post(
  "/contract/upload",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    if (req.user.type !== "driver") {
      return res.status(500).json({ err: "permision denied" });
    }
    let contractImage = req.body.image;

    if (contractImage === null || typeof contractImage === "undefined")
      return res.json({ error: "image is required" });

    let newContract = new Contracts({
      driver: req.user._id,
      image: contractImage
    });
    newContract.save();
    res.json({ success: true });
  }
);
router.post(
  "/contracts",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    if (req.user.type !== "driver") {
      return res.status(500).json({ err: "permision denied" });
    }
    Contracts.find({ driver: req.user._id }).then(response => {
      res.json(response);
    });
  }
);

router.post(
  "/online",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    if (typeof req.body.status === "undefined" || req.body.status === null) {
      res.status(400).json({ status: "status required" });
      return;
    }
    Driver.findOneAndUpdate(
      { _id: req.user._id },
      { online: req.body.status === "1" ? true : false },
      {
        new: true
      },
      (err, respo) => {
        console.log("====================================");
        console.log(respo._doc);
        console.log("====================================");
        if (err) {
          res.status(400).json({ err });
          return;
        }
        res.json({ online: req.body.status === "1" ? true : false });
      }
    );
  }
);

// @route   GET api/users/current
// @desc    Return current user
// @access  Private
router.get(
  "/current",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    res.json({
      id: req.user.id,
      name: req.user.name,
      email: req.user.email
    });
  }
);

module.exports = router;
