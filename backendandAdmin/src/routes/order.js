import ordersModel from "../models/orders";
import usersModel from "../models/User";
import adminModel from "../models/admin";
import providerOfferModel from "../models/providerOffer";
import valdiateAddOrder from "../validation/addorder";
import valdiateproviderAccept from "../validation/providerAccept";
import Chat from "../models/Chat";
import DriverServices from "../models/DriverServices";
import ProvidersModel from "../models/Provider";
import { sendNotification } from "../config/SendNotifiction";
import Provider from "../models/Provider";

const CreatNotfi = async (
  roomName,
  toType,
  notificationType,
  title,
  subTitle,
  callBack
) => {
  await providerOfferModel
    .findOne({ order: roomName })
    .populate({
      path: "order",
      select: {
        _id: 1,
        service: 1,
        user: 1,
        details: 1,
        status: 1,
        city: 1,
        files: "$files.size",
        status: 1
      },
      populate: {
        path: "service city user",
        select: {
          avatar: 0,
          password: 0,
          city: 0,
          phone: 0,
          balance: 0,
          email: 0
        }
      }
    })
    .populate("order.user", "_id name token")
    .populate("provider", "name _id rate token")
    .lean(true)
    .then(result => {
      const message = {
        user: result.order.user,
        provider: result.provider,
        service: result.order.service,
        offer: {
          _id: result._id,
          status: result.status,
          price: result.price,
          time: result.time,
          introduction: result.introduction
        },
        order: {
          ...result.order,
          files: result.order.files.map(f => f.name)
        },
        files: result.order.files.map(f => f.name),
        room: result.order._id,
        _id: result.order._id
      };
      const userToken = result.order.user.token;
      const providerToken = result.provider.token;
      if (callBack) {
        callBack(message);
      }
      sendNotification(
        title,
        subTitle,
        { message, notificationType: notificationType },
        toType === "user" ? userToken : providerToken
      );
    });
};
export const addOrder = (req, res) => {
  const { errors, isValid } = valdiateAddOrder(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }
  const { strip, service, city, pidfrom, pidto, details, files } = req.body;
  console.log("====================================");
  console.log(req.body);
  console.log("====================================");
  const user = req.user._id;
  new ordersModel({
    user,
    strip,
    city,
    service,
    pidfrom: pidfrom ? Number(pidfrom) : 0,
    pidto: pidto ? Number(pidto) : 0,
    details,
    files
  })
    .save()
    .then(async saved => {
      res.json({ success: "تم اضافة المعاملة بنجاح" });

      await DriverServices.find({ service: service }, "provider")
        .populate({ path: "provider", match: { city: city }, select: "token" })
        .then(Servicesresult => {
          saved
            .populate("service")
            .execPopulate()
            .then(re => {
              Servicesresult.map(e => {
                console.log("====================================");
                console.log(e.provider, re.service.name);
                console.log("====================================");
                e.provider &&
                  sendNotification(
                    "طلب خدمة جديد",
                    re.service.name,
                    {
                      // user,
                      // strip,
                      // city,
                      // service,
                      // pidfrom,
                      // pidto,
                      // details,
                      // _id: saved._id,
                      notificationType: "newOrder"
                    },
                    e.provider.token
                  );
              });
            });
        });
    })
    .catch(err => {
      console.log("====================================");
      console.log(err);
      console.log("====================================");
      res.status(400).json({ err });
    });
};
export const providerAcceptOrder = (req, res) => {
  const { errors, isValid } = valdiateproviderAccept(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }
  let { price, time, introduction, order_id } = req.body;
  let provider_id = req.user._id;
  ordersModel
    .findById(order_id)
    .populate("user", "token")
    .lean(true)
    .then(order_idExist => {
      if (order_idExist) {
        providerOfferModel
          .findOne({ order: order_id, provider: provider_id })
          .lean(true)
          .then(providerofferExist => {
            if (providerofferExist) {
              return res.status(400).json({ error: "تم  تقديم عرض مسبقاً" });
            }
            new providerOfferModel({
              price,
              time,
              introduction,
              order: order_id,
              provider: provider_id
            })
              .save()
              .then(saved => {
                sendNotification(
                  "عرض جديد",
                  "تم تلقي عرض جديد من " + req.user.name,
                  { orderId: order_id, notificationType: "newOffer" },
                  order_idExist.user.token
                );
                return res.json({ success: "تم اضافة عرضك بنجاح" });
              });
          });
      } else {
        res.status(400).json({ error: "للاسف العرض منتهي" });
      }
    })
    .catch(err => {
      res.status(400).json({ error: "للاسف العرض منتهي" });
    });
};
export const getOrderById = (req, res) => {
  ordersModel
    .findById(req.params.id)
    .then(result => {
      if (!result) {
        return res.status(400).json({ error: "order not found" });
      }
      return res.json({ result });
    })
    .catch(e => {
      console.log("getOrderById====================================");
      console.log(e);
      console.log("====================================");
      return res.status(400).json({ error: "order not found" });
    });
};
export const userAcceptProivederOrder = (req, res) => {
  const { order_id, provider_id } = req.body;
  if (!order_id)
    return res.status(400).json({ error: "order_id field is required" });
  // if (!provider_id)
  //   return res.status(400).json({ error: "provider_id field is required" });
  providerOfferModel
    .findOne({ _id: order_id, status: 0 })
    .populate("order")
    .then(orderdata => {
      if (orderdata) {
        if (Number(orderdata.balance) > Number(req.user.balance)) {
          return res
            .status(400)
            .json({ error: "لايوجد رصيد كافي لاتمام الطلب" });
        }
        if (orderdata.order.user._id + "" === req.user._id + "") {
          orderdata.status = 1;
          ordersModel
            .findByIdAndUpdate(orderdata.order._id, { status: 1 })
            .then(result => {
              if (result) {
                orderdata.save();
                usersModel.findById(req.user._id).then(resBalance => {
                  const userBalance =
                    Number(resBalance.balance) - orderdata.price;
                  resBalance.balance = userBalance;
                  resBalance.save();
                });
                adminModel.findOne({}).then(adminData => {
                  adminData.balance =
                    Number(orderdata.price) + adminData.balance;
                  adminData.save();
                });
                return CreatNotfi(
                  orderdata.order._id,
                  "provider",
                  "offerAccepted",
                  "تم قبول عرضك",
                  "لقد تم قبول عرضك بحصوص " + result.service.name,
                  dataToSend => {
                    res.json({
                      success: "تم اعتماد الطلب بناجح",
                      result: dataToSend
                    });
                  }
                );

                // sendNotification(
                //   "تم قبول عرضك",
                //   "لقد تم قبول عرضك بحصوص " + result.service.name,
                //   { offerId: saved._id, notificationType: "offerAccepted" },
                //   order_idExist.provider.token
                // );
              }
            });
        } else {
          return res.status(400).json({ error: "order permission denied" });
        }
      } else
        return res.status.json({
          error: "للأسف تم اسناد المعاملة لمقدم خدمة "
        });
    })
    .catch(error => {
      res.status(400).json({ error: "هذه المعاملة لم تعد موجودة" });
    });
};
export const providerOrderDone = (req, res) => {
  const { offer_id } = req.body;
  if (!offer_id)
    return res.status(400).json({ error: "offer_id field is required" });
  providerOfferModel
    .findByIdAndUpdate(offer_id, { status: 2 })
    .populate({
      path: "order",
      select: "user",
      populate: { path: "user", select: "token" }
    })
    .then(isok => {
      if (isok) {
        res.json({ success: "بانتظار تاكيد العميل" });
        return CreatNotfi(
          isok.order._id,
          "user",
          "providerDone",
          "لقد انهي " +
            req.user.name +
            " طلبك بخصوص " +
            isok.order.service.name,
          "بأنتظار تأكيدك ",
          dataToSend => {
            // res.json({
            //   success: "بانتظار تاكيد العميل",
            //   result: dataToSend
            // });
          }
        );
        sendNotification(
          "لقد انهي " +
            req.user.name +
            " طلبك بخصوص " +
            isok.order.service.name,
          "بأنتظار تأكيدك ",
          { orderId: isok.order._id, notificationType: "providerDone" },
          isok.order.user.token
        );
        return res.json({ success: "بانتظار تاكيد العميل" });
      } else {
        return res.status(400).json({ error: "حدث خطأ ما" });
      }
    })
    .catch(error => {
      return res.status(400).json({ error: "حدث خطأ ما" });
    });
};
export const orderIsCompleted = (req, res) => {
  const { offer_id } = req.body;
  if (!offer_id)
    return res.status(400).json({ error: "offer_id field is required" });
  const userType = req.user.type;
  let status;
  if (userType === "user") {
    status = 3;
  } else if (userType === "driver") {
    status = 2;
  }
  providerOfferModel
    .findOneAndUpdate(
      { _id: offer_id, status: { $ne: 3 } },
      { status: status },
      { new: true }
    )
    .lean(true)
    .then(isok => {
      if (isok) {
        ordersModel
          .findByIdAndUpdate(isok.order, { status: status })
          .populate("service")
          .lean(true)
          .then(orderUpdated => {
            res.json({
              success: status === 3 ? "شكرا لك" : "بانتظار تأكيد العميل"
            });
            if (userType === "driver") {
              CreatNotfi(
                isok.order,
                "user",
                "orderDone",
                "لقد انهي " +
                  req.user.name +
                  " طلبك بخصوص معاملة " +
                  orderUpdated.service.name,
                "بأنتظار تأكيدك ",
                dataToSend => {}
              );
            } else if (userType === "user") {
              CreatNotfi(
                isok.order,
                "provider",
                "orderDone",
                "لقد اعتمد " +
                  req.user.name +
                  " طلبك  بخصوص معاملة " +
                  orderUpdated.service.name,
                "شكرا لك",
                dataToSend => {
                  ProvidersModel.findByIdAndUpdate(isok.provider, {
                    $inc: { balance: Number(isok.price) }
                  }).then(provData => {});
                }
              );
            }
            // sendNotification(
            //   " إتمام لخدمة",
            //   "لقد تم خدمتك بخصوص  " + orderUpdated.service.name,
            //   { orderId: orderUpdated._id, notificationType: "orderDone" },
            //   isok.provider.token
            // );
          });
      } else {
        return res.status(400).json({ error: "حدث خطأ ما" });
      }
    })
    .catch(error => {
      console.log("====================================");
      console.log(error);
      console.log("====================================");
      return res.status(400).json({ error: "حدث خطأ ما" });
    });
};
export const orderCompletedByAdmin = (req, res) => {
  const { offer_id } = req.body;
  if (!offer_id)
    return res.status(400).json({ error: "offer_id field is required" });
  const userType = req.user.type;
  providerOfferModel
    .findByIdAndUpdate(
      offer_id,
      { status: 3, isProblem: false, problemIsSolved: true },
      { new: true }
    )
    .lean(true)
    .then(isok => {
      if (isok) {
        ordersModel
          .findByIdAndUpdate(isok.order, { status: 3 })
          .populate("service")
          .populate("user", "name token _id balance")
          .lean(true)
          .then(orderUpdated => {
            ProvidersModel.findByIdAndUpdate(isok.provider, {
              $inc: { balance: Number(isok.price) }
            })
              .lean(true)
              .then(provData => {
                res.json({
                  success: "تم انهاء المشكلة"
                });
              });

            CreatNotfi(
              isok.order,
              "user",
              "orderDone",
              "وكلني",
              "لقد اعتمد الأدمن  " +
                " معاملة " +
                orderUpdated.service.name +
                " وبالتالي تم تحويل المبلغ المحدد لمقدم الخدمة",
              dataToSend => {}
            );
            CreatNotfi(
              isok.order,
              "provider",
              "orderDone",
              "وكلني",
              "لقد اعتمد الأدمن  " +
                " معاملة " +
                orderUpdated.service.name +
                " وبالتالي تم تحويل المبلغ المحدد لمقدم الخدمة",
              dataToSend => {}
            );
          });
      } else {
        return res.status(400).json({ error: "حدث خطأ ما" });
      }
    })
    .catch(error => {
      console.log("====================================");
      console.log(error);
      console.log("====================================");
      return res.status(400).json({ error: "حدث خطأ ما" });
    });
};
export const orderCashBackByAdmin = (req, res) => {
  const { offer_id } = req.body;
  if (!offer_id)
    return res.status(400).json({ error: "offer_id field is required" });
  providerOfferModel
    .findByIdAndUpdate(
      offer_id,
      { status: 3, isProblem: false, problemIsSolved: true },
      { new: true }
    )
    .lean(true)
    .then(isok => {
      if (isok) {
        ordersModel
          .findByIdAndUpdate(isok.order, { status: 3 })
          .populate("service")
          .populate("user", "name token _id balance")
          .lean(true)
          .then(orderUpdated => {
            usersModel
              .findByIdAndUpdate(
                orderUpdated.user._id,
                {
                  $inc: { balance: Number(isok.price) }
                },
                { new: true }
              )
              .lean(true)
              .then(provData => {
                res.json({
                  success: "تم انهاء المشكلة"
                });
              });
            adminModel
              .findByIdAndUpdate(
                req.user._id,
                {
                  $inc: { balance: -Number(isok.price) }
                },
                { new: true }
              )
              .lean(true)
              .then(provData => {});
            CreatNotfi(
              isok.order,
              "user",
              "orderDone",
              "وكلني",
              "لقد قام الأدمن بألغاء معاملة" +
                orderUpdated.service.name +
                " وبالتالي تم إرجاع المبلغ المحدد للعميل مرة أخري",
              dataToSend => {}
            );

            CreatNotfi(
              isok.order,
              "provider",
              "orderDone",
              "وكلني",
              "لقد قام الأدمن بألغاء معاملة" +
                orderUpdated.service.name +
                " وبالتالي تم إرجاع المبلغ المحدد للعميل مرة أخري",
              dataToSend => {}
            );
          });
      } else {
        return res.status(400).json({ error: "حدث خطأ ما" });
      }
    })
    .catch(error => {
      console.log("====================================");
      console.log(error);
      console.log("====================================");
      return res.status(400).json({ error: "حدث خطأ ما" });
    });
};
export const getuserOrders = (req, res) => {
  let page = req.query.page - 1;
  let status = req.query.status;
  if (status == 1) status = { $in: [1, 2] };
  if (status == 0) {
  }
  if (status == 0)
    ordersModel
      .find({ user: req.user._id, status }, [
        "_id",
        "details",
        "status",
        "date"
      ])
      .sort({ date: -1 })
      .skip(page * 5)
      .limit(5)
      .then(orders => {
        console.log(status, "====================================");
        console.log(orders);
        console.log("====================================");
        res.json({
          orders: orders.map(item => {
            return { ...item._doc, user: null };
          })
        });
      });
  else {
    ordersModel
      .find({ user: req.user._id, status }, ["_id"])
      .lean(true)
      .then(Resultids => {
        providerOfferModel
          .find({ order: { $in: Resultids.map(e => e._id) } })
          // .populate("order", "name details status service service.name")
          .lean(true)
          .sort({ date: -1 })
          .skip(page * 5)
          .limit(5)
          .populate({
            path: "order",
            select: {
              _id: 1,
              service: 1,
              user: 1,
              details: 1,
              status: 1,
              city: 1,
              files: "$files.size"
            },
            populate: {
              path: "service city user",
              select: {
                avatar: 0,
                password: 0,
                city: 0,
                phone: 0,
                token: 0,
                balance: 0,
                email: 0
              }
            }
          })
          .populate("order.user", "_id name")
          .populate("provider", "name _id ")
          .lean(true)
          .then(result => {
            const orders = result.map(e => {
              return {
                ...e,
                order: { ...e.order, files: e.order.files.map(f => f.name) }
              };
            });
            return res.json({ orders });
          });
      });
  }
};

export const getuserOrdersCounts = async (req, res) => {
  let counts = {};
  await ordersModel
    .count({ user: req.user._id, status: { $in: [1, 2] } })
    .then(count => (counts.Running = count));
  await ordersModel
    .count({ user: req.user._id, status: 0 })
    .then(count => (counts.Waitting = count));
  await ordersModel
    .count({ user: req.user._id, status: 3 })
    .then(count => (counts.Finished = count));
  await ordersModel
    .find({ user: req.user._id })
    .select("_id")
    .then(async result => {
      // { ...result.map(r => r._id) }
      let rooms = await result.map(r => r._id);
      // return res.json({ rooms });
      await Chat.aggregate()
        .match({ room: { $in: rooms } })
        //   .sort({ date: -1 })
        .group({
          _id: "$room"
        })
        .then(count => {
          counts.Messages = count.length;
        });
    });

  return res.json({ counts });
};
export const getuserOrdersOffers = (req, res) => {
  const { order_id } = req.body;
  let page = req.body.page ? Number(req.body.page) - 1 : 0;
  if (page < 0) {
    return res.json({ result: [] });
  }
  if (!order_id)
    return res.status(400).json({ error: "order_id field is required" });
  providerOfferModel
    .find({ order: order_id }, [
      "status",
      "price",
      "time",
      "provider",
      "introduction"
    ])
    .skip(page * 5)
    .limit(5)
    .populate({
      path: "order",
      populate: {
        path: "user",
        select: { avatar: 0, password: 0, token: 0, phone: 0 }
      }
    })
    .populate({
      path: "provider",
      select: {
        avatar: 0,
        password: 0,
        token: 0,
        phone: 0,
        bankName: 0,
        bankAccount: 0,
        balance: 0
      }
    })
    .lean(true)
    .then(offers => {
      res.json({ offers });
    });
};
export const giveProviderRate = (req, res) => {
  let { rate, offer_id } = req.body;
  providerOfferModel
    .findByIdAndUpdate(offer_id, { providerRate: Number(rate) })
    .then(result => {
      if (result) {
        providerOfferModel
          .find(
            { provider: result.provider, providerRate: { $ne: null } },
            "providerRate"
          )
          .lean(true)
          .then(rates => {
            let countsSum = {
              5: 0,
              4: 0,
              3: 0,
              2: 0,
              1: 0
            };
            rates.map(rate => {
              switch (rate.providerRate) {
                case 5:
                  countsSum[5] = countsSum[5] + 1;
                  break;
                case 4:
                  countsSum[4] = countsSum[4] + 1;
                  break;
                case 3:
                  countsSum[3] = countsSum[3] + 1;
                  break;
                case 2:
                  countsSum[2] = countsSum[2] + 1;
                  break;
                case 1:
                  countsSum[1] = countsSum[1] + 1;
                  break;
                default:
                  break;
              }
            });
            const newRate =
              countsSum[5] * 5 +
              countsSum[4] * 4 +
              countsSum[3] * 3 +
              countsSum[2] * 2 +
              countsSum[1] * 1;
            Provider.findByIdAndUpdate(result.provider, { rate: newRate })
              .lean(true)
              .then(() => {});
          });
        return res.json({ success: "تم التقيم بنجاح شكرا لك" });
      }
      return res.status(400).json({ error: "حدث خطأ ما" });
    })
    .catch(err => {
      console.log("====rate Error================================");
      console.log(err);
      console.log("====================================");
      return res.status(400).json({ error: "حدث خطأ ما" });
    });
};
/****provider some functions */
export const getOpenOrdersBasedOnCity = (req, res) => {
  let page = req.params.page ? Number(req.params.page) - 1 : 0;
  if (page < 0) {
    return res.json({ result: [] });
  }
  providerOfferModel
    .find({ provider: req.user._id }, "order")
    .lean(true)
    .then(ordersArr => {
      const IdsOfOrrders = ordersArr.map(e => e.order);
      console.log("====================================");
      console.log(IdsOfOrrders);
      console.log("====================================");
      DriverServices.find({ provider: req.user._id }, ["service"])
        .lean(true)
        .then(services => {
          let servicesID = services.map(e => e.service);
          ordersModel
            .find(
              {
                _id: { $nin: IdsOfOrrders },
                status: 0,
                city: req.user.city,
                service: { $in: servicesID }
              },
              [
                "status",
                "user.name",
                "service",
                "city",
                "pidfrom",
                "pidto",
                "details",
                "date",
                "strip"
              ],
              { skip: page * 5, limit: 5 }
            )
            .populate("user", "name")
            .populate("city strip")
            .populate({ path: "service", populate: "type" })
            .lean(true)
            .sort({ date: -1 })
            .then(result => {
              return res.json({ result });
            });
        });
    });
};

/****admin some functions  */

export const getProviderOrders = (req, res) => {
  let page = req.query.page - 1;
  let status = req.query.status;
  if (status == 1) status = { $in: [1, 2] };
  providerOfferModel
    .find({ provider: req.user._id, status })
    // .populate("order", "name details status service service.name")
    .lean(true)
    .sort({ date: -1 })
    .skip(page * 5)
    .limit(5)
    .populate({
      path: "order",
      select: {
        _id: 1,
        service: 1,
        user: 1,
        details: 1,
        status: 1,
        city: 1,
        files: "$files.size"
      },
      populate: {
        path: "service city user",
        select: {
          avatar: 0,
          password: 0,
          city: 0,
          phone: 0,
          token: 0,
          balance: 0,
          email: 0
        }
      }
    })
    .populate("order.user", "_id name")
    .populate("provider", "name _id ")
    .lean(true)
    .then(result => {
      const orders = result.map(e => {
        return {
          ...e,
          order: { ...e.order, files: e.order.files.map(f => f.name) }
        };
      });
      return res.json({ orders });
    });
};

export const getRunningOrderOffers = (req, res) => {
  ordersModel.find({ status: { $in: [1, 0] } }).then(result => {
    res.json({ result });
  });
};
export const getDoneOrderOffers = (req, res) => {
  providerOfferModel
    .find({ status: 3 })
    .populate({
      path: "order",
      select: {
        _id: 1,
        service: 1,
        user: 1,
        details: 1,
        status: 1,
        city: 1,
        files: "$files.size",
        status: 1
      },
      populate: {
        path: "service city user",
        select: {
          avatar: 0,
          password: 0,
          city: 0,
          phone: 0,
          balance: 0,
          email: 0
        }
      }
    })
    .then(result => {
      res.json({ result });
    });
};
export const getProblemOrdersOffers = (req, res) => {
  const page = req.params.page ? Number(req.params.page) - 1 : 0;
  providerOfferModel
    .find({ isProblem: true, problemIsSolved: { $ne: true } })
    .sort({ date: -1 })
    // .skip(page * 5)
    // .limit(5)
    .populate({
      path: "order",
      select: {
        _id: 1,
        service: 1,
        user: 1,
        details: 1,
        status: 1,
        city: 1,
        date: 1,
        pidfrom: 1,
        pidto: 1,
        files: "$files.size"
      },
      populate: {
        path: "service city user",
        select: {
          avatar: 0,
          password: 0,
          city: 0,
          phone: 0,
          token: 0,
          balance: 0,
          email: 0
        }
      }
    })
    .populate("order.user", "_id name")
    .populate("provider", "name _id ")
    .lean(true)
    .then(result => {
      let orders = [];
      result.map(e => {
        if (e.order) {
          orders.push({
            ...e,
            order: { ...e.order, files: e.order.files.map(f => f.name) }
          });
        }
      });
      console.log("====================================");
      console.log(orders);
      console.log("====================================");
      return res.json({ orders });
    });
};

export const getCanceledOrderOffers = (req, res) => {
  ordersModel.find({ status: -1 }).then(result => {
    res.json({ result });
  });
};
