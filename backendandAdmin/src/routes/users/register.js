const gravatar = require("gravatar");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
// Load Input Validation
import validateRegisterInput from "../../validation/register";

// Load User model
import User from "../../models/User";
import keys from "../../config/keys";
import PhoneVerfiyCodes from "../../models/PhoneVerfiyCodes";

export default (req, res, next) => {
  // Check Validation
  const { errors, isValid } = validateRegisterInput(req.body);
  const { code } = req.body;
  if (!code || typeof code === "undefined") {
    return res.status(400).json({ errors: { code: "الكود غير صحيح" } });
  }
  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }
  return PhoneVerfiyCodes.findOne({
    phone: req.body.phone,
    code: code + ""
  }).then(IsPhoneExist => {
    if (!IsPhoneExist) {
      return res.status(400).json({ errors: { code: "الكود غير صحيح" } });
    } else {
      User.findOne({ email: req.body.email }).then(user => {
        if (user) {
          errors.email = "Email already exists";
          return res.status(400).json(errors);
        } else {
          User.findOne({ phone: req.body.phone }).then(phone => {
            if (phone) {
              errors.phone = "Phone already exists";
              return res.status(400).json(errors);
            } else {
              const avatar = gravatar.url(req.body.email, {
                s: "200", // Size
                r: "pg", // Rating
                d: "mm" // Default
              });

              const newUser = new User({
                name: req.body.name,
                email: req.body.email,
                avatar,
                city: req.body.city,
                password: req.body.password,
                phone: req.body.phone,
                token: req.body.token
              });

              bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                  if (err) throw err;

                  newUser.password = hash;
                  newUser
                    .save()
                    .then(user => {
                      const payload = {
                        id: user._id,
                        password: user.password,
                        name: user.name
                      }; // Create JWT Payload

                      // Sign Token
                      jwt.sign(
                        payload,
                        keys.secretOrKey,
                        // { expiresIn: 3600 },
                        (err, token) => {
                          if (err) {
                            res.json(err);
                            return;
                          }
                          let returnedData = {
                            ...user._doc,
                            token: "Bearer " + token
                          };
                          delete returnedData.password;
                          res.json({
                            user: returnedData
                          });
                        }
                      );
                    })
                    .catch(err => console.log(err));
                });
              });
            }
          });
        }
      });
    }
  });
};
