const express = require("express");
const router = express.Router();
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const passport = require("passport");
// Load Input Validation
import validateUpdateInput from "../../validation/update";

// Load User model
import User from "../../models/User";
const Driver = require("../../models/Provider");
import keys from "../../config/keys";
import register from "./register";
import login from "./login";
import {
  addOrder,
  userAcceptProivederOrder,
  getuserOrders,
  getuserOrdersOffers,
  orderIsCompleted,
  getuserOrdersCounts,
  giveProviderRate
} from "../order";
import orders from "../../models/orders";
import Countracts from "../../models/Countracts";
import VerfiyPhone from "./VerfiyPhone";

// @route   GET api/users/register
// @desc    Register user
// @access  Public
//velo done
router.post("/register", register);
router.post("/verifyphone", VerfiyPhone);

router.post("/login", login);
router.put(
  "/charge",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const balance = Number(req.body.balance);
    let user_id = req.user._id;
    if (req.user.type === "admin") {
      user_id = req.body.user;
    }

    if (!balance || typeof balance !== "number")
      return res.status(400).json({ error: "balance Required" });
    User.findById(user_id, "balance").then(result => {
      if (!result) {
        return res.status(400).json({ balance: "Something Went Wrong" });
      }
      let newBalance = balance + result.balance;
      result.balance = newBalance;
      result.save();
      return res.json({
        success: `تم شحن الحساب بنجاح الرصيد الحالي ${newBalance}`
      });
    });
  }
);
router.post(
  "/giverate",
  passport.authenticate("jwt", { session: false }),
  giveProviderRate
);
router.post(
  "/addorder",
  passport.authenticate("jwt", { session: false }),
  addOrder
);
router.post(
  "/acceptorder",
  passport.authenticate("jwt", { session: false }),
  userAcceptProivederOrder
);
router.post(
  "/ordercomplete",
  passport.authenticate("jwt", { session: false }),
  orderIsCompleted
);
router.post(
  "/checkcode",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    let code = req.body.code;

    if (!code) {
      res.status(400).json({ error: "code field is required" });
    }
    let _id = req.user._id;
    let user_type = req.user.type;
    let dnyamicUserModel = user_type === "user" ? User : Driver;
    phoneVerfiyModel
      .findOneAndUpdate(
        { user: _id },
        { vefied: true },
        { new: true, upsert: true, sort: { date: -1 } }
      )
      .then(result => {
        if (result && code === result.code) {
          return dnyamicUserModel
            .findOneAndUpdate({ _id }, { phoneverfied: true }, { new: true })
            .then(updated => {
              if (updated) {
                return res.json({ success: true });
              }
            })
            .catch(err => {
              res.status(500).json({ error: "Something went wrong" });
            });
        } else {
          return res.status(400).json({ error: "code not correct" });
        }
      });
  }
);
router.get(
  "/myorders",
  passport.authenticate("jwt", { session: false }),
  getuserOrders
);
router.get(
  "/myorderscount",
  passport.authenticate("jwt", { session: false }),
  getuserOrdersCounts
);
router.post(
  "/offers",
  passport.authenticate("jwt", { session: false }),
  getuserOrdersOffers
);
router.delete(
  "/order/:id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const id = req.params.id;
    orders.findOneAndDelete({ _id: id, user: req.user._id }).then(order => {
      if (!order) {
        res.status(400).json({ error: "order not found" });
        return;
      }
      res.json({ success: "تم إغلاق المعاملة بنجاح" });
    });
  }
);

router.post(
  "/contract",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    let { text, texttype, room } = req.body;
    console.log("====================================");
    console.log(req.body);
    console.log("====================================");
    if (!text || !texttype)
      return res.status(400).json({ error: "يجب كتابة نص الرسالة" });

    let newContract = new Countracts({
      from: req.user._id,
      text,
      texttype,
      sendertype: req.user.type,
      room
    });
    newContract
      .save()
      .then(() => {
        res.json({ success: true });
      })
      .catch(err => {
        console.log("====================================");
        console.log(err);
        console.log("====================================");
        res.status(500).json({ err });
      });
  }
);
router.get(
  "/contracts/:room/:page",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { room, page } = req.params;
    console.log("====================================");
    console.log(room, page);
    console.log("====================================");
    Countracts.find({ room })
      .sort({ date: 1 })
      // .skip((Number(page) - 1) * 5)
      // .limit(5)
      .lean(true)
      .populate("room", { name: 1 })
      .then(result => {
        res.json({ result });
      });
  }
);

// @route   GET api/users/login
// @desc    Login User / Returning JWT Token
// @access  Public

router.post("/reset", (req, res) => {
  let code = req.body.code;
  let password = req.body.password;
  let password2 = req.body.password2;
  let errors = {};
  if (!code) errors.code = "code is required";
  if (!password) errors.password = "password is required";
  if (!password2) errors.password2 = "password2 is required";

  // Check Validation
  if (errors.code || errors.password || errors.password2) {
    return res.status(400).json(errors);
  }
  ResetPass.findOne({ code: code }).then(code => {
    if (!code) {
      return res.status(400).json({ error: "code expired" });
    } else {
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(password, salt, (err, hash) => {
          if (err) throw err;

          password = hash;
          User.findByIdAndUpdate(code.user, { password: password }).then(
            user => {
              if (user) {
                return res.json({ success: "password Updated Successfuly" });
              } else {
                return res.status(400).json({ error: "user not found" });
              }
            }
          );
        });
      });
    }
  });
});

router.post(
  "/update",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { errors, isValid } = validateUpdateInput(req.body);
    const isempty = require("../../validation/is-empty");
    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors);
    }
    let user_type = req.user.type === "driver" ? Driver : User;

    user_type.findOne({ email: req.body.email }).then(user => {
      if (user) {
        if (user.email !== req.user.email)
          errors.email = "Email already in use by another USER";
      }
      user_type.findOne({ phone: req.body.phone }).then(user => {
        if (user) {
          if (user.phone !== req.user.phone)
            errors.phone = "Phone already in use by another USER";
        }
        if (errors.email || errors.phone) {
          console.log("errors", errors);
          res.status(400).json(errors);
          return;
        } else {
          user_type
            .findOneAndUpdate(
              { _id: req.user._id },
              {
                name: req.body.name,
                email: req.body.email,
                phone: req.body.phone
              },
              { new: true }
            )
            .then(result => {
              const payload = {
                id: result.id,
                name: result.name,
                avatar: result.avatar
              }; // Create JWT Payload

              // Sign Token
              jwt.sign(
                payload,
                keys.secretOrKey,
                // { expiresIn: 3600 },
                (err, token) => {
                  return res.json({
                    success: true,
                    user: {
                      ...result._doc,
                      token: "Bearer " + token
                    }
                    // token: "Bearer " + token,
                    // user: user
                  });
                }
              );
              // res.json(result);
              // console.log("=============jjjj=======================");
              // console.log(errors, result);
              // console.log("====================================");
            });
        }
      });
    });

    // console.log(isempty(errors));
  }
);
//Driver Rating
router.post(
  "/rate_driver",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const driver_id = req.body.driver_id;
    const trip_id = req.body.trip_id;
    const rate = req.body.rate;
    // Check Validation
    if (typeof driver_id === "undefined" || driver_id === null) {
      res.status(400).json({ driver_id: "driver_id is required" });
      return;
    }
    if (typeof trip_id === "undefined" || trip_id === null) {
      res.status(400).json({ trip_id: "trip_id is required" });
      return;
    }
    if (typeof rate === "undefined" || rate === null) {
      res.status(400).json({ rate: "rate is required" });
      return;
    }
    Trips.findOneAndUpdate(
      { _id: trip_id, user: req.user._id, accepted_driver: driver_id },
      { driverRating: Number(rate) },
      { new: true }
    ).then(updated => {
      console.log("=========updated===========================");
      console.log(updated, req.user.type);
      console.log("====================================");
      if (updated) {
        let newRate = getdriverRate(driver_id);
        console.log("====================================");
        console.log(newRate, updated);
        console.log("====================================");
        res.json({ success: "thanks for your review" });
      } else res.status(400).json({ error: "check your inputs" });
      return;
    });
  }
);

router.post(
  "/notification/delete/:Id",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { type } = req.user;
    let user_type =
      type === "driver"
        ? {
            driver: req.user._id
          }
        : {
            user: req.user._id
          };
    //status 0 // WIth no Driver accepted Yet
    if (type === "admin") {
      res.status(500).json({ err: "permision Denied this api for user only" });
      return;
    }
    if (!req.params.Id) {
      res.status(500).json({ err: "Id params is required" });
      return;
    }
    // return res.json(req.params.Id);
    NotificationModel.findOneAndUpdate(
      { _id: req.params.Id, ...user_type },
      { deleted: type === "driver" ? 2 : 1 },
      { new: true }
    ).then(result => {
      // As always, handle any potential errors:
      if (!result)
        return res.status(400).send({ error: "Something went wrong" });
      res.json({ success: " Deleted" });
    });
  }
);

router.post(
  "/notification/all",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { type } = req.user;
    let user_type =
      type === "driver"
        ? {
            driver: req.user._id
          }
        : {
            user: req.user._id
          };
    //status 0 // WIth no Driver accepted Yet
    if (type === "admin") {
      res.status(500).json({ err: "permision Denied this api for user only" });
      return;
    }
    let deletedArr = type === "driver" ? [0, 1] : [0, 2];
    NotificationModel.find({ ...user_type, deleted: { $in: deletedArr } })
      .populate("user", ["name", "avatar", "_id"])
      .populate("driver", ["name", "avatar", "_id", "driverRating"])
      .populate("trip")
      .sort({ date: -1 })
      .then(Nots => {
        res.json(Nots);
      });
  }
);

// @route   GET api/users/current
// @desc    Return current user
// @access  Private
router.get(
  "/current",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    res.json({
      id: req.user.id,
      name: req.user.name,
      email: req.user.email
    });
  }
);
///***func */
const getdriverRate = driver_id => {
  let driverRate = 0;
  Trips.find(
    { accepted_driver: driver_id, driverRating: { $gt: 0 } },
    ["driverRating"],
    { multi: true, new: true }
  ).then(driverRates => {
    if (driverRates) {
      let sum = 0;
      let rating = {
        one: 0,
        two: 0,
        three: 0,
        four: 0,
        five: 0
      };
      driverRates.map(dr => {
        console.log("====================================");
        console.log(dr.driverRating);
        console.log("====================================");
        switch (Number(dr.driverRating)) {
          case 1: {
            rating.one = rating.one + 1;
            break;
          }
          case 2: {
            rating.two = rating.two + 2;
            break;
          }
          case 3: {
            rating.three = rating.three + 3;
            break;
          }
          case 4: {
            rating.four = rating.four + 4;
            break;
          }
          case 5: {
            rating.five = rating.five + 5;
            break;
          }
        }
        sum = sum + Number(dr.driverRating);
      });
      let sumOFInd =
        1 * rating.one +
        2 * rating.two +
        3 * rating.three +
        4 * rating.four +
        5 * rating.five;
      driverRate = sumOFInd / sum;
      Driver.findByIdAndUpdate(driver_id, {
        driverRating: Number(driverRate)
      }).then(driver => {
        return;
      });
    }
  });
  return driverRate;
};
export default router;
