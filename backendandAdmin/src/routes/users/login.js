const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
import validateLoginInput from "../../validation/login";

// Load Input Validation

// Load User model
import User from "../../models/User";
import keys from "../../config/keys";

export default (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body);
  console.log(errors);
  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;
  const token = req.body.token;
  // Find user by email
  User.findOne({ email, deleted: { $ne: true } }, { avatar: 0 })
    .populate("city")
    .then(user => {
      // Check for user
      if (!user) {
        errors.phone = "User not found";
        return res.status(404).json(errors);
      }
      // Check Password
      bcrypt.compare(password, user.password).then(async isMatch => {
        if (isMatch) {
          user.token = token;
          await user.save();
          // User Matched
          const payload = { id: user.id, password: user.password }; // Create JWT Payload

          // Sign Token
          jwt.sign(
            payload,
            keys.secretOrKey,
            // { expiresIn: 3600 },
            (err, token) => {
              let returnedData = {
                ...user._doc,
                token: "Bearer " + token,
                avatar: null
              };
              delete returnedData.password;
              res.json({
                user: returnedData
              });
            }
          );
        } else {
          errors.password = "كلمة المرور غير صحيحة";
          return res.status(400).json(errors);
        }
      });
    });
};
