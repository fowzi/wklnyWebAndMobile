const gravatar = require("gravatar");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
// Load Input Validation
import validateRegisterInput from "../../validation/register";

// Load User model
import User from "../../models/User";
import keys from "../../config/keys";
import PhoneVerfiyCodes from "../../models/PhoneVerfiyCodes";
import request from "request";

export default (req, res, next) => {
  // Check Validation
  const { errors, isValid } = validateRegisterInput(req.body);
  if (!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({ email: req.body.email }).then(user => {
    if (user) {
      errors.email = "Email already exists";
      return res.status(400).json(errors);
    } else {
      User.findOne({ phone: req.body.phone }).then(phone => {
        if (phone) {
          errors.phone = "Phone already exists";
          return res.status(400).json(errors);
        } else {
          const randomNumberBetween0and19 =
            Math.floor(1000 + Math.random() * 9000) + "";
          let options = {
            method: "GET",
            url: "http://www.shamelsms.net/api/httpSMS.aspx",
            qs: {
              username: "skanfaker",
              password: "114477",
              message: "code : " + randomNumberBetween0and19,
              mobile: req.body.phone,
              sender: "Bashrh"
            }
          };
          request(options, function(error, response, body) {
            if (error) throw new Error(error);
          });
          PhoneVerfiyCodes.findOne({
            phone: req.body.phone,
            type: "users"
          }).then(resultPhone => {
            if (resultPhone) {
              resultPhone.code = randomNumberBetween0and19;
              resultPhone.save();
            } else {
              new PhoneVerfiyCodes({
                phone: req.body.phone,
                code: randomNumberBetween0and19,
                type: "users"
              }).save();
            }
            return res.json({ success: true });
          });
        }
      });
    }
  });
};
