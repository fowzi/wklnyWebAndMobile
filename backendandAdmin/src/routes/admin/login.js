const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
import keys from "../../config/keys";
const passport = require("passport");
import Admin from "../../models/admin";
const valdiateAdmin = require("../../validation/Adminlogin");

export default (req, res) => {
  const { errors, isValid } = valdiateAdmin(req.body);

  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const email = req.body.email;
  const password = req.body.password;
  // Find user by email
  Admin.findOne({ email }).then(admin => {
    // Check for user
    if (!admin) {
      errors.email = "Credintial error";
      return res.status(404).json(errors);
    }
    // Check Password
    bcrypt.compare(password, admin.password).then(isMatch => {
      if (isMatch) {
        const payload = { id: admin.id, name: admin.name, email: admin.email }; // Create JWT Payload
        // Sign Token
        jwt.sign(
          payload,
          keys.secretOrKey,
          // { expiresIn: 3600 },
          (err, token) => {
            let returndata = { ...admin._doc };
            delete returndata.password;
            res.json({
              success: true,
              admin: {
                ...returndata,
                token: "Bearer " + token
              }
              // token: "Bearer " + token,
              // user: user
            });
          }
        );
      } else {
        errors.password = "Password incorrect";
        return res.status(400).json(errors);
      }
    });
  });
};
