import stripsModel from "../../models/strip";

export const strips = (req, res) => {
  stripsModel.find({ deleted: { $ne: true } }).then(result => {
    return res.status(200).json({ cities: result });
  });
};
export const addStrip = (req, res) => {
  if (req.body.name) {
    stripsModel.findOne({ name: req.body.name }).then(isExist => {
      if (isExist) {
        if (isExist.deleted) {
          isExist.deleted = false;
          isExist.save();
          return res.json({ success: "تم الاضافة بنجاح" });
        }
        return res.status(400).json({ error: "المدينة موجودة بالفعل" });
      }
      new stripsModel({
        name: req.body.name
      }).save();
      return res.json({ success: "تم الاضافة بنجاح" });
    });
  } else {
    return res.status(400).json({ error: "يجب ادخال اسم القطاع" });
  }
};
export const removeStrip = (req, res) => {
  if (req.body.id) {
    stripsModel
      .findById(req.body.id)
      .then(respone => {
        if (respone) {
          respone.deleted = true;
          respone.save();
          return res.json({ success: "تمت ازالة القطاع بنجاح" });
        }
        return res.status(400).json({ error: "لايوجد قطاع بذلك ID" });
      })
      .catch(() => res.status(400).json({ error: "لاتوجد قطلع بذلك ID" }));
  } else {
    return res.status(400).json({ error: "يجب ادخال ID القطاع" });
  }
};
