import { Types } from "mongoose";
import services from "../../models/sevices";
import subsevices from "../../models/subsevices";

export const GetservicesTypes = (req, res) => {
  services.find({ deleted: { $ne: true } }).then(result => {
    res.json({ services: result });
  });
};
export const Getservices = (req, res) => {
  subsevices.find({ deleted: { $ne: true } }).then(result => {
    res.json({ services: result });
  });
};
export const AddService = (req, res) => {
  const servicesName = req.body.name;
  const strip = req.body.strip;
  if (!servicesName)
    return res.status(400).json({ err: "يجب ادخال اسم الخدمة" });

  services
    .findOne({ name: servicesName })
    .then(result => {
      if (result) {
        if (result.deleted) {
          result.deleted = false;
          result.save();
          return res.json({ success: "تهانينا تم إضافة خدمة جديدة" });
        } else return res.status(400).json({ err: " الخدمة موجودة مسبقاً" });
      }
      if (!strip) {
        return res.status(400).json({ err: "يجب ادخال اسم القطاع" });
      }
      new services({ name: servicesName + "", strip: strip }).save();
      return res.json({ success: "تهانينا تم إضافة خدمة جديدة" });
    })
    .catch({ error: "للأسف حدث خطأ ما" });
};
export const AddSubService = (req, res) => {
  const servicesName = req.body.name;
  const servicesId = req.body.id;
  if (!servicesName)
    return res.status(400).json({ err: "يجب ادخال اسم الخدمة" });
  if (!servicesId)
    return res.status(400).json({ err: "لابد من اختيار نوع الخدمة" });
  subsevices
    .findOne({ name: servicesName, type: servicesId })
    .then(result => {
      if (result) {
        if (result.deleted) {
          result.deleted = false;
          result.save();
          return res.json({ success: "تهانينا تم إضافة خدمة جديدة" });
        } else return res.status(400).json({ err: " الخدمة موجودة مسبقاً" });
      }
      services.findOne({ _id: servicesId }).then(servicesExist => {
        if (!servicesExist)
          return res.json({ error: "للأسف الخدمة غير موجودة" });
        new subsevices({ name: servicesName + "", type: servicesId }).save();
        return res.json({ success: "تهانينا تم إضافة خدمة جديدة" });
      });
    })
    .catch(err => {
      return res.json({ error: "للأسف حدث خطأ ما", exception: err });
    });
};
export const RemoveService = (req, res) => {
  const id = req.body.id;

  if (!id || !Types.ObjectId.isValid(id))
    return res.status(400).json({ error: "لابد من ادخال id صحيح" });
  services
    .findById(id)
    .then(result => {
      if (result) {
        const newResult = !result.deleted;
        result.deleted = newResult;
        result.save();
        subsevices
          .updateMany({ type: id }, { deleted: newResult })
          .then(deletedDone => {
            return res.json({ success: "تم المسح بنجاح" });
          });
      } else {
        return res.status(400).json({ error: "لابد من ادخال id صحيح" });
      }
    })
    .catch(error => {
      return res.status(400).json({ error: "حدث خطأ ما" });
    });
};
export const RemoveSubService = (req, res) => {
  const id = req.body.id;

  if (!id || !Types.ObjectId.isValid(id))
    return res.status(400).json({ error: "لابد من ادخال id صحيح" });
  subsevices
    .findById(id)
    .then(deletedDone => {
      if (deletedDone) {
        const newResult = !deletedDone.deleted;
        deletedDone.deleted = newResult;
        deletedDone.save();
        return res.json({ success: "تم الحذف بنجاح" });
      }
      return res.status(400).json({ error: "لابد من ادخال id صحيح" });
    })
    .catch(error => {
      return res.status(400).json({ error: error });
    });
};
