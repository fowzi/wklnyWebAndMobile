import citiesModel from "../../models/cities";

export const cities = (req, res) => {
  citiesModel.find({ deleted: { $ne: true } }).then(result => {
    return res.status(200).json({ cities: result });
  });
};
export const addCity = (req, res) => {
  if (req.body.name) {
    citiesModel.findOne({ name: req.body.name }).then(isExist => {
      if (isExist) {
        if (isExist.deleted) {
          isExist.deleted = false;
          isExist.save();
          return res.json({ success: "تم الاضافة بنجاح" });
        }
        return res.status(400).json({ error: "المدينة موجودة بالفعل" });
      }
      new citiesModel({
        name: req.body.name
      }).save();
      return res.json({ success: "تم الاضافة بنجاح" });
    });
  } else {
    return res.status(400).json({ error: "يجب ادخال اسم المدينة" });
  }
};
export const removeCity = (req, res) => {
  if (req.body.id) {
    citiesModel
      .findById(req.body.id)
      .then(respone => {
        if (respone) {
          respone.deleted = true;
          respone.save();
          return res.json({ success: "تمت ازالة المدينة بنجاح" });
        }
        return res.status(400).json({ error: "لاتوجد مدينة بذلك ID" });
      })
      .catch(() => res.status(400).json({ error: "لاتوجد مدينة بذلك ID" }));
  } else {
    return res.status(400).json({ error: "يجب ادخال ID المدينة" });
  }
};
