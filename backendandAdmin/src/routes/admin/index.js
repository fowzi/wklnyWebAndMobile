const express = require("express");
const router = express.Router();
const gravatar = require("gravatar");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
import keys from "../../config/keys";
import login from "./login";
import path from "path";

import { addCity, removeCity, cities } from "./Cities";
import {
  AddService,
  AddSubService,
  Getservices,
  RemoveService,
  GetservicesTypes,
  RemoveSubService
} from "./Services";
import { strips, addStrip, removeStrip } from "./strips";
import {
  getRunningOrderOffers,
  getCanceledOrderOffers,
  getDoneOrderOffers,
  getProblemOrdersOffers,
  orderCompletedByAdmin,
  orderCashBackByAdmin
} from "../order";
const passport = require("passport");

import Users from "../../models/User";
import Providers from "../../models/Provider";
import Orders from "../../models/orders";
import Admin from "../../models/admin";
import Chat from "../../models/Chat";
import bunch from "../../models/bunch";
import admin from "../../models/admin";
import Countracts from "../../models/Countracts";
import PaytabsModel from "../../models/paytabs";
const valdiateAdmin = require("../../validation/Adminlogin");
const validateRegisterInput = require("../../validation/Driver/register");
const validateUserRegisterInput = require("../../validation/register");
// const citiesName = require("../cities");

// @route   GET api/users/login
// @desc    Login User / Returning JWT Token
// @access  Public
router.post("/login", login);
router.get("/cities", cities);
router.post(
  "/bunch",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const { unit, id } = req.body;
    bunch.findByIdAndUpdate(id, { unit: Number(unit) }).then(result => {
      res.json({ success: "تم التحديث بنجاح" });
    });
  }
);
router.get(
  "/balance",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    if (req.user.type != "admin") {
      return res.status(500).json({ error: "permision Denied" });
    }
    admin.findById(req.user._id, "balance").then(result => {
      res.json({ balance: result.balance });
    });
  }
);
router.put(
  "/addcity",
  passport.authenticate("jwt", { session: false }),
  addCity
);
router.delete(
  "/city",
  passport.authenticate("jwt", { session: false }),
  removeCity
);
router.get("/strips", strips);
router.put(
  "/addstrip",
  passport.authenticate("jwt", { session: false }),
  addStrip
);
router.delete(
  "/strip",
  passport.authenticate("jwt", { session: false }),
  removeStrip
);
router.get(
  "/chat/:room",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const room = req.params.room;
    Chat.find({ room: room })
      // .sort({ date: -1 })
      .lean(true)
      .then(result => {
        return res.json({ result });
      });
  }
);
router.get("/services", Getservices);
router.get("/servicestype", GetservicesTypes);
router.put(
  "/addservice",
  passport.authenticate("jwt", { session: false }),
  AddService
);
router.put(
  "/addsubservice",
  passport.authenticate("jwt", { session: false }),
  AddSubService
);
router.delete(
  "/service",
  passport.authenticate("jwt", { session: false }),
  RemoveService
);
router.delete(
  "/subservice",
  passport.authenticate("jwt", { session: false }),
  RemoveSubService
);
router.get(
  "/runningorders",
  passport.authenticate("jwt", { session: false }),
  getRunningOrderOffers
);
router.get(
  "/cancledorders",
  passport.authenticate("jwt", { session: false }),
  getCanceledOrderOffers
);
router.get(
  "/doneorders",
  passport.authenticate("jwt", { session: false }),
  getDoneOrderOffers
);
router.get(
  "/problemorders/:page",
  passport.authenticate("jwt", { session: false }),
  getProblemOrdersOffers
);
router.post(
  "/problemdone",
  passport.authenticate("jwt", { session: false }),
  orderCompletedByAdmin
);
router.post(
  "/problemcashback",
  passport.authenticate("jwt", { session: false }),
  orderCashBackByAdmin
);
router
  .post(
    "/about",
    passport.authenticate("jwt", { session: false }),
    (req, res) => {
      let about = req.body.about;
      if (!about) {
        return res.status(400).json({ error: "برجاء مليء جميع الحقول" });
      }
      Admin.findOneAndUpdate({}, { about }).then(() => {
        return res.json({ success: "تم التحديث بنجاح" });
      });
    }
  )
  .get("/about", (req, res) => {
    Admin.findOne({}).then(result => {
      res.json({ about: result.about });
    });
  });
router
  .post(
    "/terms",
    passport.authenticate("jwt", { session: false }),
    (req, res) => {
      let terms = req.body.terms;
      if (!terms) {
        return res.status(400).json({ error: "برجاء مليء جميع الحقول" });
      }
      Admin.findOneAndUpdate({}, { terms }).then(() => {
        return res.json({ success: "تم التحديث بنجاح" });
      });
    }
  )
  .get("/terms", (req, res) => {
    Admin.findOne({}).then(result => {
      res.json({ terms: result.terms });
    });
  });
router.get("/appinfo", (req, res) => {
  admin
    .findOne({}, "contactMail phone address")
    .lean(true)
    .then(resultAdmin => {
      PaytabsModel.findOne({})
        .lean(true)
        .then(result => {
          return res.json({ ...result, ...resultAdmin });
        });
    });
});
router.post(
  "/appinfo",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    if (req.user.type !== "admin") {
      return res.status(400).json({ error: "permision Denied" });
    }
    const {
      contactMail,
      phone,
      address,
      merchant_email,
      secret_key,
      site_url
    } = req.body;
    console.log(req.body);
    let errors = {};
    if (!contactMail) errors.contactMail = "هذا الحقل مطلوب";
    if (!phone) errors.phone = "هذا الحقل مطلوب";
    if (!address) errors.address = "هذا الحقل مطلوب";
    if (!merchant_email) errors.merchant_email = "هذا الحقل مطلوب";
    if (!secret_key) errors.secret_key = "هذا الحقل مطلوب";
    if (!site_url) errors.site_url = "هذا الحقل مطلوب";
    if (Object.keys(errors).length !== 0)
      return res.status(400).json({ errors });
    admin
      .findOne({})
      .then(resultAdmin => {
        resultAdmin.contactMail = contactMail;
        resultAdmin.phone = phone;
        resultAdmin.address = address;
        resultAdmin.save().then(newAdminResult => {
          PaytabsModel.findOne({}).then(result => {
            result.site_url = site_url + "";
            result.merchant_email = merchant_email + "";
            result.secret_key = secret_key + "";
            result.save().then(newResult => {
              newAdminResult.save(err => {
                console.log(err);
              });
              return res.json({
                merchant_email: newResult.merchant_email,
                secret_key: newResult.secret_key,
                site_url: newResult.site_url,
                contactMail: newAdminResult.contactMail,
                phone: newAdminResult.phone,
                address: newAdminResult.address
              });
            });
          });
        });
      })
      .catch(err => {
        res.json({ error: "حدث خطأ ما" });
      });
  }
);
router.post(
  "/users",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const page = req.body.page ? req.body.page : 0;
    if (req.user.type !== "admin") {
      res.status(400).json({ error: "permision Deniend" });
    }
    Users.find({ deleted: false }, { avatar: 0, password: 0 })
      .sort({ data: -1 })
      .skip(10 * page)
      .limit(10)
      .then(users => {
        return res.json(users);
      });
  }
);
router.post(
  "/contracts",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const page = req.body.page ? Number(req.body.page) - 1 : 0;
    if (req.user.type !== "admin") {
      res.status(400).json({ error: "permision Deniend" });
    }
    Countracts.aggregate()
      .group({
        _id: "$room",
        text: { $last: "$text" },
        texttype: { $last: "$texttype" },
        from: { $last: "$from" },
        sendertype: { $last: "$sendertype" },
        date: { $last: "$date" },
        room: { $last: "$room" }
      })
      .sort({ date: -1 })
      .skip(page * 5)
      .limit(5)
      .lookup(
        {
          from: "users",
          localField: "room",
          foreignField: "_id",
          as: "user"
        },
        { $unwind: "$user" }
      )
      .project({
        _id: 1,
        text: 1,
        texttype: 1,
        sendertype: 1,
        date: 1,
        "user._id": 1,
        "user.name": 1,
        "user.balance": 1
      })
      .then(result => {
        res.json(result);
      });
  }
);

router.post(
  "/providers",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    const page = req.body.page ? req.body.page : 0;
    if (req.user.type !== "admin") {
      res.status(400).json({ error: "permision Deniend" });
    }
    Providers.find({ deleted: false }, { avatar: 0, password: 0 })
      .populate("city", "name")
      .sort({ data: -1 })
      .skip(10 * page)
      .limit(10)
      .lean(true)
      .then(users => {
        return res.json(users);
      });
  }
);
router.post(
  "/counts",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    if (req.user.type !== "admin") {
      res.status(400).json({ error: "permision Deniend" });
    }

    let counts = {
      users: 0,
      providers: 0,
      runningorders: 0,
      doneOrders: 0,
      cancledOrders: 0,
      balance: 0
    };
    await Providers.count({}).then(count => {
      counts.providers = count;
    });
    await Users.count({}).then(count => (counts.users = count));
    await Orders.count({ status: 2 }).then(
      count => (counts.runningorders = count)
    );
    await Orders.count({ status: 3 }).then(
      count => (counts.doneOrders = count)
    );
    await Orders.count({ status: 3 }).then(
      count => (counts.cancledOrders = count)
    );
    await Admin.findOne({}).then(result => {
      counts.balance = result.balance;
    });
    res.json(counts);
    return;
  }
);
router.post(
  "/drivers/details",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    if (req.user.type !== "admin") {
      res.status(400).json({ error: "permision Deniend" });
    }
    const driver_id = req.body.driver_id;
    // Check Validation
    if (typeof driver_id === "undefined" || driver_id === null) {
      res.status(400).json({ driver_id: "driver_id is required" });
      return;
    }
    Providers.findOne({ _id: driver_id }, { password: 0, avatar: 0 }).then(
      driver => {
        if (!driver) {
          res.status(400).json({ error: "provider not found" });
          return;
        }
        res.json({ ...driver._doc });
      }
    );
  }
);
router.post(
  "/drivers/disable",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    if (req.user.type !== "admin") {
      res.status(400).json({ error: "permision Deniend" });
    }
    const driver_id = req.body.driver_id;
    let verfied = req.body.verfied;
    // Check Validation
    if (typeof driver_id === "undefined" || driver_id === null) {
      res.status(400).json({ driver_id: "driver_id is required" });
      return;
    }
    if (typeof verfied === "undefined" || verfied === null) {
      res.status(400).json({ verfied: "driver_id is required" });
      return;
    }
    verfied = verfied === "1" ? true : false;
    Providers.findOneAndUpdate(
      { _id: driver_id },
      { verfied: verfied },
      { new: true }
    ).then(driver => {
      if (!driver) {
        res.status(400).json({ error: "driver not found" });
        return;
      }
      res.json(driver);
    });
  }
);
router.delete(
  "/drivers/delete",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    if (req.user.type !== "admin") {
      res.status(400).json({ error: "permision Deniend" });
    }
    const driver_id = req.body.driver_id;
    // Check Validation
    if (typeof driver_id === "undefined" || driver_id === null) {
      res.status(400).json({ driver_id: "driver_id is required" });
      return;
    }
    Providers.findOneAndUpdate({ _id: driver_id }, { deleted: true }).then(
      driver => {
        if (!driver) {
          res.status(400).json({ error: "driver not found" });
          return;
        }
        res.json(driver);
      }
    );
  }
);

router.post(
  "/driver/balancetransfer",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    if (req.user.type !== "admin") {
      return res.status(400).json({ error: "permision Deniend" });
    }
    const { provider_id, balance } = req.body;
    if (!provider_id || !balance) {
      return res.status(400).json({ error: "برجاء ملئ جميع الحقول" });
    }
    admin.findById(req.user._id).then(result => {
      console.log("====================================");
      console.log(balance, result.balance);
      console.log("====================================");
      if (Number(balance) > result.balance) {
        return res.status(400).json({ error: "لايوجد رصيد كافي" });
      }
      result.balance = result.balance - Number(balance);
      Providers.findById(provider_id).then(providerUpdated => {
        if (Number(balance) > providerUpdated.balance) {
          return res.status(400).json({ error: "لايوجد رصيد كافي" });
        }
        providerUpdated.balance = providerUpdated.balance - Number(balance);
        providerUpdated.save();
        result.save();

        return res.json({ success: "تم تحديث الرصيد بنجاح بنجاح" });
      });
    });
  }
);
router.post(
  "/prices/update",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    if (req.user.type !== "admin") {
      res.status(400).json({ error: "permision Deniend" });
    }
    let errors = {};
    let newprices = req.body.prices;
    if (!newprices) {
      errors.newprices = "trip price is required";
    }

    if (errors.prices) {
      return res.status(400).json(errors);
    }
    prices
      .findOneAndUpdate({}, { trip: newprices }, { new: true })
      .then(newPrice => {
        res.json(newPrice);
      });
  }
);
router.post(
  "/prices",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    if (req.user.type !== "admin") {
      res.status(400).json({ error: "permision Deniend" });
    }
    prices.find({}).then(price => {
      res.json(price);
    });
  }
);

router.post(
  "/drivers/add",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    if (req.user.type !== "admin") {
      res.status(400).json({ error: "permision Deniend" });
    }
    const { errors, isValid } = validateRegisterInput(req.body);

    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors);
    }

    Providers.findOne({ email: req.body.email }).then(driver => {
      if (driver) {
        errors.email = "Email already exists";
        return res.status(400).json(errors);
      } else {
        Providers.findOne({ phone: req.body.phone }).then(phone => {
          if (phone) {
            errors.phone = "Phone already exists";
            return res.status(400).json(errors);
          } else {
            const avatar = gravatar.url(req.body.email, {
              s: "200", // Size
              r: "pg", // Rating
              d: "mm" // Default
            });

            const newDriver = new Providers({
              name: req.body.name,
              phone: req.body.phone,
              email: req.body.email,
              avatar,
              password: req.body.password,
              city: req.body.city,

              token: req.body.token
            });

            bcrypt.genSalt(10, (err, salt) => {
              bcrypt.hash(newDriver.password, salt, (err, hash) => {
                if (err) throw err;
                newDriver.password = hash;
                newDriver
                  .save()
                  .then(driver => {
                    const payload = {
                      id: driver.id,
                      name: driver.name,
                      avatar: driver.avatar
                    }; // Create JWT Payload

                    jwt.sign(
                      payload,
                      keys.secretOrKey,
                      // { expiresIn: 3600 },
                      (err, token) => {
                        res.json({
                          success: true,
                          driver: {
                            ...driver._doc,
                            nottoken: driver.token,
                            token: "Bearer " + token
                          }
                          // token: "Bearer " + token,
                          // user: user
                        });
                      }
                    );
                    // res.json(driver)
                  })
                  .catch(err => console.log(err));
              });
            });
          }
        });
      }
    });
  }
);
router.post(
  "/users/add",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    if (req.user.type !== "admin") {
      return res.status(400).json({ error: "permision Deniend" });
    }
    req.body.token = "null";
    const { errors, isValid } = validateUserRegisterInput(req.body);

    // Check Validation
    if (!isValid) {
      return res.status(400).json(errors);
    }

    Users.findOne({ email: req.body.email }).then(user => {
      if (user) {
        errors.email = "Email already exists";
        return res.status(400).json(errors);
      } else {
        Users.findOne({ phone: req.body.phone }).then(phone => {
          if (phone) {
            errors.phone = "Phone already exists";
            return res.status(400).json(errors);
          } else {
            const avatar = gravatar.url(req.body.email, {
              s: "200", // Size
              r: "pg", // Rating
              d: "mm" // Default
            });

            const newUser = new Users({
              name: req.body.name,
              email: req.body.email,
              avatar,
              password: req.body.password,
              phone: req.body.phone,
              token: null,
              city: req.body.city
            });

            bcrypt.genSalt(10, (err, salt) => {
              bcrypt.hash(newUser.password, salt, (err, hash) => {
                if (err) throw err;

                newUser.password = hash;
                newUser
                  .save()
                  .then(user => {
                    const payload = {
                      id: user.id,
                      name: user.name,
                      avatar: user.avatar
                    }; // Create JWT Payload

                    // Sign Token
                    jwt.sign(
                      payload,
                      keys.secretOrKey,
                      // { expiresIn: 3600 },
                      (err, token) => {
                        res.json({
                          success: true,
                          user: {
                            ...user._doc,
                            token: "Bearer " + token
                          }
                        });
                      }
                    );
                  })
                  .catch(err => console.log("error", err));
              });
            });
          }
        });
      }
    });
  }
);
router.delete(
  "/users/delete",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    if (req.user.type !== "admin") {
      res.status(400).json({ error: "permision Deniend" });
    }
    const user_id = req.body.user_id;
    // Check Validation
    if (typeof user_id === "undefined" || user_id === null) {
      res.status(400).json({ user_id: "user_id is required" });
      return;
    }
    Users.findOneAndUpdate({ _id: user_id }, { deleted: true }).then(user => {
      if (!user) {
        res.status(400).json({ error: "user not found" });
        return;
      }
      res.json(user);
    });
  }
);
router.post("/banks", (req, res) => {
  banks.find({}).then(data => {
    res.json(data);
  });
});
router.post(
  "/banks/update",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    if (req.user.type !== "admin") {
      return res.status(401).json({ error: "permision Deniend" });
    }
    if (typeof req.body.id === "undefined" || req.body.id === null) {
      return res.status(400).json({ error: "id is required" });
    }
    banks
      .findOneAndUpdate(
        { _id: req.body.id },
        {
          name: req.body.name,
          iban: req.body.iban,
          accountNumber: req.body.accountNumber
        },
        { new: true }
      )
      .then(data => {
        if (!data) res.status(300).json({ err: "no Bank accounts for this" });
        res.json(data);
      });
  }
);
/***test */
const multer = require("multer");
const fileType = require("file-type");
const storage = multer.diskStorage({
  limits: {
    files: 1 // 1 file
  },
  destination: (req, file, cb) => {
    cb(null, __dirname + "../../../");
  },
  filename: (req, file, cb) => {
    if (file.mimetype !== "application/pdf")
      return cb(new Error("Only .pdf files are allowed!"));
    else return cb(null, "1.pdf");
  }
});
const upload = multer({ storage }).single("pdf");
// router.post("/terms", (req, res) =>
//   upload(req, res, err => {
//     console.log(err);
//     if (err || typeof req.file === "undefined") {
//       return res.status(400).json({ err: "file not allowed" });
//     }
//     return res.json({ message: "update Success" });
//   })
// );

module.exports = router;
