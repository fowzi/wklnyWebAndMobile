import express from "express";
import passport from "passport";
import Chat from "../../models/Chat";
import Admin from "../../models/admin";
import Provider from "../../models/Provider";
import User from "../../models/User";
import providerOffer from "../../models/providerOffer";
import orders from "../../models/orders";
import Mongoose, { Types } from "mongoose";
const router = express.Router();

router.post(
  "/room",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    let usertype = req.user._id;
    if (usertype === "admin") usertype = admin;
    if (usertype === "user") usertype = User;
    if (usertype === "driver") usertype = Provider;
    let page = req.body.page;
    console.log("============room========================");
    console.log(req.body.room);
    console.log("====================================");
    page = page ? Number(page) - 1 : 0;
    Chat.find({
      room: req.body.room + ""
    })
      .sort({ date: -1 })
      .skip(page * 5)
      .limit(5)
      .then(result => {
        return res.json({
          result: result
        });
        if (page === 0) {
          providerOffer
            .findOne({ order: req.body.room + "" })
            .lean(true)
            .populate({
              path: "order",
              populate: { path: "user", select: "name _id balance" }
            })
            .lean(true)
            .then(result2 => {
              res.json({
                info: result2,
                result: result
              });
            });
        } else {
          res.json({ result });
        }
      });
  }
);
router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    let usertype = req.user.type;
    let DynamicOrderModel;
    let SearchObject = {};
    // if (usertype === "admin") usertype = admin;
    console.log("====================================");
    console.log(usertype);
    console.log("====================================");
    if (usertype === "user") {
      DynamicOrderModel = orders;
      SearchObject = { user: req.user._id };
    }
    if (usertype === "driver") {
      DynamicOrderModel = providerOffer;
      SearchObject = { provider: req.user._id };
    }
    const page = req.body.page ? Number(req.body.page) - 1 : 0;
    let rooms = [];
    DynamicOrderModel.find(SearchObject)
      .lean(true)
      .select("_id order")
      .then(async result => {
        rooms = await result.map(r => (usertype === "user" ? r._id : r.order));
        await Chat.aggregate()
          .match({ room: { $in: rooms } })
          .group({
            _id: "$room",
            text: { $last: "$text" },
            texttype: { $last: "$texttype" },
            from: { $last: "$from" },
            isProblem: { $last: "$isProblem" },
            sendertype: { $last: "$sendertype" },
            date: { $last: "$date" },
            room: { $last: "$room" }
          })
          .sort({ date: -1 })
          .skip(page * 5)
          .limit(5)
          .lookup(
            {
              from: "providerorders",
              localField: "room",
              foreignField: "order",
              as: "offer"
            },
            { $unwind: "$offer" }
          )
          .lookup(
            {
              from: "orders",
              localField: "room",
              foreignField: "_id",
              as: "order"
            },
            { $unwind: "$order" }
          )
          .lookup(
            {
              from: "providers",
              localField: "offer.provider",
              foreignField: "_id",
              as: "provider"
            },
            { $unwind: "$provider" }
          )
          .lookup(
            {
              from: "subservices",
              localField: "order.service",
              foreignField: "_id",
              as: "service"
            }
            // ,
            // { $unwind: "$subservices" }
          )
          .lookup(
            {
              from: "admins",
              localField: "from",
              foreignField: "_id",
              as: "admin"
            },
            { $unwind: "$admin" }
          )
          .lookup(
            {
              from: "users",
              localField: "order.user",
              foreignField: "_id",
              as: "user"
            },
            { $unwind: "$user" }
          )
          .project({
            _id: 1,
            "admin._id": 1,
            "admin.name": 1,
            "admin.avatar": 1,
            "user._id": 1,
            "user.name": 1,
            "provider._id": 1,
            "provider.name": 1,
            room: 1,
            text: 1,
            from: 1,
            sendertype: 1,
            texttype: 1,
            date: 1,
            "order._id": 1,
            "order.status": 1,
            "offer.provider": 1,
            "offer.details": 1,
            "offer.status": 1,
            "offer._id": 1,
            "offer.time": 1,
            "offer.price": 1,
            "offer.introduction": 1,
            "order.user": 1,
            "order.date": 1,
            "order.details": 1,
            "order.city": 1,
            "order.pidto": 1,
            "order.pidfrom": 1,
            "offer.providerRate": 1,
            service: 1,
            // filesCounts: { $size: "$order.files" }
            files: "$order.files.name"
          })
          .then(result => {
            res.json({ result });
          });
      });
  }
);
// router.post("/send");
export default router;
