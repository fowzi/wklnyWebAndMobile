/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  I18nManager
} from "react-native";
import Spinner from "react-native-spinkit";
import Navigation from "./src/Navigation";
import codePush from "react-native-code-push";

import { RFValue, RFPercentage } from "react-native-responsive-fontsize";
import { Provider } from "react-redux";
import { persistStore } from "redux-persist";
import { PersistGate } from "redux-persist/es/integration/react";
import DropdownAlert from "react-native-dropdownalert";
import NetInfoWrapper from "./src/Components/netInfoWrapper";
import {
  width,
  height,
  moderateScale,
  scale,
  verticalScale,
  colors
} from "@config";
import store from "./src/Redux";
import AlertMeassage from "./src/Components/Alert";
import NotificationWrapper from "./src/NotificationWrapper";
import Axios from "axios";
import AppWrapper from "./AppWrapper";
class App extends Component {
  constructor(props) {
    // if (!__DEV__) {
    //   console.log = () => {};
    // }

    I18nManager.forceRTL(true);
    super(props);
    let oldRender = Text.render;
    Text.render = function(...args) {
      let origin = oldRender.call(this, ...args);
      return React.cloneElement(origin, {
        style: [
          {
            fontSize: RFValue(18),
            fontFamily: "Tajawal-Bold",
            color: "darkgrey",
            writingDirection: "rtl",
            top: Platform.OS === "ios" ? moderateScale(5) : null
          },
          origin.props.style
        ]
      });
    };
    // Axios.defaults.headers.common["Accept-Encoding"] =
    //   "gzip;q=1.0, compress;q=0.5";
    // (Headers:{headers: {'Accept-Encoding':'gzip;q=1.0, compress;q=0.5'}
    // })
  }
  codePushStatusDidChange(status) {
    switch (status) {
      case codePush.SyncStatus.CHECKING_FOR_UPDATE:
        console.log("Checking for updates.");
        break;
      case codePush.SyncStatus.DOWNLOADING_PACKAGE: {
        this.setState({ newUpdate: true });
        console.log("Downloading package.");
        break;
      }
      case codePush.SyncStatus.INSTALLING_UPDATE: {
        this.setState({ newUpdate: true });
        console.log("Installing update.");
        break;
      }
      case codePush.SyncStatus.UP_TO_DATE:
        console.log("Up-to-date.");
        break;
      case codePush.SyncStatus.UPDATE_INSTALLED: {
        this.setState({ newUpdate: false });
        console.log("Update installed.");
        break;
      }
    }
  }

  codePushDownloadDidProgress(progress) {
    console.log(
      progress.receivedBytes + " of " + progress.totalBytes + " received."
    );
  }
  componentDidMount() {}
  state = {
    newUpdate: false
  };
  render() {
    const persistor = persistStore(store);

    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <AppWrapper />
          <AlertMeassage />

          {this.state.newUpdate && (
            <View
              style={{
                position: "absolute",
                width: width,
                height: height,
                backgroundColor: "rgba(0,0,0,0.5)",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                  backgroundColor: "white",
                  padding: moderateScale(10),
                  borderRadius: moderateScale(10)
                }}
              >
                <Spinner
                  style={{
                    justifyContent: "center",
                    alignSelf: "center",
                    alignItems: "center"
                  }}
                  isVisible={true}
                  type="Circle"
                  color={"green"}
                />
                <Text style={{ color: "green" }}>جاري التحديث</Text>
              </View>
            </View>
          )}
        </PersistGate>
      </Provider>
    );
  }
}

let codePushOptions = {
  checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
  installMode: codePush.InstallMode.IMMEDIATE
};
export default codePush(codePushOptions)(App);
// export default App;
