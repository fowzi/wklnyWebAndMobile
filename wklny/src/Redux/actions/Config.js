import {
  Fetch_Cities,
  Fetch_About,
  Fetch_Privicy,
  Fetch_Services,
  Fetch_Services_Types,
  Fetch_Strips,
  Fetch_CitiesLoading,
  Fetch_CitiesFail,
  Fetch_ServicesLoading,
  Fetch_Services_TypesLoading,
  Fetch_ServicesFail,
  Fetch_Services_TypesFail
} from "../types";
import Axios from "axios";
import { baseUrl } from "@config";
export const GetAppInitials = async dispatch => {
  dispatch({ type: Fetch_CitiesLoading });
  dispatch({ type: Fetch_ServicesLoading });
  dispatch({ type: Fetch_Services_TypesLoading });

  Axios.get(baseUrl + "api/admin/services")
    .then(res => {
      dispatch({ type: Fetch_Services, payload: res.data.services });
    })
    .catch(err => {
      dispatch({ type: Fetch_ServicesFail });
    });

  Axios.get(baseUrl + "api/admin/servicestype")
    .then(res => {
      dispatch({ type: Fetch_Services_Types, payload: res.data.services });
    })
    .catch(err => {
      dispatch({ type: Fetch_Services_TypesFail });
    });

  Axios.get(baseUrl + "api/admin/about").then(res => {
    dispatch({ type: Fetch_About, payload: res.data.about });
  });

  Axios.get(baseUrl + "api/admin/terms").then(res => {
    dispatch({ type: Fetch_Privicy, payload: res.data.terms });
  });

  Axios.get(baseUrl + "api/admin/cities")
    .then(res => {
      dispatch({ type: Fetch_Cities, payload: res.data.cities });
    })
    .catch(err => {
      dispatch({ type: Fetch_CitiesFail });
    });
  Axios.get(baseUrl + "api/admin/strips").then(res => {
    dispatch({ type: Fetch_Strips, payload: res.data.cities });
  });
};
export const FetchCities = async dispatch => {
  dispatch({ type: Fetch_CitiesLoading });
  Axios.get(baseUrl + "api/admin/cities")
    .then(res => {
      dispatch({ type: Fetch_Cities, payload: res.data.cities });
    })
    .catch(e => {
      dispatch({ type: Fetch_CitiesFail });
    });
};
