import {
  LOGIN_ATTEMPT,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  REGISTER_ATTEM,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  Update_Profile_Attemp,
  Update_Profile_Success,
  Update_Profile_Fail,
  ForgetPasswordAttemp,
  ForgetPasswordSucces,
  ForgetPasswordFail
} from "../types";

import axios from "axios";

import firebase from "react-native-firebase";
import localization from "../../localization/localization";
import { baseUrl, baseUrl2 } from "@config";
import Store from "../index";
import Axios from "axios";
import {
  Fetch_Orders_Watting_Attem,
  Fetch_Orders_Watting_Success,
  Fetch_Orders_Watting_Fail,
  Fetch_Orders_Running_Attem,
  Fetch_Orders_Finished_Attem,
  Fetch_Orders_Running_Success,
  Fetch_Orders_Finished_Success,
  Fetch_Orders_Running_Fail,
  Fetch_Orders_Finished_Fail,
  Fetch_Counts_Success,
  Fetch_Counts_Fail,
  Fetch_Counts_Attemp,
  Fetch_Orders_Offers_Attem,
  Fetch_Orders_Offers_Success,
  Fetch_Orders_Offers_Fail,
  UserRateAttemp,
  UserRateSuccess,
  UserRateFailed
} from "../types/Customer";
export const GiveRate = async (rate, offer_id) => {
  const AlertMessage = Store.getState().Config.alert;
  const token = Store.getState().auth.user.token;
  return await axios
    .post(
      `${baseUrl}api/users/giverate`,
      { rate, offer_id },
      {
        headers: { Authorization: token }
      }
    )
    .then(res => {
      console.log("====================================");
      console.log(res);
      console.log("====================================");
      AlertMessage("success", "شكرا لك", "تم التقيم بنجاح");
      return;
    })
    .catch(e => {
      console.log("====================================");
      console.log(e);
      console.log("====================================");
      AlertMessage("error", "خطـأ", "ther is some thing wrong");
      return;
    });
};

export const AddOrder = async (payload, navigationaction) => {
  const AlertMessage = Store.getState().Config.alert;
  const token = Store.getState().auth.user.token;
  return await axios
    .post(`${baseUrl}api/users/addorder`, payload, {
      headers: { Authorization: token }
    })
    .then(res => {
      AlertMessage("success", "", "تم إضافة المعاملة بنجاح");
      navigationaction();
      return res.data;
    })
    .catch(e => {
      AlertMessage("error", "خطأ", "حدث خطأ ما");
      return e.response;
    });
};
export const getMyOrdersByStatus = async (page, status, dispatch) => {
  const storedPage = await Store.getState().Customer.myOrders.waitting.page;

  switch (status) {
    case 0:
      dispatch({ type: Fetch_Orders_Watting_Attem });
      break;
    case 1:
      dispatch({ type: Fetch_Orders_Running_Attem });
      break;
    case 3:
      dispatch({ type: Fetch_Orders_Finished_Attem });
      break;
    default:
      break;
  }
  const token = Store.getState().auth.user.token;

  Axios.get(baseUrl + "api/users/myorders?status=" + status + "&page=" + page, {
    headers: { Authorization: token }
  })
    .then(res => {
      console.log("==res==================================");
      console.log(page, res);
      console.log("====================================");
      if (res.data.orders && (page === 1 || res.data.orders.length > 0)) {
        switch (status) {
          case 0:
            dispatch({
              type: Fetch_Orders_Watting_Success,
              payload: { page: page, data: res.data.orders }
            });
            break;
          case 1:
            dispatch({
              type: Fetch_Orders_Running_Success,
              payload: { page: page, data: res.data.orders }
            });
            break;
          case 3:
            dispatch({
              type: Fetch_Orders_Finished_Success,
              payload: { page: page, data: res.data.orders }
            });
            break;
          default:
            break;
        }
      } else {
        switch (status) {
          case 0:
            dispatch({
              type: Fetch_Orders_Watting_Fail,
              payload: { isEnd: true }
            });
            break;
          case 1:
            dispatch({
              type: Fetch_Orders_Running_Fail,
              payload: { isEnd: true }
            });
            break;
          case 3:
            dispatch({
              type: Fetch_Orders_Finished_Fail,
              payload: { isEnd: true }
            });

            break;
          default:
            break;
        }
      }
    })
    .catch(err => {
      switch (status) {
        case 0:
          dispatch({
            type: Fetch_Orders_Watting_Fail,
            payload: { isEnd: false }
          });
          break;
        case 1:
          dispatch({
            type: Fetch_Orders_Running_Fail,
            payload: { isEnd: false }
          });
          break;
        case 3:
          dispatch({
            type: Fetch_Orders_Finished_Fail,
            payload: { isEnd: false }
          });

          break;
        default:
          break;
      }
    });
};
export const RemoveMyOrder = async order_id => {
  const AlertMessage = Store.getState().Config.alert;
  const token = Store.getState().auth.user.token;
  return await axios
    .delete(`${baseUrl}api/users/order/${order_id}`, {
      headers: { Authorization: token }
    })
    .then(result => {
      AlertMessage("success", "", "تم إغلاق المعاملة بنجاخ");
    })
    .catch(err => {
      AlertMessage("error", "", "حدث خطأ ما");
    });
};
export const getMyOrdersCount = async dispatch => {
  const token = Store.getState().auth.user.token;
  dispatch({ type: Fetch_Counts_Attemp });
  Axios.get(baseUrl + "api/users/myorderscount", {
    headers: { Authorization: token }
  })
    .then(newConts => {
      console.log("==========cou==========================");
      console.log(newConts);
      console.log("====================================");
      dispatch({ type: Fetch_Counts_Success, payload: newConts.data.counts });
    })
    .catch(err => dispatch({ type: Fetch_Counts_Fail }));
};
export const getOrderOffers = async (order_id, dispatch, page) => {
  dispatch({ type: Fetch_Orders_Offers_Attem, payload: { order_id, page } });
  const token = Store.getState().auth.user.token;
  return await axios
    .post(
      `${baseUrl}api/users/offers?page=${page}`,
      { order_id, page },
      {
        headers: { Authorization: token }
      }
    )
    .then(res => {
      if (res.data.offers.length < 1) {
        dispatch({ type: Fetch_Orders_Offers_Fail, payload: { isEnd: true } });
        return;
      } else
        dispatch({
          type: Fetch_Orders_Offers_Success,
          payload: { data: res.data.offers, page: page }
        });
    })
    .catch(e => {
      dispatch({ type: Fetch_Orders_Offers_Fail, payload: { isEnd: true } });
    });
};
export const userAcceptOffer = async (order_id, provider_id, navigation) => {
  const token = Store.getState().auth.user.token;
  return await axios
    .post(
      `${baseUrl}api/users/acceptorder`,
      { order_id, provider_id },
      {
        headers: { Authorization: token }
      }
    )
    .then(res => {
      navigation.navigate("MyMessages", { item: res.data.result });
      // navigation.navigate("MyHandels", { index: 1 });
    })
    .catch(e => {
      console.log("=========err===========================");
      console.log(e.response);
      console.log("====================================");
      const AlertMessage = Store.getState().Config.alert;
      AlertMessage("error", "خطـأ", e.response.data.error);
    });
};
