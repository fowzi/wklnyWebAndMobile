export const addService_City = "addService_City";
export const addService_Sector = "addService_Sector";
export const addService_Employer = "addService_Employer";
export const addService_Address = "addService_Address";
export const addService_PriceFrom = "addService_PriceFrom";
export const addService_PriceTo = "addService_PriceTo";
export const addService_Details = "addService_Details";
export const addService_Files = "addService_Details";

export const AddOrder_Attemp = "AddOrder_Attemp";
export const AddOrder_Fail = "AddOrder_Fail";
export const AddOrder_Success = "AddOrder_Success";

export const Fetch_Orders_Watting_Attem = "Fetch_Orders_Watting_Attem";
export const Fetch_Orders_Watting_Fail = "Fetch_Orders_Watting_Fail";
export const Fetch_Orders_Watting_Success = "Fetch_Orders_Watting_Success";

export const Fetch_Orders_Running_Attem = "Fetch_Orders_Running_Attem";
export const Fetch_Orders_Running_Fail = "Fetch_Orders_Running_Fail";
export const Fetch_Orders_Running_Success = "Fetch_Orders_Running_Success";

export const Fetch_Orders_Finished_Attem = "Fetch_Orders_Finished_Attem";
export const Fetch_Orders_Finished_Fail = "Fetch_Orders_Finished_Fail";
export const Fetch_Orders_Finished_Success = "Fetch_Orders_Finished_Success";

export const Fetch_Orders_Offers_Attem = "Fetch_Orders_Offers_Attem";
export const Fetch_Orders_Offers_Fail = "Fetch_Orders_Offers_Fail";
export const Fetch_Orders_Offers_Success = "Fetch_Orders_Offers_Success";

export const Fetch_Counts_Success = "Fetch_Counts_Success";
export const Fetch_Counts_Fail = "Fetch_Counts_Fail";
export const Fetch_Counts_Attemp = "Fetch_Counts_Attemp";

export const UserRateAttemp = "user-rate-attep";
export const UserRateSuccess = "user-rate-success";
export const UserRateFailed = "user-rate-failed";
