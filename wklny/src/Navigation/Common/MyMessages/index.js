import React, { Fragment, useRef, useState, useEffect } from "react";
import {
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  Text,
  FlatList,
  KeyboardAvoidingView,
  Platform
} from "react-native";
import StarRating from "react-native-star-rating";
import {
  width,
  height,
  moderateScale,
  scale,
  verticalScale,
  colors
} from "@config";
import Header from "@components/Header";
import { Images } from "@assets";
import { RFValue, RFPercentage } from "react-native-responsive-fontsize";
import ActionSheet from "react-native-actionsheet";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import SimpleLineIcons from "react-native-vector-icons/SimpleLineIcons";
import { Input } from "native-base";
import { SimpleAnimation } from "react-native-simple-animations";
import { connect } from "react-redux";
import {
  FetchRoomChat,
  orderCompleted,
  getOrderDetails
} from "../../../Redux/actions/Chat";
import io from "socket.io-client";
import { baseUrl } from "@config";
import {
  Reset_CHat_IN_Room,
  Fetch_Chat_IN_Room_Success
} from "../../../Redux/types";
import Moment from "moment";
import Spinner from "react-native-spinkit";
import Pdf from "react-native-pdf";
import { GiveRate } from "../../../Redux/actions/Customer";
const RenderIemCard = props => {
  const { provider, service, user } = props;
  const { from, sendertype, text, texttype, icon, details, date } = props.item;
  const name =
    sendertype === "users"
      ? user.name
      : sendertype === "providers"
      ? provider.name
      : "الإدارة";
  const adminImage = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABTVBMVEX///8/UbX/t01CQkL/mADTLy83SrOLlM7/pyZ4Rxk9QEL/uE3/nRgzMzP/lgD/u03FxcU2PEL/s0MxRrJWVlY8PDz/vk4rNkElPa//kgAuRLHQFBSsgkcxMTHh4/L/8uPd3d1tPRTsq0z/sjnji4v/oQDSJSVOXrpvb2/v7+/RHBz/qzYwU7vt7vf/5MP/v2T01NT/+vRdUUPR0dFseMNhbsCrq6uvtd2mrNnS1etEVrf/27jqqanaXV3ihob0rknfnUHGyub/9+3/y4V5g8j/rE3/16T55eX/qkPbLBz/rCNnSpvDhjd3R5FfX1+QkJC5NlD/0Z2HQ4SvOV7GMj7MMDaWQHjdKg5WTaaiPWuSmtHnnJzvwcH229vYSEior9rebW1xWT2MaDnftYLmmSuHVB/EkkqgaimcnJxra2ulfkf/x3vvlGMQMa3/7NS8HcLdAAAJ1UlEQVR4nO2aa3vTNhSAkzRL07hpmqShWUZ6C+ktKVAgvcHonVIu7RiXscHGBoPC2Pj/HyfJl0i2bMuWHCmg90Ofp46tR2+OfM6xnFRKo9FoNBqNRqPRaDQajUaj0Wg0Go3mG2NucuPj+aezsymCc9nTEsPK5PlUs1ks1gAjJM052ZPjZ2XjrFl0iznUlmXPj5fJ5QC9ryCIGyPNQL1hD+JGrRiiN9xBvDnF4jfEQVxuMvkNbRBvBqeX4Q/iR+YAQoobc7InHBX2FWopNovLkyuyZ83OyhT7CnWoFYGk7JkzsjISQxBJNkc2ZE+ehdiCkGJtCBzjLFHccWpOtkEIZ3yCcK1+lO0QyDJbHxNIU+X6uBGxTNCpjShbOeaECKqsKMYPKcpWoXMu4Ca0Fc9ky9AQtUYRRRW3qDgroYvmTdk+HiZFhhAiW8iD0AgCiqpV/g1xacaiqVjJEO0H8qlayYYjhIVymf6BWkGciq03fePy5WnqZ0WVHqVuxkqkSG+hVLrko6hSOj2PnklNvUslI5PJ+CiqVBOj3oV29KBexldRoVwzGckQj57NpX+oUZQt5rDMvkhper5RVGc7nDWE7sWZCYmiMtmULZP6RS8gisps+H8MX6RB0QuKomw1i08hhmHR84+iKm1N4CJliZ6voiIVMeDhnjV6fgtVkVTjVw2jRM8niorUfGqiiaPnjaIiydRb76MuTv8oTsmWQ0zR9KJHjx5F2XKImkvvn5jRo0WxJlsOstIUFT1KFIsqFESnWBS4o9dXtDc2lCj5TldaMIToAQyjYBvOydZLYeWwIEYPoQ0HSrKGKjSmiRoWVfiRjTbUhtpQPtpQG2pD+XwDhs0EDZsqGL4rFxIzLNTeydZLrVfaf48UEjIs3+i0K+uSDTt5wI0y3dAwWB6JaWchw/JfcPCKXMHDNpxE/q9piqFR2t1bKJVC/Eqlhb1dz+YANJx++wCO3T6UKThrCuYfvJ32GBqZi/n5+bG9YMXS3hg46yLjUgSG05cfmIO3DyQaWiEES+nydMFt+H5+DDAfqGg8Mk967zYsTH+oWGO3n0g07ORtw0sfymUyOObcAe74EHG2zpl/RJ5ULu+VbMN8RqJh2zHMlBZGSMP39uT3Agz37K/hPRnpkYVSxjFsyxNcxwwzxi45+TEnPP7LtB9o1zLdBf/2DSUWDNzQtRj5YggvUSGGqQpu6BeeoJpoBARaifvwSdvXMFO6mA8LoRPE+QvKSlYilx4EGBqlR6DUXSwE10NjAVTNMeqt6hjKrIdOQaQYwnZlNxP6KsMoZbwtDW4oNYSATNvfELacIX5BJ5mG7V25gqnUbjvAkAtk2L4mWzCVupcHji1B79UwjBbwy9+TrYe4+nk3fMIxuHZ4VbZany/il2nri2wpgtmWeMNZ2VIk4mMo+dnewzXhhgokUYInooNYkVzoPVwVbqhQHkWsi041Ldm7iB5E34iq3YagtRG7TCtqNDM4B2KXaUvqIxMdscv0s2wdCldFBrGlWiZFCH28kC1D5ba4XFO5LVuGjjBBRUMIgijqTmwpGkJx6VS9am/zRUwQFXv2JRDyhKHcUwWBiHWq7hqFCNjNUG33wg13PlWzm8F5wqfYUvomNDnkyTYVqT+8YOVzfMWKio8UFGIrDosgWKjx7sXWUCxRk3txFFvqbVwEEP1x2FC/TJAcRO1urim4MRNCh+kFsBVAoyN7ujEYz1ZYFY1Kdlz2dGMwPjExXmGIo2FU4KmypxuD8YkscgyNH/DLDqthFjp2An+b2IF+2SE2hI5AEkbScLmB6EE966whNjQls/lOBf2i2zD/ZCqdfNbWG35Dy3IiOw7Iwz/mv/jHw2/Y93S5fWWGvmhDJdGG2lB9tOHQG84cfw+g1T633AQ88XhG9oQjsbZ5VP1vPzV7+/A4G6SJ5LLHh7dnU/v/VY8212RPnI2ZraVGNZfO9cx/geZ9KOK2RMfuQzlEL53OVRu9LeUl194s1XNpRB1bdzCaE3Y0UeQmjh05yEzdvChX720Oftbs7N9qVNM2uVHXp2Y0s3jkHEZzzmXVxqiq9+TmTsOZJ5or7aSDf6l7alXiwnpvP9mpxmIrR8wSUKWttys/XKEc3XRdm2vsqLZYvX6AHe95V374jqa44724saNSHDdpfmCS2+4TgeB3FMXtBvXyJc/1ktheok4QrLUj15lIkKJ4lKMPUL+lRPEYrdOnB2dITtAS9Ciu+Y6Qq28NUoXKftrn+0cTJObnCLoVtwKGqC5JLh1H/gFEitipmKBLMXCItNQwbgcFENLo50NCkFDc97mNnUFuyXBDbAYHEMawZ5/rEsQVeyFfE/gaJSXVkBWKcJrTk0WX4eKJ9ckMyzBS6n+PWgPdX7/TnJ6sEoKrtiDWkgYp/jZwv7WwW9Ci6lxBRNGJoKsl9WXgN+PaDpsg3pxiipiguyX1HWiwims5RsF0eql/1XWn4l/vH1xiHaiftQYhyLhEIfV+HnxNMdxmyDO24gCjyLpE0bz6zekdO9ms3nGO+bWkNKruPjcxQgsYQcNpTu/aN+LiXfvQWki1dw01oIx6xJgbLPrN6Y+O4Y/2oaCWlMJg6uJWpK89jRWM504ufW4fivZlEXd1YkRIDRZ2c7qOPVtYg4W1pF6omz9iiTqlfpr/HTP83TwU7Y5GgyWeUG9FnpPTnL7u922rr9ERlpbUTSPhW3E/xpzs5vQVZvgKHWFqSd3Uk93ZiDMl8L2ja+9iXZtZLiLfhZBk12msL91uTv/AOu8/4AHWltRFPcFtRv8toxBQc4o/Pq3CA8wtqZvkDOOkGQQsYyfEPs1JnLpjUX2TlGDsKaHm9DpheD1aS+oaLinD2CFEzekdYpXeidiSEiQVxDjVywY0p1eIZ/xXUVtScrhkDOOvKkjqT8Lwz5iFx4T6WosbjlWVhs0pXixAuYjekuIshc83Om/iVS+bHrmduNjjGq2exF5/7Opl0v2JWKU/dblG87xCF8AM16oCPD7FDE8fc46WQK75jSvPALrEKuULIe3tKzeUF9ERDV9gnfcLXkPxyzR2S9rnJWb4kns04c1pzOcAnO5TZyfqKW8IE8imfOXe5KGda04f8g8mvOjzTwkE8Zll+Iw/hN6fQXDC19BYdH82m+/VnwUY0n6twwNfj+VgxVDIWA2x+zXc1RDR/RXmmsVfRYRQdEUUkWgAL2GuORVQKtLCUw13vTeBzSlvS2ojuObz13sT0Jxyt6QWYncVBXQ0Jt3ni8/FhFBwMqX/eDAG3Ren3C2pTV2koaBiAdj5RdRIYg0FdKUWXSHVHiH0BQbnDkYyNET23jw7f4khtOR/A4b1nHoIfQe1P6oiKv2YX6PRaDQajVz+BwAXZkEbOTj/AAAAAElFTkSuQmCC`;
  const profile =
    sendertype === "users"
      ? baseUrl + "api/user/image/0/" + user._id
      : sendertype === "providers"
      ? baseUrl + "api/user/image/1/" + provider._id
      : adminImage;
  return (
    <View style={styles.card}>
      <View style={{}}>
        <View>
          <View
            style={{
              flexDirection: "row"
            }}
          >
            <Image
              source={{ uri: profile }}
              style={{
                width: width * 0.2,
                height: height * 0.1,
                resizeMode: "stretch",
                backgroundColor: "#eee"
              }}
            />
            <Text
              style={{
                fontWeight: "bold",
                top: "8%",
                marginHorizontal: moderateScale(15)
              }}
            >
              {name}
            </Text>
          </View>
          <Text
            style={{
              fontSize: RFValue(14),
              width: width * 0.7,
              flexWrap: "wrap",
              textAlign: "left"
            }}
          >
            {text}
          </Text>
        </View>
        <Text
          style={{
            writingDirection: "ltr",
            fontSize: RFValue(14),
            alignSelf: "flex-end",
            color: "#ccc"
          }}
        >
          {Moment(date).format('DD-MM-YYYY hh:mm A"')}
        </Text>
      </View>
    </View>
  );
};
const detailsTextRow = ({ title, text }) => {
  return (
    <View
      style={{
        marginTop: moderateScale(10),
        flexDirection: "row",
        width: width * 0.9,
        justifyContent: "space-evenly",
        alignItems: "center"
      }}
    >
      <Text
        style={{
          color: "#57B235",
          width: "50%",
          textDecorationLine: "underline"
        }}
      >
        {title}
      </Text>
      <Text style={{ width: "50%" }}>{text}</Text>
    </View>
  );
};
let socket;
const MyHandels = props => {
  const [firstLoad, setfirstLoad] = useState(true);
  const [text, settext] = useState(null);
  const [openDetails, setopenDetails] = useState(false);
  const [PdfurlIndex, setPdfurlIndex] = useState(false);
  const [rateView, setRateView] = useState(false);
  // let order = props.navigation.state.params.item;
  const [order, setOrderData] = useState(props.navigation.state.params.item);
  const callBack = props.navigation.state.params.callBack;
  const provider = order.provider;
  const user = order.user;
  const service = order.service;
  const [starCount, setStarRating] = useState(order.offer.providerRate || 0);

  useEffect(() => {
    return () => {
      try {
        callBack();
      } catch (error) {
        console.log("====================================");
        console.log(error);
        console.log("====================================");
      }
      socket.disconnect();
    };
  }, []);
  if (firstLoad) {
    socket = io(baseUrl);
    props.messages.data[0] &&
      props.messages.data[0].room != order._id &&
      props.dispatch({ type: Reset_CHat_IN_Room });
    setfirstLoad(false);
    props.fetchMessages(order._id, 1);
    socket.emit("joinroom", {
      room: order._id,
      token: props.user.token
    });
    socket.on("send", payload => {
      if (
        payload.sendertype == "users" &&
        payload.text == "شكرا لك ,  لقد قمت بإعتماد الخدمة"
      ) {
        setOrderData({
          ...order,
          order: { ...order.order, status: 3 },
          status: 3
        });
      }
      props.dispatch({
        type: Fetch_Chat_IN_Room_Success,
        payload: {
          data: [payload],
          page: props.messages.page,
          isSocket: true
        }
      });
    });
  }

  const [typing, setTyping] = useState(false);
  const [cancelJobModel, SetCancelJobModel] = useState(false);
  const [EndJobModel, SetEndJobModel] = useState(false);
  const ActionModel = Internalprops => {
    const { type } = Internalprops;
    const { userType } = props;
    const [doneLoading, setdoneLoading] = useState(false);
    const textline1 =
      type === "end" ? "بانتهاء المعاملة" : "تعثر المعاملة وعدم استكمالها";
    const textline2 =
      userType === "Customer"
        ? type === "end"
          ? "وبالتالي إستحقاق مقدم الخدمة المبلغ المدفوع"
          : "وبالتالي عدم استحقاق مقدم الخدمة المبلغ المتفق عليه"
        : type === "end"
        ? "وبالتالي إستحقاقي المبلغ المتفق عليه"
        : "";

    return (
      <View
        style={{
          flex: 1,
          width: "80%",
          backgroundColor: colors.main,
          position: "absolute",
          alignSelf: "center",
          top: height * 0.3,
          borderRadius: moderateScale(10),
          padding: moderateScale(10),
          zIndex: 10000,
          elevation: 9
        }}
      >
        <Text style={{ color: "white", fontSize: RFValue(14) }}>
          اقر انا {props.user.name}
        </Text>
        <Text style={{ color: "white", fontSize: RFValue(14) }}>
          {textline1}
        </Text>
        <Text style={{ color: "white", fontSize: RFValue(14) }}>
          {textline2}
        </Text>
        <Text style={{ color: "white", fontSize: RFValue(14) }}>
          {type === "cancle" &&
            " ارجو التدخل السريع من إدارة الخدمة واتحاذ كافة الاجراءات"}
        </Text>
        <View
          style={{
            flexDirection: "row",
            width: "80%",
            justifyContent: "space-between",
            alignSelf: "center",
            marginTop: moderateScale(15)
          }}
        >
          <TouchableOpacity
            style={{
              backgroundColor: "white",
              borderRadius: moderateScale(15),
              paddingHorizontal: moderateScale(15)
            }}
            disabled={doneLoading}
            onPress={() => {
              setdoneLoading(true);
              if (type === "end") {
                orderCompleted(order.offer._id).then(() => {
                  socket.emit("send", {
                    token: props.user.token,
                    text: "شكرا لك ,  لقد قمت بإعتماد الخدمة",
                    texttype: "text",
                    isProblem: false,
                    room: order._id
                  });
                  if (userType === "Customer") {
                    // props.navigation.navigate("Profile");
                    setOrderData({
                      ...order,
                      order: { ...order.order, status: 3 },
                      status: 3
                    });
                  }
                  SetEndJobModel(false);
                  setdoneLoading(false);
                });
              }
              if (type === "cancle") {
                socket.emit("send", {
                  token: props.user.token,
                  text: "برجاء التدخل السريع من الإدارة",
                  texttype: "text",
                  isProblem: true,
                  room: order._id
                });
                SetCancelJobModel(false);
                setdoneLoading(false);
              }
            }}
          >
            {!doneLoading && <Text style={{ color: colors.main }}>تأكيد</Text>}
            {doneLoading && (
              <Spinner
                style={{
                  justifyContent: "center",
                  alignSelf: "center",
                  alignItems: "center"
                }}
                size={moderateScale(18)}
                isVisible={true}
                type="Circle"
                color={"#57B235"}
              />
            )}
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              backgroundColor: "white",
              borderRadius: moderateScale(15),
              paddingHorizontal: moderateScale(15)
            }}
            onPress={() => {
              type === "cancle"
                ? SetCancelJobModel(false)
                : SetEndJobModel(false);
            }}
          >
            <Text style={{ color: colors.main }}>الغاء</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };
  return (
    <View bounces={false} style={{ flex: 1 }}>
      <Header navigation={props.navigation} title="مراسلاتي" />
      <View style={styles.card}>
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <View style={{ flexDirection: "row" }}>
            <MaterialCommunityIcons
              name="wallet-travel"
              size={RFValue(20)}
              color="#57B235"
              style={{ alignSelf: "center" }}
            />
            <Text style={{ fontWeight: "bold", top: "8%" }}>
              {service.name}
            </Text>
          </View>
        </View>
        <View
          style={{
            marginVertical: moderateScale(15),
            flexDirection: "row",
            justifyContent: "space-evenly",
            alignItems: "center"
          }}
        >
          <TouchableOpacity
            style={{
              flexDirection: "row",
              backgroundColor: colors.main,
              borderWidth: 1,
              borderColor: colors.main,
              borderRadius: moderateScale(1),
              width: width * 0.2,
              alignItems: "center",
              justifyContent: "center",
              marginRight: moderateScale(10),
              paddingVertical: moderateScale(5),
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 4
              },
              shadowOpacity: 0.3,
              shadowRadius: 4.65,
              elevation: 8,
              opacity: order.order.status === 3 ? 0.5 : 1
            }}
            disabled={order.order.status === 3}
            onPress={() => SetEndJobModel(true)}
          >
            <Text
              style={{
                fontSize: RFValue(10),
                height: "100%",
                color: "white"
              }}
            >
              انتهاء المعاملة
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flexDirection: "row",
              backgroundColor: colors.main,
              borderWidth: 1,
              borderColor: colors.main,
              width: width * 0.2,
              alignItems: "center",
              justifyContent: "center",
              paddingVertical: moderateScale(5),
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 4
              },
              shadowOpacity: 0.3,
              shadowRadius: 4.65,
              elevation: 8,
              opacity: order.order.status === 3 ? 0.5 : 1
            }}
            onPress={() => SetCancelJobModel(true)}
            disabled={order.order.status === 3}
          >
            <Text
              style={{
                fontSize: RFValue(10),
                height: "100%",
                color: "white"
              }}
            >
              تعثر المعاملة
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              flexDirection: "row",
              backgroundColor: colors.main,
              borderWidth: 1,
              borderColor: colors.main,
              width: width * 0.2,
              alignItems: "center",
              justifyContent: "center",
              paddingVertical: moderateScale(5),
              shadowColor: "#000",
              shadowOffset: {
                width: 0,
                height: 4
              },
              shadowOpacity: 0.3,
              shadowRadius: 4.65,
              elevation: 8
            }}
            onPress={() => setopenDetails(true)}
          >
            <Text
              style={{
                fontSize: RFValue(10),
                height: "100%",
                color: "white"
              }}
            >
              التفاصيل
            </Text>
          </TouchableOpacity>
        </View>
      </View>

      <FlatList
        inverted
        bounces={true}
        data={props.messages.data}
        extraData={props.messages.data}
        onEndReachedThreshold={0.5}
        onEndReached={({ distanceFromEnd }) => {
          let pageNum = Number((props.messages.data.length / 5).toFixed(0)) + 1;
          !props.messages.isEnd && props.fetchMessages(order._id, pageNum);
        }}
        style={{
          flex: 1,
          height: height * 0.7,
          marginBottom: moderateScale(20)
        }}
        renderItem={({ item, index }) => (
          <RenderIemCard
            provider={provider}
            service={service}
            user={user}
            item={item}
            key={index}
          />
        )}
        keyExtractor={(item, index) => {
          item.title + index;
        }}
        ListEmptyComponent={
          props.messages.loading ? (
            <View
              style={{
                flex: 1,
                height: height * 0.5,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Spinner
                style={{
                  justifyContent: "center",
                  alignSelf: "center",
                  alignItems: "center"
                }}
                isVisible={props.loading}
                type="Circle"
                color={"#57B235"}
              />
            </View>
          ) : (
            <View
              style={{
                flex: 1,
                height: height * 0.5,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text>لايوجد رسائل</Text>
            </View>
          )
        }
      />
      {!typing ? (
        order.order.status !== 3 && (
          <TouchableOpacity
            onPress={() => setTyping(true)}
            style={{
              position: "absolute",
              bottom: moderateScale(10),
              right: moderateScale(10)
            }}
          >
            <MaterialCommunityIcons
              name="plus-circle"
              color={colors.main}
              size={RFValue(50)}
            />
          </TouchableOpacity>
        )
      ) : (
        <KeyboardAvoidingView
          behavior={Platform.OS === "ios" ? "position" : "height"}
          keyboardVerticalOffset={moderateScale(10)}
          style={{
            width: "100%",
            maxHeight: height * 0.2,
            alignSelf: "center",
            position: "absolute",
            left: 0,
            bottom: moderateScale(15),
            zIndex: 1000,
            elevation: 24
          }}
        >
          <SimpleAnimation
            delay={100}
            duration={500}
            movementType="slide"
            staticType="bounce"
          >
            <View
              style={{
                width: "100%",
                maxHeight: height * 0.2,
                alignSelf: "center",
                flexDirection: "row",
                justifyContent: "space-between"
              }}
            >
              <Input
                multiline
                style={{
                  backgroundColor: "white",
                  writingDirection: "rtl",
                  textAlign: "right",
                  fontSize: RFValue(16),
                  fontFamily: "Tajawal-light",
                  borderRadius: moderateScale(10),
                  marginHorizontal: moderateScale(10),
                  paddingTop: moderateScale(10),
                  shadowColor: "#000",
                  shadowOffset: {
                    width: 0,
                    height: 12
                  },
                  shadowOpacity: 0.58,
                  shadowRadius: 16.0,
                  elevation: 50,
                  borderWidth: 1,
                  borderColor: "#ccc"
                }}
                textAlignVertical="center"
                placeholder="اكتب رسالتك"
                value={text}
                onChangeText={v => settext(v)}
              />
              <View style={{ width: "30%" }}>
                <TouchableOpacity
                  style={{
                    backgroundColor: colors.main,
                    padding: moderateScale(10),
                    alignSelf: "center",
                    marginHorizontal: moderateScale(10),
                    borderRadius: moderateScale(10),
                    justifyContent: "center",
                    alignItems: "center",
                    marginBottom: moderateScale(10)
                  }}
                  onPress={() => {
                    setTyping(false);
                  }}
                >
                  <Text style={{ color: "white" }}>إلغاء</Text>
                </TouchableOpacity>
                <TouchableOpacity
                  style={{
                    backgroundColor: colors.main,
                    padding: moderateScale(10),
                    alignSelf: "center",
                    marginHorizontal: moderateScale(10),
                    borderRadius: moderateScale(10),
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                  onPress={() => {
                    socket.emit("send", {
                      token: props.user.token,
                      text: text,
                      texttype: "text",
                      isProblem: false,
                      room: order._id
                    });
                    settext(null);
                    setTyping(false);
                  }}
                >
                  <Text style={{ color: "white" }}>إرسال</Text>
                </TouchableOpacity>
              </View>
            </View>
          </SimpleAnimation>
        </KeyboardAvoidingView>
      )}
      {order.order.status !== 3 ? (
        <View />
      ) : !rateView ? (
        <TouchableOpacity
          onPress={() => setRateView(true)}
          style={{
            position: "absolute",
            bottom: moderateScale(20),
            right: moderateScale(10)
          }}
        >
          <MaterialCommunityIcons
            name="star-circle"
            color={colors.main}
            size={RFValue(50)}
          />
        </TouchableOpacity>
      ) : (
        <KeyboardAvoidingView
          behavior={Platform.OS === "ios" ? "position" : "height"}
          keyboardVerticalOffset={moderateScale(10)}
          style={{
            width: "100%",
            maxHeight: height * 0.3,
            alignSelf: "center",
            position: "absolute",
            left: 0,
            top: height * 0.5,
            zIndex: 1000,
            elevation: 24
          }}
        >
          <SimpleAnimation
            delay={100}
            duration={500}
            movementType="slide"
            staticType="bounce"
          >
            <View
              style={{
                width: "90%",
                backgroundColor: "white",
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center",
                shadowColor: "#000",
                shadowOffset: {
                  width: 0,
                  height: 4
                },
                padding: moderateScale(10),
                paddingVertical: moderateScale(30),
                shadowOpacity: 0.32,
                shadowRadius: 5.46,
                borderColor: "#eee",
                borderWidth: 1,
                elevation: 9,
                borderRadius: moderateScale(4),
                backgroundColor: "white"
              }}
            >
              {props.userType === "Customer" && (
                <Text style={{ alignSelf: "center" }}>تقيم المعاملة </Text>
              )}
              <SimpleLineIcons
                name="like"
                color="#57B235"
                size={moderateScale(50)}
              />
              <StarRating
                disabled={props.userType !== "Customer"}
                maxStars={5}
                rating={starCount}
                reversed
                fullStarColor={colors.main}
                selectedStar={rating => setStarRating(rating)}
              />
              {props.userType === "Customer" && (
                <TouchableOpacity
                  style={{
                    marginTop: moderateScale(10),
                    backgroundColor: colors.main,
                    paddingHorizontal: moderateScale(40),
                    paddingVertical: moderateScale(10),
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                  onPress={() => {
                    GiveRate(starCount, order.offer._id);
                  }}
                >
                  <Text style={{ color: "white" }}>تقيم</Text>
                </TouchableOpacity>
              )}
            </View>
          </SimpleAnimation>
          <TouchableOpacity
            style={{
              position: "absolute",
              top: moderateScale(10),
              right: width * 0.11
            }}
            onPress={() => setRateView(false)}
          >
            <Text>إغلاق</Text>
          </TouchableOpacity>
        </KeyboardAvoidingView>
      )}

      {EndJobModel && <ActionModel type="end" />}
      {cancelJobModel && <ActionModel type="cancle" />}
      {openDetails && (
        <ScrollView
          bounces={false}
          style={{
            position: "absolute",
            bottom: 0,
            left: 0,
            width: width,
            height: height * 0.8,
            backgroundColor: "white",
            elevation: 9
          }}
        >
          <SimpleAnimation
            delay={100}
            duration={500}
            movementType="slide"
            staticType="bounce"
          >
            <View
              style={{
                width: "100%",
                alignSelf: "center",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  color: "#57B235",
                  textDecorationLine: "underline",
                  alignSelf: "center",
                  textAlign: "center"
                }}
              >
                المعاملة
              </Text>
              {detailsTextRow({ title: "الخدمة", text: service.name })}
              {detailsTextRow({
                title: "السعر",
                text: `من ${
                  order.order.pidfrom ? order.order.pidfrom : "--"
                } إلي ${order.order.pidto ? order.order.pidto : "--"} ريال`
              })}
              {detailsTextRow({
                title: "التفاصيل",
                text: order.order.details
              })}
              {detailsTextRow({
                title: "التاريخ",
                text: Moment(order.order.date).format('DD-MM-YYYY hh:mm A"')
              })}
              {order.files.map((e, index) => {
                return (
                  <View
                    style={{
                      marginTop: moderateScale(10),
                      flexDirection: "row",
                      width: width * 0.9,
                      justifyContent: "space-evenly",
                      alignItems: "center"
                    }}
                  >
                    <Text
                      style={{
                        color: "#57B235",
                        width: "50%",
                        textDecorationLine: "underline"
                      }}
                    >{`ملف #${index}`}</Text>
                    <TouchableOpacity
                      onPress={() => setPdfurlIndex(index)}
                      style={{ width: "50%" }}
                    >
                      <Text
                        style={{
                          textDecorationLine: "underline",
                          color: "black"
                        }}
                      >
                        {e}
                      </Text>
                    </TouchableOpacity>
                  </View>
                );
              })}
              <View
                style={{
                  borderTopColor: "#eee",
                  borderTopWidth: 1,
                  marginTop: moderateScale(10)
                }}
              >
                <Text
                  style={{
                    color: "#57B235",
                    textDecorationLine: "underline",
                    alignSelf: "center",
                    textAlign: "center"
                  }}
                >
                  العرض
                </Text>
                {detailsTextRow({
                  title: "مقدمة العرض",
                  text: order.offer.introduction
                })}
                {detailsTextRow({
                  title: "السعر",
                  text: `${order.offer.price} ريال`
                })}
                {detailsTextRow({
                  title: "الوقت",
                  text: order.offer.time
                })}
              </View>
            </View>
          </SimpleAnimation>
          <TouchableOpacity
            style={{ position: "absolute", top: 0, left: 0 }}
            onPress={() => setopenDetails(false)}
          >
            <Text>إغلاق</Text>
          </TouchableOpacity>
        </ScrollView>
      )}
      {PdfurlIndex !== false && (
        <ScrollView
          bounces={false}
          style={{
            position: "absolute",
            bottom: 0,
            left: 0,
            width: width,
            height: height * 0.8,
            backgroundColor: "white",
            elevation: 10
          }}
        >
          <Pdf
            source={{
              cache: false,
              uri: `${baseUrl}api/order/file/${PdfurlIndex}/${order.order._id}`
            }}
            onError={error => {
              console.log(error);
            }}
            style={{
              flex: 1,
              width: width,
              height: height * 0.8,
              backgroundColor: "white"
            }}
          />
          <TouchableOpacity
            style={{ position: "absolute", top: 0, left: 0 }}
            onPress={() => setPdfurlIndex(false)}
          >
            <Text>إغلاق</Text>
          </TouchableOpacity>
        </ScrollView>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    marginTop: moderateScale(20),
    alignSelf: "center",
    width: width * 0.9,
    marginHorizontal: moderateScale(2),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4
    },
    padding: moderateScale(10),
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    borderColor: "#eee",
    borderWidth: 1,
    elevation: 9,
    borderRadius: moderateScale(4),
    backgroundColor: "white"
  },
  drawer: {
    marginHorizontal: moderateScale(30)
  },
  Icon: {
    textAlign: "right",
    height: "100%",
    marginRight: moderateScale(3)
  }
});

export default connect(
  state => {
    return {
      messages: state.Chat.chatRoom,
      user: state.auth.user,
      userType: state.Config.userType
    };
  },
  dispatch => {
    return {
      fetchMessages: (id, page) => FetchRoomChat(id, page, dispatch),
      fetchOrderDetails: id => getOrderDetails(id, dispatch),
      dispatch
    };
  }
)(MyHandels);
