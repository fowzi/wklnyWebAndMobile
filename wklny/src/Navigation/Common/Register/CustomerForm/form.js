import React, { Fragment, useRef, useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Alert,
  StyleSheet,
  ImageBackground,
  ScrollView,
  TouchableWithoutFeedback,
  Platform
} from "react-native";
import ActionSheet from "react-native-actionsheet";
import { SimpleAnimation } from "react-native-simple-animations";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import { connect } from "react-redux";
import Spinner from "react-native-spinkit";

import {
  width,
  height,
  moderateScale,
  scale,
  verticalScale,
  colors
} from "@config";
import { Input, Item, Button } from "native-base";
import { Formik } from "formik";
import * as yup from "yup";
import { RFValue, RFPercentage } from "react-native-responsive-fontsize";
import {
  RegistClient,
  VerfiyPhone
} from "../../../../Redux/actions/AuthActions";
import { Images } from "@assets";
import CustomPicker from "./CustomPicker";
import { FetchCities, GetAppInitials } from "../../../../Redux/actions/Config";

const Form = props => {
  const [Refs, SetRefs] = useState([null]);
  const [servicesError, SetServicesError] = useState(false);
  const [firstLoading, setFirstLoading] = useState(true);
  if (firstLoading) {
    setFirstLoading(false);
    props.Reset();
    if (props.cities.length < 1 && !props.citiesLoading) {
      // props.FetchCities();
      props.getInitials();
    }
  }
  return (
    <Formik
      initialValues={{
        username: "",
        email: "",
        phone: "",
        password: "",
        repassword: "",
        agreeToTerms: false,
        city: "",
        selectedCity: null
      }}
      onSubmit={async values => {
        if (props.type === "Customer") {
          props.Register({
            form: {
              name: values.username,
              email: values.email,
              phone: values.phone,
              password: values.password,
              password2: values.repassword,
              agreeToTerms: true,
              city: values.selectedCity
            },
            navigation: props.navigation,
            type: props.type
          });
        } else {
          let isContainsServices = false;
          let servs = [];
          await Refs.forEach(e => {
            if (e !== null) {
              isContainsServices = true;
              servs.push(e);
            }
          });
          SetServicesError(!isContainsServices);
          if (!isContainsServices) return;
          props.Register({
            form: {
              name: values.username,
              email: values.email,
              phone: values.phone,
              password: values.password,
              password2: values.repassword,
              agreeToTerms: true,
              city: values.selectedCity,
              services: servs
            },
            navigation: props.navigation,
            type: props.type
          });
        }
      }}
      validationSchema={yup.object().shape({
        username: yup.string().required("هذا الحقل مطلوب"),
        email: yup
          .string()
          .email("يرجيء ادخال بريد الكتروني صحيح")
          .required("هذا الحقل مطلوب"),
        phone: yup.string().required("هذا الحقل مطلوب"),
        password: yup
          .string()
          .min(6, "يجب الا تقل كلمة المرور عن ٦ احرف")
          .required("هذا الحقل مطلوب"),
        repassword: yup
          .string()
          .min(6, "يجب الا تقل كلمة المرور عن ٦ احرف")
          .test("passwords-match", "يحب تطابق كلمتي المرور", function(value) {
            return this.parent.password === value;
          })
          .required("هذا الحقل مطلوب"),
        agreeToTerms: yup
          .boolean()
          .test(
            "is-true",
            "لابد من الموافقة علي شروط الإتفاقية",
            value => value === true
          ),
        city: yup
          .string()
          .min(1, "لابد من إختيار المدينة")
          .required("لابد من إختيار المدينة")
      })}
    >
      {({
        values,
        handleChange,
        errors,
        setFieldTouched,
        touched,
        isValid,
        handleSubmit,
        setFieldValue
      }) => (
        <View style={{ marginTop: moderateScale(10) }}>
          <View
            style={{
              width: width * 0.8,
              alignSelf: "center"
            }}
          >
            <Item
              style={{
                borderBottomColor:
                  touched.username && errors.username ? "red" : "gray",
                marginBottom: height * 0.02
              }}
            >
              <Input
                value={values.username}
                onChangeText={handleChange("username")}
                onBlur={() => setFieldTouched("username")}
                placeholder="اسم المستخدم"
                style={[
                  styles.Input,
                  {
                    borderBottomColor:
                      touched.username && errors.username ? "red" : "gray"
                  }
                ]}
              />
            </Item>
            {touched.username && errors.username && (
              <AnimationTextError error={errors.username} />
            )}
          </View>

          <View
            style={{
              width: width * 0.8,
              alignSelf: "center"
            }}
          >
            <Item
              style={{
                borderBottomColor:
                  touched.email && errors.email ? "red" : "gray",
                marginBottom: height * 0.02
              }}
            >
              <Input
                value={values.email}
                onChangeText={handleChange("email")}
                onBlur={() => setFieldTouched("email")}
                placeholder="البريد الإلكتروني"
                keyboardType="email-address"
                style={[
                  styles.Input,
                  {
                    borderBottomColor:
                      touched.email && errors.email ? "red" : "gray"
                  }
                ]}
              />
            </Item>
            {touched.email && errors.email && (
              <AnimationTextError error={errors.email} />
            )}
          </View>
          <View
            style={{
              width: width * 0.8,
              alignSelf: "center"
            }}
          >
            <Item
              style={{
                borderBottomColor:
                  touched.phone && errors.phone ? "red" : "gray",
                marginBottom: height * 0.02
              }}
            >
              <Input
                value={values.phone}
                onChangeText={handleChange("phone")}
                onBlur={() => setFieldTouched("phone")}
                placeholder="رقم الجوال"
                style={[
                  styles.Input,
                  {
                    borderBottomColor:
                      touched.phone && errors.phone ? "red" : "gray"
                  }
                ]}
              />
            </Item>
            {touched.phone && errors.phone && (
              <AnimationTextError error={errors.phone} />
            )}
          </View>

          <View
            style={{
              width: width * 0.8,
              alignSelf: "center"
            }}
          >
            <Item
              style={{
                borderBottomColor:
                  touched.email && errors.email ? "red" : "gray",
                marginBottom: height * 0.02
              }}
            >
              <Input
                value={values.password}
                onChangeText={handleChange("password")}
                placeholder="كلمة المرور"
                onBlur={() => setFieldTouched("password")}
                secureTextEntry={true}
                style={[
                  styles.Input,
                  {
                    borderBottomColor:
                      touched.password && errors.password ? "red" : "gray"
                  }
                ]}
              />
            </Item>
            {touched.password && errors.password && (
              <AnimationTextError error={errors.password} />
            )}
          </View>
          <View
            style={{
              width: width * 0.8,
              alignSelf: "center"
            }}
          >
            <Item
              style={{
                borderBottomColor:
                  touched.email && errors.email ? "red" : "gray",
                marginBottom: height * 0.02
              }}
            >
              <Input
                value={values.repassword}
                onChangeText={handleChange("repassword")}
                placeholder="تأكيد كلمة المرور"
                onBlur={() => setFieldTouched("repassword")}
                secureTextEntry={true}
                style={[
                  styles.Input,
                  {
                    borderBottomColor:
                      touched.repassword && errors.repassword ? "red" : "gray"
                  }
                ]}
              />
            </Item>
            {touched.repassword && errors.repassword && (
              <AnimationTextError error={errors.repassword} />
            )}
          </View>

          <View
            style={{
              width: "80%",
              alignSelf: "center",
              marginTop: moderateScale(1)
            }}
          >
            <ActionSheet
              ref={a => (city = a)}
              title={"أختر المدينة"}
              options={[...props.cities.map(a => a.name), "إلغاء"]}
              cancelButtonIndex={props.cities.length}
              onPress={selected => {
                if (selected !== props.cities.length) {
                  setFieldValue("city", props.cities[selected].name);
                  setFieldValue("selectedCity", props.cities[selected]._id);
                }
              }}
            />
            <TouchableOpacity
              style={{
                flexDirection: "row",
                borderWidth: 1,
                borderColor: "#ccc",
                alignItems: "center",
                marginTop: moderateScale(1)
              }}
              // disabled={props.citiesLoading || props.cities.length < 1}
              onPress={() => {
                setFieldTouched("city", true);
                city.show();
              }}
            >
              {!props.citiesLoading ? (
                <Text
                  style={{
                    width: "90%",
                    marginVertical: moderateScale(10),
                    textAlign: "center",
                    alignSelf: "center",
                    fontSize: RFValue(14)
                  }}
                >
                  {values.city ? values.city : "أختر المدينة"}
                </Text>
              ) : (
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "center",
                    alignItems: "center",
                    width: "90%"
                  }}
                >
                  <Text>اختر المدينة</Text>
                  <Spinner
                    style={{
                      justifyContent: "center",
                      alignSelf: "center",
                      alignItems: "center",
                      padding: moderateScale(5)
                    }}
                    size={moderateScale(20)}
                    isVisible={true}
                    type="Circle"
                    color={"#57B235"}
                  />
                </View>
              )}
              <MaterialCommunityIcons name="menu-down" size={RFValue(22)} />
            </TouchableOpacity>
            {touched.city && errors.city && (
              <AnimationTextError error={errors.city} />
            )}
          </View>

          {props.type === "Provider" && (
            <View>
              <View
                style={{
                  width: "80%",
                  alignSelf: "center",
                  marginTop: moderateScale(50)
                }}
              >
                <Text
                  style={{
                    flex: 1,
                    fontWeight: "bold",
                    fontSize: RFValue(18),
                    flexWrap: "wrap-reverse"
                  }}
                >
                  الخدمات المقدمة
                </Text>
              </View>
              {Refs.map((item, index) => {
                return (
                  <View key={index} style={{ marginTop: moderateScale(10) }}>
                    <CustomPicker
                      serviceref={`service${index}`}
                      subServiceref={`subService${index}`}
                      services={props.services}
                      servicesTypes={props.servicesTypes}
                      SetSelection={payload => {
                        const RefsNew = Refs;
                        RefsNew[payload.index] = payload._id;
                        SetRefs(RefsNew);
                      }}
                      index={index}
                      SetOfServices={Refs}
                    />
                    {Refs.length === index + 1 && (
                      <TouchableOpacity
                        onPress={async () => {
                          if (Refs.length > 1) {
                            let NewRefs = await [...Refs];
                            await NewRefs.splice(index, 1);
                            console.log("====================================");
                            console.log(NewRefs);
                            console.log("====================================");
                            SetRefs(NewRefs);
                          }
                        }}
                        style={{ position: "absolute", top: 0, right: "3%" }}
                      >
                        <MaterialIcons
                          name="remove-circle-outline"
                          color={colors.main}
                          size={RFValue(22)}
                        />
                      </TouchableOpacity>
                    )}
                  </View>
                );
              })}

              <View
                style={{
                  width: "80%",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignSelf: "center"
                }}
              >
                <Text>اضافة المزيد من الخدمات</Text>
                <TouchableOpacity
                  onPress={() => {
                    SetRefs([...Refs, 0]);
                  }}
                >
                  <MaterialIcons
                    name="add-circle"
                    color={colors.main}
                    size={RFValue(22)}
                  />
                </TouchableOpacity>
              </View>
              {servicesError && (
                <AnimationTextError error="يجب اختيار خدمة واحدة علي الأقل" />
              )}
            </View>
          )}
          <View
            style={{
              width: width * 0.8,
              alignSelf: "center",
              marginTop: height * 0.03
            }}
          >
            <View>
              <Text
                style={{
                  fontFamily: "Tajawal-ExtraBold",
                  fontSize: RFValue(14),
                  color: "grey"
                }}
              >
                شروط الاتفاقية
              </Text>
              <ScrollView
                ref={r => (_scroll = r)}
                style={{
                  borderWidth: 1,
                  borderColor: "gray",
                  maxHeight: height * 0.3,
                  borderRadius: moderateScale(10)
                }}
                contentContainerStyle={{ zIndex: 1000 }}
                nestedScrollEnabled
                // onTouchMove={() => {
                //   Platform.OS === "android" &&
                //     parentScroll.setNativeProps({ scrollEnabled: false });
                // }}
                // onTouchCancel={() => ß{
                //   Platform.OS === "android" &&
                //     parentScroll.setNativeProps({ scrollEnabled: true });
                // }}
              >
                <Text
                  onPress={() => {
                    setFieldTouched("agreeToTerms", true);
                  }}
                  style={{
                    width: "90%",
                    alignSelf: "center"
                  }}
                >
                  {props.privicy}
                </Text>

                <View
                  style={{
                    flexDirection: "row",
                    marginTop: moderateScale(10),
                    justifyContent: "flex-end"
                  }}
                >
                  <TouchableOpacity
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    onPress={() => {
                      setFieldValue("agreeToTerms", true);
                    }}
                  >
                    <MaterialCommunityIcons
                      name="checkbox-marked-circle"
                      size={RFValue(28)}
                      color={values.agreeToTerms === false ? "gray" : "#57B235"}
                      style={{ alignSelf: "center" }}
                    />
                    <Text style={{ alignSelf: "center" }}>أوافق</Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    style={{
                      flexDirection: "row",
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    onPress={() => {
                      setFieldValue("agreeToTerms", false);
                    }}
                  >
                    <MaterialCommunityIcons
                      name="close-circle"
                      size={RFValue(28)}
                      color={values.agreeToTerms === true ? "gray" : "#57B235"}
                      style={{ alignSelf: "center" }}
                    />
                    <Text style={{ alignSelf: "center" }}>لا أوافق</Text>
                  </TouchableOpacity>
                </View>
              </ScrollView>
            </View>

            {/* </TouchableWithoutFeedback> */}
            {touched.agreeToTerms && errors.agreeToTerms && (
              <AnimationTextError error={errors.agreeToTerms} />
            )}
          </View>
          <View style={{ marginTop: moderateScale(50) }}>
            <Button
              bordered
              style={{
                backgroundColor: "#57B235",
                borderRadius: width * 0.3,
                width: width * 0.5,
                height: height * 0.08,
                alignSelf: "center",
                justifyContent: "center",
                alignItems: "center"
              }}
              title="Sign In"
              disabled={props.loading}
              onPress={handleSubmit}
            >
              {props.loading ? (
                <Spinner
                  style={{
                    justifyContent: "center",
                    alignSelf: "center",
                    alignItems: "center"
                  }}
                  isVisible={props.loading}
                  type="Circle"
                  color={"white"}
                />
              ) : (
                <Text
                  style={{
                    color: "white",
                    alignSelf: "center",
                    textAlign: "center",
                    fontWeight: "800"
                  }}
                >
                  تسجيل
                </Text>
              )}
            </Button>

            {/* <Button
              bordered
              style={{
                backgroundColor: "#57B235",
                borderRadius: width * 0.3,
                width: width * 0.5,
                height: height * 0.08,
                alignSelf: "center"
              }}
              title="Sign In"
              // disabled={!isValid}
              onPress={handleSubmit}
            >
              <Text
                style={{
                  color: "white",
                  flex: 1,
                  alignSelf: "center",
                  textAlign: "center",
                  fontWeight: "800"
                }}
              >
                تسجيل
              </Text>
            </Button> */}
            <TouchableOpacity
              style={{ alignSelf: "center", marginTop: moderateScale(50) }}
              onPress={() => props.navigation.navigate("Login")}
            >
              <Text
                style={{
                  fontSize: RFValue(12)
                }}
              >
                لديك حساب بالفعل ؟
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      )}
    </Formik>
  );
};

const AnimationTextError = ({ error }) => (
  <SimpleAnimation
    delay={100}
    duration={500}
    movementType="slide"
    staticType="bounce"
    direction="up"
  >
    <Text style={styles.TextError}>{error}</Text>
  </SimpleAnimation>
);
const styles = StyleSheet.create({
  Input: {
    borderBottomWidth: 1,
    textAlign: "right",
    fontSize: RFValue(14),
    color: "gray"
  },
  TextError: {
    flex: 1,
    color: "red",
    textAlign: "left",
    fontSize: RFPercentage(1.5)
  }
});
export default connect(
  state => {
    return {
      type: state.Config.userType,
      privicy: state.Config.privicy,
      cities: state.Config.cities,
      citiesLoading: state.Config.citiesLoading,
      servicesLoading: state.Config.servicesLoading,
      servicesTypesLoading: state.Config.servicesTypesLoading,
      services: state.Config.services,
      servicesTypes: state.Config.servicesTypes,
      loading: state.auth.VerfiyPhoneLoading
    };
  },
  dispatch => {
    return {
      Reset: () => dispatch({ type: "resetload" }),
      Register: payload => VerfiyPhone({ ...payload, dispatch }),
      FetchCities: () => FetchCities(dispatch),
      getInitials: () => GetAppInitials(dispatch)
    };
  }
)(Form);
