import React, { Fragment, Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  Alert,
  StyleSheet,
  ImageBackground
} from "react-native";
import { SimpleAnimation } from "react-native-simple-animations";
import Spinner from "react-native-spinkit";
import {
  width,
  height,
  moderateScale,
  scale,
  verticalScale,
  colors
} from "@config";
import { Input, Item, Button } from "native-base";
import { Formik } from "formik";
import * as yup from "yup";
import { RFValue, RFPercentage } from "react-native-responsive-fontsize";

import Header from "../../../Components/Header";
import { Images } from "@assets";
import { connect } from "react-redux";
import { LoginClient } from "../../../Redux/actions/AuthActions";
import { LOGIN_FAILED } from "../../../Redux/types";
class Form extends Component {
  componentDidMount() {
    this.props.reset();
  }
  render() {
    let props = this.props;
    return (
      <Formik
        initialValues={{
          // email:
          //   props.type === "Customer" ? "test@test.tests" : "test1@test.test",
          // password: "123456"
          email: "",
          password: ""
        }}
        onSubmit={values =>
          props.Login({
            ...values,
            email: values.email,
            type: props.type,
            navigation: props.navigation
          })
        }
        validationSchema={yup.object().shape({
          email: yup
            .string()
            .email()
            .required("هذا الحقل مطلوب"),
          password: yup
            .string()
            .min(6)
            .required("هذا الحقل مطلوب")
        })}
      >
        {({
          values,
          handleChange,
          errors,
          setFieldTouched,
          touched,
          isValid,
          handleSubmit
        }) => (
          <View style={{ marginTop: moderateScale(50) }}>
            <View
              style={{
                width: width * 0.8,
                alignSelf: "center"
              }}
            >
              <Item
                style={{
                  borderBottomColor:
                    touched.email && errors.email ? "red" : "gray",
                  marginBottom: height * 0.02
                }}
              >
                <Input
                  value={values.email}
                  onChangeText={handleChange("email")}
                  onBlur={() => setFieldTouched("email")}
                  placeholder="البريد الإلكتروني"
                  style={[
                    styles.Input,
                    {
                      borderBottomColor:
                        touched.email && errors.email ? "red" : "gray"
                    }
                  ]}
                />
              </Item>
              {touched.email && errors.email && (
                <SimpleAnimation
                  delay={100}
                  duration={500}
                  movementType="slide"
                  staticType="bounce"
                  direction="up"
                >
                  <Text style={styles.TextError}>{errors.email}</Text>
                </SimpleAnimation>
              )}
            </View>

            <View
              style={{
                width: width * 0.8,
                alignSelf: "center"
              }}
            >
              <Item
                style={{
                  borderBottomColor:
                    touched.email && errors.email ? "red" : "gray",
                  marginBottom: height * 0.02
                }}
              >
                <Input
                  value={values.password}
                  onChangeText={handleChange("password")}
                  placeholder="كلمة المرور"
                  onBlur={() => setFieldTouched("password")}
                  secureTextEntry={true}
                  style={[
                    styles.Input,
                    {
                      borderBottomColor:
                        touched.password && errors.password ? "red" : "gray"
                    }
                  ]}
                />
              </Item>
              {touched.password && errors.password && (
                <SimpleAnimation
                  delay={100}
                  duration={500}
                  movementType="slide"
                  staticType="bounce"
                >
                  <Text style={styles.TextError}>{errors.password}</Text>
                </SimpleAnimation>
              )}
            </View>
            <View
              style={{
                width: width * 0.8,
                alignSelf: "center",
                marginTop: moderateScale(20)
              }}
            >
              <TouchableOpacity
                style={{ width: width * 0.3 }}
                onPress={() => {
                  props.navigation.navigate("ForgetPassword");
                }}
              >
                <Text
                  style={{
                    fontSize: RFValue(12)
                  }}
                >
                  نسيت كلمة المرور ؟
                </Text>
              </TouchableOpacity>
            </View>
            <View
              style={{
                justifyContent: "flex-end",
                height: height * 0.3
              }}
            >
              <Button
                bordered
                style={{
                  backgroundColor: "#57B235",
                  borderRadius: width * 0.3,
                  width: width * 0.5,
                  height: height * 0.08,
                  alignSelf: "center",
                  justifyContent: "center",
                  alignItems: "center"
                }}
                title="Sign In"
                disabled={props.loading}
                onPress={handleSubmit}
              >
                {props.loading ? (
                  <Spinner
                    style={{
                      justifyContent: "center",
                      alignSelf: "center",
                      alignItems: "center"
                    }}
                    isVisible={props.loading}
                    type="Circle"
                    color={"white"}
                  />
                ) : (
                  <Text
                    style={{
                      color: "white",
                      alignSelf: "center",
                      textAlign: "center",
                      fontWeight: "800"
                    }}
                  >
                    تسجيل
                  </Text>
                )}
              </Button>
              <TouchableOpacity
                style={{ alignSelf: "center", marginTop: moderateScale(50) }}
                onPress={() => props.navigation.navigate("Register")}
              >
                <Text
                  style={{
                    fontSize: RFValue(12)
                  }}
                >
                  تسجيل حساب جديد
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </Formik>
    );
  }
}
const styles = StyleSheet.create({
  Input: {
    borderBottomWidth: 1,
    textAlign: "right",
    fontSize: RFValue(14),
    color: "gray"
  },
  TextError: {
    color: "red",
    textAlign: "left",
    fontSize: RFPercentage(1.5)
  }
});
export default connect(
  state => {
    return {
      type: state.Config.userType,
      loading: state.auth.loginLoading
    };
  },
  dispatch => {
    return {
      reset: () => dispatch({ type: LOGIN_FAILED }),
      Login: payload => LoginClient({ ...payload, dispatch })
    };
  }
)(Form);
