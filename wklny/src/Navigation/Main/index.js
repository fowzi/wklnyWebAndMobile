import React from "react";
import { createStackNavigator } from "react-navigation";
import Main from "./Main";
import {
  CallUs,
  Login,
  Register,
  About,
  Policy,
  ForgetPassword,
  ForgetPasswordCode,
  PhoneVerfiy
} from "../Common";

export default MainApp = createStackNavigator(
  {
    Main: Main,
    PhoneVerfiy,
    ForgetPassword: ForgetPassword,
    ForgetPasswordCode: ForgetPasswordCode,
    Login: Login,
    Register: Register,
    CallUs: CallUs,
    About,
    Policy
  },

  {
    headerMode: "none"
  }
);
