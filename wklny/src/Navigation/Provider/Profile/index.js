import React, { Fragment } from "react";
import {
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  Text,
  RefreshControl
} from "react-native";

import {
  width,
  height,
  moderateScale,
  scale,
  verticalScale,
  colors
} from "@config";
import Header from "@components/Header";
import { Images } from "@assets";
import Card from "../../../Components/Card";
import { RFValue, RFPercentage } from "react-native-responsive-fontsize";
import { connect } from "react-redux";
import { getMyOrdersCount } from "../../../Redux/actions/provider";
import Spinner from "react-native-spinkit";
import { baseUrl } from "@config";

class Profile extends React.Component {
  componentWillMount() {
    this.props.getCounts();
  }
  render() {
    let props = this.props;
    const {
      _id,
      avatar,
      name,
      Finished = "0",
      Messages = "0",
      Running = "0",
      openOrders = "0"
    } = this.props;
    let AVATAR = `${baseUrl}api/user/image/1/${_id}?${new Date()}`;

    return (
      <View style={{ flex: 1 }}>
        <Header navigation={this.props.navigation} title="بروفايل" hideback />
        <ScrollView
          bounces={true}
          style={{ flex: 1 }}
          refreshControl={
            <RefreshControl
              refreshing={false}
              onRefresh={() => this.props.getCounts()}
              //    colors="white"
              tintColor="white"
              style={{
                color: "white"
              }}
            />
          }
        >
          {props.loadingCount && (
            <Spinner
              style={{
                marginVertical: moderateScale(20),
                justifyContent: "center",
                alignSelf: "center",
                alignItems: "center"
              }}
              isVisible={true}
              type="Circle"
              color={"#57B235"}
            />
          )}
          <View
            style={{
              height: height * 0.35,
              width: "80%",
              alignSelf: "center",
              borderWidth: 1,
              borderColor: "#eee",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Image
              source={{ uri: AVATAR }}
              style={{
                width: width * 0.3,
                height: width * 0.3,
                alignSelf: "center"
              }}
              resizeMode="stretch"
            />
            <Text>{name}</Text>
            <View
              style={{
                width: "80%",
                alignSelf: "center",
                justifyContent: "space-between",
                flexDirection: "row",
                marginTop: moderateScale(5)
              }}
            >
              <TouchableOpacity
                style={{
                  flex: 1,
                  backgroundColor: colors.main,
                  paddingVertical: moderateScale(10),
                  marginHorizontal: moderateScale(5),
                  justifyContent: "center",
                  alignItems: "center",
                  borderRadius: moderateScale(5)
                }}
                onPress={() => {
                  props.navigation.navigate("Charge");
                }}
              >
                <Text style={{ color: "white", fontSize: RFValue(14) }}>
                  الرصيد
                  <Text
                    style={{
                      color: "white",
                      fontSize: RFValue(16),
                      fontWeight: "600"
                    }}
                  >
                    $
                  </Text>
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  flex: 1,
                  backgroundColor: colors.main,
                  paddingVertical: moderateScale(5),
                  marginHorizontal: moderateScale(5),
                  justifyContent: "center",
                  alignItems: "center",
                  borderRadius: moderateScale(5)
                }}
                onPress={() => {
                  props.navigation.navigate("OpenDeals");
                }}
              >
                <Text
                  style={{
                    color: "white",
                    fontSize: RFValue(14),
                    textAlign: "center"
                  }}
                >
                  البحث عن معاملة جديدة
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              width: width,
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              marginTop: moderateScale(20)
            }}
          >
            <TouchableOpacity
              style={styles.card}
              onPress={() =>
                props.navigation.navigate("MyHandels", { index: 0 })
              }
            >
              <Card
                image={Images.cardBack}
                text="صفقات جارية"
                text2={String(Running)} //Running
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.card}
              onPress={() => props.navigation.navigate("OpenDeals")}
            >
              <Card
                image={Images.cardBack}
                text="عروض صفقات"
                text2={String(openOrders)} //
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: width,
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              marginTop: moderateScale(20)
            }}
          >
            <TouchableOpacity
              style={styles.card}
              onPress={() =>
                props.navigation.navigate("MyHandels", { index: 1 })
              }
            >
              <Card
                image={Images.cardBack}
                text="عروض معتمدة"
                text2={String(Finished)} //
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.card}
              onPress={() =>
                props.navigation.navigate("Chat", { item: "messages" })
              }
            >
              <Card
                image={Images.cardBack}
                text="مراسلاتي"
                text2={String(Messages)} //Messages
              />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  card: {
    alignSelf: "center",
    width: width * 0.44,
    height: height * 0.15,
    marginHorizontal: moderateScale(2),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,

    elevation: 9
  },
  drawer: {
    marginHorizontal: moderateScale(30)
  }
});

const mapState = state => {
  return {
    ...state.auth.user,
    ...state.provider.counts,
    loadingCount: state.provider.countsLodaing
  };
};
const mapDispatch = dispatch => {
  return {
    getCounts: () => getMyOrdersCount(dispatch)
  };
};

export default connect(
  mapState,
  mapDispatch
)(Profile);
