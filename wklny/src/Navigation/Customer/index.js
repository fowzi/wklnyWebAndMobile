import { createStackNavigator } from "react-navigation";
import Main from "./Main";
import Profile from "./Profile";
import MyHandels from "./MyHandels";
import Offers from "./Offers";
import DealFollow from "./DealFollow";
import MyMessages from "../Common/MyMessages";
import AddOrder from "./AddOrder";
import Contracts from "./Contract";
import EditeProfile from "../Common/EditeProfile";
import ProfileSettings from "../Common/ProfileSetting";
import ChangePassword from "../Common/ChangePassword";
import CallUs from "../Common/CallUs";
import About from "../Common/About";
import Policy from "../Common/Policy";
import Charge from "../Common/Charge";
import Chat from "../Common/Chat";
import RechargeAccount from "./RechargeAccount";
import RateProvider from "./RateProvider/indx";
export default createStackNavigator(
  {
    Profile: Profile,
    Contracts,
    Charge: Charge,
    Customer: Main,
    MyHandels: MyHandels,
    Offers,
    DealFollow,
    MyMessages,
    AddOrder,
    EditeProfile,
    ProfileSettings: ProfileSettings,
    ChangePassword: ChangePassword,
    CallUs: CallUs,
    About: About,
    Policy: Policy,
    Chat: Chat,
    RechargeAccount,
    RateProvider
  },
  {
    headerMode: "none"
    // initialRouteName: "MyMessages"
  }
);
