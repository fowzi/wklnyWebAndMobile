import React, { Fragment, useRef, useState } from "react";
import {
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  Text,
  FlatList
} from "react-native";

import {
  width,
  height,
  moderateScale,
  scale,
  verticalScale,
  colors
} from "@config";
import Header from "@components/Header";
import { Images } from "@assets";
import Card from "../../../Components/Card";
import { RFValue, RFPercentage } from "react-native-responsive-fontsize";
import ActionSheet from "react-native-actionsheet";
import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";
import StarRating from "react-native-star-rating";
import { connect } from "react-redux";
import Store from "../../../Redux";
import {
  getOrderOffers,
  userAcceptOffer
} from "../../../Redux/actions/Customer";
import Spinner from "react-native-spinkit";
import { baseUrl } from "../../../config";
import { GetMyPalance } from "../../../Redux/actions/AuthActions";

const RenderIemCard = props => {
  console.log("====offfer================================");
  console.log(props);
  const { provider, rate, introduction, price, time } = props.item;
  const [loading, setLoading] = useState(false);
  console.log(provider._id, "====================================");
  const AlertMessage = Store.getState().Config.alert;

  return (
    <View style={styles.card}>
      <View style={{ flexDirection: "row" }}>
        <View style={{ flexDirection: "row" }}>
          <Image
            source={{ uri: baseUrl + "api/user/image/1/" + provider._id }}
            style={{ width: width * 0.2, height: width * 0.2 }}
          />
          <View
            style={{
              justifyContent: "space-between",
              padding: moderateScale(10)
            }}
          >
            <Text
              style={{
                fontSize: RFValue(12),
                fontWeight: "bold"
              }}
            >
              {provider.name}
            </Text>
            <StarRating
              fullStarColor={colors.main}
              disabled={true}
              maxStars={5}
              rating={provider.rate}
              starSize={RFValue(12)}
              containerStyle={{
                alignSelf: "flex-start",
                justifyContent: "center",
                flexDirection: "row-reverse"
              }}
            />
          </View>
        </View>
      </View>
      <View>
        <Text
          style={{
            marginTop: moderateScale(10),
            fontSize: RFValue(12),
            width: "60%",
            flexWrap: "wrap"
          }}
        >
          {introduction}
        </Text>
      </View>
      <View
        style={{
          marginVertical: moderateScale(15),
          flexDirection: "row"
        }}
      >
        <TouchableOpacity
          style={{
            flexDirection: "row",
            backgroundColor: "#EDF9F1",
            borderWidth: 1,
            borderColor: colors.main,
            borderRadius: moderateScale(5),
            width: width * 0.2,
            alignItems: "center",
            justifyContent: "center",
            marginHorizontal: moderateScale(10),
            paddingVertical: moderateScale(5)
          }}
          disabled={true}
        >
          <Text
            style={{
              fontSize: RFValue(12),
              height: "100%",
              color: colors.main,
              fontWeight: "bold"
            }}
          >
            {price} ريال
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            flexDirection: "row",
            backgroundColor: "#EDF9F1",
            borderWidth: 1,
            borderColor: colors.main,
            borderRadius: moderateScale(5),
            width: width * 0.2,
            alignItems: "center",
            justifyContent: "center",
            marginRight: moderateScale(10),
            paddingVertical: moderateScale(5)
          }}
          disabled
        >
          <Text
            style={{
              fontSize: RFValue(12),
              height: "100%",
              color: colors.main,
              fontWeight: "bold"
            }}
          >
            {time}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            flexDirection: "row",
            backgroundColor: colors.main,
            borderRadius: moderateScale(10),
            width: width * 0.25,
            alignItems: "center",
            justifyContent: "center",
            marginRight: moderateScale(10),
            position: "absolute",
            right: 0,
            top: -moderateScale(10),
            paddingVertical: moderateScale(10),
            paddingHorizontal: moderateScale(5)
          }}
          onPress={() => {
            // props.navigation.navigate("MyMessages");
            if (Number(props.balance) < Number(price)) {
              AlertMessage(
                "error",
                "عفواً",
                "لايوجد رصيد كافي لقبول العرض برجاء شحن الحساب"
              );
              return;
            }
            setLoading(true);
            props.item._id &&
              userAcceptOffer(
                props.item._id,
                provider._id,
                props.navigation
              ).then(() => setLoading(false));
          }}
        >
          {loading ? (
            <Spinner
              style={{
                marginVertical: moderateScale(2),
                justifyContent: "center",
                alignSelf: "center",
                alignItems: "center"
              }}
              isVisible={true}
              type="Circle"
              color={"white"}
              size={moderateScale(20)}
            />
          ) : (
            <Text
              style={{
                fontSize: RFValue(12),
                color: "white",
                fontWeight: "bold"
              }}
            >
              قبول العرض
            </Text>
          )}
        </TouchableOpacity>
      </View>
    </View>
  );
};

const MyHandels = props => {
  let refs_ActionSheet = useRef();
  let data = ["مشروع منتهي", "مشروع قائم", "إلغاء"];
  const [currentIndex, setcurrentIndex] = useState(0);
  const [firstLoad, SetFirstLoad] = useState(true);

  const order_id = props.navigation.state.params.item;
  if (firstLoad) {
    props.fetchOffers(order_id, 1);
    SetFirstLoad(false);
    props.getMyBalance();
  }
  return (
    <View style={{ flex: 1 }}>
      <Header navigation={props.navigation} title="العروض المقدمة" />
      <ScrollView bounces={false} style={{ flex: 1 }}>
        {props.offers.balanceLoding ? (
          <Spinner
            style={{
              marginTop: moderateScale(5),
              justifyContent: "center",
              alignSelf: "center",
              alignItems: "center"
            }}
            isVisible={true}
            type="Circle"
            color={"#57B235"}
            size={moderateScale(50)}
          />
        ) : (
          <FlatList
            bounces={false}
            data={props.offers.data}
            style={{
              flex: 1,
              marginBottom: moderateScale(30)
            }}
            renderItem={({ item, index }) =>
              item._id && (
                <RenderIemCard
                  navigation={props.navigation}
                  item={item}
                  key={index}
                  balance={props.balance}
                />
              )
            }
            ListFooterComponent={
              props.offers.loading && (
                <Spinner
                  style={{
                    marginTop: moderateScale(5),
                    justifyContent: "center",
                    alignSelf: "center",
                    alignItems: "center"
                  }}
                  isVisible={true}
                  type="Circle"
                  color={"#57B235"}
                  size={moderateScale(50)}
                />
              )
            }
            ListEmptyComponent={
              !props.offers.loading && (
                <Text
                  style={{
                    textAlign: "center",
                    marginTop: moderateScale(30),
                    alignSelf: "center"
                  }}
                >
                  لايوجد عروض لهذه الخدمة
                </Text>
              )
            }
            keyExtractor={(item, index) => {
              item.title + index;
            }}
            onEndReached={() => {
              !props.offers.isEnd &&
                !props.offers.loading &&
                props.fetchOffers(order_id, props.offers.page);
            }}
          />
        )}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    marginTop: moderateScale(20),
    alignSelf: "center",
    width: width * 0.9,
    marginHorizontal: moderateScale(2),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4
    },
    padding: moderateScale(10),
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    borderColor: "#eee",
    borderWidth: 1,
    elevation: 9,
    borderRadius: moderateScale(4),
    backgroundColor: "white"
  },
  drawer: {
    marginHorizontal: moderateScale(30)
  },
  Icon: {
    textAlign: "right",
    height: "100%",
    marginRight: moderateScale(3)
  }
});

export default connect(
  state => {
    return {
      offers: state.Customer.Offers,
      loading: state.Customer.Offers.loading,
      balance: state.auth.balance,
      balanceLoding: state.auth.balanceLoding
    };
  },
  dispatch => {
    return {
      fetchOffers: (id, page) => getOrderOffers(id, dispatch, page),
      getMyBalance: () => GetMyPalance(dispatch)
    };
  }
)(MyHandels);
