import React, { Fragment, Component } from "react";
import {
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  Text,
  RefreshControl
} from "react-native";

import {
  width,
  height,
  moderateScale,
  scale,
  verticalScale,
  colors,
  baseUrl
} from "@config";

import Header from "@components/Header";
import { Images } from "@assets";
import Card from "../../../Components/Card";
import { RFValue, RFPercentage } from "react-native-responsive-fontsize";
import { connect } from "react-redux";
import { getMyOrdersCount } from "../../../Redux/actions/Customer";
import Spinner from "react-native-spinkit";
class Login extends Component {
  componentDidMount() {
    this.props.getCounts();
  }
  render() {
    let props = this.props;
    return (
      <View style={{ flex: 1 }}>
        <Header navigation={props.navigation} title="بروفايل" hideback />
        <ScrollView
          bounces={true}
          style={{ flex: 1 }}
          refreshControl={
            <RefreshControl
              refreshing={false}
              onRefresh={() => this.props.getCounts()} // colors="white"
              tintColor="white"
              style={{
                color: "white"
              }}
            />
          }
        >
          {props.counts.loading && (
            <Spinner
              style={{
                marginVertical: moderateScale(20),
                justifyContent: "center",
                alignSelf: "center",
                alignItems: "center"
              }}
              isVisible={true}
              type="Circle"
              color={"#57B235"}
            />
          )}
          <View
            style={{
              height: height * 0.35,
              width: "80%",
              alignSelf: "center",
              borderWidth: 1,
              borderColor: "#eee",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <Image
              source={{
                uri:
                  props.user && props.user._id
                    ? `${baseUrl}api/user/image/${
                        props.type === "Customer" ? 0 : 1
                      }/${props.user._id}?${new Date()}`
                    : "",
                CACHE: "reload"
              }}
              style={{
                width: width * 0.3,
                height: width * 0.3,
                alignSelf: "center"
              }}
              resizeMode="stretch"
            />
            <Text>{props.user.name}</Text>
            <View
              style={{
                width: "80%",
                alignSelf: "center",
                justifyContent: "space-between",
                flexDirection: "row",
                marginTop: moderateScale(5)
              }}
            >
              <TouchableOpacity
                style={{
                  flex: 1,
                  backgroundColor: colors.main,
                  paddingVertical: moderateScale(10),
                  marginHorizontal: moderateScale(5),
                  justifyContent: "center",
                  alignItems: "center",
                  borderRadius: moderateScale(5)
                }}
                onPress={() => {
                  props.navigation.navigate("MyHandels", { index: 0 });
                }}
              >
                <Text style={{ color: "white", fontSize: RFValue(14) }}>
                  معاملاتي
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={{
                  flex: 1,
                  backgroundColor: colors.main,
                  paddingVertical: moderateScale(5),
                  marginHorizontal: moderateScale(5),
                  justifyContent: "center",
                  alignItems: "center",
                  borderRadius: moderateScale(5)
                }}
                onPress={() => {
                  props.navigation.navigate("AddOrder");
                }}
              >
                <Text
                  style={{
                    color: "white",
                    fontSize: RFValue(14),
                    textAlign: "center"
                  }}
                >
                  اضافة معاملة جديدة
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={{
              width: width,
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              marginTop: moderateScale(20)
            }}
          >
            <TouchableOpacity
              style={styles.card}
              onPress={() => {
                props.navigation.navigate("MyHandels", { index: 1 });
              }}
            >
              <Card
                image={Images.cardBack}
                text="معاملات جارية"
                text2={
                  props.counts &&
                  props.counts.values &&
                  props.counts.values.Running
                    ? props.counts.values.Running + ""
                    : "0"
                }
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.card}
              onPress={() => {
                props.navigation.navigate("MyHandels", { index: 2 });
              }}
            >
              <Card
                image={Images.cardBack}
                text="معاملات منتهية"
                text2={
                  props.counts &&
                  props.counts.values &&
                  props.counts.values.Finished
                    ? props.counts.values.Finished + ""
                    : "0"
                }
              />
            </TouchableOpacity>
          </View>
          <View
            style={{
              width: width,
              flexDirection: "row",
              justifyContent: "center",
              alignItems: "center",
              marginTop: moderateScale(20)
            }}
          >
            <TouchableOpacity
              style={styles.card}
              onPress={() => {
                props.navigation.navigate("MyHandels", { index: 0 });
              }}
            >
              <Card
                image={Images.cardBack}
                text="قيد الإنتظار"
                text2={
                  props.counts &&
                  props.counts.values &&
                  props.counts.values.Waitting
                    ? props.counts.values.Waitting + ""
                    : "0"
                }
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.card}
              onPress={() => {
                props.navigation.navigate("Chat");
              }}
            >
              <Card
                image={Images.cardBack}
                text="مراسلاتي"
                text2={
                  props.counts &&
                  props.counts.values &&
                  props.counts.values.Messages
                    ? props.counts.values.Messages + ""
                    : "0"
                }
              />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  card: {
    alignSelf: "center",
    width: width * 0.44,
    height: height * 0.15,
    marginHorizontal: moderateScale(2),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowOpacity: 0.32,
    shadowRadius: 5.46,

    elevation: 9
  },
  drawer: {
    marginHorizontal: moderateScale(30)
  }
});

export default connect(
  state => {
    return {
      counts: state.Customer.counts,
      user: state.auth.user,
      type: state.Config.userType
    };
  },
  dispatch => {
    return { getCounts: () => getMyOrdersCount(dispatch) };
  }
)(Login);
