/**
 * @format
 */

import { AppRegistry } from "react-native";
//import App from "./paytabs";

import App from "./App";
import { name as appName } from "./app.json";
console.ignoredYellowBox = true;
AppRegistry.registerComponent(appName, () => App);
